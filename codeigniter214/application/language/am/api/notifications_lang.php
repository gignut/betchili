<?php

$lang['TR_BET_ERROR_CODE1']	     = "Անբավարար հաշվեկշիռ"; 
$lang['TR_BET_ERROR_CODE2']	     = "Սխալ էքսպրես.";
$lang['TR_BET_ERROR_CODE3']	     = "Խաղը արդեն սկսվել է.";
$lang['TR_BET_ERROR_CODE4']	     = "Խաղադրույքը փակ է";
$lang['TR_BET_ERROR_CODE5']	     = "Իրադարձություն արդեն ունի արդյունք.";
$lang['TR_BET_ERROR_CODE6']	     = "Գործակիցը կամ հաշվեկշիռը թարմացվել են. Վերաբեռնեք ձեր խաղը եւ փորձեք կրկին.";
$lang['TR_BET_ERROR_CODE7']	     = "Խաղադրույքի գումարը պետք է լինի".MIN_BET_AMOUNT." ից մինչև ".MAX_BET_AMOUNT.".";

$lang['TR_CHALLENGE_ERROR_CODE1'] = "Գրազի գումարը պետք է լինի ".MIN_CHALLANGE_AMOUNT." ից մինչև ".MAX_CHALLANGE_AMOUNT.".";
$lang['TR_CHALLENGE_ERROR_CODE2'] = "Անբավարար հաշվեկշիռ գրազը ընդունելու համար.";
$lang['TR_CHALLENGE_ERROR_CODE3'] = "Ընտրված ելքը բլոկավորված է.";
$lang['TR_CHALLENGE_ERROR_CODE4'] = "Ելքը արժեք չունի.";
$lang['TR_CHALLENGE_ERROR_CODE5'] = "Ելքը արդեն ունի արդյունք.";
$lang['TR_CHALLENGE_ERROR_CODE6'] = "Գործակիցը կամ հաշվեկշիռը թարմացվել են. Վերաբեռնեք ձեր խաղը եւ փորձեք կրկին.";
$lang['TR_CHALLENGE_ERROR_CODE7'] = "Խնդրում ենք կրկին փորձել.";
$lang['TR_CHALLENGE_ERROR_CODE8'] = "Խաղը արդեն սկսվել է.";
$lang['TR_CHALLENGE_ERROR_CODE9'] = "Հակառակորդը չի համապատասխանում նպատակային խաղացողին.";
$lang['TR_CHALLENGE_ERROR_CODE10'] = "Մրցակիցը արդեն ընդունել է մարտահրավերը.";
$lang['TR_CHALLENGE_ERROR_CODE11'] = "Գրազը ստեղծվել է Ձեզ համար.";
$lang['TR_CHALLENGE_ERROR_CODE12'] = "Ընդհանուր գրազը արդեն ընդունված է";

$lang['TR_GAME_ERROR_CODE1'] = "Խնդրում ենք կրկին փորձել.";

$lang['TR_BLOKED_USER_LOGIN'] = "Օգտատերը արգելափակված է ադմինիստրացիայի կողմից.";