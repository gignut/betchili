<?php

$lang['TR_BETCHILI'] = 'BETCHILI';
$lang['TR_HOME'] = 'Главная';
$lang['TR_ABOUT'] = 'О продукте';
$lang['TR_TESTIMONIALS'] = 'Отзывы';
$lang['TR_CTESTIMONIALS'] = 'Отзывы клиентов';
$lang['TR_CONTACT'] = "Контакты";
$lang['TR_SOCIAL'] = "Соц-сеть";
$lang['TR_ACCOUNT']    = 'Запрос аккаунта';
$lang['TR_LOGIN'] 	  = 'Войти';
$lang['TR_SUBTITLE'] 	 = 'Connecting brands with sports fans in digital world';
//$lang['TR_PROFILE']  = 'Профил';
$lang['TR_TOUR']   = 'Обзор';
$lang['TR_WHAT'] = "Что такое Betchili";
$lang['TR_WHAT_IS']       = "Betchili платформа обеспечивает <b> социальное спортивное развлекательное </b> онлайн решение /3-th party widget/ который поможет связивать бренды с любителями спорта и построить лояльность клиентов, имидж бренда и двигать бизнес метрики.";
$lang['TR_BENEFITS']           = "Ваши Выгоды";
$lang['TR_BENEFITS_IS']          = "- Увеличит социальную видимость<br>
- Увеличит трафик новых внешних пользователей<br>
- Увеличит лояльность ваших пользователей<br>
  - Создает сильный эмоциональный контакт между спортивными болельщиками и вашим брендом";
$lang['TR_PRICING']  = "Pricing";
$lang['TR_PRICING_IS'] = "У нас есть <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <b>бесплатные </b> пакеты<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <b>премиум</b> пакеты <br>
Для премиум пакета вы можете связаться с нами.";
$lang['TR_INTEGRATION']  = "Интеграция";
$lang['TR_INTEGRATION_IS'] = "Вы можете интегрировать Betchili виджет везде - веб-страницы, соцсетях, в отдельном микросайте, и т.д..";
$lang['TR_STEP1']         = "Шаг 1";
$lang['TR_STEP1_IS']         = "Регистрация в платформе";
$lang['TR_STEP2']         = "Шаг 2";
$lang['TR_STEP2_IS']     = "Выбор любимых спортивных лиг";
$lang['TR_STEP3']         = "Шаг 3";
$lang['TR_STEP3_IS']     = "Сделать дизайн виджета чтобы соответствовал вашему сайту";
$lang['TR_STEP4']         = "Шаг 4";
$lang['TR_STEP4_IS']     = "Просто скопируйте 2 линии в вашу веб-страницу HTML";
$lang['TR_RIGHT']     = "Все права защищены";