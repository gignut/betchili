<?php

$lang['TR_BETCHILI'] = 'BETCHILI';
$lang['TR_HOME'] = 'Главная';
$lang['TR_ABOUT'] = 'О продукте';
$lang['TR_TESTIMONIALS'] = 'Отзывы';
$lang['TR_CTESTIMONIALS'] = 'Client Testimonials';
$lang['TR_CONTACT'] = "Контакты";
$lang['TR_SOCIAL'] = "Соц-сеть";
$lang['TR_ACCOUNT']    = 'Запрос аккаунта';
$lang['TR_DEMO']    = 'ИГРАТЬ';
$lang['TR_LOGIN'] 	  = 'Войти';
$lang['TR_SUBTITLE'] 	 = 'Connecting brands with sports fans in digital world';
//$lang['TR_PROFILE']  = 'Профил';
$lang['TR_TOUR']   = 'Product Tour';
$lang['TR_WHAT'] = "What is Betchili";
$lang['TR_WHAT_IS']       = "Betchili platform provides <b>social sports entertaining</b> online solution, as a 3-th party widget, which will help brands to connect with sports fans and build strong customer loyalty, brand image and drive the business metrics.";
$lang['TR_BENEFITS']           = "Your Benefits";
$lang['TR_BENEFITS_IS']          = "Digital solution to engage sports fans with your brand will<br>
                            - increase your social appearense<br>
                            - increase new external users traffic<br>
                            - increase your brand users loyalty<br>
                            - creating strong emotional contact between sports fans and your brands";
$lang['TR_PRICING']  = "Pricing";
$lang['TR_PRICING_IS'] = "We have both <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <b>free</b> packages<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <b>advanced premium</b> packages <br>
                           For premium options you can contact us.";
$lang['TR_INTEGRATION']  = "Integration";
$lang['TR_INTEGRATION_IS'] = "You can integrate Betchili widget everywhere – webpages, social network pages, in a separate microsite, etc.";
$lang['TR_STEP1']         = "Step 1";
$lang['TR_STEP1_IS']         = "Register in platform";
$lang['TR_STEP2']         = "Step 2";
$lang['TR_STEP2_IS']     = "Choose favorite sport leagues and games";
$lang['TR_STEP3']         = "Step 3";
$lang['TR_STEP3_IS']     = "Сделать дизайн чтобы соответствовал вашему сайту";
$lang['TR_STEP4']         = "Step 4";
$lang['TR_STEP4_IS']     = "Just copy paste 2 line into your webpage HTML";
$lang['TR_RIGHT']     = "All Right Reserved";
