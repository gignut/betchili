<?php

$lang['TR_SURE']     = 'Вы уверены';
$lang['TR_YES']      = 'Да';
$lang['TR_NO']       = 'Нет';
$lang['TR_NEW']      = "Создать новый виджет";
$lang['TR_CONFIG']   = "КОНФИГУРАЦИЯ";
$lang['TR_DESIGN']   = "ДИЗАЙН";
$lang['TR_REMOVE']   = "Удалить";
$lang['TR_ALL']      = "Все виджеты";
$lang['TR_CREATE']   = "Создать";
$lang['TR_UPDATE']   = "Обновить";
$lang['TR_NAME']     = "Имя";
$lang['TR_WIDGET']   = "Виджет";
$lang['TR_REQUIRED'] = "Обязательный";
$lang['TR_CHOOSE_SIZE']     = "Размер виджета";
$lang['TR_CHOOSE_LANGUAGE'] = "Выберите язык";
$lang['TR_LANGUAGE'] = "Язык";
$lang['TR_LEAGUES']  = "Лиги";
$lang['TR_SIZE']     = "Размеры";
$lang['TR_WNAME']    = "Имя Виджета";
$lang['TR_SELECTL']  = "Выберите Лиги";