<?php

$lang['TR_BETCHILI']  = 'Betchili';
$lang['TR_NOT_REGISTERED_EMAIL']  = 'Not registered email.';
$lang['TR_EMAIL']     = 'Эл. почта';
$lang['TR_TIPEMAIL']  = 'Введите действительный адрес эл. почты. например: ';
$lang['TR_FORGOT']    = 'Забыли пароль';
$lang['TR_LOGIN']     = 'Войти';
$lang['TR_RESET']     = 'Сбросить';
$lang['TR_CHECK']     = 'Проверьте электронную почту';
$lang['TR_RESETLINK'] = 'Мы послали ссылку сброса пароля';
$lang['TR_BACK']      = 'Назад к ';
