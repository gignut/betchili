<?php

$lang['TR_BETCHILI']            = 'Betchili';
$lang['TR_REQUEST_BETCHILI']    = 'Отправить запрос.';
$lang['TR_REQUIRED']   			= 'Обязательный';
$lang['TR_NAME']       			= 'Имя';
$lang['TR_EMAIL']      			= 'Эл. почта';
$lang['TR_DOMAIN']     			= 'Домен';
$lang['TR_TIPEMAIL']   			= 'Введите действительный адрес эл. почты. например: ';
$lang['TR_TIPDOMAIN']  			= 'Введите имя действующего домена. например: ';
$lang['TR_REQUEST']    			= 'Отправить';
$lang['TR_REQUEST_TEXT']        = "Хотите БетЧили на вашем сайте? Пожалуйста, заполните форму и отправьте нам.";
$lang['TR_THANKS_FOR_REQUEST']  = "Мы свяжемся с Вами по электронной почте в течение 48 часов. Спасибо!";
