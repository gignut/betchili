<?php

$lang['TR_BETCHILI']     = 'Betchili';
$lang['TR_SIGNUP']       = 'Регистрация';
$lang['TR_FIRSTNAME']    = 'Имя';
$lang['TR_LASTNAME']     = 'Фамилия';
$lang['TR_EMAIL']        = 'Эл. почта';
$lang['TR_DOMAIN']       = 'Домен';
$lang['TR_PASSWORD']     = 'Пароль';
$lang['TR_CONFIRMPASS']  = 'Подтверждение пароля';
$lang['TR_LOGIN']        = 'Войти';
$lang['TR_QUEST']        = 'Уже зарегистрировались?';
$lang['TR_REQUIRED']     = 'Обязательный';
$lang['TR_TIPEMAIL']     = 'Введите действительный адрес эл. почты. например: ';
$lang['TR_TIPDOMAIN']    = 'Введите имя действующего домена. например: ';
$lang['TR_TIPPASS']      = 'Введите не менее 6 символов';
$lang['TR_TIPPASS1']     = 'Пароли не совпадают';
$lang['TR_TIP1']         = 'Ваша учетная запись успешно создана!';
$lang['TR_TIP2']         = 'Пожалуйста, проверьте и подтвердите ваш адрес эл. почты:';