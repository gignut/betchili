<?php

$lang['TR_SURE']   = 'Are you sure';
$lang['TR_YES']    = 'Yes';
$lang['TR_NO']     = 'No';
$lang['TR_NEW']    = "Create new widget";
$lang['TR_CONFIG'] = "CONFIG";
$lang['TR_DESIGN'] = "DESIGN";
$lang['TR_REMOVE'] = "Remove";
$lang['TR_ALL']    = "All widgets";
$lang['TR_CREATE'] = "Create";
$lang['TR_UPDATE'] = "Update";
$lang['TR_NAME']   = "Name";
$lang['TR_WIDGET'] = "Widget";
$lang['TR_REQUIRED']    = "Required";
$lang['TR_CHOOSE_SIZE'] = "Choose Widget Size";
$lang['TR_CHOOSE_LANGUAGE'] = "Choose Language";
$lang['TR_LANGUAGE'] = "Language";
$lang['TR_LEAGUES']  = "Leagues";
$lang['TR_SIZE']     = "Size";
$lang['TR_WNAME']    = "Widget Name";
$lang['TR_SELECTL']  = "Select Leagues";