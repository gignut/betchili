<?php

$lang['TR_HTI_HEADER']    = 'How to integrate in 2 minutes';
$lang['TR_HTI_IMPORTANT'] = "Important Note";
$lang['TR_HTI_NOTE1'] = "Every widget can be used only on the";
$lang['TR_HTI_NOTE2'] = "domain. You can change it from ";
$lang['TR_HTI_NOTE3'] = "account settings";
$lang['TR_HTI_DESC1'] = "Copy paste";
$lang['TR_HTI_DESC2'] = "into";
$lang['TR_HTI_DESC3'] = "section";
$lang['TR_HTI_DESC4'] = "Like this";
$lang['TR_CLOSE']     = "Close";
$lang['TR_STEP']      = "Step";
$lang['TR_ALL']       = "All";
$lang['TR_GAME']      = "Game";
$lang['TR_TOPUSERS']  = "Top Users";
$lang['TR_CREATE']    = "Create Widget";
$lang['TR_HELP']      = "Help";
$lang['TR_EMPTY']     = "You have no widgets yet";
$lang['TR_LANGUAGE']  = "Language";
$lang['TR_HOWTO']     = "How to integrate";
$lang['TR_LEAGUES']   = "Leagues";
$lang['TR_STATS']     = "Stats";
$lang['TR_EDIT']      = "Edit";
$lang['TR_SIZE']      = "Size";