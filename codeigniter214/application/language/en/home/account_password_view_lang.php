<?php

$lang['TR_BETCHILI']          = 'Betchili';
$lang['TR_ACCOUNT_SETTINGS']  = 'Account settings';
$lang['TR_CHANGE_PASSWORD']   = 'Change password';

$lang['TR_FIRSTNAME']         = 'First Name';
$lang['TR_LASTNAME']          = 'Last Name';
$lang['TR_EMAIL']             = 'Email';
$lang['TR_DOMAIN']            = 'Domain';
$lang['TR_NEW_PASSWORD']      = 'New password';
$lang['TR_CONFIRM_PASSWORD']  = 'Confirm password';

$lang['TR_REQUIRED']          = 'Required';
$lang['TR_TIPEMAIL']          = 'Enter valid email address e.g. ';
$lang['TR_TIPDOMAIN']         = 'Enter valid domain e.g. ';
$lang['TR_TIPPASS']           = 'Enter at least 6 symbols';
$lang['TR_TIPPASS1']          = 'Passwords don\'t match';
$lang['TR_ACCOUNT_UPDATED']   = 'Your account updated successfully!';

$lang['TR_PASSWORD_UPDATED']  = 'Your password updated successfully!';
$lang['TR_UPDATE']            = 'Update';
