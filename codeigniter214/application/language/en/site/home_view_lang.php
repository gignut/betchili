<?php
//$lang['TR_PROFILE']  = 'Профил';

$lang['TR_BETCHILI']     = 'BETCHILI';
$lang['TR_TEXTLINE1']    = 'Social betting game';
$lang['TR_TEXTLINE2']    = 'Virtual coins';
$lang['TR_TEXTLINE3']    = 'Real odds on real games';
$lang['TR_TEXTLINE4']    = "It's free !";
$lang['TR_SIGNUP']       = 'Sign Up Free';
$lang['TR_LOGIN'] 	     = 'Login';
$lang['TR_REQUEST_DEMO'] = 'Request';
$lang['TR_LOGOUT']       = 'Logout';
$lang['TR_MIDDLE_PANE_HEADER'] = "Your website booster. Why?";
$lang['TR_CONTACT']       = "Contacts";
$lang['TR_WHY']           = "Why";
$lang['TR_FIRST_HEADER']  = "RICH CONTENT";
$lang['TR_SECOND_HEADER'] = "SOCIAL";
$lang['TR_THIRD_HEADER']  = "TRAFFIC";
$lang['TR_COPYRIGHT']     = "Copyright © 2014 Betchili";
$lang['TR_TEXT1'] = "Betchili is a social betting game with virtual currency based on real sport games with real odds. Just register in our platform, choose the sports or leagues you want, customize the design and you'll recieve a unique link. Put this link in your website and you'll have a social betting game for your website visitors. It will make your website more interesting, fun also dynamic.";
$lang['TR_TEXT2'] = "If you have a Betchili game in your website then you can be sure that you have a strong appearance in social networks. Why? Becouse Betchili is highly integrated with social networks. Game mechanics like challanging a friend, putting public bets, etc.. Owning Betchili will increase your social appearance and spread about your website through social channels.";
$lang['TR_TEXT3'] = "The interesting game mechanics, the thrill of winning, the fun of your users, Betchili social integration - all this are the proven facts that your website visitors will become more loyal, will visit your website more often, and also attract new users. Is it cool?";