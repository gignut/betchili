<?php

$lang['TR_BETCHILI']  = 'BetChili';
$lang['TR_SIGNUP']    = 'Sign Up';
$lang['TR_FIRSTNAME'] = 'First Name';
$lang['TR_LASTNAME']  = 'LAST Name';
$lang['TR_EMAIL']     = 'Email';
$lang['TR_DOMAIN']    = 'Domain';
$lang['TR_PASSWORD']  = 'Password';
$lang['TR_CONFIRMPASS']  = 'Confirm Password';
$lang['TR_LOGIN']  = 'Log In';
$lang['TR_QUEST']  = 'Already have an account?';
$lang['TR_REQUIRED']  = 'Required';
$lang['TR_TIPEMAIL']  = 'Enter valid email address e.g. ';
$lang['TR_TIPDOMAIN'] = 'Enter valid domain e.g. ';
$lang['TR_TIPPASS']   = 'Enter at least 6 symbols';
$lang['TR_TIPPASS1']   = 'Passwords don\'t match';
$lang['TR_TIP1']   = 'Your account created successfully!';
$lang['TR_TIP2']   = 'Please check and confirm your email address:';


