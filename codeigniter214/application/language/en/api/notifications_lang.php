<?php

$lang['TR_BET_ERROR_CODE1']	     = "No enough balance to accept bet."; 
$lang['TR_BET_ERROR_CODE2']	     = "Wrong bet chain.";
$lang['TR_BET_ERROR_CODE3']	     = "Game already started.";
$lang['TR_BET_ERROR_CODE4']	     = "Event is blocked.";
$lang['TR_BET_ERROR_CODE5']	     = "Event already has result.";
$lang['TR_BET_ERROR_CODE6']	     = "Coefficient or balance was updated. Please refresh the game and try again.";
$lang['TR_BET_ERROR_CODE7']	     = "Bet amount should be between ".MIN_BET_AMOUNT." and ".MAX_BET_AMOUNT.".";

$lang['TR_CHALLENGE_ERROR_CODE1'] = "Challenge amount should be between ".MIN_CHALLANGE_AMOUNT." and ".MAX_CHALLANGE_AMOUNT.".";
$lang['TR_CHALLENGE_ERROR_CODE2'] = "No enough balanec to accept the challenge.";
$lang['TR_CHALLENGE_ERROR_CODE3'] = "Selected event is blocked.";
$lang['TR_CHALLENGE_ERROR_CODE4'] = "Event has no value.";
$lang['TR_CHALLENGE_ERROR_CODE5'] = "Event already has result.";
$lang['TR_CHALLENGE_ERROR_CODE6'] = "Coefficient or balance was updated. Please refresh the page and try again.";
$lang['TR_CHALLENGE_ERROR_CODE7'] = "Please try again.";
$lang['TR_CHALLENGE_ERROR_CODE8'] = "Game already started.";
$lang['TR_CHALLENGE_ERROR_CODE9'] = "Opponent user do not match to target user.";
$lang['TR_CHALLENGE_ERROR_CODE10'] = "Target opponent already accepted the challenge.";
$lang['TR_CHALLENGE_ERROR_CODE11'] = "Challenge created by you.";
$lang['TR_CHALLENGE_ERROR_CODE12'] = "Public challenge already accepted.";

$lang['TR_GAME_ERROR_CODE1'] = "Please try again.";

$lang['TR_BLOKED_USER_LOGIN'] = "User is blocked by administration.";