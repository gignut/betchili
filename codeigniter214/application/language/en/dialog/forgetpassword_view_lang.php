<?php

//FORGOT PASSWORD
$lang['TR_BETCHILI']  = 'BetChili';
$lang['TR_NOT_REGISTERED_EMAIL']  = 'Not registered email.';
$lang['TR_EMAIL']     = 'Email';
$lang['TR_TIPEMAIL']  = 'Enter valid email address e.g. ';
$lang['TR_FORGOT']    = 'Forgot your password';
$lang['TR_LOGIN']     = 'Log In';
$lang['TR_RESET']     = 'Reset';
$lang['TR_CHECK']     = 'Please check your email';
$lang['TR_RESETLINK'] = 'We have sent you password reset link';
$lang['TR_BACK']      = 'Back to ';

