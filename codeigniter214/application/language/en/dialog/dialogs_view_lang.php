<?php
//GLOBALS
$lang['TR_BETCHILI']  = 'Betchili';

$lang['TR_EMAIL']       = 'Email';
$lang['TR_LOGIN']       = 'Log In';
$lang['TR_PASSWORD']    = 'Password';
$lang['TR_DOMAIN']      = 'Domain';
$lang['TR_SIGNUP']      = 'Sign Up';
$lang['TR_FIRSTNAME']   = 'First Name';
$lang['TR_LASTNAME']    = 'Last Name';
$lang['TR_REQUEST']    	= 'Request';
$lang['TR_CONFIRMPASS'] = 'Confirm Password';
$lang['TR_UPDATE']      = 'Update';
$lang['TR_SAVE']        = 'Save';
$lang['TR_YES']         = 'Yes';
$lang['TR_NO']          = 'No';
$lang['TR_NAME']       	= 'Name';

//Error cases
$lang['TR_REQUIRED']    = 'Required';
$lang['TR_TIPPASS']     = 'Enter at least 6 symbols';
$lang['TR_TIPEMAIL']  	= 'Enter valid email address e.g. ';
$lang['TR_TIPDOMAIN']   = 'Enter valid domain e.g. ';

//FORGOT PASSWORD
$lang['TR_NOT_REGISTERED_EMAIL']  = 'Not registered email.';
$lang['TR_FORGOT']    			  = 'Forgot your password';
$lang['TR_RESET']     			  = 'Reset';
$lang['TR_CHECK']     			  = 'Please check your email';
$lang['TR_RESETLINK'] 			  = 'We have sent you password reset link';
$lang['TR_BACK']      			  = 'Back to ';

//LOGIN
$lang['TR_BLOCKED']    = 'Your account is blocked';
$lang['TR_NOT_VERIF']  = 'is not verified email';
$lang['TR_VERIFEMAIL'] = 'email for verification';
$lang['TR_RESEND']     = 'Resend';
$lang['TR_INVALID']    = 'Invalid username or password';
$lang['TR_FORGOT']     = 'Forgot your password';
$lang['TR_NOTHAVE']    = 'Don\'t have an account yet';

//REQUEST
$lang['TR_REQUEST_BETCHILI']    = 'Send your request.';
$lang['TR_REQUEST_TEXT']        = "Want to get Betchili to your website? Please fill the form bellow and send us.";
$lang['TR_THANKS_FOR_REQUEST']  = "We'll contact you via email during 48 hours. Thanks.";

//SIGNUP
$lang['TR_QUEST']       = 'Already have an account?';
$lang['TR_TIPPASS1']    = 'Passwords don\'t match';
$lang['TR_TIP1']        = 'Your account created successfully!';
$lang['TR_TIP2']        = 'Please check and confirm your email address:';