<?php

$lang['TR_BETCHILI']  = 'BetChili';
$lang['TR_EMAIL']     = 'Email';
$lang['TR_PASSWORD']  = 'Password';
$lang['TR_LOGIN']     = 'Log In';
$lang['TR_REQUIRED']  = 'Required';
$lang['TR_TIPPASS']   = 'Enter at least 6 symbols';
$lang['TR_TIPEMAIL']  = 'Enter valid email address e.g. ';
$lang['TR_BLOCKED']  = 'Your account is blocked';
$lang['TR_NOT_VERIF']  = 'is not verified email';
$lang['TR_VERIFEMAIL']  = 'email for verification';
$lang['TR_RESEND']  = 'Resend';
$lang['TR_INVALID']  = 'Invalid username or password';
$lang['TR_FORGOT']  = 'Forgot your password';
$lang['TR_NOTHAVE']  = 'Don\'t have an account yet';