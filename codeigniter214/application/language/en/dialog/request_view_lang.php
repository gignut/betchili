<?php

$lang['TR_BETCHILI']  			= 'BetChili';
$lang['TR_REQUEST_BETCHILI']    = 'Send your request.';
$lang['TR_REQUIRED']   			= 'Required';
$lang['TR_NAME']       			= 'Name';
$lang['TR_EMAIL']      			= 'Email';
$lang['TR_DOMAIN']     			= 'Domain';
$lang['TR_TIPEMAIL']   			= 'Enter valid email address e.g. ';
$lang['TR_TIPDOMAIN']  			= 'Enter valid domain e.g. ';
$lang['TR_REQUEST']    			= 'Request';
$lang['TR_REQUEST_TEXT']        = "Want to get Betchili to your website? Please fill the form bellow and send us.";
$lang['TR_THANKS_FOR_REQUEST']  = "We'll contact you via email during 48 hours. Thanks.";

