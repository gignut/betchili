<?php

$lang['TR_LOGIN_WITH']         = 'Register and play with one of your social accounts - ';
$lang['TR_BALANCE']            = 'Balance';
$lang['TR_LOAD_MORE']          = 'Load More';
$lang['TR_LEADERBOARD']        = 'Leaderboard';
$lang['TR_ACCURACY']           = 'Accuracy';
$lang['TR_SELECT_COMPETITION'] = 'Select Competition';


$lang['TR_WIN_1']           = 'WIN 1';
$lang['TR_DRAW']            = 'DRAW';
$lang['TR_WIN_2']           = 'WIN 2';

$lang['TR_CHALLENGE']       = 'Challenge';
$lang['TR_SIMPLE']          = 'simple';
$lang['TR_ADVANCED']        = 'advanced';

$lang['TR_ACCEPT']          = 'Accept';
$lang['TR_NEXT']            = 'Next';
$lang['TR_CANCEL']          = 'Cancel';
$lang['TR_CLOSE']           = 'Close';

$lang['TR_CLOSE']           = 'Close';


$lang['TR_OR']              = 'or';
$lang['TR_PLEASE_SELECT_OPTION'] = 'Please select your option';
$lang['TR_AVAILABLE_BETS']  = 'available bets';
$lang['TR_OPPONENT']        = 'opponent';

$lang['TR_CHOOSE_OPPONENT']  = 'Choose opponent';
$lang['TR_ANY_OPPONENT']     = 'Any opponent';
$lang['TR_TYPE_NAME']        = 'Type player name';


$lang['TR_SHARE']          = 'Share';
$lang['TR_AMOUNT']         = 'Amount';
$lang['TR_BET_AMOUNT']     = 'Bet Amount';
$lang['TR_COEFFICIENT']    = 'Coefficient';
$lang['TR_WIN']            = 'Win';
$lang['TR_GAME']           = 'Game';
$lang['TR_ODD']            = 'Odd';
$lang['TR_SELECTION']      = 'Selection';
$lang['TR_HOLD']           = 'Multi Bet';
$lang['TR_REMOVE']   	   = "Remove";
$lang['TR_BET']            = 'Bet';
$lang['TR_MINMAX_BET_AMOUNT']   = 'Minimal bet amount is ' . MIN_BET_AMOUNT . ", maximal is " . MAX_BET_AMOUNT;
$lang['TR_MINMAX_CH_AMOUNT']    = 'Minimal amount is' . MIN_CHALLANGE_AMOUNT . ", maximal is " . MAX_CHALLANGE_AMOUNT;
$lang['TR_SHARE_YOUR_BET']      = "Share your bet and use shield";
$lang['TR_SHIELD_TIP']          = "(get ". SHIELD_PERCENT . "% cashback in case of loose)";

$lang['TR_ACT_BETS']       = 'Bets';
$lang['TR_ACT_CHALLANGES'] = 'Challanges';
$lang['TR_ACT_WIN']        = 'Win';
$lang['TR_ACT_LOOSE']      = 'Loose';
$lang['TR_ACT_WAITING']    = 'Waiting';
$lang['TR_ACT_IGNORED']    = 'Ignored';

$lang['TR_ACT_STATUS']     = 'Status';
$lang['TR_ACT_ESTIMATED']  = 'ESTIMATED';
$lang['TR_ACT_RETURNED']   = 'RETURNED';
$lang['TR_ACT_CASHBACK']   = 'cashback when loose';


$lang['TR_NO_MORE_CHALLANGES'] = 'No more challenges';
$lang['TR_NO_MORE_ACTIVITY']   = 'No bets or challenges';

$lang['TR_ESTIMATED_WIN']           = "estimated win";
$lang['TR_SELECT_OPTION_AND_FIGHT'] = "Select your option and fight.";
$lang['TR_CHALLANGES_YOU']          = "challenges you.";

$lang['TR_MENU_HOME']          = "Home";
$lang['TR_MENU_ACTIVITY']      = "Activities";
$lang['TR_MENU_USERS']         = "Top users";
$lang['TR_MENU_CART']          = "Cart";
$lang['TR_FEEDBACK']           = "Feedback";
$lang['TR_MENU_HELP']          = "Help";
$lang['TR_MENU_EXIT']          = "Exit";
$lang['TR_EARN_BOINTS']        = "Earn extra points by watching video";


$lang['TR_ACCURACY']          = "Accuracy";
$lang['TR_SELECT_LEAGUE']     = "Select league";
$lang['TR_ALL_LEAGUES']       = "All leagues";
$lang['TR_ALL_USERS']         = "All users"; 
$lang['TR_CLICK_TO_CHANGE']   = "Click to change"; 
$lang['TR_RESULT_SOON']       = "Result will be set soon";

$lang['TR_BET_REPLACE']         = 'Replace';
$lang['TR_BET_ON_SAME_GAME']    = 'You have a bet on the same game';
$lang['TR_EXISTING_BET']        = 'You have already set this bet.Check betchart';
$lang['TR_WANT_TO_REPLACE_BET'] = 'Do you want to replace it with';
$lang['TR_GAMESMESSAGE'] = 'No game available for selected day. Here is a list of games from ';

$lang['TR_HELP_DIALOG_TITLE']          = "Help";
$lang['TR_HELP_DIALOG_SUBTITLE']       = "Please go through each help link to have overall view of existing cool futures of the game.";
$lang['TR_HELP_DIALOG_POINT1']         = "How to challnge";
$lang['TR_ODD_EXP_0']        		 =  "game will end with first team win";
$lang['TR_ODD_EXP_1']        		 =  "game will end with draw";
$lang['TR_ODD_EXP_2']        		 =  "game will end with second team win";
$lang['TR_ODD_EXP_3']        		 =  "game will end with first team win or draw";
$lang['TR_ODD_EXP_4']        		 =  "game will end with one of team win";
$lang['TR_ODD_EXP_5']        		 =  "game will end with second team win or draw";
$lang['TR_ODD_EXP_6']        		 =  "first team goals plus basis is greater than second team goals at the end of game";
$lang['TR_ODD_EXP_7']        		 =  "second team goals plus basis is greater than first team goals at the end of game";
$lang['TR_ODD_EXP_8']        		 =  "overall goals count is less than total basis at the end of game";
$lang['TR_ODD_EXP_9']        		 =  "total basis - used in TU/TO";
$lang['TR_ODD_EXP_10']        		 =  "overall goals count is greater than total basis at the end of game";

$lang['TR_BETTING_OPTIONS']      =  "Betting odds";
$lang['TR_HELP_BACK']      		 =  "Back";
$lang['TR_ATTENTION']      		 =  "ATTENTION";
$lang['TR_ATTENTION_MSG']      	 =  "All odds will be calculated based on results of game during regulation time: 90(+) minutes";
$lang['TR_CH_HELP_MAIN']      	 =  "Challenge is a chance to earn more money compared with standard betting";
$lang['TR_CH_HELP_SUB']      	 =  "There are 3 points which should be taken into account before making challenge";
$lang['TR_CH_HELP_POINT1']       =  "extra money in case of win (challenge amount gathered from opponent)";
$lang['TR_CH_HELP_POINT2']       =  "losing challenge means losing challenge amount (the same as in case of betting)";
$lang['TR_CH_HELP_POINT3']       =  "challenge may be not accepted by other players (money will cash back after game end: no money lose)";
$lang['TR_CH_HELP_POINT4']  	 =  "A challenge is valid from the date of its registration and if no warning message has appeared. You can check your challenges in page <b>Activity</b> under sign ";

$lang['TR_G_HELP_POINT1']  =  "Mentioned rules determine the order of play in the application. Participation in the game automatically means you agree with them.";
$lang['TR_G_HELP_POINT2']  =  "The administration retains the right to modify or amend the text of the rules at any time. At the same time the new rules come into force and will be applied immediately after their publication in the application.";
$lang['TR_G_HELP_POINT3']  =  "Administration is not responsible for the admitted obvious errors, which may cause incorrect display of the coefficients, the results or the start of matches. In each such case, at the discretion of possible cancellation of bets or correction of the data.";
$lang['TR_G_HELP_POINT4']  =  "Do not use the possible application errors. In case of such acts should be punished in the form of cancellation of all bets and lock the user account in the application.";
$lang['TR_G_HELP_POINT5']  =  "Administration is not responsible for any direct or indirect damages of users associated with their participation in the application.";
$lang['TR_G_HELP_POINT6']  =  "Matches in the line are completely determined by the administration.";
$lang['TR_G_HELP_POINT7']  =  "Click to see ";
$lang['TR_G_HELP_POINT8']  =  "Intro tips";

$lang['TR_M_HELP_POINT1']  =  "В игре используется виртуальные деньги ";
$lang['TR_M_HELP_POINT2']  =  "На каждую из них можно <b>делать ставки</b> и <b>Оспорить</b> результат матча с друзьями. Баланс виртуальных денег может изменяться администрацией при необходимости в очень редких случаях чтобы исправить ошибочные ситуации.";
$lang['TR_M_HELP_POINT3']  =  "Initial amount of <b>Virtual Money</b> is handed at first time registration into game. Initial amount decides administration. All players are given an equal amount of virtual money for a certain period or tournament.";
$lang['TR_M_HELP_POINT4']  =  "You can get <b>Virtual money</b> when winning <b>bets</b> and <b>challenges</b>.";
// $lang['TR_M_HELP_POINT5']  =  " (icon on the main menu).";

$lang['TR_B_HELP_POINT1']  =  "<b>Bets</b> are accepted on all events line of the matches till 10 minute before of the start or remove them from the line.";
$lang['TR_B_HELP_POINT2']  =  "Bet odds are completely determined by the administration.";
$lang['TR_B_HELP_POINT3']  =  "A bet is valid from the date of its registration and if no warning message has appeared. You can check your bets in page <b>Activity</b> under sign ";
$lang['TR_B_HELP_POINT4']  =  "Bets on deferred, transferred or interrupted matches administration considered individually for each case.";
$lang['TR_B_HELP_POINT5']  =  "The result is always determined by <b>regular time</b>, excluding extra time and penalties or a shootout.";
$lang['TR_B_HELP_POINT6']  =  "In order to avoid errors on the calculation of the results of played games events may takes up to one day after the match's end.";
$lang['TR_B_HELP_POINT7']  =  "Below are restrictions on bet amount";
$lang['TR_B_HELP_POINT8']  =  "minimum bet amount";
$lang['TR_B_HELP_POINT9']  =  "maximum bet amount";

$lang['TR_NAV_HELP_GEN']    =  "General rules";
$lang['TR_NAV_HELP_MONEY']  =  "Virtual money";
$lang['TR_NAV_HELP_BETS']   =  "Bets";
$lang['TR_NAV_MULTI_BETS']  =  "MultiBets";
$lang['TR_NAV_HELP_BETO']   =  "Bet odds";
$lang['TR_NAV_HELP_CHAL']   =  "Challenges";


$lang['TR_MB_HELP_POINT1']  =  "The <b>MultiBet</b> gives you the chance of placing one stake on multiple matches.";
$lang['TR_MB_HELP_POINT2']  =  "Every prediction must be correct in order to win the combination bet. If any selection in a combo bet loses, the entire combo bet will be considered a loss.";
$lang['TR_MB_HELP_POINT3']  =  "Interwetten will multiply the stake with the odds of each selection in a combo bet for the <b>total payout</b>.";
$lang['TR_MB_HELP_POINT4']  =  "A MultiBet is valid from the date of its registration and if no warning message has appeared. You can check your multibets in page <b>Activity</b> under sign ";
$lang['TR_MB_HELP_POINT5']  =  "For <b>Remaining rules</b> go to section Bets";
$lang['TR_MB_HELP_POINT6']  =  "";
$lang['TR_MB_HELP_POINT7']  =  "";
$lang['TR_MB_HELP_POINT8']  =  "";
$lang['TR_MB_HELP_POINT9']  =  "";
$lang['TR_HELP_TITLE'] = "Help & Rules";

$lang['TR_INTRO_BOTTOM']  = "Return to this welcome screen by clicking <b>Help <i class='icon-question-sign'></i> > Intro tips</b>";
$lang['TR_INTRO_CHECKBOX'] = "Show on login";
$lang['TR_INTRO_NEXT']    = "Next";
$lang['TR_INTRO_CLOSE']   = "Close";
