<?php

$lang['TR_BETCHILI']          = 'Betchili';
$lang['TR_ACCOUNT_SETTINGS']  = 'Ваш аккаунт';
$lang['TR_CHANGE_PASSWORD']   = 'Изменить пароль';

$lang['TR_FIRSTNAME']         = 'Имя';
$lang['TR_LASTNAME']          = 'Фамилия';
$lang['TR_EMAIL']             = 'Эл. почта';
$lang['TR_DOMAIN']            = 'Домен';
$lang['TR_NEW_PASSWORD']      = 'Пароль';
$lang['TR_CONFIRM_PASSWORD']  = 'Подтверждение пароля';

$lang['TR_REQUIRED']          = 'Обязательный';
$lang['TR_TIPEMAIL']          = 'Введите действительный адрес эл. почты. например: ';
$lang['TR_TIPDOMAIN']         = 'Введите имя действующего домена. например';
$lang['TR_TIPPASS']           = 'Введите не менее 6 символов';
$lang['TR_TIPPASS1']          = 'Пароли не совпадают';
$lang['TR_ACCOUNT_UPDATED']   = 'Ваша учетная запись успешно обновленa';
$lang['TR_PASSWORD_UPDATED']  = 'Ваш пароль успешно изменен';

$lang['TR_PASSWORD_UPDATED']  = 'Ваш пароль успешно изменен';
$lang['TR_UPDATE']            = 'Oбновить';
