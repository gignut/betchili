<?php

$lang['TR_HTI_HEADER']    = 'Как интегрировать в 2 минуты';
$lang['TR_HTI_IMPORTANT'] = "Важное примечание";
$lang['TR_HTI_NOTE1']     = "Каждый виджет может быть использован только на";
$lang['TR_HTI_NOTE2']     = "домене. Вы можете изменить домен с";
$lang['TR_HTI_NOTE3']     = "настройки аккаунта";
$lang['TR_HTI_DESC1']     = "Скопируйте и вставьте";
$lang['TR_HTI_DESC2']     = "в";
$lang['TR_HTI_DESC3']     = "раздел";
$lang['TR_HTI_DESC4']     = "Вот так";
$lang['TR_CLOSE']         = "Закрыть";
$lang['TR_STEP']          = "Шаг";
$lang['TR_ALL']           = "Все";
$lang['TR_GAME']          = "Игра";
$lang['TR_TOPUSERS']      = "Лучшие Пользователи";
$lang['TR_CREATE']        = "Создать Виджет";
$lang['TR_HELP']          = "Помощь";
$lang['TR_EMPTY']         = "У вас нет еще виджеты";
$lang['TR_LANGUAGE']      = "Язык";
$lang['TR_HOWTO']         = "Как интегрировать";
$lang['TR_LEAGUES']       = "Лиги";
$lang['TR_STATS']         = "Статистика";
$lang['TR_EDIT']          = "Изменить";
$lang['TR_SIZE']          = "Размеры";