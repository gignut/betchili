<?php

$lang['TR_BET_ERROR_CODE1']	     = "Недостаточный баланс чтобы принять ставку."; 
$lang['TR_BET_ERROR_CODE2']	     = "Неправильная мультиставка.";
$lang['TR_BET_ERROR_CODE3']	     = "Игра уже началась.";
$lang['TR_BET_ERROR_CODE4']	     = "Ставка заблокирована.";
$lang['TR_BET_ERROR_CODE5']	     = "Событие уже имеет результат.";
$lang['TR_BET_ERROR_CODE6']	     = "Коэффициент или баланс был обновлен. Обновите игру и попробуйте снова.";
$lang['TR_BET_ERROR_CODE7']	     = "Сумма ставки должна быть в пределах ".MIN_BET_AMOUNT." и ".MAX_BET_AMOUNT.".";

$lang['TR_CHALLENGE_ERROR_CODE1'] = "Сумма спора должен быть между ".MIN_CHALLANGE_AMOUNT." и ".MAX_CHALLANGE_AMOUNT.".";
$lang['TR_CHALLENGE_ERROR_CODE2'] = "Недостаточный баланс чтобы принять спор.";
$lang['TR_CHALLENGE_ERROR_CODE3'] = "Выбранная ставка заблокирована.";
$lang['TR_CHALLENGE_ERROR_CODE4'] = "Событие не имеет значения.";
$lang['TR_CHALLENGE_ERROR_CODE5'] = "Событие уже имеет результат.";
$lang['TR_CHALLENGE_ERROR_CODE6'] = "Коэффициент или баланс был обновлен. Обновите игру и попробуйте снова.";
$lang['TR_CHALLENGE_ERROR_CODE7'] = "Пожалуйста, попробуйте еще раз.";
$lang['TR_CHALLENGE_ERROR_CODE8'] = "Игра уже началась.";
$lang['TR_CHALLENGE_ERROR_CODE9'] = "Противник не совпадает с целевой пользователю.";
$lang['TR_CHALLENGE_ERROR_CODE10'] = "Оппонент уже принял вызов.";
$lang['TR_CHALLENGE_ERROR_CODE11'] = "Вызов создан вами.";
$lang['TR_CHALLENGE_ERROR_CODE12'] = "Общий спор уже принято.";

$lang['TR_GAME_ERROR_CODE1'] = "Пожалуйста, попробуйте еще раз.";

$lang['TR_BLOKED_USER_LOGIN'] = "Пользователь заблокирован администрацией.";