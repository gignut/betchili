<?php

$lang['TR_BETCHILI']    = 'Betchili';
$lang['TR_EMAIL']       = 'Эл. почта';
$lang['TR_PASSWORD']    = 'Пароль';
$lang['TR_LOGIN']       = 'Войти';
$lang['TR_REQUIRED']    = 'Обязательный';
$lang['TR_TIPPASS']     = 'Введите не менее 6 символов';
$lang['TR_TIPEMAIL']    = 'Введите действительный адрес эл. почты. например: ';
$lang['TR_BLOCKED']     = 'Ваша учетная запись заблокирована';
$lang['TR_NOT_VERIF']   = 'не подтвержден e-mail';
$lang['TR_VERIFEMAIL']  = 'эл. почта для проверки';
$lang['TR_RESEND']      = 'Отправить повторно';
$lang['TR_INVALID']     = 'Неверное имя пользователя или пароль';
$lang['TR_FORGOT']      = 'Забыли пароль';
$lang['TR_NOTHAVE']     = 'Не зарегистрированы еще';