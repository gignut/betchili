<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$ci =& get_instance();

if ( ! function_exists('css'))
{
	function css($name, $autoVersion = FALSE)
	{			
		if($autoVersion)
		{
			 $name =  $name . "?version=" . time();
		}       	
		echo '<link rel="stylesheet" type="text/css"  href="' . MEDIA_URL  . $name .  '">';
	}
}

if ( ! function_exists('css_remote'))
{
	function css_remote($path)
	{			       	
		echo '<link rel="stylesheet" type="text/css"  href="' . $path .  '">';
	}
}

if ( ! function_exists('script'))
{
	function script($name, $autoVersion = FALSE)
	{
		if($autoVersion)
		{
			 $name =  $name . "?version=" . time();
		}
		echo '<script type= "text/javascript" src="' . MEDIA_URL . $name .  '"></script>';
	}
}

if ( ! function_exists('script_remote'))
{
	function script_remote($path)
	{
		echo '<script type= "text/javascript" src="' . $path . '"></script>';
	}
}

if ( ! function_exists('setFieldAsKey'))
{
	function setFieldAsKey($field, $data)
	{
		$tmp = array();
		foreach($data as $key => $val) {
		   $tmp[$val[$field]] = $val;
		}
		return $tmp;
	}
}

if (!function_exists('html2imgwin'))
{
    function html2imgwin($html, $w, $h)
    {
        header("Content-Type: image/jpeg");
        $cmdStr = "echo '$html' | /var/wkhtmltox/bin/wkhtmltoimage --crop-x 0 --crop-y 0 --crop-w " . $w . " --crop-h " . $h ." - -";
        passthru($cmdStr);
    }
}
