<?php

class Errorhandler {

    public static function register_handlers()
    {
        set_error_handler("errorHandler");
        register_shutdown_function("shutdownHandler");
        set_exception_handler('HandleExceptions');
    }
}

    function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context)
    {
        $error = "lvl: " . $error_level . " | msg:" . $error_message . " | file:" . $error_file . " | ln:" . $error_line;
        $date = date("Y-m-d");
        $hash = hash("md5", $error);
        switch ($error_level) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_PARSE:
                logr($error, "fatal", $date, $hash);
                break;
            case E_USER_ERROR:
            case E_RECOVERABLE_ERROR:
                logr($error, "error", $date, $hash);
                break;
            case E_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_USER_WARNING:
                logr($error, "warn", $date, $hash);
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                logr($error, "info", $date, $hash);
                break;
            case E_STRICT:
                logr($error, "debug", $date, $hash);
                break;
            default:
                logr($error, "warn", $date, $hash);
                break;
        }
    }

    function shutdownHandler() //will be called when php script ends.
    {
        $lasterror = error_get_last();
        if ($lasterror)
        {
            $type = "";
            switch ($lasterror['type'])
            {
                case E_ERROR:
                    $type="E_ERROR";
                    break;
                case E_CORE_ERROR:
                    $type="E_CORE_ERROR";
                    break;
                case E_COMPILE_ERROR:
                    $type="E_COMPILE_ERROR";
                    break;
                case E_USER_ERROR:
                    $type="E_USER_ERROR";
                    break;
                case E_RECOVERABLE_ERROR:
                    $type="E_RECOVERABLE_ERROR";
                    break;
                case E_CORE_WARNING:
                    $type="E_CORE_WARNING";
                    break;
                case E_COMPILE_WARNING:
                    $type="E_ERRE_COMPILE_WARNINGOR";
                    break;
                case E_PARSE:
                    $type="E_PARSE";
                    break;
            }
            $date = date("Y-m-d");
            $error = "[SHUTDOWN] lvl:" . $lasterror['type'] . " | msg:" . $lasterror['message'] . " | file:" . $lasterror['file'] . " | ln:" . $lasterror['line'];
            $hash = hash("md5", $error);
            logr($error, $type, $date, $hash);
        }
    }

    function HandleExceptions($exception)
    {
    
        $msg ='Exception of type \''.get_class($exception).'\' occurred with Message: '.$exception->getMessage().' in File '.$exception->getFile().' at Line '.$exception->getLine();
            
        $msg .="\r\n Backtrace \r\n";
        $msg .=$exception->getTraceAsString();

        $date = date("Y-m-d");
        $hash = hash("md5", $msg);
        logr($msg, "uncauth exception!!!", $date, $hash);

    }

    function logr($error, $errlvl, $date="", $hash="")
    {
        $url = FIREBASE;
        $fb = new fireBase($url);
        $response = $fb->set("/server/".$errlvl."/".$date."/".$hash, $error);
    }
