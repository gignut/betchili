<?php

require_once (APPPATH  . '/libraries/nut/core/SplClassLoader.php');

function chili_load_libraries()
{
    require_once (APPPATH  . 'libraries/Firebase/firebaseLib.php');
	require_once (APPPATH  . 'libraries/exceptions/db_exception.php');
	require_once (APPPATH  . 'libraries/exceptions/api_exception.php');
	
  	$classLoaderRespect = new SplClassLoader('Respect\Validation', APPPATH . '/libraries');
    $classLoaderRespect->register();


    $classLoaderCarbon = new SplClassLoader('Carbon', APPPATH . '/libraries');
    $classLoaderCarbon->register();
}

function chili_load_redis()
{

	require_once(APPPATH.'libraries/Predis/Autoloader.php');
	require_once(APPPATH.'libraries/nut/core/RedisSessionHandler.php');
	Predis\Autoloader::register();

	$redis = NULL;

	try {
        $redis = new Predis\Client(array(
            'scheme' => REDIS_SCHEME,
            'host'   => REDIS_HOST,
            'port'   => REDIS_PORT,
        ));
    }
    catch (Exception $e) {
        echo "Couldn't connected to Redis";
        echo $e->getMessage();
    }

	$sessHandler = new RedisSessionHandler($redis);

	session_set_save_handler(
        array($sessHandler, 'open'),
        array($sessHandler, 'close'),
        array($sessHandler, 'read'),
        array($sessHandler, 'write'),
        array($sessHandler, 'destroy'),
        array($sessHandler, 'gc')
        );
    
}
