<?php

function chili_define_constants()
{
	define("SPORT_SOCCER_ID", 1);

	define("EN_ID", 1);
	define("RU_ID", 2);
	define("AM_ID", 3);
	define("EN", "en");
	define("RU", "ru");
	define("AM", "am");
	define("DEFAULT_EN", "provide translation");
	define("DEFAULT_RU", "укажите перевод");
	define("DEFAULT_AM", "թարգմանություն");

	define("DEFAULT_LANGUAGE", EN); 
	
	//pages
	define("COUNTRIES",     "countries");
	define("PARTICIPANTS",  "participants");
	define("LEAGUES",       "leagues");
	define("COMPETITIONS",  "competitions");
	define("GAMES",         "games");
	define("USERS",         "users");
	define("NEWS",          "news");
	define("BETS",          "bets");

	/* MODULES FOLDERS*/
	define("MODULE_API_FOLDER",      "api");
	define("MODULE_ADMIN_FOLDER",    "admin");
	define("MODULE_EMBED_FOLDER",    "embed");
	define("MODULE_SITE_FOLDER",     "betchili");
	define("MODULE_GENERAL_FOLDER",  "general");

	// * WIDGET CONSTANTS
	define("WIDGET_GAME",  1);



	// SHORTLINK TYPES
	define("SIGNUP"          , 0);
	define("RESSET_PASSWORD" , 1);

	//EXEPTION MESSAGES
    define("SOMETHING_WENT_WRONG"  , "Something went wrong");
    define("COMPLETE SUCCESSFULLY" , "Complete successfully");


    //GAME SPECIFIC SETTINGS
   define("PLAYER_INITIAL_BALANCE"  , 2000);
   define("MAX_BET"  , 500);
   define("MIN_BET"  , 100);
   define("BOOST_COUNT", 0);
   define("SHIELD_COUNT", 0);
   define("BOOST_MULTIPLIER", 2);
   define("SHIELD_PERCENT"  , 20);
}
