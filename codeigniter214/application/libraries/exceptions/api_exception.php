<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Api_exception extends Exception {

		private $data;

		public static $SITEUSER_SIGNUP_SAME_EMAIL = 100;
		public static $SITEUSER_BLOCKED = 101;
		public static $SITEUSER_EMAIL_NOT_CONFIRMED = 102;
		public static $SITEUSER_INVALID_USERNAME_OR_PASSWORD = 103;

		public static $BET_ERROR_CODE1 = 104;
		public static $BET_ERROR_CODE2 = 105;
		public static $BET_ERROR_CODE3 = 106;
		public static $BET_ERROR_CODE4 = 107;
		public static $BET_ERROR_CODE5 = 108;
		public static $BET_ERROR_CODE6 = 109;
		public static $BET_ERROR_CODE7 = 110;

		public static $CHALLENGE_ERROR_CODE1 = 111;
		public static $CHALLENGE_ERROR_CODE2 = 112;
		public static $CHALLENGE_ERROR_CODE3 = 113;
		public static $CHALLENGE_ERROR_CODE4 = 114;
		public static $CHALLENGE_ERROR_CODE5 = 115;
		public static $CHALLENGE_ERROR_CODE6 = 116;
		public static $CHALLENGE_ERROR_CODE7 = 117;
		public static $CHALLENGE_ERROR_CODE8 = 118;
		public static $CHALLENGE_ERROR_CODE9 = 119;
		public static $CHALLENGE_ERROR_CODE10 = 120;
		public static $CHALLENGE_ERROR_CODE11 = 121;
		public static $CHALLENGE_ERROR_CODE12 = 122;

		public static $GAME_ERROR_CODE1 = 123;

		public static $BLOKED_USER_LOGIN = 404;
		

		public function __construct($message = '', $code = null, $data = null)
		{
			parent::__construct($message, $code);
			$this->data = $data;
		}

		public function getData()
		{
			return $this->data;
		}
	}

		