<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class nut_api 
{

    public static $PARAM = "chili";

    /**
     * [api description]
     * @param  string  $method            api endpont path, like "api/blabla"
     * @param  Array   $post              parameters sent to api
     * @param  boolean $returnAssocArray  if the returned data is assoc array or object
     * @return Array or Object            [description]
     */
    public static function api($method, $post, $returnAssocArray = true)
    {
        try
            {
                $API_URI = BASE_URL . INDEX_PHP . "/" . MODULE_API_FOLDER . "/" ;
                $url =  $API_URI . $method;
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, 1);
                curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($post));
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                curl_close($ch);
                
                $jsonobj  = json_decode($result, $returnAssocArray);

                // throw exception in case of api errors  
                
                $isErrorExists = $returnAssocArray ? isset($jsonobj['exception']) : isset($jsonobj->exception);
                if($isErrorExists)
                {
                  $exceptionCode    = $returnAssocArray ? $jsonobj['exception']: $jsonobj->exception;
                  $exceptionMessage = $returnAssocArray ? $jsonobj['message']: $jsonobj->message;
                  $exceptionData    = $returnAssocArray ? $jsonobj['data']: $jsonobj->data;
                    
                  if ($exceptionCode < 3) // except general and respect validation exception codes (0, 1, 2)
                  {
                      throw new Exception($exceptionMessage);
                  } else {
                      throw new Api_exception($exceptionMessage, $exceptionCode, $exceptionData);
                   }
                } 
              
                return $jsonobj;
            }
            catch(Exception $e)
            {
                throw $e;
            }
    }

    public static function wrapData($data)
    {
        return array(self::$PARAM => $data);
    }
}
?>