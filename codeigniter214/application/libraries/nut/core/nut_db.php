<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class nut_db {
		private static $db;
		private static $ci;
		private static $transaction_started;
		private static $currentTransactionName;

		public static function connectToDB($configs = null)
		{
			$db = null;
			self::$ci = & get_instance();
			if(is_array($configs))
			{
				$db =  self::$ci->load->database($configs, TRUE);
			}
			else
			{
				$db= self::$ci->load->database("default", TRUE);
			}

			nut_db::$db= $db;
			return nut_db::$db;
		}

		private static function getDb()
		{
			if(!isset(nut_db::$db))
			{
				self::$ci = & get_instance();
				self::$ci->load->database();
				nut_db::$db= self::$ci->db;
			}
			nut_db::$db->query("SET time_zone='+00:00';");

			return  nut_db::$db;
		}

		private function logr($error, $errlvl, $date="", $hash="")
	    {
	        $url = FIREBASE;
	        $fb = new fireBase($url);
	        $response = $fb->set("/server/".$errlvl."/".$date."/".$hash, $error);
	    }

		private static function rollBack_Log($e)
		{
			if(nut_db::$transaction_started)
			{
				// TODO:: add try catch
				if(nut_db::getDb()->trans_status() === FALSE)
				{
					nut_db::transactionRollback();
				}
			}

			$date = date("Y-m-d");
		    $error = "[DB-ERROR] lvl: DB | msg:" . $e->getMessage();
		    $hash = hash("md5", $error);
		    logr($error, "DB_ERROR", $date, $hash);
		}
		
		public static function getLastID($table, $idColumn = "id", $count = 1)
		{
			try {
				$sql = "SELECT $idColumn as id FROM " . $table . " ORDER BY " .$idColumn. " DESC LIMIT " . $count;
				$query = self::query($sql);
				
				if($query->num_rows()>0)
				{
					if($count == 1)
					{
						$row = $query->row_array();
						return $row['id'];
					}
					else 
					{
						$res = $query->result_array();
						$data = array();
						foreach ($res as $key => $value)
						{
							array_unshift($data, $value['id']); 
						}
						return $data;
					}
				}
				else
				{
					return null;
				}
			}
			catch (Db_exception $e) {
				throw $e;
			}
		}

		public static function query($sql, $paramsArray = null)
		{
			try {
				$query = nut_db::getDb()->query($sql, $paramsArray);
				return $query;
			}
			catch (Db_exception $e) {
				nut_db::rollBack_Log($e);
				throw $e;
			}
		}

		public static function selectQuery($table, $fields = "*", $where = null, $limit = null, $offset = null)
		{
			try {
				$db =  nut_db::getDb();

				$db->select($fields);
				$db->from($table);
				if(!is_null($where))
				{
					$db->where($where);
				}

				if(!is_null($limit) )
				{
					if(!is_null($offset))
					{
						$db->limit($limit, $offset);
					}
					else
					{
						$db->limit($limit);
					}
				}

				$query = $db->get();
				return $query;
			}
			catch (Db_exception $e) {
				throw $e;
			}
		}

		// Wrapper for active record functions START

		public static function select($select = '*')
		{
			nut_db::getDb()->select($select);
		}

		public static function from($table)
		{
			nut_db::getDb()->from($table);
		}

		//Note: All values passed to this function are escaped automatically, producing safer queries.
		public static function where($key, $value = NULL, $escape = TRUE)
		{
			nut_db::getDb()->where($key, $value, $escape);
		}

		public static function get($table = '', $limit = null, $offset = null)
		{
			$query = nut_db::getDb()->get($table, $limit, $offset);
			return $query;
		}

		public static function limit($value, $offset = '')
		{
			nut_db::getDb()->limit($value, $offset);
		}

		
		public static function set($key, $value = '', $escape = TRUE)
		{
			nut_db::getDb()->set($key, $value, $escape);
		}
		
		//Note: All values are escaped automatically producing safer queries.
		public static function insert($table = '', $set = NULL)
		{
			$table = !is_null($table)? trim($table) : '';
			$query = nut_db::getDb()->insert($table, $set);
			return $query;
		}

		//Note: All values are escaped automatically producing safer queries.
		public static function insert_batch($table = '', $set = NULL)
		{
			$table = !is_null($table)? trim($table) : '';
			$query = nut_db::getDb()->insert_batch($table, $set);
			return $query;			
		}

		//Note: All values are escaped automatically producing safer queries.
		public static function update($table, $data, $where = null)
		{
			$table = !is_null($table)? trim($table) : '';

			if($table != '' && !is_null($data))
			{
				$db = nut_db::getDb();
				
				if(!is_null($where))
				{
					$db->where($where);
				}

				$query = $db->update($table, $data);
				return $query;
			}
			else
			{
				log_message("error", "nut_db::update- table name or data is empty!");
			}
		}

		//Note: All values are escaped automatically producing safer queries.
		public static function update_batch($table, $data, $where = null)
		{
			$table = !is_null($table)? trim($table) : '';

			if($table != '' && !is_null($data))
			{
				$query = nut_db::getDb()->update_batch($table, $data, $where);
				return $query;
			}
			else
			{
				log_message("error", "nut_db::update_batch - table name or data is empty!");
			}
		}

		public static function delete($table, $whereclause = null)
		{
			$table = !is_null($table)? trim($table) : '';

			if($table != '')
			{
				$query = nut_db::getDb()->delete($table, $whereclause);
				return $query;
			}
			else
			{
				log_message("error", "nut_db::delete- table name is empty!");
			}
		}

		// Wrapper for active record functions END

		public static function close()
		{
			if(isset(nut_db::$db))
			{
				nut_db::$db->close();
				nut_db::$db = null;
			}
		}

		public static function transactionStart($transactionName)
		{
			// TODO:: maybe it can fail,
			if(!nut_db::$transaction_started)
			{	
				nut_db::$currentTransactionName =  $transactionName;
				nut_db::getDb()->trans_begin();
				nut_db::$transaction_started = true;
			}
		}

		public static function transactionCommit($transactionName)
		{
			// TODO:: maybe it can fail,
			if(nut_db::$transaction_started && nut_db::$currentTransactionName === $transactionName)
			{
				nut_db::getDb()->trans_commit();
				nut_db::$transaction_started = false;
				nut_db::$currentTransactionName = "";
			}
		}
		
		public static function getTransactionStatus()
		{
			return self::$transaction_started;
		}

		public static function transactionRollback($transactionName)
		{
			// TODO:: maybe it can fail,
			if(nut_db::$transaction_started && nut_db::$currentTransactionName === $transactionName)
			{
				nut_db::getDb()->trans_rollback();
				nut_db::$transaction_started = false;
				nut_db::$currentTransactionName = "";
			}
			else
			{
				log_message("error", "nut_db::transactionRollback- roolbacked transaction without started!");
			}
		}

		public static function lastQuery()
		{
			return nut_db::getDb()->last_query();
		}
		
		public static function get_lock($lock, $timeout = 1)
		{
			$sql = "SELECT GET_LOCK(\"?\", ?)";
			$query = nut_db::query($sql, array($lock, $timeout));
			$val = $query->row_array();
			$arrValues  = array_values($val);
			$res = $arrValues[0];
			return $res;
		}

		public static function release_lock($lock)
		{
			$sql = "SELECT RELEASE_LOCK(\"?\")";
			$query = nut_db::query($sql, array($lock));
			$val = $query->row_array();
			$arrValues  = array_values($val);
			$res = $arrValues[0];
			return $res;
		}

		public static function is_free_lock($lock)
		{
			$sql = "SELECT IS_FREE_LOCK(\"?\")";
			$query = nut_db::query($sql, array($lock));
			$val = $query->row_array();
			$arrValues  = array_values($val);
			$res = $arrValues[0];
			return $res;
		}

		function __destruct()
		{
			if(self::$transaction_started === TRUE)
			{
				self::transactionRollback();
			}
		}
	}


