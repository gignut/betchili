<?php
//namespace nut\core;

	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class nut_email{
	//The following is a list of all the preferences that can be set when sending email.
	// have a look http://ellislab.com/codeigniter/user-guide/libraries/email.html  for more details
		
		private static $default_config = array(
			"useragent" =>	"CodeIgniter",
			"protocol"  =>	"mail",  					// 	mail, sendmail, or smtp	The mail sending protocol.
			"mailpath"  =>	"/usr/sbin/sendmail",   	//  None	The server path to Sendmail.
			"smtp_host"	=>	"",							//  No Default	None	SMTP Server Address.
			"smtp_user"	=>	"",							//  No Default	None	SMTP Username.
			"smtp_pass" =>  "",	 						//  No Default	None	SMTP Password.
			"smtp_port"	=>  25,  						//  25	None	SMTP Port.
			"smtp_timeout" => 5,						//  None	SMTP Timeout (in seconds).
			"wordwrap" =>	TRUE,						//  TRUE or FALSE (boolean)	Enable word-wrap.
			"wrapchars" =>	76,							//  Character count to wrap at.
			"mailtype" =>	"text",						//  text or html	Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
			"charset"  =>  "utf-8",						//  Character set (utf-8, iso-8859-1, etc.).
			"validate" =>	FALSE,						//  TRUE or FALSE (boolean)	Whether to validate the email address.
			"priority" => 3,							//  1, 2, 3, 4, 5	Email Priority. 1 = highest. 5 = lowest. 3 = normal.
			"crlf" => "\r\n",							//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
			"newline" => "\r\n",						//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
			"bcc_batch_mode" =>	FALSE,					//  TRUE or FALSE (boolean)	Enable BCC Batch Mode.
			"bcc_batch_size" =>	200						//  None	Number of emails in each BCC batch.
		);
		
		//codeigniter instance
		private $ci;
		
		// sent email data
		public $from_email="noreply@betchili.com";
		public $from_name;
		public $to_email;
		public $cc_email;
		public $bcc_email;
		public $subject;
		public $message;
		public $alt_message;
		public $reply_email;
		public $reply_name;
		public $attachments;
		
		/*
		Overriding Word Wrapping

		If you have word wrapping enabled (recommended to comply with RFC 822) and you have a very long link in your email it can get wrapped too,
		causing it to become un-clickable by the person receiving it. CodeIgniter lets you manually override word wrapping within part of your message like this:

		The text of your email that
		gets wrapped normally.

		##############  IMPORTANT ##################
		{unwrap}http://example.com/a_long_link_that_should_not_be_wrapped.html{/unwrap}

		More text that will be
		wrapped normally.
		Place the item you do not want word-wrapped between: {unwrap} {/unwrap}
		*/
		
		public function __construct()
		{
			$this->ci=&get_instance();
			$this->ci->load->library('email');
		}
		
		/**
		 * [initialize with configuration data, some of them are set by default]
		 * @param  array  $config_data [description]
		 * @return [type]              [description]
		 */
		public  function initialize($config_data = array())
		{
			if(!is_array($config_data))
			{
				$config_data = nut_test_emails::$email_accounts[$config_data];
			}
			
			$config = array();
			foreach (self::$default_config as $key => $value)
			{
				$config[$key] = isset($config_data[$key]) ? $config_data[$key] : self::$default_config[$key];
			}
			$this->ci->email->initialize($config);
			$this->ci->email->set_newline("\r\n");
		}
			
		//reset co	
		public function resetData()
		{
			$this->from_email	= null;
			$this->from_name	= null;
			$this->to_email		= null;
			$this->cc_email		= null;
			$this->bcc_email	= null;
			$this->subject      = null;
			$this->message      = null;
			$this->alt_message  = null;
			$this->reply_email  = null;
			$this->reply_name   = null;
			$this->attachments  = array();

			$this->ci->email->clear(TRUE);
		}	
		

		//		   - from_email, from_name
		//		   - to -> Can be a single email, a comma-delimited list or an array	 
		//		   - css -> Can be a single email, a comma-delimited list or an array
		//         - reply_email, reply_name
		//		   - alt_message -> Sets the alternative email message body:
		public  function send($data = array())
		{
			$from_email	  = isset($data["from_email"])  ? $data["from_email"]  : $this->from_email;
			$from_name	  = isset($data["from_name"])   ? $data["from_name"]   : $this->from_name;              
			$to_email	  = isset($data["to_email"])    ? $data["to_email"]    : $this->to_email;
			$cc_email	  = isset($data["cc_email"])    ? $data["cc_email"]    : $this->cc_email;
			$bcc_email	  = isset($data["bcc_email"])   ? $data["bcc_email"]   : $this->bcc_email;
			$subject      = isset($data["subject"])     ? $data["subject"]     : $this->subject;
			$message      = isset($data["message"])     ? $data["message"]     : $this->message;
			$alt_message  = isset($data["alt_message"]) ? $data["alt_message"] : $this->alt_message;
			$reply_email  = isset($data["reply_email"]) ? $data["reply_email"] : $this->reply_email;
			$reply_name   = isset($data["reply_name"])  ? $data["reply_name"]  : $this->reply_name;
			$attachments  = isset($data["attachments"]) ? $data["attachments"] : $this->attachments;
			
			///** VALIDATION OF INPUT DATA **/	

			// clear attachments and data
			$this->ci->email->clear(TRUE);
			
			$this->ci->email->from($from_email, $from_name);
			//Sets the email address(s) of the recipient(s). Can be a single email, a comma-delimited list or an array:
			$this->ci->email->to($to_email);

			$this->ci->email->subject($subject);
			$this->ci->email->message($message);
			$this->ci->email->set_alt_message($alt_message);
			$this->ci->email->reply_to($reply_email, $reply_name);

			//Sets the CC email address(s). Just like the "to", can be a single email, a comma-delimited list or an array.
			$this->ci->email->cc($cc_email);
			$this->ci->email->bcc($bcc_email);
		
			//Put the file path/name in the first parameter. Note: Use a file path, not a URL. 
			if(!is_null($attachments))
			{
				foreach($attachments as $path)
				{
					$this->ci->email->attach($path);
				}
			}
			
			if ( ! $this->ci->email->send())
			{
				nut_log::log("error" , "Error: nut_email->send [email: " .$to_email . " ]");
			}
		}
	}



	class nut_test_emails
	{
		// "mailpath"  =>	"/usr/sbin/sendmail",  //  None	The server path to Sendmail.

		public static $email_accounts = array(
					// "protocol"  =>	"smtp",  					// 	mail, sendmail, or smtp	The mail sending protocol.
					// "smtp_host"	=>	"ssl://smtp.gmail.com",		//  No Default	None	SMTP Server Address.
					// "smtp_user"	=>	"nutdev1@gmail.com",	   //  No Default	None	SMTP Username.
					// "smtp_pass" =>   "popoqner",	 				//  No Default	None	SMTP Password.
					// "smtp_port"	=>   465,  					//  25	None	SMTP Port.

			"noreply" => 
			 array(
					// "useragent" =>	"CodeIgniter",
					"protocol"  =>	"smtp",  					// 	mail, sendmail, or smtp	The mail sending protocol.
					"smtp_host"	=>	"mx26.valuehost.ru",		//  No Default	None	SMTP Server Address.
					"smtp_user"	=>	"noreply@betchili.com",	   //  No Default	None	SMTP Username.
					"smtp_pass" =>  "qwe123",	 				//  No Default	None	SMTP Password.
					"smtp_port"	=>   2525,  					//  25	None	SMTP Port.
					"smtp_timeout" => 5,						//  None	SMTP Timeout (in seconds).
					"wordwrap" =>	TRUE,						//  TRUE or FALSE (boolean)	Enable word-wrap.
					"wrapchars" =>	76,							//  Character count to wrap at.
					"mailtype" =>	"html",						//  text or html	Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
					"charset"  =>  "utf-8",						//  Character set (utf-8, iso-8859-1, etc.).
					"validate" =>	FALSE,						//  TRUE or FALSE (boolean)	Whether to validate the email address.
					"priority" => 3,							//  1, 2, 3, 4, 5	Email Priority. 1 = highest. 5 = lowest. 3 = normal.
					"crlf" => "\r\n",							//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
					"newline" => "\r\n",						//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
					"bcc_batch_mode" =>	TRUE,					//  TRUE or FALSE (boolean)	Enable BCC Batch Mode.
					"bcc_batch_size" =>	200	
				)	
			);
	}

