<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class nut_session 
{
	
	public static function init()
	{
		if(!session_id())
		{
			session_name(SESSION_NAME);
			session_start();
		}
	}

	public static function set($key, $value)
	{
		try
		{
			validator::notEmpty()->noWhitespace()->check($key);
			$_SESSION[$key] = $value;
			return true;	
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	public static function get($key = NULL, $default = NULL)
	{
		try
		{
			if(isset($key))
			{
				return isset($_SESSION[$key])  ? $_SESSION[$key] : $default;
			}
			else
			{
				return $default;
			}	
		}
		catch(Exception $e)
		{
			return NULL;
		}
	}

	public static function destroy()
	{
		session_destroy();
	}

}