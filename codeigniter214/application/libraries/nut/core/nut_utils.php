<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	use Respect\Validation\Validator as validator;

	/**
	 * nut_utils class for utility functions
	 */
	class nut_utils 
	{

		public static function evalRedirect($path)
		{
			echo json_encode(array("module" => "redirect", "action" => "redirect",  "message" => $path));
		}
		
		public static function  scriptRedirect($path)
		{
			echo "<script> window.location.href = '" . $path . "' </script>";
			exit();
		}

		public static function reloadPage()
		{
			echo json_encode(array("module" => "redirect", "action" => "reload"));	
		}

		public static function print_error($publicText = null, $code = 1, $data = null)
		{
			echo json_encode(array("error" =>  $code, "message" => $publicText, 'data' => $data));
		}

		public static function noty_success($message = "Everything is fine")
		{
			echo json_encode(array("module" => "noty", "action" => "success",  "message" => $message));
		}

		public static function noty_error($message = "Something went wrong")
		{
			echo json_encode(array("module" => "noty", "action" => "error",  "message" => $message));
		}

		public static function locationRedirect($path)
		{
			header('Location: ' . $path . ' ');
		}

		public static function error($message = "Something went wrong ..." , $status = 0)
		{
			echo json_encode(array("error" => $status, "message" => $message));
		}

		public static function error_page()
		{
			self::locationRedirect(MODULE_SITE_FOLDER . "/siteuser/error");
		}

		public static function adjust_timestamp_to_tz($stamp, $tz, $start=FALSE, $end=FALSE)
		{
            $stamp = $stamp - $tz*60;
            if ($start) {
            	$adjusted = $stamp - $stamp%(24*60*60) + $tz*60;
            } else if ($end) {
            	$adjusted = $stamp - $stamp%(24*60*60) + 24*60*60 - 1 + $tz*60;
            } else {
            	$adjusted = $stamp + $tz * 60;
            }

            return $adjusted;
		}

		public static function sanitize_keys($arr, $keys, $setDef = FALSE, $default = NULL)
		{
			$new_arr = array();
			if(validator::arr()->each(validator::arr())->validate($arr))
			{
				foreach($arr as $k => $v)
				{
					$var_arr = array();
					foreach($keys as $key)
					{
						if ($setDef)
						{
							$var_arr[$key] = element($key, $v, $default);
						}
						elseif(isset($v[$key]))
						{
							$e = element($key, $v);
							$var_arr[$key] = $e;
						}
					}
					$new_arr[] = $var_arr;
				}
			}
			else 
			{
				foreach($keys as $key)
				{
					if ($setDef)
					{
						$new_arr[$key] = element($key, $arr, $default);
					}
					elseif(isset($arr[$key]))
					{
						$e = element($key, $arr);
						$new_arr[$key] = $e;
					}
				}
			}

			return $new_arr;
		}
	}