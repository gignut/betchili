<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Define all tables used in BF
 */
class chili_sport 
{	
	public static $SOCCER_TIME = 6300; // in seconds
	public static $SOCCER_PARTICIPANTS_COUNT = 2;
}
