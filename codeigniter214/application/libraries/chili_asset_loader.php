<?php
	class chili_asset_loader
	{
		public static function angularjsVersion($local = true, $version = "1.2.15", $lang = "en")
		{
			if($local)
			{
				script(VENDORLIB . "/angular-" . $version. "/angular.js");
				script(VENDORLIB . "/angular-" . $version. "/angular-resource.js");
				script(VENDORLIB . "/angular-" . $version. "/angular-animate.js");

				if($lang)
				{
					switch($lang)
					{
						case "ru":
							// script(VENDORLIB . "/angular-" . $version. "/i18n/angular-locale_ru-ru.js");
							break;
						case "en" :
							// script(VENDORLIB . "/angular-" . $version. "/i18n/angular-locale_en-us.js");
							break;
					}
					
				}
			}
			else
			{	
				script_remote("//ajax.googleapis.com/ajax/libs/angularjs/" . $version . "/angular.min.js");
				script_remote("//ajax.googleapis.com/ajax/libs/angularjs/" . $version . "/angular-resource.min.js");
				script_remote("//ajax.googleapis.com/ajax/libs/angularjs/" . $version . "/angular-animate.min.js");

				switch($lang)
				{
					case "ru":
						// script_remote("//ajax.googleapis.com/ajax/libs/angularjs/" . $version . "/i18n/angular-locale_ru-ru.js");
						break;
					case "en" :
						// script_remote("//ajax.googleapis.com/ajax/libs/angularjs/" . $version . "/i18n/angular-locale_en-us.js");
						break;
				}
			}
		}

		public static function angularstrap()
		{
			script(VENDORLIB . "/angular-strap/angular-strap.min.js");
			script(VENDORLIB . "/angular-strap/tooltip.js");
		}

		public static function uibootstrap()
		{
			script(VENDORLIB . "/ui-bootstrap/ui-bootstrap-0.6.0.min.js");
			script(VENDORLIB . "/ui-bootstrap/ui-bootstrap-tpls-0.6.0.min.js");
			script(VENDORLIB . "/ui-bootstrap/tooltip.js");
			script(VENDORLIB . "/ui-bootstrap/popover.js");
		}

		public static function bootstrap($local = true)
		{
			if($local)
			{
				css(VENDORLIB    . "/bootstrap/css/bootstrap.min.css");
				css(VENDORLIB    . "/bootstrap/css/bootstrap-responsive.min.css");
				script(VENDORLIB . "/bootstrap/js/bootstrap.min.js");
			}
			else
			{
				//remote bootstrap CDN
			}
		}

		public static function bootstrapNotResponsive($local = true)
		{
			if($local)
			{
				css(VENDORLIB    . "/bootstrap/css/bootstrap.min.css");
				script(VENDORLIB . "/bootstrap/js/bootstrap.min.js");
			}
			else
			{
				//remote bootstrap CDN
			}
		}


		public static function flatstrap($local = true)
		{
			if($local)
			{
				css(VENDORLIB     . "/flatstrap/css/bootstrap.css");
				css(VENDORLIB     . "/flatstrap/css/bootstrap-responsive.min.css");
				script(VENDORLIB  . "/flatstrap/js/bootstrap.min.js");
			}
			else
			{
				//remote bootstrap CDN
			}
		}

		public static function jquery($local = true, $includejQueryUI = true)
		{
			if($local)
			{
				script(VENDORLIB . "/jquery-ui-1.10.3/js/jquery-1.9.1.min.js");

				//jQueryUI CSS	
				if($includejQueryUI)
				{
					css(VENDORLIB . "/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css");
					script(VENDORLIB . "/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js");
				}
			}
			else
			{
				// remote files
			}
		}

		public static function backstretch()
		{
			script(VENDORLIB . "/backstretch/jquery.backstretch.min.js");
		}

		public static function chosen()
		{
			css(VENDORLIB    . "/chosen_v1.0.0/chosen.min.css");
			script(VENDORLIB . "/chosen_v1.0.0/chosen.jquery.min.js");
		}

		public static function fileuploader()
		{
			css(VENDORLIB    . "/fileupload/css/jquery.fileupload.css");
			css(VENDORLIB    . "/fileupload/css/jquery.fileupload-ui.css");
			//script(VENDORLIB . "/fileupload/js/jquery.postmessage-transport.js");
			//script(VENDORLIB . "/fileupload/js/jquery.xdr-transport.js");
			//script(VENDORLIB . "/fileupload/js/jquery.iframe-transport.js");
			script(VENDORLIB . "/fileupload/js/jquery.fileupload.js");
		}

		public static function boostrapDateTimePicker()
		{
			css(VENDORLIB . "/bootstrap-datetimepicker-0.0.11/css/bootstrap-datetimepicker.min.css");
			script(VENDORLIB . "/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.min.js");	
		}

		public static function noty()
		{
			script(VENDORLIB . "/noty/js/jquery.noty.js");
			//script(VENDORLIB . "/noty/js/promise.js");
			script(VENDORLIB . "/noty/js/themes/default.js");
			script(VENDORLIB . "/noty/js/layouts/bottom.js");
			script(VENDORLIB . "/noty/js/layouts/bottomCenter.js");
			script(VENDORLIB . "/noty/js/layouts/bottomLeft.js");
			script(VENDORLIB . "/noty/js/layouts/bottomRight.js");
			script(VENDORLIB . "/noty/js/layouts/center.js");
			script(VENDORLIB . "/noty/js/layouts/centerLeft.js");
			script(VENDORLIB . "/noty/js/layouts/centerRight.js");
			script(VENDORLIB . "/noty/js/layouts/inline.js");	
			script(VENDORLIB . "/noty/js/layouts/top.js");
			script(VENDORLIB . "/noty/js/layouts/topCenter.js");
			script(VENDORLIB . "/noty/js/layouts/topLeft.js");
			script(VENDORLIB . "/noty/js/layouts/topRight.js");
		}

		public static function responseJs()
		{
			script(VENDORLIB . "/responsejs/response.min.js");
		}

		public static function momentJs()
		{
			script(VENDORLIB . "/momentjs/moment.min.js");
		}

		public static function scrollBar()
		{
			css(VENDORLIB . "/scrollBar/css/jquery.mCustomScrollbar.css");
			script(VENDORLIB . "/scrollBar/js/jquery.mCustomScrollbar.min.js");
		}

		public static function sidrJs()
		{
			css(VENDORLIB . "/sidr-package-1.1.1/stylesheets/jquery.sidr.light.css");
			script(VENDORLIB . "/sidr-package-1.1.1/jquery.sidr.min.js");
		}

		public static function syntaxHighlighter()
		{
			css(VENDORLIB . "/syntaxHighlighter/styles/shCore.css");
			script(VENDORLIB . "/syntaxHighlighter/scripts/shCore.js");
			script(VENDORLIB . "/syntaxHighlighter/scripts/shBrushXml.js");
			
		}

		public static function colorPicker()
		{
			css(VENDORLIB . "/angular-bootstrap-colorpicker/css/colorpicker.css");
			script(VENDORLIB . "/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js");
		}

		public static function hopscotch(){
			css(VENDORLIB . "/hopscotch/dist/css/hopscotch.css");
			script(VENDORLIB . "/hopscotch/dist/js/hopscotch.js");
			
		}

		public static function introjs(){
			css(VENDORLIB . "/introjs/introjs.css");
			script(VENDORLIB . "/introjs/intro.js");
		}		
	}

?>