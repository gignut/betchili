<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MailruException extends Exception 
{

	private $data;
	public function __construct($message = '', $code = null, $data = null)
	{
		parent::__construct($message, $code);
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}
}

class Mailru
{

	const TOKEN_URI        = "https://connect.mail.ru/oauth/token";
	const API_URI          = "http://www.appsmail.ru/platform/api"; 
    const AUTH_DIALOG_PATH = "https://connect.mail.ru/oauth/authorize?";

	private $ERROR;
	private $APP_ID;
	private $APP_SECRET;
	private $REDIRECT_URI;
	private $SCOPE;


	private $AUTH_PATH;

    private $userData;

    private $ACCESS_TOKEN;
    private $REFRESH_TOKEN;

	public function __construct($config) 
	{
	    if (!session_id()) 
	    {
	      session_start();
	    }

        $this->userData = array();

	    $this->APP_ID       = isset($config["app_id"])       ? $config["app_id"] : null;
	    $this->APP_SECRET   = isset($config["app_secret"])   ? $config["app_secret"] : null;
	    $this->REDIRECT_URI = isset($config["redirect_uri"]) ? $config["redirect_uri"] : null;
	    $this->SCOPE        = isset($config["scope"])        ? $config["scope"] : "";


	    if(!$this->APP_ID){
	    	throw new MailruException("'app_id' not set", 1);
	    }

	    if(!$this->APP_SECRET){
	    	throw new MailruException("'app_secret' not set", 2);
	    }

	    if(!$this->REDIRECT_URI){
	    	throw new MailruException("'redirect_uri' not set", 3);
	    }

	    $this->AUTH_PATH = self::AUTH_DIALOG_PATH . "client_id=" . $this->APP_ID . 
	    				                             "&scope="    . $this->SCOPE .
	    				                             "&redirect_uri=". urlencode($this->REDIRECT_URI) . 
                                                     "&response_type=code"; 
    }

    public function getAuthPath()
    {
        return   $this->AUTH_PATH;
    }

    public function getUserId()
    {
        return   isset($this->userData["user_id"]) ? $this->userData["user_id"] : null ;
    }

    public function init($code)
    {
    	try
    	{
            $postFields = array( "code"          => $code, 
                                "redirect_uri"  => $this->REDIRECT_URI,
                                "client_id"     => $this->APP_ID, 
                                "client_secret" => $this->APP_SECRET,
                                "grant_type"    => "authorization_code");  

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::TOKEN_URI);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields, null, '&'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           
            $s = curl_exec($ch);
            curl_close($ch);
            $authData = json_decode($s, true);

            $accessToken    = isset($authData["access_token"]) ? $authData["access_token"] : null;
            $expiresSeconds = isset($authData["expires_in"])   ? intval($authData["expires_in"])   : 0; 

            if(!$accessToken)
            {   
                throw new MailruException("Cant get access token", 4);
            }
            else
            {
                $this->setAccessToken($accessToken, $expiresSeconds);
                $this->userData["user_id"] = isset($authData["x_mailru_vid"])   ? $authData["x_mailru_vid"]   : null; 
                $this->setRefreshToken($authData["refresh_token"]);
            }

            return  $authData;
    	}
    	catch(MailruException $ve)
    	{
    		$this->ERROR = $ve;
    	}
    	catch(Exception $e)
    	{
    		$this->ERROR = $e;
    	}
    }

    private function sign_server_server(array $request_params, $secret_key) {
          ksort($request_params);
          $params = '';
          foreach ($request_params as $key => $value) {
            $params .= "$key=$value";
          }
             // return $params . $secret_key;
          return md5($params . $secret_key);
    }

    public function api($method, $params = array())
    {
        if($method)
        {
            $accessToken = $this->getAccessToken();

            $params["method"]       = $method;
            $params["access_token"] = $accessToken;
            $params["session_key"]  =  $accessToken;
            $params["app_id"]       =  $this->APP_ID;
            $params["secure"]       = 1;

            $sig = $this->sign_server_server($params, $this->APP_SECRET);
            $paramsQueryString = http_build_query($params, null, '&');
           
            $apiMethodPath =  self::API_URI . '?' .  $paramsQueryString  . "&sig=" .$sig;

            $ch = curl_init($apiMethodPath);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $s = curl_exec($ch);
            curl_close($ch);
            $responseData = json_decode($s, true);
            return $responseData;
        }
        else
        {
            throw new MailruException("Method not provided", 5);
        }
    }

    private function setAccessToken($accessToken, $expiresSeconds = 870)
    {
    	$_SESSION["mailru_access_token"] = $accessToken;
    	//access token is invalid after $expiresSeconds seconds
    	$_SESSION["mailru_access_token_expire"] = time() +  intval($expiresSeconds);

    	$this->ACCESS_TOKEN = $accessToken;
    }

    public function getAccessToken()
    {
    	$expireTime = intval($_SESSION["mailru_access_token_expire"]);
    	if($expireTime < time())
    	{
    		$this->refreshAccessToken();
    	}

    	return $this->ACCESS_TOKEN;
    }

    private function setRefreshToken($refreshToken)
    {
        $this->REFRESH_TOKEN = $refreshToken;
        $_SESSION["mailru_refresh_token"] = $refreshToken;
    }

    private function getRefreshToken()
    {
        return $this->REFRESH_TOKEN;
    }

    public function refreshAccessToken()
    {
        $refreshToken = $this->getRefreshToken();

        $ch = curl_init(self::TOKEN_URI);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "refresh_token=" . $refreshToken . 
                                             "&grant_type=refresh_token" . 
                                             "&client_id=" . $this->APP_ID . 
                                             "&client_secret=" . $this->APP_SECRET);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $s = curl_exec($ch);
        curl_close($ch);
        $responseData = json_decode($s, true);

        return $responseData ;
    }

    public function getError()
    {
    	return $this->ERROR;
    }

    public function transformUserData($mData)
    {
        $mData = $mData[0];

        $data = array();

        $data['platform_id']  = PLATFORM_GOOGLE; 
        $data['platform_uid'] = $mData["uid"]; 
        $data['first_name']   = $mData["first_name"];
        $data['last_name']    = $mData["last_name"];
        $data['image']        = $mData["pic_50"]; 
        $data['gender']       = $mData["sex"]; 
        if(isset($mData["email"]))
        {
            $data['email'] = $mData["email"];
        }
     
        $data['blocked']      = 0; 
        return $data;
    }
}