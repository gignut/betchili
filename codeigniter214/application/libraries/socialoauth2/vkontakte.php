<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VkontakteException extends Exception 
{

	private $data;
	public function __construct($message = '', $code = null, $data = null)
	{
		parent::__construct($message, $code);
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}
}

class Vkontakte
{

	const TOKEN_URI        = "https://oauth.vk.com/access_token?";
	const API_URI          = "https://api.vk.com/method/";
    const AUTH_DIALOG_PATH = "https://oauth.vk.com/authorize?";

	private $ERROR;
	private $APP_ID;
	private $APP_SECRET;
	private $REDIRECT_URI;
	private $SCOPE;

	private $AUTH_PATH;

    private $userData;

    private $ACCESS_TOKEN;
    private $REFRESH_TOKEN;

	public function __construct($config) 
	{
	    if (!session_id()) 
	    {
	      session_start();
	    }

        $this->userData = array();

	    $this->APP_ID       = isset($config["app_id"])       ? $config["app_id"] : null;
	    $this->APP_SECRET   = isset($config["app_secret"])   ? $config["app_secret"] : null;
	    $this->REDIRECT_URI = isset($config["redirect_uri"]) ? $config["redirect_uri"] : null;
	    $this->SCOPE        = isset($config["scope"])        ? $config["scope"] : "";


	    if(!$this->APP_ID){
	    	throw new VkontakteException("'app_id' not set", 1);
	    }

	    if(!$this->APP_SECRET){
	    	throw new VkontakteException("'app_secret' not set", 2);
	    }

	    if(!$this->REDIRECT_URI){
	    	throw new VkontakteException("'redirect_uri' not set", 3);
	    }

	    $this->AUTH_PATH = self::AUTH_DIALOG_PATH . "client_id=" . $this->APP_ID . 
	    				                             "&scope="    . $this->SCOPE .
	    				                             "&redirect_uri=". urlencode($this->REDIRECT_URI) . 
                                                     "&response_type=code"; 
    }

    public function getAuthPath()
    {
        return   $this->AUTH_PATH;
    }

    public function getUserId()
    {
        return   isset($this->userData["user_id"]) ? $this->userData["user_id"] : null ;
    }

    public function init($code)
    {
    	try
    	{
            $getFields = array("code"          => $code, 
                                "redirect_uri"  => $this->REDIRECT_URI,
                                "client_id"     => $this->APP_ID, 
                                "client_secret" => $this->APP_SECRET);  




            $accessTockenRequest = self::TOKEN_URI . http_build_query($getFields, null, '&');

            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $accessTockenRequest);      
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	        $s = curl_exec($ch);
            if($s === FALSE)
            {
               // return curl_error($ch);
            }
	        curl_close($ch);
	        $authData = json_decode($s, true);

	        $accessToken    = isset($authData["access_token"]) ? $authData["access_token"] : null;
            $expiresSeconds = isset($authData["expires_in"])   ? $authData["expires_in"]   : null; 

	        if(!$accessToken)
	        {	
	        	throw new VkontakteException("Cant get access token", 4);
	        }
	        else
	        {
               
	        	$this->setAccessToken($accessToken, $expiresSeconds);
                $this->userData["user_id"] = isset($authData["user_id"])   ? $authData["user_id"]   : null; 
	        }

	        return  $authData;
    	}
    	catch(VkontakteException $ve)
    	{
    		$this->ERROR = $ve;
    	}
    	catch(Exception $e)
    	{
    		$this->ERROR = $e;
    	}
    }

    public function api($method, $params = null)
    {
        if($method)
        {
            $paramsQueryString = http_build_query($params, null, '&');
            $accessToken = $this->getAccessToken();
           
            $apiMethodPath =  self::API_URI . $method . '?' .  $paramsQueryString . '&access_token=' . $accessToken;
            $ch = curl_init($apiMethodPath);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $s = curl_exec($ch);
            curl_close($ch);
            $responseData = json_decode($s, true);
            return $responseData;
        }
        else
        {
            throw new VkontakteException("Method not provided", 5);
        }
    }

    private function setAccessToken($accessToken, $expiresSeconds = 870)
    {
    	$_SESSION["vkontakte_access_token"] = $accessToken;
    	//access token is invalid after $expiresSeconds seconds
    	$_SESSION["vkontakte_access_token_expire"] = time() +  intval($expiresSeconds);

    	$this->ACCESS_TOKEN = $accessToken;
    }

    public function getAccessToken()
    {
    	$expireTime = intval($_SESSION["vkontakte_access_token_expire"]);
    	if($expireTime < time())
    	{
    		$this->refreshAccessToken();
    	}

    	return $this->ACCESS_TOKEN;
    }

    public function refreshAccessToken()
    {
        $this->ACCESS_TOKEN = null;
    }

    public function getError()
    {
    	return $this->ERROR;
    }

    public function transformUserData($vkData)
    {
        $vkData = $vkData["response"][0];
        $data = array();

        $data['platform_id']  = PLATFORM_VKONTAKTE; 
        $data['platform_uid'] = $vkData["uid"]; 
        $data['first_name']   = $vkData["first_name"];
        $data['last_name']    = $vkData["last_name"];
        $data['image']        = $vkData["photo_50"]; 
        $data['gender']       = $vkData["sex"]; 
        if(isset($vkData["email"]))
        {
            $data['email'] = $vkData["email"];
        }
        $data['blocked']      = 0; 
        return $data;
    }
}