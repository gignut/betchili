<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OdnoklassnikiException extends Exception 
{

	private $data;
	public function __construct($message = '', $code = null, $data = null)
	{
		parent::__construct($message, $code);
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}
}


class Odnoklassniki
{

	const TOKEN_URI = "http://api.odnoklassniki.ru/oauth/token.do";
	const API_URI   = "http://api.odnoklassniki.ru/fb.do";


	private $ERROR;
	private $APP_ID;
	private $APP_SECRET;
	private $APP_PUBLIC_KEY;
	private $REDIRECT_URI;
	private $SCOPE;

	private $AUTH_PATH;

    private $ACCESS_TOKEN;
    private $REFRESH_TOKEN;

	public function __construct($config) 
	{
	    if (!session_id()) 
	    {
	      session_start();
	    }

	    $this->APP_ID          = isset($config["app_id"]) ? $config["app_id"] : null;
	    $this->APP_SECRET      = isset($config["app_secret"]) ? $config["app_secret"] : null;
	    $this->APP_PUBLIC_KEY  = isset($config["app_secret"]) ? $config["app_public_key"] : null;
	    $this->REDIRECT_URI    = isset($config["redirect_uri"]) ? $config["redirect_uri"] : null;
	    $this->SCOPE           = isset($config["scope"]) ? $config["scope"] : "VALUABLE_ACCESS;GET_EMAIL";

	    if(!$this->APP_ID){
	    	throw new OdnoklassnikiException("'app_id' not set", 1);
	    }

	    if(!$this->APP_SECRET){
	    	throw new OdnoklassnikiException("'app_secret' not set", 2);
	    }

	    if(!$this->APP_PUBLIC_KEY){
	    	throw new OdnoklassnikiException("'app_public_key' not set", 2);
	    }

	    if(!$this->REDIRECT_URI){
	    	throw new OdnoklassnikiException("'redirect_uri' not set", 3);
	    }

	    $this->AUTH_PATH = "http://www.odnoklassniki.ru/oauth/authorize?" . 
	    				      "client_id=" . $this->APP_ID . 
	    				      "&scope="    . $this->SCOPE .
	    				      "&response_type=code" . 
	    				      "&redirect_uri=". $this->REDIRECT_URI;
    }

    public function getAuthPath()
    {
        return $this->AUTH_PATH;
    }

    public function init($code)
    {
    	try
    	{
    		$postFields = array("code"          => $code, 
	         					"redirect_uri"  => $this->REDIRECT_URI,
	         				    "grant_type"    => "authorization_code", 
	         				    "client_id"     => $this->APP_ID, 
	         					"client_secret" => $this->APP_SECRET);	

    		 $ch = curl_init();
    		 curl_setopt($ch, CURLOPT_URL, self::TOKEN_URI);
	         curl_setopt($ch, CURLOPT_POST, 1);
	         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields, null, '&'));
	         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	         // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	       
	        $s = curl_exec($ch);
	        curl_close($ch);
	        $authData = json_decode($s, true);

	        $accessToken = isset($authData["access_token"]) ? $authData["access_token"] : null;

	        if(!$accessToken)
	        {	
	        	throw new OdnoklassnikiException("Cant get access token", 4);
	        }
	        else
	        {
	        	$this->setAccessToken($accessToken);
	        	$this->setRefreshToken($authData["refresh_token"]);
	        }

	        return  $authData;
    	}
    	catch(OdnoklassnikiException $oe)
    	{
    		$this->ERROR = $oe;
    		
    	}
    	catch(Exception $e)
    	{
    		$this->ERROR = $e;
    	}

    }

    public function api($method, $params = null)
    {
    	switch ($method) {
    		case 'users.getCurrentUser':
    			break;
    		default:
    			return;
    	}

    	$accessToken = $this->getAccessToken();
    	
    	$ch = curl_init( self::API_URI . '?access_token=' . $accessToken . 
    									'&application_key=' .  $this->APP_PUBLIC_KEY . 
    									'&method=' . $method .
    									'&sig=' . md5('application_key=' .  $this->APP_PUBLIC_KEY . 'method=' . $method . 
    									 md5($accessToken .  $this->APP_SECRET)));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $s = curl_exec($ch);
        curl_close($ch);
        $responseData = json_decode($s, true);
        return $responseData;
    }

    private function setRefreshToken($refreshToken)
    {
    	$this->REFRESH_TOKEN = $refreshToken;
    	$_SESSION["odnoklassniki_refresh_token"] = $refreshToken;
    }

    private function getRefreshToken()
    {
    	return $this->REFRESH_TOKEN;
    }

    private function setAccessToken($accessToken)
    {
    	$_SESSION["odnoklassniki_access_token"] = $accessToken;
    	//access token is invalid after 30minutes
    	$_SESSION["odnoklassniki_access_token_expire"] = time() + 29*60;

    	$this->ACCESS_TOKEN = $accessToken;
    }

    public function getAccessToken()
    {
    	$expireTime = intval($_SESSION["odnoklassniki_access_token_expire"]);
    	if($expireTime < time())
    	{
    		$this->refreshAccessToken();
    	}

    	return $this->ACCESS_TOKEN;
    }

    public function refreshAccessToken()
    {
    	$refreshToken = $this->getRefreshToken();

    	$ch = curl_init(self::TOKEN_URI);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "refresh_token=" . $refreshToken . 
         									 "&grant_type=refresh_token" . 
         									 "&client_id=" . $this->APP_ID . 
         									 "&client_secret=" . $this->APP_SECRET);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $s = curl_exec($ch);
        curl_close($ch);
        $responseData = json_decode($s, true);

        return $responseData ;
    }

    public function getError()
    {
    	return $this->ERROR;
    }

    public function transformUserData($odnData)
    {
        $data = array();
        $data['platform_id']  = PLATFORM_ODNOKLASSNIKI; 
        $data['platform_uid'] = $odnData["uid"]; 
        $data['first_name']   = $odnData["first_name"];
        $data['last_name']    = $odnData["last_name"];
        $data['image']        = $odnData["pic_1"]; 
        $data['gender']       = $odnData["gender"]; 
        
        if(isset($odnData["email"]))
        {
            $data['email'] = $odnData["email"];
        }

        $data['blocked']      = 0; 
        return $data;
    }
}