<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chili_translations
{
	public static function load_translations($page, $language)
	{
		$ci =& get_instance();
		$ci->lang->load($page, $language);
		$data = array();
		foreach($ci->lang->language as $key => $val)
		{
			$data[$key] = $val;
		}
		return $data;
	}
}