<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class chili_api_controller extends MX_Controller {

	protected $lang_id;
	protected $postdata;
	protected $postKey;
	protected $postDataSource;
	protected $translationData;

	public function __construct($p = null)
	{
		parent::__construct();
		$this->postKey = "chili";
		//IMPORTANT, initialiazing post data src, as for batch action we need to imitate it
		$postDataSrc = isset($p) ? $p : $this->input->post(); 
		$this->initData($postDataSrc);
	}

	public function initData($p = null)
	{
		$this->postDataSource = $p; 
		$this->init_post_data();
		$this->init_language();	
		$this->init_translations();	
	}

	private function init_translations()
	{
		$langCode = chili_translate::getLangCode($this->lang_id);
		$this->translationData = chili_translations::load_translations('api/notifications', $langCode);
	}

	private function init_post_data()
	{
		try
		{
			$this->postdata = array();
			
			validator::key("chili", validator::oneOf(validator::object(), validator::arr()))
			->check($this->postDataSource);

			$this->postdata = element($this->postKey, $this->postDataSource, array());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chili_api_controller", "init_post_data" , 0 , "Chili Api call error: 'chili' key doesn't exist");
			exit();
		}
		catch(Exception $e)
		{
			$this->api_error("chili_api_controller", "init_post_data" , 0 , $e->getMessage());
			exit();
		}
	}

	private function init_language()
	{
		try 
		{
			validator::key('language_id', validator::numeric(), FALSE)->check($this->postdata);
			$langId = element("language_id", $this->postdata, null);
			
			if(!$langId)
			{
				validator::key('language', validator::alpha()->notEmpty()->noWhitespace()->length(2, 3, TRUE))->check($this->postdata);
				$langCode = $this->postdata["language"];
				$langId  = $this->chili_translate->getLangId($langCode);
			}

			$this->lang_id = $langId ? $langId : chili_translate::getLangId(DEFAULT_LANGUAGE);
		} 
		catch (InvalidArgumentException $e) 
		{
			$this->lang_id = chili_translate::getLangId(DEFAULT_LANGUAGE);
			//$this->api_error("chili_api_controller", "init_language" , $e, $e->getMainMessage());
		}
		catch(Exception $e)
		{
			$this->lang_id = chili_translate::getLangId(DEFAULT_LANGUAGE);
			$this->api_error("chili_api_controller", "init_language" , $e, $e->getMessage());
			exit();
		}
	}

	public function get_error_translation($error_code)
	{
		switch ($error_code)
		{
			case 104: return $this->translationData['TR_BET_ERROR_CODE1'];
			case 105: return $this->translationData['TR_BET_ERROR_CODE2'];
			case 106: return $this->translationData['TR_BET_ERROR_CODE3'];
			case 107: return $this->translationData['TR_BET_ERROR_CODE4'];
			case 108: return $this->translationData['TR_BET_ERROR_CODE5'];
			case 109: return $this->translationData['TR_BET_ERROR_CODE6'];
			case 110: return $this->translationData['TR_BET_ERROR_CODE7'];

			case 111: return $this->translationData['TR_CHALLENGE_ERROR_CODE1'];
			case 112: return $this->translationData['TR_CHALLENGE_ERROR_CODE2'];
			case 113: return $this->translationData['TR_CHALLENGE_ERROR_CODE3'];
			case 114: return $this->translationData['TR_CHALLENGE_ERROR_CODE4'];
			case 115: return $this->translationData['TR_CHALLENGE_ERROR_CODE5'];
			case 116: return $this->translationData['TR_CHALLENGE_ERROR_CODE6'];
			case 117: return $this->translationData['TR_CHALLENGE_ERROR_CODE7'];
			case 118: return $this->translationData['TR_CHALLENGE_ERROR_CODE8'];
			case 119: return $this->translationData['TR_CHALLENGE_ERROR_CODE9'];
			case 120: return $this->translationData['TR_CHALLENGE_ERROR_CODE10'];
			case 121: return $this->translationData['TR_CHALLENGE_ERROR_CODE11'];
			case 122: return $this->translationData['TR_CHALLENGE_ERROR_CODE12'];

			case 123: return $this->translationData['TR_GAME_ERROR_CODE1'];

			case 404: return $this->translationData['TR_BLOKED_USER_LOGIN'];

			default: return "Please try again.";
		}
	}

	protected function api_error($ctrl, $fnc, $publicText = null, $code = 1, $data = null, $log = FALSE)
	{
		if ($log)
		{
			$ctrlTxt = "\nController: " . $ctrl . "\n";
			$fncTxt = "Function: " .$fnc . "\n";
			$pText = $publicText ? "Code: " . $code . " \n PublicText: " . $publicText . "\n" :  "Code: " . $code . " \n PublicText: BetChili api_error \n";
			nut_log::log("error", $ctrlTxt . $fncTxt . $pText );
		}
		if($publicText)
		{
			$ret =  json_encode(array("exception" =>  $code , "message" => $publicText, "data" => $data));
			nut_log::log("error", $ret);
			echo $ret;
		}
		else
		{
			echo json_encode(array("exception" =>  $code , "message" => 'BetChili: General exception', "data" => $data));
		}
	}
}