<?php
class asset_model extends CI_Model {

	private static $ASSETS = array(	"countries" 	=> "media/assets/countries/",
									"participants"  => "media/assets/participants/",
									"leagues" 		=> "media/assets/leagues/",	
									"competitions"  =>  "media/assets/competitions/");

	private static $DEFAULT_ASSET_FOLDER = "media/assets/common/";
	function __construct()
	{
		parent::__construct();
	}

	public static function getAssetFolder($type)
	{
		$folder = self::$DEFAULT_ASSET_FOLDER;
		if(isset($type) )
		{
			$folder = isset(self::$ASSETS[$type]) ? self::$ASSETS[$type] : self::$DEFAULT_ASSET_FOLDER;
		}
		return $folder;
	}

	public static function removeAsset($path)
	{
		try
		{
			$status = false;
			if($path && file_exists($path))
			{
				unlink($path);	
				$status = true;
			}
			
			return $status;	
		}
		catch(Exception $e)
		{
			Firelog::log("asset_model::removeAsset");
			throw $e;
		}
	}

	// if $forceCopy = true, function will generate a new name (in case we have such file in $toPath) , copy , and return new name
	public static function copyAsset($copyFromPath, $toPath , $sourceFilename , $forceCopy = false)
	{
		try
		{
			$status = false;
			$destFileName = "";
			if($copyFromPath && $toPath && $sourceFilename)
			{
				$copyFileExists = file_exists($copyFromPath . $sourceFilename);
				//if to path file exists then we'll not overwrite the file
				
				$destFileName =  $sourceFilename;	
				if($copyFileExists)
				{
					if($forceCopy)
					{
						//generate new file name	
						$destFileName = "comp" . rand(0,1000) . "_" .  $sourceFilename;
					}	
					
					$toPathExists = file_exists($toPath .$destFileName );
					$status  = copy($copyFromPath . $sourceFilename , $toPath .$destFileName );
				}
			}	

			return  $forceCopy ? $destFileName : $status;
		}
		catch(Exception $e)
		{
			Firelog::log("asset_model::copyAsset");
			throw $e;
		}
	}
}
	