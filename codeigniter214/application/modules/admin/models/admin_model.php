<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_model extends CI_Model {

	private static $SPORT = "sport_type"; 
	private static $ADMIN_USER = "admin_user"; 

	public static function setSportType($sportId)
	{
		nut_session::set(self::$SPORT, $sportId);
	}

	public static function getSportType()
	{
		return nut_session::get(self::$SPORT);
	}

	public static function login($userId)
	{
		nut_session::set(self::$ADMIN_USER, $userId);	
	}

	public static function logout()
	{
		nut_session::set(self::$ADMIN_USER, null);
	}

	public static function getAdminId()
	{
		return nut_session::get(self::$ADMIN_USER);
	}

	public static function isRegistered($userid, $pass)
	{
		$isRegistered = false;
		if($userid == "user" && $pass == "pass")
		{
			$isRegistered =  true;
		}

		return $isRegistered;
	} 

	public static function isAutorized()
	{
		$userId = nut_session::get(self::$ADMIN_USER);
		return !is_null($userId);
	}

}