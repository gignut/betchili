<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chili_oauth extends MX_Controller {
	
	private $isAuthorized;
	private $data;
	
	public function __construct()
	{
		nut_session::init();
		parent::__construct();
	
		$this->load->model(MODULE_ADMIN_FOLDER . "/admin_model");

		$this->data = array();
		$this->data[COUNTRIES]    = COUNTRIES;
		$this->data[PARTICIPANTS] = PARTICIPANTS;
		$this->data[LEAGUES] 	  = LEAGUES;
		$this->data[COMPETITIONS] = COMPETITIONS;
		$this->data[GAMES] 		  = GAMES;
		$this->data[USERS] 		  = USERS;
		$this->data[NEWS]    	  = NEWS;
		$this->data[BETS]    	  = BETS;
		$this->isAuthorized = false;
	}

	//main function to check autorization and redirect to login or ...
	public function autorize()
    {
    	try
    	{
    		$userId = admin_model::isAutorized();
	        if(!$userId)
	        {
	            nut_utils::evalRedirect(BASE_INDEX . MODULE_ADMIN_FOLDER . "/main/signin");
	            exit();
	        }
	        else 
	        {
	            return  $userId;
	        }
    	}
    	catch(Exception $e)
    	{
    		//CHILI::TODO
    	}
    } 

    //main function to check autorization and redirect to login or ...
	public function php_autorize()
    {
    	try
    	{
    		$userId = admin_model::isAutorized();
	        if(!$userId)
	        {
	            nut_utils::locationRedirect(BASE_INDEX . MODULE_ADMIN_FOLDER . "/main/signin");
	            exit();
	        }
	        else 
	        {
	            return  $userId;
	        }
    	}
    	catch(Exception $e)
    	{
    		//CHILI::TODO
    	}
    } 

	// ajax
	public function signin()
	{
		try
		{
			if($this->input->post('user') == "user" && $this->input->post('pass') == "pass")
			{
			 	admin_model::login(1000);
			 	nut_utils::evalRedirect(BASE_INDEX . MODULE_ADMIN_FOLDER . "/main/index");
			}
		}
		catch(Exception $e)
		{
			//CHILI::TODO
		}
	}

	//ajax	
	public function logout()
	{
		try
		{
			admin_model::logout();
			nut_utils::scriptRedirect(BASE_INDEX . MODULE_ADMIN_FOLDER . "/main/signin");
		}
		catch(Exception $e)
		{
			//CHILI::TODO
		}
	}
}
