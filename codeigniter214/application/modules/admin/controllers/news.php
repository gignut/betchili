<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class News extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('news/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_lastest_news_by()
	{
		try
		{
			$data = nut_api::api('news/get_lastest_news_by', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function add()
	{
		try
		{
			$data = nut_api::api('news/add', $this->input->post());

			$object = array('chili' => array('id' => $data[0]));
			$newsData = nut_api::api('news/get_by_id', $object);

			echo json_encode($newsData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

}