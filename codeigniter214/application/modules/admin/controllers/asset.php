<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model(MODULE_API_FOLDER ."/countries_model");
		$this->load->model(MODULE_API_FOLDER ."/participants_model");
		$this->load->model(MODULE_API_FOLDER ."/leagues_model");
		$this->load->model(MODULE_API_FOLDER ."/competitions_model");
	}

	public function upload($type = null, $id = null)
	{
		try
		{
			$type = !is_null($type) ? $type : "common";
			$uploadPath = "";

			$status = false;
			if(!is_null($id))
			{
				if(!empty($_FILES)) {
					
					$file = $_FILES['file'];
					if ( $file['type'] == 'image/png' ) {
						
						$nameChunks = explode (" ", $file['name']);
						$sanitizedFileName = implode("_", $nameChunks);
						$filename = "ch_" . $id . "_" . $sanitizedFileName;
						$uploadPath = 	asset_model::getAssetFolder($type) .  $filename;
					
						if(!file_exists($uploadPath))
						{
							$copied = copy($file['tmp_name'], $uploadPath);
							
							if ($copied)
							{
								switch($type)
								{
									case "countries" :
										$this->countries_model->setFlag($id, $filename);
										break;
									case "participants" :
										$this->participants_model->setLogo($id, $filename);
										break;
									case "leagues" :
										$this->leagues_model->setLogo($id, $filename);
										break;	
									case "competitions" :
										$this->competitions_model->setLogo($id, $filename);
										break;											
								}
								
								$status = "Ok";
							}
						}
						else
						{
							nut_log::log("error", "File already exists:" . $uploadPath);
						}
					}
				}
			}
				
			echo $status;	
		}
		catch(Exception $e)
		{
			nut_log::log("error", "Asset:: File upload");		
		}
	}

	public function remove($type  = null, $id = null)
	{
		try 
		{
			nut_db::transactionStart("removeAsset");
			$imagePath = "";
			$imageFile = "";

			$status = false;
			if($id != null)
			{
				switch($type)
				{
					case "countries" :
						$imageFile =  $this->countries_model->getFlag($id);
						$this->countries_model->setFlag($id, "");
						$imagePath = asset_model::getAssetFolder($type) . $imageFile;
						break;
					case "participants" :
						$imageFile =  $this->participants_model->getLogo($id);
						$this->participants_model->setLogo($id, "");
						$imagePath = asset_model::getAssetFolder($type) . $imageFile;
						break;
					case "leagues" :
						$imageFile =  $this->leagues_model->getLogo($id);
						$this->leagues_model->setLogo($id, "");
						$imagePath = asset_model::getAssetFolder($type) . $imageFile;
						break;
					case "competitions" :
						$imageFile =  $this->competitions_model->getLogo($id);
						$this->competitions_model->setLogo($id, "");
						$imagePath = asset_model::getAssetFolder($type) . $imageFile;
						break;							
				}
				
				if($imageFile)
				{
					asset_model::removeAsset($imagePath);	
				}	
				
				nut_db::transactionCommit("removeAsset");
				echo "Ok";
			}
		} 
		catch (Exception $e) 
		{
			nut_db::transactionRollback("removeAsset");
			nut_log::log("error", "Asset:: File remove");	
		}	
	}
}

