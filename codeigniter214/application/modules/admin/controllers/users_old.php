<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('users/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_filtered_by()
	{
		try
		{
			$data = nut_api::api('users/get_filtered_by', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}	
	}

	public function add()
	{
		try
		{
			$post = $this->input->post();
			$data = nut_api::api('users/add', $post);
			$aux_array = array();

			$object = array('chili' => array('id' => $data[0], 'language' => 'en')); // TODO add site_id to into get_by_id
			$gameData = nut_api::api('users/get_by_id', $object);

			echo json_encode($gameData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function update()
	{
		try
		{
			$post = $this->input->post();
			nut_api::api('users/update', $post);
			$data = $this->input->post('chili');
			$id   = $data[0]['id'];
			$site_id   = $data[0]['site_id'];

			$postData = array('chili' => array('id' => $id, 'site_id' => $site_id, 'language' => 'en'));

			$data = nut_api::api('users/get_by_id', $postData);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function remove()
	{
		try
		{
			nut_api::api('users/remove', $this->input->post());
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
}

