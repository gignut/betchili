<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Sports extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function setSportType()
	{
		try
		{
			$sportId = $this->input->post("sport_id");
			validator::numeric()->check($sportId);	
			$this->load->model(MODULE_ADMIN_FOLDER . "/admin_model");
			$this->admin_model->setSportType($sportId);

			echo $sportId;
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
}