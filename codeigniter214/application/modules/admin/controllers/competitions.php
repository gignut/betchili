<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Competitions extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_all()
	{
		try
		{
			$data = nut_api::api('competitions/get_all', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('competitions/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_by_league()
	{
		try
		{
			$data = nut_api::api('competitions/get_by_league', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function update()
	{
		try
		{
			$data = $this->input->post();
			nut_api::api('competitions/update',  $data);

			$auxData = array("translations", "participants");
			$id = $data[nut_api::$PARAM][0]['id']; 
			$params=  nut_api::wrapData(array("id" => $id, "language" => "en", "aux_info" => $auxData));
			$data = nut_api::api('competitions/get_by_id', $params);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function update_participants()
	{
		try
		{
			$data = $this->input->post();
			nut_api::api('competitions/update_participants',  $data);

			$auxData = array("translations", "participants");
			$id = $data[nut_api::$PARAM]['id']; 
			$params=  nut_api::wrapData(array("id" => $id, "language" => "en", "aux_info" => $auxData));
			$data = nut_api::api('competitions/get_by_id', $params);
			echo json_encode($data);	
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


	public function add()
	{
		try
		{
			$idsArray = nut_api::api('competitions/add',  $this->input->post());
			$id = $idsArray[0];
			$postdData = array("id" => $id);
			$params=  nut_api::wrapData($postdData);
			$data  = nut_api::api('competitions/get_by_id', $params);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function remove()
	{
		try
		{
			nut_api::api('competitions/remove',  $this->input->post());
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
}

