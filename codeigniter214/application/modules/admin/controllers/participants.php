<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Participants extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('participants/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_by_country()
	{
		try
		{
			$data = nut_api::api('participants/get_by_country', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}		
	}

	// TODO::NUT WE HAVE THE SAME AS AUX IN COMPETITIONS CONTROLLER
	public function get_by_competition()
	{
		try
		{
			$data = nut_api::api('participants/get_by_competition', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}	
	}

	public function get_by_game()
	{
		try
		{
			$data = nut_api::api('participants/get_by_game', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


	public function add()
	{
		try
		{
			$idsArray = nut_api::api('participants/add', $this->input->post());
			$postdData = array("ids" => $idsArray);
			$params =  nut_api::wrapData($postdData);
			$participantsData = nut_api::api('participants/get_by_ids', $params);

			echo json_encode($participantsData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


	public function update()
	{
		try
		{
			$data = $this->input->post();
			nut_api::api('participants/update', $data);

			$id = $data[nut_api::$PARAM][0]['id']; 
			$params=  nut_api::wrapData(array("id" => $id, "language" => "en", "aux_info" => "translations"));
			$data = nut_api::api('participants/get_by_id', $params);
			echo json_encode($data);			
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	//remove participant from DB and remove participant assets
	public function remove()
	{
		try
		{
			nut_api::api('participants/remove', $this->input->post());
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}	
}

