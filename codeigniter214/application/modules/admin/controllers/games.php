<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Games extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('games/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function set_only_game_result()
	{
		try
		{
			nut_api::api('games/set_only_game_result', $this->input->post());
			$post = $this->input->post('chili');
			$id = $post['game_id'];
			$object = array('chili' => array('id' => $id, 'language' => 'en', 'aux_info' => array('participants')));
			$data = nut_api::api('games/get_by_id', $object);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}	

	public function set_game_result()
	{
		try
		{
			nut_api::api('games/set_game_result', $this->input->post());
			$post = $this->input->post('chili');
			$id = $post['game_id'];
			$bookmaker_id = $post['bookmaker_id'];
			$object = array('chili' => array('id' => $id, 'language' => 'en', 'aux_info' => array('events', 'participants'), 'bookmaker_id' => $bookmaker_id));
			$data = nut_api::api('games/get_by_id', $object);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}	

	public function return_game_result()
	{
		try
		{
			$x = nut_api::api('games/return_game_result', $this->input->post());
			$post = $this->input->post('chili');
			$id = $post['game_id'];
			$bookmaker_id = $post['bookmaker_id'];
			$object = array('chili' => array('id' => $id, 'language' => 'en', 'aux_info' => array('events', 'participants'), 'bookmaker_id' => $bookmaker_id));
			$data = nut_api::api('games/get_by_id', $object);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}	

	public function get_filtered_by()
	{
		try
		{
			if (array_key_exists('count', $this->input->post('chili'))) {
				 $apiData  = array();
				 $vars = $this->input->post('chili');
				 $vars['state'] = 1; // blocked 
				 $apiData["blockedCount"] = array("url"  => "games/get_filtered_by", "vars"  => $vars);
				 $vars['result'] = 0; // unset
				 unset($vars['state']);
				 $apiData["unsetCount"] = array("url"  => "games/get_filtered_by", "vars"  => $vars);
				 $vars['to_date'] = date("Y-m-d H:m:s");  // finished games 2014-03-26 21:49:51
				 $vars['result'] = 0;    // unset
				 $apiData["finished"] = array("url"  => "games/get_filtered_by", "vars"  => $vars);

				 $data = nut_api::api('batch/action', array( nut_api::$PARAM => $apiData));
			} else {
				$data = nut_api::api('games/get_filtered_by', $this->input->post());
			}
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}	
	}

	public function add()
	{
		try
		{
			$post = $this->input->post();
			$data = nut_api::api('games/add', $post);
			$aux_array = array();

			$object = array('chili' => array('id' => $data[0], 'language' => 'en', 'aux_info' => array('participants', 'events')));
			$gameData = nut_api::api('games/get_by_id', $object);

			echo json_encode($gameData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function update()
	{
		try
		{
			$post = $this->input->post();
			nut_api::api('games/update', $post);
			$data = $this->input->post('chili');
			$id   = $data[0]['id'];

			$aux_array = array('events');
			if(array_key_exists('aux_info', $data[0])) {
				if (array_key_exists('participants', $data[0]['aux_info'])) {
					$aux_array[] = 'participants';
				} 
				if (array_key_exists('translations', $data[0]['aux_info'])) {
					$aux_array[] = 'translations';
				}
			}

			$postData = array('chili' => array('id' => $id, 'language' => 'en'));
			if (array_key_exists('bookmaker_id', $data[0]))
			{
				$postData['chili']['bookmaker_id'] = $data[0]['bookmaker_id'];
			}
			
			if(count($aux_array) != 0) {
				$postData['chili']['aux_info'] = $aux_array;
			}

			$data = nut_api::api('games/get_by_id', $postData);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function remove()
	{
		try
		{
			nut_api::api('games/remove', $this->input->post());
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
}

