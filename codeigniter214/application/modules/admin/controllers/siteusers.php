<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siteusers extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function update()
	{
		try
		{
			nut_api::api('siteusers/update', $this->input->post());
			$data = $this->input->post('chili');
			$id   = $data['id'];
			
			$postData = array('chili' => array('id' => $id, 'language' => 'en'));

			$data = nut_api::api('siteusers/get_by_id', $postData);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_widgets() {
		try
		{
			$data = nut_api::api('siteusers/get_widgets', $this->input->post());

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}		
	}

}