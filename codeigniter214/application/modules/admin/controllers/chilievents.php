<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChiliEvents extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('chilievents/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_all()
	{
		try
		{
			$data = nut_api::api('chilievents/get_all', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function add()
	{
		try
		{
			$post = $this->input->post();
			$data = nut_api::api('chilievents/add', $post);
			$postData = $this->input->post('chili');
			$object = array('chili' => array('game_id' => $postData['game_id'], 'bookmaker_id' => $postData['bookmaker_id']));
			$eventsData = nut_api::api('chilievents/get_all', $object);

			echo json_encode($eventsData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function update()
	{
		try
		{
			$post = $this->input->post();
			nut_api::api('chilievents/update', $post);
			$data = $this->input->post('chili');
			$id   = $data[0]['id'];

			$data = nut_api::api('chilievents/get_by_id', array('chili' => array('id' => $id)));

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

}

