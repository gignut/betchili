<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {
	
	private $data;
	private $currentSportId;
	
	public function __construct()
	{
		nut_session::init();
		parent::__construct();

		$this->data = array();
		$this->data[COUNTRIES]    = COUNTRIES;
		$this->data[PARTICIPANTS] = PARTICIPANTS;
		$this->data[LEAGUES] 	  = LEAGUES;
		$this->data[COMPETITIONS] = COMPETITIONS;
		$this->data[GAMES] 		  = GAMES;
		$this->data[USERS]	 	  = USERS;
		$this->data[NEWS] 	  	  = NEWS;
		$this->data[BETS] 	  	  = BETS;
		$this->data[MODULE_ADMIN_FOLDER] = MODULE_ADMIN_FOLDER;

		$this->load->model(MODULE_ADMIN_FOLDER . "/admin_model");

		$this->currentSportId  = admin_model::getSportType();

		if(is_null($this->currentSportId))
		{
			 $this->currentSportId = SPORT_SOCCER_ID;
			 admin_model::setSportType($this->currentSportId);
		}

		$this->data['currentSportId'] = $this->currentSportId;		

		$apiData = nut_api::wrapData(array("language" => "en"));
		$this->sportData = nut_api::api('sports/get_all', $apiData); 
		$this->data['sportData'] = json_encode($this->sportData);	
	}
	
	public function index()
	{
		try
		{
			$this->data['isAutorized'] = false; 	
			$this->data['CONTENT'] = $this->load->view(MODULE_ADMIN_FOLDER . "/index.php", '', true);
			$this->load->view(MODULE_ADMIN_FOLDER . "/main_view.php", $this->data);
		}
		catch(Exception $e)
		{
			//TODO::CHILI
		}
	}
	
	public function show($page = null, $sub1 = null, $sub2 = null)
	{
		$pageData = array();
		$menuItem = 0;

		$this->data['isAutorized'] = Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/php_autorize');
		
		switch($page)
		{
			case COUNTRIES:
				$apiData = nut_api::wrapData(array("language" => "en"));
				$countryData = nut_api::api('countries/get_all', $apiData);
				$pageData['countryData'] = json_encode($countryData);	

				$menuItem = 1;
				break;

			case PARTICIPANTS:
				$apiData = nut_api::wrapData(array("language" => "en"));
				$countryData = nut_api::api('countries/get_all', $apiData);
				$pageData['countryData'] = json_encode($countryData);
				$menuItem = 2;
				break;

			case LEAGUES:
				$apiData = nut_api::wrapData(array("language" => "en"));
				$countryData = nut_api::api('countries/get_all', $apiData);
				$pageData['countryData'] = json_encode($countryData);
				$apiData = nut_api::wrapData(array('sport_id' => $this->currentSportId, 'language' => 'en'));
				$leagueData = nut_api::api('leagues/get_by_sport', $apiData);
				$pageData['leagueData'] = json_encode($leagueData);
				$menuItem = 3;
				break;

			case COMPETITIONS:
				$apiData = nut_api::wrapData(array("language" => "en"));
				$countryData = nut_api::api('countries/get_all', $apiData );
				$pageData['countryData'] = json_encode($countryData);	
				$apiData = nut_api::wrapData(array('sport_id' => $this->currentSportId, 'language' => 'en', 'aux_info' => 'translations'));
				$leagueData = nut_api::api('leagues/get_by_sport', $apiData);
				$pageData['leagueData'] = json_encode($leagueData);
				$menuItem = 4;
				break;

			case GAMES:
				$competitionVars = array('active' => 1, 'sport_id' => $this->currentSportId, 'language' => 'en');
				$bookmakerVars   = array("language" => "en");
				$eventVars       = array('sport_id' => $this->currentSportId, "a" => "aaaa");
				$apiData = array();
				$apiData["competitions"] 	= array("url" => "competitions/get_by_sport",           "vars"  => $competitionVars);
				$apiData["bookmakers"]   	= array("url" => "chilievents/get_all_bookmakers",      "vars"  => $bookmakerVars);
				$apiData["event_groups"] 	= array("url" => "chilievents/get_all_event_groups",    "vars"  => $eventVars);
				$apiData["event_templates"] = array("url" => "chilievents/get_all_event_templates", "vars"  => $eventVars);

				$batchData = nut_api::api('batch/action', array("chili" => $apiData));
				$competitionData = $batchData["competitions"];
				$bookmakerData   = $batchData["bookmakers"];
				$eventGroups     = $batchData["event_groups"];
				$eventTemplates  = $batchData["event_templates"];

				$pageData['competitionData'] = json_encode($competitionData);
				$pageData['bookmakerData']   = json_encode($bookmakerData);
				$pageData['eventGroups']     = json_encode($eventGroups);
				$pageData['eventTemplates']  = json_encode($eventTemplates);

				$pageData['showGame'] = $sub1;
			
				$menuItem = 5;
				break;

			case USERS:
				// $siteUsersVars = array("language" => "en", "aux_info" => ["widgetsCount", "usersCount", "betsCount", "challengesCount"]);
				// $eventVars     = array('sport_id' => $this->currentSportId);
				// $apiData = array();
				// $apiData["event_templates"] = array("url" => "chilievents/get_all_event_templates", "vars"  => $eventVars);
				// $apiData["siteUsersData"]   = array("url" => "siteusers/get_all", "vars" => $siteUsersVars);

				// $batchData = nut_api::api('batch/action', array("chili" => $apiData));
				// $siteUsersData = $batchData["siteUsersData"];
				// $eventTemplates = $batchData["event_templates"];

				// $pageData['siteUsersData']  = json_encode($siteUsersData);
				// $pageData['eventTemplates']  = json_encode($eventTemplates);

				$apiData = nut_api::wrapData(array("language" => "en", "aux_info" => array("widgetsCount", "usersCount", "betsCount", "challengesCount")));
				$siteUsersData = nut_api::api('siteusers/get_all', $apiData);
				$pageData["siteUsersData"] = json_encode($siteUsersData);

				$apiData = nut_api::wrapData(array('sport_id' => $this->currentSportId));
				$event_templates = nut_api::api('chilievents/get_all_event_templates', $apiData);
				$pageData["eventTemplates"] = json_encode($event_templates);

				$menuItem = 6;
				break;	

			case NEWS:
				$apiData = nut_api::wrapData(array("language" => "en", 'limit' => 15));
				$news = nut_api::api('news/get_lastest_news_by', $apiData);
				$pageData['newsData'] = json_encode($news);
				$pageData['sub1'] = $sub1;
				$pageData['sub2'] = $sub2;
				$menuItem = 7;
				break;	

			case 'test':
				$pageData = null;
				break;

			default: 
			$this->index(); 
		}

		$this->data['menuItem'] = $menuItem; 	
		$this->data['CONTENT'] = $this->load->view(MODULE_ADMIN_FOLDER . "/" . $page . ".php", $pageData , true);
		$this->load->view(MODULE_ADMIN_FOLDER . "/main_view.php", $this->data);
	}

	public function signin()
	{
		try
		{
			$this->data['CONTENT'] = $this->load->view(MODULE_ADMIN_FOLDER . "/admin_login_form.php", "" , true);
			$this->load->view(MODULE_ADMIN_FOLDER . "/main_view.php", $this->data);
		}
		catch(Exception $e)
		{
			//CHILI::TODO
		}
	}
	
}

