<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bets extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('bets/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_all_by_game()
	{
		try
		{
			$data = nut_api::api('bets/get_all_by_game', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_all_by_user()
	{
		try
		{
			$data = nut_api::api('bets/get_all_by_user', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


	public function get_all_by_site_and_widget()
	{
		try
		{
			$data = nut_api::api('bets/get_all_by_site_and_widget', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function add()
	{
		try
		{
			$idsArray = nut_api::api('bets/add',  $this->input->post());
			$id = $idsArray[0];
			$postdData = array("id" => $id);
			$params=  nut_api::wrapData($postdData);
			$data  = nut_api::api('bets/get_by_id', $params);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

}

