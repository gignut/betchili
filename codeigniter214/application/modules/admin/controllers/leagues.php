<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leagues extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_all()
	{
		try
		{
			$data = nut_api::api('leagues/get_all',  $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
	
	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('leagues/get_by_id',  $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
	
	public function get_by_sport()
	{
		try
		{
			$data = nut_api::api('leagues/get_by_sport',  $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


	public function add()
	{
		try
		{
			$idsArray = nut_api::api('leagues/add',  $this->input->post());
			$postdData = array("ids" => $idsArray);
			$params=  nut_api::wrapData($postdData);
			$data  = nut_api::api('leagues/get_by_ids', $params);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function update()
	{
		try
		{
			$data = $this->input->post();
			nut_api::api('leagues/update',  $this->input->post());

			$id = $data[nut_api::$PARAM][0]['id']; 
			$params=  nut_api::wrapData(array("id" => $id, "language" => "en", "aux_info" => "translations"));
			$data = nut_api::api('leagues/get_by_id', $params);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	//remove the league from DB and remove league assets
	public function remove()
	{
		try
		{
			nut_api::api('leagues/remove',  $this->input->post());
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}		

}

