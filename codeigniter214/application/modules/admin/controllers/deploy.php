<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deploy extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function execute() {
		$deploy_pass = $this->input->post('deploy');
		if($deploy_pass == md5('automated deploy')){
			$this->backup();
			$this->update();
		}
		else {
			echo 'fail';
		}
	}

	public function backup()
	{
		$time = time();
		chdir('../');
		$commands = array(
					'rm -rf ./backup*',
					'mkdir /tmp/backup',
					'cp -rf ./* /tmp/backup',
					'rm -rf /tmp/backup/.git',
					'rm -rf /tmp/backup/www/media/nutron/.git',
					'zip -9 -r ./backup_'.$time.'.zip /tmp/backup',
					'rm -rf /tmp/backup*',
				);

		foreach($commands AS $command){
			// Run it
			shell_exec($command);
		}
		chdir('www');
	}

	public function update()
	{
		// Run the commands for output
		$output = '';
		$result = array();
		$commands = array(
					'echo $PWD',
					'whoami',
				);

		foreach($commands AS $command){
			// Run it
			$tmp = shell_exec($command);
			// Output
			$result[] = array("command" => $command, "result" => htmlentities(trim($tmp)));
		}
		chdir('../');
		$commands = array(
					'git pull 2>&1',
					'git status -s 2>&1',
					'git submodule sync 2>&1',
					'git submodule update 2>&1',
					'git submodule status 2>&1',
				);

		foreach($commands AS $command){
			// Run it
			$tmp = shell_exec($command);
			// Output
			$result[] = array("command" => $command, "result" => htmlentities(trim($tmp)));
		}
		chdir('./www');
		echo json_encode($result);
	}

}
