<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		
		Modules::run(MODULE_ADMIN_FOLDER . '/chili_oauth/autorize');
	}

	public function get_by_id()
	{
		try
		{
			$data = nut_api::api('users/get_by_id', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function remove()
	{
		try
		{
			nut_api::api('users/remove', $this->input->post());
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function get_filtered_by()
	{
		try
		{
			$data = nut_api::api('users/get_filtered_by', $this->input->post());
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}	
	}

	public function update()
	{
		try
		{
			nut_api::api('users/update_user_profile', $this->input->post());
			$data = $this->input->post('chili');
			$id   = $data['id'];

			$postData = array('chili' => array('id' => $id, 'language' => 'en'));

			$data = nut_api::api('users/get_profile_data_by_id', $postData);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}


}


