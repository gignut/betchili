
<?php 
	css("modules/m_admin/css/games.css"); 
 	script("modules/m_admin/js/ng_games.js");
 ?>

<script>
	var competitionData = <?php echo $competitionData;?>;
	var eventTemplates = <?php echo $eventTemplates;?>;
	var eventGroups = <?php echo $eventGroups;?>;
	var bookmakerData = <?php echo $bookmakerData;?>;
	var showGame = '<?php echo $showGame;?>';
</script>

<script id="removePopover" type="text/ng-template">
	<div>
	    <h5>Are you shure</h5>
	    <button class="btn btn-primary" type = "button" ng-click = "removeGame(); hide()">Yes</button>
		<button class="btn" type = "button" ng-click = "hide()">No</button>
	</div>
</script>

<div ng-controller = "GameCtrl" id="gamesView">
	<div ng-init="init()"></div>

	<div ng-show="!filtermodel.competitionList">There are no any competition !</div>
	<div class="row-fluid" ng-show="filtermodel.competitionList">
		<div class="span12 well" style="height: 130px">
			<div style="float:left; height:90px; border-right:1px gray solid; margin-right:10px; padding-right:10px">
				<div style="margin-top:15px;width:80px">
					<label class="label">Game ID</label>
					<input type="text" class="input-mini" ng-model="filtermodel.game_id" nut-enter="searchById()"></input>
				</div>
			</div>
			<div class="input-append" style="float:left; width:250px">
				<h5> Select competitions</h5>
				<select name="competitions" multiple nutchosen="filtermodel.competitionList" data-placeholder="All from {{currentSport.name}}" ngwidth="95%" 
						ng-model="filtermodel.selectedCompetitions" ng-change="filtermodel.competitionsChanged()"
						ng-options="comp.id as comp.name for comp in filtermodel.competitionList">
				</select>
				<span class="add-on"><i class="icon-search"></i> </span>
				<div  style="clear:both; width:700px">
					<div style="float:left;"><h5 ><a href="#">Found: {{gamesCount}}</a></h5></div>
					<div style="float:left;"><h5 style="float:left; padding-left:50px"><a href="#" ng-click="showGames('unset')">Unresult games: {{countData.unsetCount.count}}</a></h5></div>
					<div style="float:left;"><h5 style="float:left; padding-left:50px"><a href="#" ng-click="showGames('blocked')">Blocked games: {{countData.blockedCount.count}}</a></h5></div>
					<div style="float:left;"><h5 style="float:left; padding-left:50px"><a style="color:red" href="#" ng-click="showGames('finished')">Finished: {{countData.finished.count}}</a></h5></div>
				</div>
			</div>
			<div style="float:left; padding-left:50px;">
				<div style="float:left; margin-right:10px;">
					<h5>From Date</h5>
					<div class="span12">
						<nutdatetimepicker  nutdate  = "filtermodel.fromdate"
											mindate  = "minDate"
											nutdatechange = "updateFilterFromDate">
						</nutdatetimepicker>
					</div>
				</div>

				<div  style="float:left;">
					<h5>To Date</h5>
					<div class="span12">
						<nutdatetimepicker  nutdate  = "filtermodel.todate"
											mindate  = "minDate"
											nutdatechange = "updateFilterToDate">
						</nutdatetimepicker>
					</div>
				</div>
			</div>
			<div style="float:left; position:relative; width:300px">
				<div style="float:left; position:absolute; left:10px">
					<h5>Game state</h5>
					<div class="gameSwitch">
						<input type="radio" name="php" id="state-{{item}}" ng-checked="item == filtermodel.state" ng-repeat="item in filtermodel.states" />
						<label for="state-{{item}}" ng-click="filtermodel.filterState(item)" ng-class="{checked : item == filtermodel.state}" ng-repeat="item in filtermodel.states">{{item}}</label>
					</div>
				</div>
				<div style="float:left; position:absolute; left:150px">
					<h5>Game result</h5>
					<div class="gameSwitch">
						<input type="radio" name="php" id="status-{{item}}" ng-checked="item == filtermodel.status" ng-repeat="item in filtermodel.statuses" />
						<label for="status-{{item}}" ng-click="filtermodel.filterStatus(item)" ng-class="{checked : item == filtermodel.status}" ng-repeat="item in filtermodel.statuses">{{item}}</label>
					</div>
				</div>
			</div>
			
			<div style="float:left; position:relative; left:300px" ng-show="page==1">
					<h5>Bookmaker</h5>
					<select ngwidth="100%" data-placeholder="Choose bookmaker" ng-model="filtermodel.selectedBookmakerId" nutchosen="filtermodel.bookmakersList"
							ng-options="b.id as b.name for b in filtermodel.bookmakersList" nutchosenselected="filtermodel.selectedBookmakerId" 
							ng-change="filtermodel.changeBookmaker()">
				    </select>
			</div>
		</div>

	<div class="row-fluid">
		<!-- GAMES LIST START -->
		<div class="span9 well">
			<div class="row-fluid" style="position:relative">
				<div style="position:relative; float:left" class="span8">
					<div class="input-append" style="float:left">
						<input type="text" class="span12" ng-model="gametxt" placeholder="Type game name">
						<span class="add-on"><i class="icon-search"></i></span>
					</div>
					<div style="position:absolute; right:0px; float:left">
						<button ng-click="page=1" class="btn btn-small" ng-class="{'btn-info': page==1, 'btn-white': page==2}" >Events</button>
						<button ng-click="page=2" class="btn btn-small" ng-class="{'btn-info': page==2, 'btn-white': page==1}" >Results</button>
					</div>
				</div>
				<div style="position:absolute; right:1px; float:left">
					<button class="btn btn-success" type="button" ng-click="createNewGame()">New Game</button>
				</div>
			</div>

			<div class="row-fluid">
				<table class="table table-hover table-striped table-bordered gameTableEvent" ng-hide="gamesList.length == 0" >
					<thead>
						<th style="width:40px">ID</th>
						<th style="width:150px">Game</th>
						<th style="width:90px">Start</th>
						<th style="width:30px">State</th>
						<th style="width:30px">Media</th>
						<th style="width:30px">Edit</th>
						<th ng-repeat="item in baseEventsTemplates" style="width:30px" ng-click="checkAll(item)" class="handPointer">
							{{item.name}}
							<div ng-show="page==1">
								<div class="pull-center">
									<div ng-show="item.st_blocked == 0"><img class="checkHeader" ng-src="{{unblockedEventIcon}}" tooltip="Unblocked"/></div>
									<div ng-show="item.st_blocked == 1"><img class="checkHeader" ng-src="{{blockedEventIcon}}" tooltip="Blocked"/></div>
								</div>
							</div>
						</th>
						<th style="width:50px">---</th>
					</thead>
					<tbody ng-repeat="gitem in gamesList |filter: gametxt track by gitem.id">
						<tr ng-hide="gamesList.length == 0">
							<td style="width:40px">{{gitem.id}}</td>
							<td style="width:150px; text-align:left">
								<div ng-repeat="pitem in gitem.aux_info.participants track by pitem.id">
									<img height="30px" width="30px" ng-src="{{getParticipantLogo(pitem.logo)}}"/>
									{{pitem.name}}
								</div>
							</td>
							<td style="width:90px">{{gitem.start_date * 1000 | date:'MMM d, y H:mm:ss'}}</td>
							<td style="width:40px">
								<button  ng-show= "gitem.blocked == 0" class="btn btn-mini btn-success" ng-click="changeGameState(gitem.id, gitem.blocked)"><i class="icon-ok icon-white"></i></button>
								<button  ng-show= "gitem.blocked == 1"class="btn btn-mini btn-danger" ng-click="changeGameState(gitem.id, gitem.blocked)"><i class="icon-remove icon-white"></i></button>
							</td>
							<td class="handPointer" ng-click="showNews(gitem.id)"><i class="icon-film"></i></td>
							<td class="handPointer" ng-click="showItem(gitem.id, gitem.competition_id)"><i class="icon-edit"></i></td>
							<td style="width:40px" ng-repeat="item in baseEventsTemplates">
								<div ng-repeat="eitem in gitem.aux_info.events['base_events']">
									<div ng-if="item.id == eitem.template_id">
												<div ng-show="page==2"> <!-- results --> 
													<img ng-src="{{showResultStatusLogo(eitem.result_status, eitem.blocked)}}" tooltip="showResultStatusTooltip(eitem.result_status, eitem.blocked)" ng-show="eitem.template_id != 10"/>
													<p style="color:#888888; font-size:12px;" ng-show="eitem.template_id==7 || eitem.template_id==8">[{{eitem.basis}}]</p>
													<h5 ng-show="eitem.template_id == 10">{{eitem.coefficient}}</h5>
												</div>
												<div ng-show="page==1"> <!-- events --> 
													<div ng-click="updateEventState(gitem, eitem)"  class="handPointer">
														<div ng-show="eitem.blocked == 0"><img class="checkItem" ng-src="{{unblockedEventIcon}}" tooltip="Unblocked"/></div>
														<div ng-show="eitem.blocked == 1"><img class="checkItem" ng-src="{{blockedEventIcon}}"   tooltip="Blocked"/></div>
													</div>
													<input nut-select-on-click ng-pattern="/^[0-9]+(\.[0-9]+)?$/" class="input-mini coefficient" value="{{eitem.coefficient}}" nut-blur="updateEventValue(gitem, eitem, $event)"></input>
													<input nut-select-on-click ng-pattern="/^[0-9]+(\.[0-9]+)?$/" class="input-mini basis" value="{{eitem.basis}}" nut-blur="updateBasisValue(gitem, eitem, $event)" ng-show="eitem.template_id==7 || eitem.template_id==8"></input>
												</div>
									</div>
								</div>
								<div ng-show="gitem.aux_info.events['base_events'].length == 0"><b>--</b></div> <!-- results help-->
							</td>
							<td style="width:50px">
								<div ng-show="page==1"> <!-- events -->
									<button class="btn btn-mini btn-danger" type="button" ng-click="addEvents(gitem)" ng-show="gitem.aux_info.events['base_events'].length == 0">
										<i class="icon-plus icon-white"></i>
									</button>
									<button class="btn btn-mini" type="button" ng-click="showCustomEvents(gitem)" ng-show="gitem.aux_info.events['base_events'].length != 0">AUX({{gitem.aux_info.events['aux_events'].length}})</button>
								</div>
								<div ng-show="page==2"> <!-- results -->
									<button class="btn btn-small" type="button" ng-click="showCustomEvents(gitem)" ng-show="gitem.result_text != '' && gitem.result_text != null"><b>{{gitem.result_text}}</b> [{{gitem.aux_info.events['aux_events'].length}}]</button>
									<button class="btn btn-small" type="button" ng-click="showCustomEvents(gitem)" ng-show="gitem.result_text == '' || gitem.result_text == null">--:-- [{{gitem.aux_info.events['aux_events'].length}}]</button>
								</div>
							</td>
						</tr>
						<tr ng-show="currentGameId == gitem.id">
							<!-- Events -->
							<td colspan="100%" style="text-align:left;padding:20px" ng-show="page==1">
								<div class="row-fluid">
									<div class="span12">
								      <div class="row-fluid">
								      			<div class="span12"><h5>Create Aux Events</h5></div>
								        </div>
								        <div class="row-fluid" style="float:left">
					                		<div class="span3">
					                			<select  name="auxEventChooser" ngwidth="70%" data-placeholder="Choose event type"
					                					 ng-model="auxEventModel.template_id" nutchosen="auxEventsTemplates"	ng-options="b.id as b.name for b in auxEventsTemplates" nutchosenselected="auxEventModel.template_id" ng-change="auxEventChanged()">
												</select>
					                		</div>
					                        <div class="span2" ng-show="auxEventModel.template_id >0">
					                        		<div class="input-prepend">
									                  <span class="add-on">Coeff: </span>
									                  <input nut-select-on-click class="input-mini" type="text" ng-model="auxEventModel.coefficient">
									                </div>
					                        </div>
											
											<div class="span1" ng-show="auxEventModel.template_id >0">
												<button ng-show= "auxEventModel.blocked == 0" class="btn btn-small btn-success" ng-click="auxEventModel.blocked = 1"><i class="icon-ok icon-white"></i></button>
												<button ng-show= "auxEventModel.blocked == 1" class="btn btn-small btn-danger"  ng-click="auxEventModel.blocked = 0"><i class="icon-remove icon-white"></i></button>
											</div>

											 <div class="span2" ng-show="auxEventModel.template_id >0">
					                        		<div class="input-prepend">
									                  <span class="add-on">Basis: </span>
									                  <input nut-select-on-click class="input-mini" type="text" ng-model="auxEventModel.basis">
									                </div>
					                        </div>		
					                        
					                        <div class="span2" ng-show="auxEventModel.template_id >0 && showDataField()" >
					                        	Data
					                        </div>   
					                        <div class="span2" ng-show="auxEventModel.template_id > 0 ">
					                        	<button class="btn btn-success btn-small" type="button" ng-click="addAuxEvents()">Add</button>
					                        </div>
								        </div>
								      </div>
								</div>
								<hr size = "3px;"/>
								<div class="span12">
									<div ng-show="gitem.aux_info.events['aux_events'].length != 0">
										<div class="row-fluid">
											<div class="span12" style="overflow:auto">
												<div ng-repeat="item in gitem.aux_info.events['aux_events']" class="box" ng-class="{'blockedEventClass': item.blocked == 1, 'unblockedEventClass' : item.blocked == 0}" >
													<div>
														<h5><a href="" ng-click="updateEventState(gitem, item)">
																{{getAuxTemplateName(item.template_id)}}
															</a>
														</h5>
													</div>
													<div>
														<div>
															<h5 style="float:left">Coeff:</h5>
														</div>
														<div>
															<input nut-select-on-click class="input-mini coefficient" value="{{item.coefficient}}" nut-blur="updateEventValue(gitem, item, $event)" ></input>
														</div>
													</div>
													<div style="clear:both">
														<div>
															<h5 style="float:left">Basis:</h5>
														</div>
														<div  style="float:left">
															<input nut-select-on-click class="input-mini coefficient" value="{{item.basis}}" nut-blur="updateBasisValue(gitem, item, $event)"></input>
														</div>
													</div>
												</div>	
											</div>
										</div>	
									</div>
								</div>
							</td>
							<!-- Results -->
							<td colspan="100%" style="text-align:left;padding:20px" ng-show="page==2">
								<div class="row-fluid">
									<div ng-show="gitem.aux_info.events['base_events'].length == 0" class="span3">
										<table>
											<tr>
												<td>1-st team goals count</td>
												<td>
													<input  type="text" class="input-mini" required ng-model="resultModel.values[1]" >
												</td>
											</tr>
											<tr>
												<td>2-nd team goals count</td>
												<td>
													<input  type="text" class="input-mini" required ng-model="resultModel.values[3]" >
												</td>
											</tr>
										</table>
										<button class="btn btn-small btn-success" type="button" ng-disabled="resultModel.values[1] == null || resultModel.values[3] == null" ng-click="saveOnlyResults()">Save</button>
									</div>
									<div class="span3" style="border-right:1px solid gray" ng-form name="insertResultsForm" ng-hide="resultTemplates.length == 0">
										<table>
											<tr ng-repeat="item in resultTemplates">
												<td>{{item.result_text}}</td>
												<td>
													<input  type="text" class="input-mini" required ng-model="resultModel.values[item.id]" 
															ng-disabled="gitem.calculated == 1">
												</td>
											</tr>
										</table>
										<button class="btn btn-mini" type="button" ng-disabled="insertResultsForm.$invalid || gitem.calculated == 1" ng-click="saveResults()">Save</button>
										<button class="btn btn-mini" style="float:right" type="button" ng-disabled="gitem.calculated == -1" ng-click="returnResults()">Return</button>
									</div>
									<div class="span8">
								        <div ng-show="gitem.aux_info.events['aux_events'].length != 0">
											<div class="row-fluid">
												<div class="span12" style="overflow:auto">
													<div class="box blockedEventClass" ng-repeat="item in gitem.aux_info.events['aux_events']" >
														<div style="float:left">
															<h5>
																{{getAuxTemplateName(item.template_id)}}
															</h5>
														</div>
														<div>
															<img ng-src="{{showResultStatusLogo(item.result_status, item.blocked)}}" tooltip="showResultStatusTooltip(item.result_status, item.blocked)"/>
														</div>																	
														<div>
															<div>
																<b>Basis:</b> {{item.basis}}
															</div>
														</div>
													</div>	
												</div>
											</div>	
										</div>
								    </div>
								</div>	
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="span4" ng-show="gamesList == null || gamesList.length == 0">
				<h1>No active game</h1>
			</div>
		</div>

		<!-- GAMES LIST END -->
		<div class="span3 well" ng-show="selectedGame || isNewGame">
			<a href="#" style="left:10px;" ng-click="selectedGame=false; isNewGame=false">Close</a>
			<div height="50px"></div>
			<div class="row-fluid">
				<div class="span12">

					<div class="row-fluid">
						<div class="span6 offset3" ng-show="isNewGame == false">
							<select disabled name="competitions" nutchosen="gamemodel.competition_id" ngwidth="100%" 
									ng-model="gamemodel.competition_id" ng-options="comp.id as comp.name for comp in filtermodel.competitionList">
							</select>
						</div>
						<div class="span6 offset3" ng-show="isNewGame == true">
							<select name="competitions" nutchosen="gamemodel.competition_id" ngwidth="100%" data-placeholder="Choose competition"
									ng-model="gamemodel.competition_id" ng-options="comp.id as comp.name for comp in filtermodel.competitionList"
									ng-change="setCompetition()">
							</select>
						</div>
					</div>
					<div class="row-fluid">
						
						<div class="span5">
							<h3>Game</h3>
						</div>
						<div class="span7">
							<div class="pull-right">
								<button ng-show="selectedGame == true"
										class="btn btn-danger btn-small" 
									    data-unique="1" 
			          					bs-popover = "'removePopover'" 
			          					data-container="#gamesView"
			          					data-placement="left"
			          					data-trigger = "click">
			          					Remove
	          					</button>
							</div>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span12">
							<select data-placeholder="Choose participant1" ng-model="gamemodel.participants[0]" nutchosen="compList[gamemodel.competition_id]"
									ng-options="p.id as p.name for p in  getArray(compList[gamemodel.competition_id])" nutchosenselected="gamemodel.participants[0]" ng-change="setParticipant(0, gamemodel.competition_id)">
								<option></option>
						    </select>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span12">
							<select data-placeholder="Choose participant2" ng-model="gamemodel.participants[1]" nutchosen="compList[gamemodel.competition_id]"
									ng-options="p.id as p.name for p in  getArray(compList[gamemodel.competition_id])" nutchosenselected="gamemodel.participants[1]" ng-change="setParticipant(1, gamemodel.competition_id)">
								<option></option>
						    </select>
						</div>
					</div>

					<div class="row-fluid" ng-show="gamemodel.participants[0] == gamemodel.participants[1]" class="push-right">
						<div class="span12">
								<span class="badge badge-important" >Wrong selection !</span>
						</div>
					</div>	

					<hr size="3px"></hr>

					<!-- TRANSLATION -->
					<h4>Translations</h4>
					<div  class="row-fluid" ng-repeat="(lang, translationObj) in gamemodel.translations">
						<div class="span12">	
							<div class="input-append span8" >
									<input type="text" ng-model="translationObj.text"> 
									<span class="add-on">{{lang}}</span>
							</div>
						</div>
					</div>
					<!-- TRANSLATION -->
				</div>
			</div>
	
			<hr size="3px"></hr>

			<div class="row-fluid">
				<div class="span6">
					<div class="row-fluid">
						<div class="span12">
							<h4>Start Date</h4>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span12">
							<nutdatetimepicker  nutdate  = "gamemodel.start_date"
												mindate  = "minDate"
												nutdatechange = "updateGameDate">
							</nutdatetimepicker>
						</div>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<h5>Priority</h5>
						<div class="span6">
							<input type="range" class="span12" name="points" min="0" max="10" ng-model="gamemodel.priority"/>
						</div>
						<div class="span4">
							<span class="badge badge-info">{{gamemodel.priority}}</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row-fluid"><hr size="3px"></hr></div>

			<div class="row-fluid">
				<div class="span12">
					<button ng-click="addOrUpdateGame()" class="btn btn-success btn-large btn-block" ng-disabled="gamemodel.participants[0] == gamemodel.participants[1] && gamemodel.competition_id == null">
						<h5>Save</h5>
					</button>
				</div>
			</div>
		</div>

	</div>
	</div>
</div>