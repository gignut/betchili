<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php 
  //LIBS
  chili_asset_loader::jquery(true, true);
  chili_asset_loader::angularjsVersion(true, "1.2.15");
  chili_asset_loader::angularstrap();
  chili_asset_loader::bootstrap();
  chili_asset_loader::fileuploader();
  chili_asset_loader::chosen();
  chili_asset_loader::boostrapDateTimePicker();
  chili_asset_loader::noty();
  chili_asset_loader::momentJs();
  // chili_asset_loader::scrollBar();
  
  script("modules/js/jquery.blockUI.js");
  script("modules/js/json2.js");
?>

<script>
    window.BASE_URL     = <?php echo '"' . BASE_URL  . '"'; ?>;
    window.INDEX_PHP    = <?php echo '"' . INDEX_PHP . '"'; ?>;
    window.BASE_INDEX   = <?php echo '"' . BASE_INDEX  . '"'; ?>;
    window.MEDIA_URL    = <?php echo '"' . MEDIA_URL . '"'; ?>;
    window.MODULE_ADMIN_FOLDER = <?php echo '"' . MODULE_ADMIN_FOLDER . '"'; ?>; 
</script>

<?php  
  
  css("modules/m_admin/css/chiliadmin.css");
  script("nutron/js/ng-nutron.js"); 
  script("modules/m_admin/js/ng_chiliadmin.js"); 
  script("modules/m_admin/js/chiliadmin_api.js"); 
  script("modules/m_admin/js/chiliadmin_directives.js"); 
?>

<script>
    var sportData       = <?php echo isset($sportData)?$sportData : "1";?>;
    var currentSportId  = <?php echo isset($currentSportId) ? $currentSportId : "1" ;?>;
    var isAutorized     = <?php echo isset($isAutorized) ?  1 : 0 ;?>;
    var menuItem        = <?php echo isset($menuItem) ?   $menuItem : 0 ;?>;
</script>

</head>

<body ng-app="chiliadminApp" id="ng-app" class="ng-cloak" >

<div class="navbar navbar-inverse" style="position: static;" ng-controller="MainCtrl" ng-init="init()">
              <div class="navbar-inner">
                <div class="container">
                  <a class="btn btn-navbar" data-toggle="collapse" data-target=".subnav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </a>
                  <a class="brand" href="<?php echo BASE_INDEX . MODULE_ADMIN_FOLDER . '/main/index'?>">BetChili Admin</a>
                  <div class="nav-collapse subnav-collapse">
                    <ul class="nav">
                      <li ng-class="{selectedMenu: menuItem == 1}"> <a href= "<?php echo BASE_INDEX . 'admin/main/show/'  .  $countries;    ?>" >Countries</a></li>
                      <li ng-class="{selectedMenu: menuItem == 2}"> <a href= "<?php echo BASE_INDEX . 'admin/main/show/'  .  $participants; ?>" >Participants</a></li>
                      <li ng-class="{selectedMenu: menuItem == 3}"> <a href="<?php  echo BASE_INDEX . 'admin/main/show/'  .  $leagues;      ?>" >Leagues</a></li>
                      <li ng-class="{selectedMenu: menuItem == 4}"> <a href="<?php  echo BASE_INDEX . 'admin/main/show/'  .  $competitions; ?>" >Competitions</a></li>
                      <li ng-class="{selectedMenu: menuItem == 5}"><a href="<?php  echo BASE_INDEX . 'admin/main/show/'  .   $games;         ?>" >Games</a></li>
                      <li ng-class="{selectedMenu: menuItem == 6}"> <a href="<?php  echo BASE_INDEX . 'admin/main/show/'  .  $users;        ?>" >Users</a></li>
                      <li ng-class="{selectedMenu: menuItem == 7}"> <a href="<?php  echo BASE_INDEX . 'admin/main/show/'  .  $news;         ?>" >News</a></li>
                      
                    </ul>
              
                    <ul class="nav pull-right" ng-show="isAutorized == 1">
                      <li><a href="<?php echo BASE_INDEX . MODULE_ADMIN_FOLDER . '/chili_oauth/logout' ?>">Logout</a></li>
                      <li class="divider-vertical"></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{currentSport.name}} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="#" ng-repeat = "sport in sports" ng-click="setSport(sport)">
                              {{sport.name}}
                            </a>
                         </li>
                        </ul>
                      </li>
                    </ul>
                  </div><!-- /.nav-collapse -->
                </div>
              </div><!-- /navbar-inner -->
</div>

<div > 
  <?php echo $CONTENT;?>
</div>
</body>
</html>