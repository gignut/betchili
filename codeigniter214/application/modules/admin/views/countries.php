<?php script("modules/m_admin/js/ng_countries.js"); ?>

<script>
	var countryData = <?php echo $countryData;?>;
</script>

<script id="removePopover" type="text/ng-template">
	<div>
	    <h5>Are you shure</h5>
	    <button class="btn btn-primary" type = "button" ng-click = "removeListItem(); hide()">Yes</button>
		<button class="btn" type = "button" ng-click = "hide()">No</button>
	</div>
</script>

<div ng-controller = "CountriesCtrl" id="countriesView">
		<div ng-init="init()"></div>
		<div class="row-fluid">
			<div class="span3">
					<div class="row-fluid">
						<div class="span12 well">
							<div class="row-fluid">
								<div class="span10">
									<div class="input-append span12">
										<input type="text" class="span10"  ng-model="searchtxt" placeholder="Type country name"> 
										<span class="add-on"><i class="icon-search"></i> </span>
									</div>
								</div>
								<div class="span2">
									<button class="btn btn-block btn-success" type="button" ng-click="createNewItem()">New</button>
								</div>
							</div>	
						</div>
					</div>

					<div class = "row-fluid" >
						<div class="span12 well" style="height: 700px; overflow: auto;" >
							<div  >
								<ul id="" class="nav nav-pills nav-stacked nut-sidenav" >
									<li ng-repeat="item in list |filter:searchtxt" ng-class="{active: item.id==countrymodel.id}">
											<a href="#" ng-click = "showItem(item.id, $event)"><i class="icon-chevron-right"></i> {{item.name}}</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
			</div>

			<div class="span9" id="container">

			<div ng-show="countrymodel != null">	
				<div style="position:relative;">
					<h2> {{countrymodel.name}}</h2>
					<button class="btn btn-danger btn-small" 
						    data-unique="1" 
	      					bs-popover = "'removePopover'" 
	      					data-container="#countriesView"
	      					data-placement="bottom"
	      					data-trigger = "focus"
	      					style="position:absolute;top:0px; right: 33px;">
      					    Remove
      				</button>
				</div>	

				<!-- UPLOADER START -->
				<div class="row-fluid ">
					<div class="span12 well" >
							<div id = "uploadPluginContainer" >
									<nutupload 	uploadaction = "<?php echo BASE_INDEX . 'admin/asset/upload/';              ?>"
												assetpath    = "<?php echo MEDIA_URL  . 'assets/countries/';                ?>"
												nophoto 	 = "<?php echo MEDIA_URL  . 'modules/img/no_avatar_medium.gif'; ?>"
												removeaction = "<?php echo BASE_INDEX . 'admin/asset/remove/';              ?>" >
									</nutupload>
							</div>
					</div>
				</div>
				<!-- UPLOADER END -->
	
				<!-- COUNTRY DATA START -->
				<div class="row-fluid" >
				<div class="span12 well">
					<div>
						<div class="row-fluid" ng-form name="updateCountriesForm">
							<div class="span6">
								<h3 style="margin-bottom:20px;">Data</h3>
								<div class = "row-fluid">
									<div class = "span8">	
										<input id="country_name" class="span12" type="text" placeholder="Name" required ng-model="countrymodel.name"> 
									</div>
								</div>

								<div class = "row-fluid">
									<div class = "span8">	
										<input id="country_code" class="span12" type="text" placeholder="Code" required ng-model="countrymodel.code"> 
									</div>
								</div>

								<div class = "row-fluid">
									<div class = "span3">
										 <button  class="btn  btn-success btn-block" type="button" ng-disabled="updateCountriesForm.$invalid" ng-click="updateListItem()">Save</button>
									</div>
								</div>	
							</div>

							<!-- translations -->	
							<div class="span6">
								<h3 style="margin-bottom:20px;">Translations</h3>
								<div  class="row-fluid" ng-repeat=" (lang, trObject) in countrymodel.translations">
										<div class="span12">	
											<div class="input-append span8" >
		 										<input size="25" type="text" required ng-model="trObject.text" > 
		 										<span class="add-on">{{lang}}</span>
											</div>
										</div>
								</div>	
							</div>
						</div>
					</div>	
				</div>
				</div>
				<!-- COUNTRY DATA END -->	
			</div>
			
			<div>	
				<!-- NEW COUNTRIES ADD -->	
				
				<div class = "row-fluid" ng-show = "countrymodel == null">
						<div class="span6" ng-form name="newCountriesForm">
							<div  class="well">
								<div>
									<button class="btn btn-large btn-block btn-success" type="button" ng-disabled="!newItemList.length || newCountriesForm.$invalid" ng-click="saveNewItems()">
										Save All
									</button>
								</div>
									<h1 style="margin-top:50px;margin-bottom:20px;">New Countries</h1>
								<div>
									<div ng-repeat = "newItem in newItemList">
									<div class="row-fluid" >
										<div class="span6">
											<input class="span12" type="text" placeholder="Name" required ng-model="newItem.name">
										</div>
										<div class="span2">
											<input class="span12" type="text" placeholder="Code" required ng-model="newItem.code">
										</div>
										<div class="span4">
											<button type="button" class="btn btn-danger delete btn-small" ng-click="removeNewItem($index);">
												<i class="icon-trash icon-white"></i>
												<span>Delete</span>
											</button>
										</div>
									</div>
								</div>
								</div>
							</div>
						</div>
						<div class="span6">
						</div>
				</div>
			</div>	
		</div>
	</div>
	</div>
</div>




