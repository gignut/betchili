<script>
	function SigninCntrl($scope, $http, ChiliConfigs, ChiliUtils)
	{
		$scope.signinModel = { "name" : "", "pass" : ""};

		$scope.signin = function(){
			$http({	method: "POST",
					url: ChiliConfigs.BASE_INDEX + "admin/chili_oauth/signin",
					data :  $.param($scope.signinModel),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    	}).success(function(data, status, headers, config){
	  			ChiliUtils.action(data);
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::SigninCntrl signin");
	    	});
		};
	}
</script>

<div class="container ng-cloak" style="margin-top:150px;" ng-controller="SigninCntrl">
	<div class="row">
		<div class="span6 offset3">
			<form class="well form-inline" method="POST">
			  <input name = "user" ng-model="signinModel.user" type="text" class="input-small"     placeholder="Email"    style="height:25px;width:150px;">
			  <input name = "pass" ng-model="signinModel.pass" type="password" class="input-small" placeholder="Password" style="height:25px;width:150px;">
			  
			  <button name="signin"  ng-click="signin()" data-loading-text="Signing..." class="btn btn-primary" type="submit" style="float:right;">
			        Sign in
			  </button>
			</form>
		</div>
	</div>
</div>


