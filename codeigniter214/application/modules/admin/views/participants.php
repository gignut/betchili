<?php script("modules/m_admin/js/ng_participants.js"); ?>
<script>
	var countryData = <?php echo $countryData;?>;
</script>

<script id="removePopover" type="text/ng-template">
	<div>
	    <h5>Are you shure</h5>
	    <button class="btn btn-primary" type = "button" ng-click = "removeParticipant(); hide()">Yes</button>
		<button class="btn" type = "button" ng-click = "hide()">No</button>
	</div>
</script>

<div ng-controller = "ParticipantsCtrl" id="participantsView">
		<div ng-init="init()"></div>
		<div class="row-fluid">
			<div class="span3">
					<div class="well">
						<div class="row-fluid">
							<div class="span12">
							<h3 >Choose Country</h3>
						    </div>	
						</div>
					
						<div class="row-fluid">
							<div class="span12 ">
							<select  ngwidth="70%" data-placeholder="Choose country" ng-model="selectedCountry" ng-change="countrySelect()" nutchosen="countryList" nutchosenselected="selectedCountry"
									ng-options="c.name for c in  countryList">
									<option></option>
						    	</select>
						    </div>	
						</div>
						<div class="height1"></div>	
						<!-- PARTICIPANT SEARCHBOX -->	
						<div class="row-fluid" ng-show="selectedCountry != null ">
							<div class="span12">
								<div class="row-fluid">
									<div class="span10">
										<div class="input-append span12">
											<input type="text" class="span10"  ng-model="searchtxt" placeholder="Type participant name"> 
											<span class="add-on"><i class="icon-search"></i> </span>
										</div>
									</div>
									<div class="span2">
										<button class="btn btn-block btn-success" type="button" ng-click="createNewItem()">New</button>
									</div>
								</div>	
							</div>
						</div>
					</div>
					

					<div class = "row-fluid" style="height: 700px; overflow: auto;" ng-show="selectedCountry != null ">
						<div class="span12 well" >
								<h3 ng-show="participantsList.length == 0">No Participants</h3>
								<ul id="" class="nav nav-pills nav-stacked nut-sidenav" ng-hide="participantsList.length == 0">
									<li ng-repeat="pitem in participantsList | array|filter: searchtxt" ng-class="{active: pitem.id==selectedItem.id}">
											<a href="#"  ng-click = "showItem(pitem.id)"><i class="icon-chevron-right"></i> {{pitem.name}}</a>

									</li>
								</ul>
						</div>
					</div>
			</div>

			<div class="span9" id="container" >

			<div ng-show="participantmodel != null && selectedCountry != null">
				<div class="row-fluid">
						<div class="span8">
							<h2> {{participantmodel.name}}</h2>
						</div>
						<div class="span4">
							<button  class="btn btn-danger btn-small" 
							    data-unique="1" 
	          					bs-popover = "'removePopover'" 
	          					data-container="#participantsView"
	          					data-placement="bottom"
	          					data-trigger = "click">
	          					Remove
	          				</button>
						</div>
				</div>	
				
				<div ng-show="participantmodel != null">
				<!-- UPLOADER START -->
				<div class="row-fluid ">
					<div class="span12 well" >
							<div id = "uploadPluginContainer" >
									<nutupload 	uploadaction = "<?php echo BASE_INDEX . 'admin/asset/upload/';              ?>"
												assetpath    = "<?php echo MEDIA_URL  . 'assets/participants/';                ?>"
												nophoto 	 = "<?php echo MEDIA_URL  . 'modules/img/no_avatar_medium.gif'; ?>"
												removeaction = "<?php echo BASE_INDEX . 'admin/asset/remove/';              ?>" >
									</nutupload>
							</div>
					</div>
				</div>
				<!-- UPLOADER END -->
	
				<!-- PARTICIPANT DATA START -->
				<div class="row-fluid" >
				<div class="span12 well">
					<div>
						<div class="row-fluid">
							<div class="span6">
								<h3 style="margin-bottom:20px;">Data</h3>
								<div class = "row-fluid">
									<div class = "span8">	
										<input class="span12" type="text" placeholder="Name" ng-model="participantmodel.name"> 
									</div>
								</div>

								<div class="row-fluid">
									<h5 >Priority</h5>
									<div class="span4">	
										<input type="range" name="points" min="0" max="10" ng-model="participantmodel.priority">
									</div>
									<div class="span2">
										<span  class="badge badge-info">{{participantmodel.priority}}</span>
									</div>
								</div>
								

								<div class = "row-fluid">
									<div class = "span3">
										 <button  class="btn  btn-success btn-block" type="button" ng-click="updateParticipant()">Save</button>
									</div>
								</div>	
							</div>

							<!-- translations -->	
							<div class="span6">

								<h3 style="margin-bottom:20px;">Translations</h3>
								<div  class="row-fluid" ng-repeat="(lang, translationObj) in participantmodel.translations">
									<div class="span12">	
										<div class="input-append span8" >
												<input type="text" ng-model="translationObj.text">
												<span class="add-on">{{lang}}</span>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>	
				</div>
				</div>
				<!-- PARTICIPANT DATA END -->	
			</div>
		</div>
			
			<!-- NEW PARTICIPANTS ADD -->
			<div class = "row-fluid"  ng-show = "selectedCountry != null && participantmodel == null && selectedItem == null">
					<div class="span6" ng-form name="newParticipantsForm">
						<div  class="well">
							<div>
								<button class="btn btn-large btn-block btn-success" type="button" ng-disabled="!newItemList.length || newParticipantsForm.$invalid" ng-click="saveNewItems()">
									Save All
								</button>
							</div>
								<h3 style="margin-top:50px;margin-bottom:20px;"> New participants - {{selectedCountry.name}}  </h3>
							<div>
								<div ng-repeat = "newItem in newItemList">
								<div class="row-fluid" >
									<div class="span6">
										<input class="span12" type="text" placeholder="Name" required ng-model="newItem.name">
									</div>
									<div class="span2">
										<button type="button" class="btn btn-danger delete btn-small" ng-click="removeNewItem($index);">
											<i class="icon-trash icon-white"></i>
											<span>Delete</span>
										</button>
									</div>
									<div class="span4" ng-show="showNameError != null && showNameError[newItem.name] == newItem.name">
										<p>Name already exist</p>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="span6">
					</div>
			</div>
			
		</div>
	</div>
	</div>
</div>




