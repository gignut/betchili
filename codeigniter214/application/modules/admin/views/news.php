
<?php

 	script_remote("//popcornjs.org/code/dist/popcorn-complete.min.js");
	css("modules/m_admin/css/blocks.css");
	echo script("modules/m_admin/js/ng_news.js");
?>

<script>
	var newsData = <?php echo $newsData;?>;
	var sub1 = '<?php echo $sub1;?>';
	var sub2 = '<?php echo $sub2;?>';
</script>

<div ng-controller = "NewsCtrl">
	<div ng-init="init()">
	</div>

 	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<div class="span4 well">
					<select ngwidth="50%" data-placeholder="Choose relation" ng-model="filtermodel.ftable" nutchosen="filtermodel.ftables"
							ng-options="t for t in filtermodel.ftables" nutchosenselected="filtermodel.ftable" ng-change="setRelation()">
						<option></option>
				    </select>
				    <div ng-show="filtermodel.ftable != 'All'" >
				    	<h5>ID:</h5>
				    	<input type="text" ng-model="filtermodel.row_number" class="input-small" nut-enter="setRelation()"></input>
					</div>
				    <button class="btn btn-success" type="button" ng-click="createNews()" style="float:right">New video</button>
				</div>
				<div class="span6 well" ng-show="showCreate != null">
					<div class="row-fluid">
						<div class="span8">
							<h5>URL</h5>
							<input type="text" class="input-block-level" ng-model="newNews.link">
							<h5>Description</h5>
							<input type="text" class="input-block-level" ng-model="newNews.description">
						</div>
						<div class="span4">
							<div class="row-fluid">
								<h5>Type</h5>
								<select ngwidth="90%" data-placeholder="Choose type" ng-model="newNews.type" nutchosen="newNews.types"
									ng-options="t for t in newNews.types" nutchosenselected="newNews.type">
									<option></option>
					    		</select>
					    		<div>
					    			<select ngwidth="90%" data-placeholder="Choose table" ng-model="newNews.table" nutchosen="newNews.tables"
										ng-options="t for t in newNews.tables" nutchosenselected="newNews.table">
										<option></option>
					    			</select>
					    			<div ng-show="newNews.table != 'Any'">
					    				<div class="span2"><h5>ID#</h5></div>
					    				<div class="span3"><input type="text" ng-model="newNews.row_id" class="input-small"></div>
					    			</div>
					    		</div>
				    		</div>
				    		<div class="row-fluid">
				    			<div class="height1"></div>
				    			<div class="span9">
				    				<button class="btn btn-success" type="button" ng-click="addNews()" style="float:right">Add news</button>
				    			</div>
				    		</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<blocks class="blocks">
						<div ng-repeat="n in newsList" class="block">
							<news nglink="{{n.link}}" ngtype="{{n.type}}" ngdesc="{{n.description}}" ngtable="{{n.refer_table}}" ngrowid="{{n.field_id}}">
							</news>
						</div>
					</blocks>
				</div>
			</div>			
		</div>
	</div>
</div>