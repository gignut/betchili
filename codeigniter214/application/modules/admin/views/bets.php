<?php script("modules/m_admin/js/ng_bets.js"); ?>

<script>
	var sitesData = <?php echo $sitesData;?>;
</script>

<div ng-controller = "BetsCtrl" >
		<div ng-init="init()"></div>
		<div class="row-fluid">
			<div class="span3 well">
				<div class="row-fluid">
					<select  ngwidth="80%" data-placeholder="Choose site"
							 ng-model="selectedDomain"
							 ng-change="siteSelect()"
							 nutchosen="sitesData"
							 nutchosenselected="selectedDomain"
							 ng-options="s.site_domain for s in  sitesData">
							 <option></option>
			    	</select>
		    	</div>
		    	<hr>
		    	<div class="row-fluid">
					<select  ngwidth="80%" data-placeholder="Choose widget"
							 ng-model="selectedWidget"
							 ng-change="widgetSelect()"
							 nutchosen="widgetList"
							 nutchosenselected="selectedWidget"
							 ng-options="w.name for w in  widgetList">
							 <option></option>
			    	</select>
		    	</div>
	    	</div>
	    	<div class="span9">
	    		<ul ng-show="selectedWidget != null">
					<li ng-repeat="item in betsList"> [{{item.id}}] -- {{item.amount}}$ -- C:{{item.coefficient}}
						<ul>
							<li ng-repeat="chain in item.event_chain">
								{{chain.event_name}} -- C:{{chain.event_coefficient}}
							</li>
						</ul>
					</li>
				</ul>
	    	</div>
   		</div>
</div>
