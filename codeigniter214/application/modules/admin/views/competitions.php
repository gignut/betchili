<?php script("modules/m_admin/js/ng_competitions.js"); ?>
<style>
.participantsDivider
{
	height: 1px;
	background-color: rgb(243, 235, 235);
	margin-top: 15px;
	margin-bottom: 32px;
}
</style>

<script>
	var leagueData = <?php echo $leagueData;?>;
	var countryData = <?php echo $countryData;?>;
</script>

<script id="removePopover" type="text/ng-template">
	<div>
	    <h5>Are you shure</h5>
	    <button class="btn btn-primary" type = "button" ng-click = "removeCompetition(); hide()">Yes</button>
		<button class="btn" type = "button" ng-click = "hide()">No</button>
	</div>
</script>

<div ng-controller="CompetitionsCtrl" id="competitionView">
	<div ng-init="init()"></div>
	<div class="row-fluid">
		<div class="span3">
					<div class="well">
						<div class="row-fluid">
							<div class="span12">
							<h3 >Choose League</h3>
						    </div>	
						</div>
					
						<div class="row-fluid">
							<div class="span12 ">
							<select  ngwidth="70%" data-placeholder="Choose League" ng-model="selectedLeague" ng-change="leagueSelect()" nutchosen="leagueList" nutchosenselected="selectedLeague"
									ng-options="c.name for c in  leagueList">
									<option></option>
						    	</select>
						    </div>	
						</div>
						<div class="height1"></div>	
						<!-- Competitions SEARCHBOX -->	
						<div class="row-fluid" ng-show="selectedLeague != null ">
							<div class="span12">
								<div class="row-fluid">
									<div class="span10">
										<div class="input-append span12">
											<input type="text" class="span10"  ng-model="searchtxt" placeholder="Type competition name"> 
											<span class="add-on"><i class="icon-search"></i> </span>
										</div>
									</div>
									<div class="span2">
										<button class="btn btn-block btn-success" type="button" ng-disabled= "newCompetition != null" ng-click="createNewCompetition()">New</button>
									</div>
								</div>	
							</div>
						</div>
					</div>
					

					<div class = "row-fluid" style="height: 700px; overflow: auto;" ng-show="selectedLeague">
						<div class="span12 well" >
								<h3 ng-show="currentCompetitionsList.length == 0">No active Competitions</h3>
								<ul  class="nav nav-pills nav-stacked nut-sidenav" ng-hide="currentCompetitionsList.length == 0">
									<li ng-repeat="comp in currentCompetitionsList |filter: searchtxt" ng-class="{active: comp.id == competitionModel.id}">
										<a href="#" ng-click="showItem(comp.id, $event)"><i class="icon-chevron-right"></i> {{comp.name}}</a>
									</li>
								</ul>
								<div ng-show="oldCompetitionsList.length">
									<hr>
									<div ng-show="showOldCompetitions == false"><a href="#" ng-click="showOldCompetitions = true">[+] Old competitions</a></div>
									<div ng-show="showOldCompetitions">
										<div><a href="#" ng-click="showOldCompetitions = false">[-] Old competitions</a></div>
										<hr>
										<ul  class="nav nav-pills nav-stacked nut-sidenav" ng-hide="oldCompetitionsList.length == 0">
											<li ng-repeat="comp in oldCompetitionsList |filter: searchtxt" ng-class="{active: comp.id == competitionModel.id}">
												<a href="#"  ng-click="showItem(comp.id, $event)"><i class="icon-chevron-right"></i> {{comp.name}}</a>
											</li>
										</ul>
									</div>
								</div>
						</div>
					</div>
			</div>

			<div class="span9">

			<div ng-show="competitionModel != null">	
				<div class="row-fluid">
						<div class="span8">
							<h2> {{competitionModel.name}}</h2>
						</div>
						<div class="span4">
							<button class="btn btn-small" 
								ng-click="changeStatus()"
	          					ng-class="{'btn-success': competitionModel.blocked == '1', 'btn-danger': competitionModel.blocked == '0'}">
	          					<div ng-show="competitionModel.blocked == '0'">Disable</div>
	          					<div ng-show="competitionModel.blocked == '1'">Enable</div>
	          				</button>
							<button  class="btn btn-danger btn-small" 
							    data-unique="1" 
	          					bs-popover = "'removePopover'" 
	          					data-container="#competitionView"
	          					data-placement="bottom"
	          					data-trigger = "click">
	          					Remove
	          				</button>
						</div>
				</div>	

			<ul class="nav nav-tabs">
              <li class="active"><a href="#competitionData" data-toggle="tab">Data</a></li>
              <li ><a href="#competitionParticipants" data-toggle="tab">Participants</a></li>
            </ul>

            <div class="tab-content">
            	<!-- DATA -->
			  <div class="tab-pane active" id="competitionData">
			  		<!-- UPLOADER START -->
					<div class="row-fluid ">
						<div class="span12 well" >
								<div id = "uploadPluginContainer" >
										<nutupload 	uploadaction = "<?php echo BASE_INDEX . 'admin/asset/upload/';              ?>"
													assetpath    = "<?php echo MEDIA_URL  . 'assets/competitions/';             ?>"
													nophoto 	 = "<?php echo MEDIA_URL  . 'modules/img/no_avatar_medium.gif'; ?>"
													removeaction = "<?php echo BASE_INDEX . 'admin/asset/remove/';              ?>" >
										</nutupload>
								</div>
						</div>
					</div>
					<!-- UPLOADER END -->
					
					<!-- Competition DATA START -->
					<div class="row-fluid" >
					<div class="span12 well">
						<div>
							<div class="row-fluid">
								<div class="span6">
									<h3 style="margin-bottom:20px;">Data</h3>
									<div class = "row-fluid">
										<div class = "span8">	
											<input  class="span12" type="text" placeholder="Name" ng-model="competitionModel.name"> 
										</div>
									</div>
									<div class="row-fluid">
										<h5 >Date</h5>
										<nutdatetimepicker  maxdate    = "competitionModel.end_date"
															nutdate    = "competitionModel.start_date"
															nutdatechange = "updateStartDate"
															nuthide    = "startDateHide"
									                        nutshow    = "endDateShow">
										</nutdatetimepicker>
									    <nutdatetimepicker  mindate    = "competitionModel.start_date"
									                        nutdate    = "competitionModel.end_date"
									                        nutdatechange = "updateEndDate"
									                        nuthide    = "endDateHide"
									                        nutshow    = "startDateShow">
									    </nutdatetimepicker>
									</div>

									<div class="row-fluid">
										<h5 >Priority</h5>
										<div class="span4">	
											<input type="range" name="points" min="0" max="10" ng-model="competitionModel.priority">
										</div>
										<div class="span2">
											<span  class="badge badge-info">{{competitionModel.priority}}</span>
										</div>
									</div>

									<div class = "row-fluid">
										<div class = "span3">
											 <button  class="btn  btn-success btn-block" type="button" ng-click="updateCompetition()">Save</button>
										</div>
									</div>	
								</div>

								<!-- translations -->
								<div class="span6">
									<h3 style="margin-bottom:20px;">Translations</h3>
									<div  class="row-fluid" ng-repeat=" titem in competitionModel.translations">
											<div class="span12">
												<div class="input-append span8" >
			 										<input  type="text" ng-model="titem.text"> 
			 										<span class="add-on">{{titem.lang}}</span>
												</div>
											</div>
									</div>	
								</div>
							</div>
						</div>	
					</div>
					</div>
					<!-- COMPETITION DATA END -->	
			  </div>

			  <!-- PARTICIPANTS -->
			  <div class="tab-pane" id="competitionParticipants" style="height:500px;">
			  		<div class="row-fluid">
			  			<div class="span3">
			  				<select ngwidth="90%" data-placeholder="Choose country" 
			  						ng-model="selectedCountry"
			  						ng-change="countrySelect()"
			  						nutchosen="countryList"
			  						nutchosenselected="selectedCountry"
									ng-options="c.name for c in  countryList">
									<option></option>
							</select>
			  			</div>
			  			<div class="span3">
			  				<select ngwidth="90%" data-placeholder="Choose participant"
			  						ng-model="selectedParticipant"
			  						ng-change="participantSelect()"
			  						nutchosen="participantsList"
			  						nutchosenselected="selectedParticipant"
									ng-options="p.name for p in getArray(participantsList)">
									<option></option>
							</select>
			  			</div>
			  
			  			<div class="span2">
			  				<button class="btn btn-success" ng-disabled="!determineAddBtnState()" ng-click="addParticipant()"> Add</button>	
			  			</div>
			  		</div>

					<div class="participantsDivider">	</div>
					
					<div class="row-fluid">
						<div>
						<div class="span3">
			  				<div class="input-append span12">
									<input tabindex="1" style="width:260px;" type="text"  ng-model="participantSearchTxt" placeholder="Type participant name"> 
									<span class="add-on"><i class="icon-search"></i> </span>
							</div>
			  			</div>
			  			<div class="span1">
			  				<button tabindex="2" class="btn btn-success push-left " ng-click="saveParticipants()">Save</button>
			  			</div>
			  			<div class="span7 well">
			  				 <b>Pts</b>: Points, <b>P</b>: Games Played, <b>W</b>: Wins, <b>D</b>: Draws, <b>L</b>: Losses, <b>F</b>: Goals Out, <b>A</b>: Goals In
			  			</div>
						</div>
					</div>

					<div style="height:15px;">	</div>

			  		<div class="row-fluid">
			  			<div class="span8" style="height: 600px; overflow: auto;">
			  				<table class="table table-hover">
			  					<thead>
			  						<tr>
			  							<td style="width:100px;">ID</td>
			  							<td style="width:200px;">Name</td>
			  							<td style="width:50px;">Pts</td>
			  							<td style="width:50px;">P</td>
			  							<td style="width:50px;">W</td>
			  							<td style="width:50px;">D</td>
			  							<td style="width:50px;">L</td>
			  							<td style="width:50px;">F</td>
			  							<td style="width:50px;">A</td>
			  							<td>Action</td>
			  						</tr>
			  					</thead>
			  					<tbody>
			  						<tr ng-repeat = "participant in competitionModel.participants| array| filter: participantSearchTxt">
			  							<td>{{participant.id}}</td>
			  							<td>{{participant.name}}</td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.score" ng-change="editParticipantScore(participant)"></td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.played_games" ng-change="editParticipantScore(participant)"></td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.wins" ng-change="editParticipantScore(participant)"></td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.draws" ng-change="editParticipantScore(participant)"></td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.looses" ng-change="editParticipantScore(participant)"></td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.goals_out" ng-change="editParticipantScore(participant)"></td>
			  							<td><input style="width:30px" tabindex="{{$index+3}}" type="text" ng-model="participant.goals_in" ng-change="editParticipantScore(participant)"></td>
			  							<td>
			  								<button tabindex="-1" class="btn btn-small btn-warning" ng-click="enableParticipant(participant)" ng-show="participant.disabled">Restore</button>
			  								<button tabindex="-1" class="btn btn-small btn-danger"  ng-click="disableParticipant(participant)" ng-show="!participant.disabled">Remove</button>
			  							</td>
			  						</tr>
			  					</tbody>
			  				</table>
			  			</div>
			  		</div>
			  </div>
			</div>

				
			</div>
			
			<div>	
				<!-- NEW Competition ADD -->	
				<div class = "row-fluid"  ng-show = "selectedLeague != null && newCompetition != null">
								<div class="span6 well"  ng-form name="newCompetitionForm">
									<h3 style="margin-bottom:20px;">New competition</h3>
									<div class = "row-fluid">
										<div class = "span8">	
											<input  class="span12" type="text" placeholder="Name" required ng-model="newCompetition.name"> 
										</div>
									</div>

									<div class="row-fluid">
										<h5 >Date</h5>
										<nutdatetimepicker  nutdate = "newCompetition.start_date"
															maxdate = "newCompetition.end_date"
															nutdatechange = "updateNewStartDate">
										</nutdatetimepicker>
									    <nutdatetimepicker  nutdate = "newCompetition.end_date"
									    					mindate = "newCompetition.start_date"
									    					nutdatechange = "updateNewEndDate">
									    </nutdatetimepicker>
									</div>

									<div class="row-fluid">
										<h5 >Priority</h5>
										<div class="span4">
											<input type="range" name="points" min="0" max="10" ng-model="newCompetition.priority">
										</div>
										<div class="span2">
											<span  class="badge badge-info">{{newCompetition.priority}}</span>
										</div>
									</div>

									<div class = "row-fluid">
										<div class = "span3">
											 <button  class="btn  btn-success btn-block" type="button" ng-click="saveNewCompetition()" ng-disabled="newCompetitionForm.$invalid || newCompetition.start_date == '' || newCompetition.end_date == ''">Save</button>
										</div>
									</div>	
								</div>
						<div class="span6">
						</div>
				</div>
			
			</div>	
		</div>

	</div>
</div>

