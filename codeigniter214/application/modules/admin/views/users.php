
<?php 
	css("modules/m_admin/css/users.css"); 
	script("modules/m_admin/js/ng_users.js"); 
?>
<script>
	var siteUsersData = <?php echo $siteUsersData;?>;
	var eventTemplates = <?php echo $eventTemplates;?>;
</script>

<style>
.img18{
 width: 18px;
 height: 18px;
 padding: 2px;
 margin: 2px;
 border-radius: 4px;
 background-color: white;
}

.betItemHeaderLabel{
 padding: 8px 10px !important;
 font-size: 14.844px !important;
}

.betItemGames{
 background-color: rgb(255, 255, 255);
 border-radius: 4px;
 margin-top: 10px;
 padding: 10px;
}
</style>

<div ng-controller = "ActivityCtrl">
	<div ng-init="init()">
	</div>

	<div class="row-fluid">
		<div class="span3">
			<div class="row-fluid">
				<div class="span12 well">
					<div class="input-append span12">
						<input type="text" class="span10"  ng-model="searchtxt" placeholder="Type site name">
						<span class="add-on"><i class="icon-search"></i> </span>
					</div>
				</div>
			</div>
			
			<div class = "row-fluid" style="height: 800px; overflow: auto;">
				<div class="span12 well">
					<h3 ng-show="siteUsersList.length == 0">No Active User</h3>
					<ul class="nav nav-pills nav-stacked nut-sidenav" ng-hide="siteUsersList.length == 0">
						<li ng-repeat="citem in siteUsersList |filter: searchtxt" ng-class="{active: citem.id==selectedSiteUser.id}">
							<a href="#" ng-click = "siteUserSelect(citem)" style="position:relative"> {{citem.domain}} 
								<span class="badge" ng-class="{'badge-success': citem.blocked == 0 && citem.confirmed == 1, 'badge-important': citem.blocked == 1 || citem.confirmed == 0}" style="right:30px; position:absolute">{{getWidgetsCount(citem.id)}}</span>
								<i class="icon-chevron-right pull-right"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="span9" ng-show="selectedSiteUser">
			<div class="row-fluid">
				<div class="span12 well" style="height:100px">
					<div style="position: relative">
						<div style="position: absolute">
							<h1>{{selectedSiteUser.domain}}</h1>
						</div>
						
						<div style="position: absolute; right:150px;">
							<h5>State</h5>
							<button ng-show= "selectedSiteUser.blocked == 0" class="btn btn-small btn-success" ng-click="changeSiteUserState(selectedSiteUser.id, selectedSiteUser.blocked)"><i class="icon-ok icon-white"></i></button>
							<button ng-show= "selectedSiteUser.blocked == 1" class="btn btn-small btn-danger"  ng-click="changeSiteUserState(selectedSiteUser.id, selectedSiteUser.blocked)"><i class="icon-remove icon-white"></i></button>
						</div>						
					</div>
					<div ng-switch on="selectedSiteUser.confirmed" style="position:relative">
						<span ng-switch-when="1" class="label label-success">
							Confirmed
						</span>
						<span ng-switch-when="0" class="label label-important">
							Not Confirmed
						</span>
					</div>
					<div style="width:150px; margin-left:300px">
						<select  ngwidth="100%" data-placeholder="All widgets"
								 ng-model="selectedWidget"
								 ng-change="widgetSelect()"
								 nutchosen="widgetList"
								 nutchosenselected="selectedWidget"
								 ng-options="w as w.name for w in getWidgets()">
				    	</select>
			    	</div>
				</div>
			</div>
			<div ng-show="selectedSiteUser.widgets_count">

				<ul class="nav nav-tabs" id="mytabs">
		            <li class="active" ng-click="showPage('users')"><a href="#usersData" data-toggle="tab">Users({{selectedSiteUser.users_count}})</a></li>
		            <li ng-click="showPage('bets')"><a href="#betsData" data-toggle="tab">Bets({{selectedSiteUser.bets_count}})</a></li>
		            <li ng-click="showPage('challenges')"><a href="#challengesData" data-toggle="tab">Challenges({{selectedSiteUser.challenges_count}})</a></li>
	            </ul>

	            <div class="tab-content">
	            	<div class="tab-pane active" id="usersData" ng-controller = "UsersCtrl">
						<div class="row-fluid">
							<div class="span12 well" style="height: 650px; overflow: auto;">
								<div style="position:relative">
									<div style="position:absolute;">
										<b>User ID:</b> <input type="text" ng-model="search.user_id" class="input-small" nut-enter="makeSearch()"></input>
									</div>
									<div style=" margin-left:200px;">
										<div class="span2">
											<div class="switch" style="width:120px">
												<input type="radio" name="php" id="state-{{item}}" ng-checked="item == filtermodel.state" ng-repeat="item in filtermodel.states" />
				    							<label for="state-{{item}}" ng-click="filterState(item)" ng-class="{checked : item == filtermodel.state}" ng-repeat="item in filtermodel.states">{{item}}</label>
				    						</div>
										</div>
									</div>
									<div>
										<a href="#" style="position:absolute; right:50px"><h5>UsersCount: {{usersCount}}/{{selectedSiteUser.users_count}}</h5></a>
									</div>
								</div>
								
								<div class="row-fluid"><hr size="3px"></hr></div>

								<div class="row-fluid">
									<div class="input-append span4">
										<input type="text" class="span12" ng-model="usertxt" placeholder="Type user name">
										<span class="add-on"><i class="icon-search"></i></span>
									</div>
								</div>

								<div ng-show="usersList.length == 0">
									<h3>No users found</h3>
								</div>

								<div class="row-fluid">
									<table class="table table-hover table-striped table-bordered userTable" ng-hide="usersList.length == 0">
										<thead>
											<th style="cursor:pointer" >ID</th>
											<th style="cursor:pointer" >Name</th>
											<th style="cursor:pointer" >Balance</th>
											<th>Bets</th>
											<th>Challenges</th>
											<th>Widget</th>
											<th>Email</th>
											<th style="cursor:pointer" ng-click="sort('State')">State<i ng-class="{'icon-chevron-up': columnSorting['State']['by'] == 'desc', 'icon-chevron-down': columnSorting['State']['by'] == 'asc'}"></i></th>
											<th>Mode</th>
										</thead>
										<tr ng-repeat="uitem in usersList |filter: usertxt">
											<td><b>{{uitem.id}}</b></td>
											<td><b>{{uitem.first_name}} {{uitem.last_name}}</b></td>
											<td><b>{{uitem.balance}}</b></td>
											<td><b><a href="#" ng-click="showBets(uitem.id)">{{uitem.bets_count}}</a></b></td>
											<td><b><a href="#" ng-click="showChallenges(uitem.id)">{{uitem.challenges_count}}</a></b></td>
											<td><b>{{getWidgetName(uitem.widget_id)}}</b></td>								
											<td><b>{{uitem.email}}</b></td>
											<td>
												<button ng-show= "uitem.blocked == 0" class="btn btn-small btn-success" ng-click="changeUserState(uitem.id, uitem.blocked, 'state')"><i class="icon-ok icon-white"></i></button>
												<button ng-show= "uitem.blocked == 1" class="btn btn-small btn-danger"  ng-click="changeUserState(uitem.id, uitem.blocked, 'state')"><i class="icon-remove icon-white"></i></button>
											</td>
											<td>
												<button ng-show= "uitem.mode == 0" class="btn btn-small btn-success" ng-click="changeUserState(uitem.id, uitem.mode, 'mode')"><i class="icon-user icon-white"></i></button>
												<button ng-show= "uitem.mode == 1" class="btn btn-small btn-warning" ng-click="changeUserState(uitem.id, uitem.mode, 'mode')"><i class="icon-star icon-white"></i></button>
											</td>											
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="betsData" ng-controller="BetsCtrl">
						<div style="position:relative; border:1px dashed gray; padding:5px; height:30px">
							<div style="float:left; margin-right:30px">
								<b>User ID:</b> <input type="text" ng-model="search.user_id" class="input-small" nut-enter="makeSearch()"></input>
							</div>
							<div style="float:left;  margin-right:30px">
								<b>Game ID:</b> <input type="text" ng-model="search.game_id" class="input-small" nut-enter="makeSearch()"></input>
							</div>
							<div>
								<a href="#"><h5>BetsCount: {{betsCount}}/{{selectedSiteUser.bets_count}}</h5></a>
							</div>
						</div>
						<div ng-repeat="(betItemID, betItem) in betsList" style="padding:0px; border-bottom: 1px dashedblack; background-color:rgb(233, 233, 233);"
							 ng-init="wName = widgetList[betItem.widget_id].name">
				            <div>
				            	<table class="table table-hover table-striped table-bordered userTable">
				            	  	<tbody>
						                <tr ng-click="showContent(betItem.id)" >
						                  	<td style="width:50px; height:20px">ID : {{betItem.id}} </td>
						                  	<td style="width:50px">User : {{betItem.user_id}} </td>
						                    <td style="width:50px">Amount: {{betItem.amount}}$</td>
						                    <td style="width:50px">Odd: {{betItem.coefficient}}</td>
						                    <td style="width:50px">Win: {{betItem.won_amount}}$ </td>
						                    <td style="width:50px">Widget : {{wName}} </td>
						                    <td style="width:70px">Time : {{betItem.create_stamp}} </td>
						                </tr>
				              		</tbody>
				                </table>
				                <div class="betItemGames well" ng-show="showID == betItem.id">
				                    <table>
				                      <thead>
				                        <th class="text-left"><b><small>Game</b></small></th>
				                        <th style="width:100px;"><b><small>Selection</b></small></th>
				                        <th style="width:100px;"><b><small>Odd</b></small></th>
				                        <th><b><small>Status</b></small></th>
				                      </thead>
				                      <tbody>
											<tr  ng-repeat="(betEventItemID, betEventItem) in betItem.event_chain">
				                              <td>
				                              	<a <a href="<?php  echo BASE_INDEX . 'admin/main/show/games/{{betEventItem.game_id}}'?>">{{betEventItem.game_id}}</a>
				                              </td>
				                              <td class="text-center"><b><small>{{betEventItem.event_name}}</small></b></td>
				                              <td class="text-center"><b><small>{{betEventItem.event_coefficient}}/{{betEventItem.basis}}</b></small></td>
				                              <td class="text-center"><img ng-src="{{showResultStatusLogo(betEventItem.event_result_status, 0)}}"/></td>
				                          </tr>
				                      </tbody>
				                    </table>
				                </div>
				            </div>
	            		</div>
					</div>
					<div class="tab-pane" id="challengesData" ng-controller="ChallengesCtrl">
						<div style="position:relative; border:1px dashed gray; padding:5px;">
							<div style="float:left; margin-right:30px">
								<b>User ID:</b> <input type="text" ng-model="search.user_id" class="input-small" nut-enter="makeSearch()"></input>
							</div>
							<div style="float:left; margin-right:30px">
								<b>Game ID:</b> <input type="text" ng-model="search.game_id" class="input-small" nut-enter="makeSearch()"></input>
							</div>
							<div>
								<a href="#"><h5>ChallengesCount: {{challengesCount}}/{{selectedSiteUser.challenges_count}}</h5></a>
							</div>
						</div>
						<table class="table table-hover table-striped table-bordered userTable" ng-init="order=true">
							<thead style="background-color:grey">
								<th style="cursor:pointer" ng-click="pred = 'id'; order=!order">ID</th>
								<th style="cursor:pointer" ng-click="pred = 'publisher_id'; order=!order">Publisher</th>
								<th style="cursor:pointer" ng-click="pred = 'amount'; order=!order">Amount</th>
								<th style="cursor:pointer" ng-click="pred = 'opponent_id'; order=!order">Opponent</th>
								<th style="cursor:pointer" ng-click="pred = 'game_id'; order=!order">Game ID</th>
								<th style="cursor:pointer" ng-click="pred = 'widget_id'; order=!order">Widget</th>
								<th style="cursor:pointer" ng-click="pred = 'create_stamp'; order=!order">Time</th>
							</thead>
							<tbody ng-repeat="chItem in challengesList | orderBy:pred:order" ng-init="wName = widgetList[chItem.widget_id].name" >
				            <tr ng-click="showGameInfo(chItem.id)">
			                  	  <td >{{chItem.id}} </td>
			                  	  <td >{{chItem.publisher_id}} </td>
			                  	  <td >{{chItem.amount}} </td>
			                  	  <td >{{chItem.opponent_id}} </td>
			                  	  <td ><a href="<?php  echo BASE_INDEX . 'admin/main/show/games/{{chItem.game_id}}'?>">{{chItem.game_id}}</a></td>
			                      <td >{{wName}} </td>
			                      <td >{{chItem.create_stamp}} </td>
				            </tr>
				            <tr ng-show="chllengeGameInfo == chItem.id">
				            	<td colspan="100%">
				            		<div>
					            		<div style="float:left; padding-right:50px"><a class="label label-info betItemHeaderLabel" href="#" ng-click="showUser(chItem.publisher_id)">User: {{chItem.publisher_id}}</a></div>
					            		<div style="float:left; padding-right:50px"><b>{{getEventTemplateName(chItem.publisher_event)}}</b></div>
					            		<div style="float:left; padding-right:50px">{{chItem.participant1_name}} - {{chItem.participant2_name}}</div>
					            		<div style="float:left; padding-right:50px" ng-show="chItem.opponent_id"><b>{{getEventTemplateName(chItem.opponent_event)}}</b></div>
					            		<div style="float:left; padding-right:50px" ng-show="chItem.opponent_id"><a class="label label-info betItemHeaderLabel" href="#" ng-click="showUser(chItem.opponent_id)">User: {{chItem.opponent_id}}</a></div>
					            		<div style="float:left; padding-right:50px" ng-show="chItem.opponent_id == null">
					            			<span class="label label-info betItemHeaderLabel">Waiting</span>
					            		</div>
				            		</div>
				            	</td>
				            </tr>
	            			</tbody>
	            		</table>					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>