<?php script("modules/m_admin/js/ng_leagues.js"); ?>
<script>
	var leagueData = <?php echo $leagueData;?>;
	var countryData = <?php echo $countryData;?>;
</script>

<script id="removePopover" type="text/ng-template">
	<div>
	    <h5>Are you shure</h5>
	    <button class="btn btn-primary" type = "button" ng-click = "removeListItem(); hide()">Yes</button>
		<button class="btn" type = "button" ng-click = "hide()">No</button>
	</div>
</script>

<div ng-controller = "LeaguesCtrl" id="leaguesView">
		<div ng-init="init()"></div>
		<p>{{errorMessage}}</p>
		<div class="row-fluid">
			<div class="span3">
					<div class="row-fluid">
						<div class="span12 well">
							<div class="row-fluid">
								<div class="span10">
									<div class="input-append span12">
										<input type="text" class="span10"  ng-model="searchtxt" placeholder="Type league name"> 
										<span class="add-on"><i class="icon-search"></i> </span>
									</div>
								</div>
								<div class="span2">
									<button class="btn btn-block btn-success" type="button" ng-click="createNewItem()">New</button>
								</div>
							</div>	
						</div>
					</div>

					<div class = "row-fluid" style="height: 700px; overflow: auto;" >
						<div class="span12 well" >
								<h3 ng-show="list.length == 0">No Leagues</h3>
								<ul id="" class="nav nav-pills nav-stacked nut-sidenav" >
									<li ng-repeat="item in list |filter:searchtxt" ng-class="{active: item.id==selectedItem.id}">
										<a href="#" ng-click = "showItem(item.id, $event)"><i class="icon-chevron-right"></i> {{item.name}}</a>
									</li>
								</ul>
						</div>
					</div>
			</div>

			<div class="span9" id="container">

			<div ng-show="selectedItem != null">	
				<div class="row-fluid">
						<div class="span8">
							<h2> {{selectedItem.name}}</h2>
						</div>
						<div class="span4">
							<button  class="btn btn-danger btn-small" 
							    data-unique="1" 
	          					bs-popover = "'removePopover'" 
	          					data-container="#leaguesView"
	          					data-placement="bottom"
	          					data-trigger = "click">
	          					Remove
	          				</button>
						</div>
				</div>	

				<!-- UPLOADER START -->
				<div class="row-fluid ">
					<div class="span12 well" >
						<div id = "uploadPluginContainer" >
							<nutupload 	uploadaction = "<?php echo BASE_INDEX . 'admin/asset/upload/';              ?>"
										assetpath    = "<?php echo MEDIA_URL  . 'assets/leagues/';                  ?>"
										nophoto 	 = "<?php echo MEDIA_URL  . 'modules/img/no_avatar_medium.gif'; ?>"
										removeaction = "<?php echo BASE_INDEX . 'admin/asset/remove/';              ?>" >
							</nutupload>
						</div>
					</div>
				</div>
				<!-- UPLOADER END -->
	
				<!-- LEAGUE DATA START -->
				<div class="row-fluid" >
				<div class="span12 well">
					<div>
						<div class="row-fluid">
							<div class="span6">
								<h3 style="margin-bottom:20px;">Data</h3>
								<div class = "row-fluid">
									<div class = "span8">	
										<input id="league_name" class="span12" type="text" placeholder="Name" ng-model="leaguemodel.name">
									</div>
								</div>

								<div class="row-fluid">
									<div class="span12 ">
										<h5 >Country</h5>
										<select  ngwidth="40%" data-placeholder="Choose country" ng-model="leaguemodel.country_id" nutchosen="countryList" nutchosenselected="leaguemodel"
											ng-options="c.id as c.name for c in countryList">
											<option></option>
								    	</select>
								    </div>
								</div>

								<div class="height1">
								</div>

								<div class="row-fluid">
									<div class="span6">	
										<h5 >Priority</h5>
										<input type="range" name="points" min="0" max="10" ng-model="leaguemodel.priority">
										<span  class="badge badge-info">{{leaguemodel.priority}}</span>
									</div>
								</div>
						
								<div class="height1">
								</div>

								<div class = "row-fluid">
									<div class = "span3">
										 <button  class="btn  btn-success btn-block" type="button" ng-click="updateListItem()">Save</button>
									</div>
								</div>	
							</div>

							<!-- translations -->	
							<div class="span6">

								<h3 style="margin-bottom:20px;">Translations</h3>
								<div  class="row-fluid" ng-repeat=" (lang, trObject) in leaguemodel.translations">
										<div class="span12">	
											<div class="input-append span8" >
		 										<input size="25" type="text" ng-model="trObject.text" > 
		 										<span class="add-on">{{lang}}</span>
											</div>
										</div>
								</div>		
							</div>
						</div>
					</div>	
				</div>
				</div>
				<!-- LEAGUE DATA END -->	
			</div>
			
			<div>	
				<!-- NEW LEAGUES ADD -->	
				
				<div class = "row-fluid" ng-show = "leaguemodel == null">
						<div class="span6" ng-form name="newLeaguesForm">
							<div  class="well">
								<div>
									<button class="btn btn-large btn-block btn-success" type="button" ng-disabled="!newItemList.length || newLeaguesForm.$invalid" ng-click="saveNewItems()">
										Save All
									</button>
								</div>
									<h1 style="margin-top:50px;margin-bottom:20px;">New Leagues</h1>
								<div>
									<div ng-repeat = "newItem in newItemList">
									<div class="row-fluid" >
										<div class="span6">
											<input class="span12" type="text" required placeholder="Name" ng-model="newItem.name">
										</div>
										<div class="span4">
											<button type="button" class="btn btn-danger delete btn-small" ng-click="removeNewItem($index);">
												<i class="icon-trash icon-white"></i>
												<span>Delete</span>
											</button>
										</div>
									</div>
								</div>
								</div>
							</div>
						</div>
						<div class="span6">
						</div>
				</div>
			
			</div>	
		</div>
	</div>
	</div>
</div>




