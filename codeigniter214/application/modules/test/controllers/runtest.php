<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Runtest extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
		$pData["widget_id"] = 35;
		$pData["site_id"]   = 835;
		$pData["user_id"]   = 54;
		$pData["change"]    = 10;

		$apiData     = nut_api::wrapData($pData);
		$widgetCss   = nut_api::api('users/change_user_balance', $apiData);
		unset($pData["change"]);
		$apiData     = nut_api::wrapData($pData);
		$newBalance  = nut_api::api('users/get_player_balance', $apiData);
		echo $newBalance;
	}	

}
