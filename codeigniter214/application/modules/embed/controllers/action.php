<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// header("P3P: CP = ADM DEV PSA COM OUR STP IND");
use Respect\Validation\Validator as v;

class Action extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(MODULE_EMBED_FOLDER . "/site_model");
		$this->load->model(MODULE_EMBED_FOLDER . "/css_model");
	}

	// its an ajax call
	public function logoutplayer()
	{
		try
		{
			$this->site_model->logoutPlayer();
			echo json_encode(array("action" => "logout"));
		}
		catch(Exception $e)
		{

		}
	}

	// filter games by competition and date
	public function getFilteredGames()
	{
		try
		{
			$postData = $this->input->post();

			$date = element("date",    $postData, time());
			$page = element("page",    $postData, 0);

			$widgetId     = element("widget_id",      $postData, NULL);
			$widgetLangId = element("widget_lang_id", $postData, NULL);
			$bookmakerId  = element("bookmaker_id",   $postData, 0);
			$timezone     = element("timezone",       $postData, 0);

			$from = nut_utils::adjust_timestamp_to_tz($date, $timezone, TRUE);
			$to   = nut_utils::adjust_timestamp_to_tz($date, $timezone, FALSE, TRUE);

			$gamesVars  = array();

            $gamesVars["widget_id"]    = $widgetId;
            $gamesVars["language_id"]  = $widgetLangId;
            $gamesVars["bookmaker_id"] = $bookmakerId;
			$gamesVars["league_id"]    = element("league_id", $postData, null);
			$gamesVars["user_id"]      = $this->site_model->getCurrentPlayerId();
            $gamesVars["from"]   = $from;
            $gamesVars["to"]     = $to;
            $gamesVars["limit"]  = GAME_QTY_AUTHORIZED;
            $gamesVars["offset"] = $page * GAME_QTY_AUTHORIZED;
            $gamesVars["user_timezone"] = $timezone;

			$apiData   = array("chili" => $gamesVars);	
			$gamesData = nut_api::api('games/get_widget_games', $apiData);
			
			echo json_encode($gamesData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}	
	}

	public function bet()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$widgetData = $this->site_model->getWidgetSessionData();

			$postData = $this->input->post();
			$widgetId = $widgetData["id"];

			$postData["widget_id"] = $widgetId;
			$postData["site_id"]   = $widgetData["siteuser_id"];
			$postData["language_id"]  = $widgetData["language_id"];
			$postData["won_amount"]   = 0;
			$postData["user_id"]   = $this->site_model->getCurrentPlayerId();

			$apiData = array(nut_api::$PARAM => $postData);	
			$responseData = nut_api::api('bets/add', $apiData);

			$betId = $responseData["bet_id"];
			$responseData["share_link"] = $this->generateShareLink($widgetId, $betId);
			
			echo json_encode($responseData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}	
	}

	public function updateIntroShow()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$currentUserID = $this->site_model->getCurrentPlayerId();
			$widgetData = $this->site_model->getWidgetSessionData();
			$postData = $this->input->post();

			$updateShow = array();
			$updateShow["site_id"]   = $widgetData["siteuser_id"];
			$updateShow["widget_id"] = $widgetData["id"];
			$updateShow["user_id"]   = $currentUserID;
			$show = element("show", $postData, null);
			if(!is_null($show))
			{
				$updateShow["show"]      = $show;
				$apiData = array(nut_api::$PARAM => $updateShow);
				$responseData = nut_api::api('users/update_show_intro', $apiData);
				echo json_encode($responseData);
			}
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function setShieldOnBet()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$postData = $this->input->post();

			$betId = element("bet_id", $postData, null);
			if($betId)
			{
				$apiData = array(nut_api::$PARAM => array("id" => $betId, "bet_type" => 1));	
				$responseData = nut_api::api('bets/boostbet', $apiData);
				echo json_encode($responseData);
			}
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	private function generateShareLink($widgetId, $betId)
	{
		$platformId = $this->site_model->getCurrentPlayerPlatformId();
		$link = BASE_INDEX . MODULE_EMBED_FOLDER . "/social/betshared/" . $platformId  . "/" . $widgetId  . "/" . $betId;
		return $link;
	}

	public function getUserActivities()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$currentUserID = $this->site_model->getCurrentPlayerId();

			$postData  = $this->input->post();
			$page      = element("page", $postData, 0);
			$page      = intval($page);
			$betOffset = ACTIVITY_BET_OFFSET;

			$widgetData = $this->site_model->getWidgetSessionData();
			$userVars = array();
			$userVars["widget_id"]   = $widgetData["id"];
			$userVars["user_id"]     = $currentUserID;
			$userVars["language_id"] = $widgetData["language_id"];
			$userVars["offset"]      = $page * $betOffset; 
			$userVars["limit"]       = $betOffset; 

			$statsVars = array();
			$statsVars["site_id"]   = $widgetData["siteuser_id"];
			$statsVars["widget_id"] = $widgetData["id"];
			$statsVars["user_id"]   = $currentUserID;

			$apiData = array();
			$apiData["bet_history"]        = array("url"  => "bets/get_user_bets",  "vars"  => $userVars);
			$apiData["challenge_history"]  = array("url"  => "challenge/get_all_by_user", "vars"  => $userVars);
			$apiData["user_stats"]         = array("url"  => "users/get_user_activity_statistics", "vars"  => $statsVars);

			$batchData = nut_api::api('batch/action', array(nut_api::$PARAM => $apiData));
			
			$betHistoryData       = $batchData["bet_history"];
			$challengeHistoryData = $batchData["challenge_history"];

			$activityHistoryData = array();

			foreach ($betHistoryData as $key => &$value) {
				$value["sort_stamp"]   = intval($value["create_stamp"]);
				$value["type"] = "b";
				$activityHistoryData[] = $value;
			}

			foreach ($challengeHistoryData as $key => &$value) {
				$value["sort_stamp"]   = $currentUserID == $value["publisher_id"] ? intval($value["create_stamp"]) : intval($value["accept_stamp"]);
				$value["type"] = "ch";
				$activityHistoryData[] = $value;
			}

			usort($activityHistoryData, "activityHistorySortByDate");
			$retData["activity_history"] = $activityHistoryData;
			$retData["user_stats"]       = $batchData["user_stats"];
			echo json_encode($retData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function createChallenge()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$postData  = $this->input->post();
			//TODO validation

            $data["source_user_id"]   = $this->site_model->getCurrentPlayerId();
            $data["amount"]           = $postData["amount"];
            $data["event_id"]         = $postData["event_id"];
            $data["coefficient"]      = $postData["coefficient"];
            $data["basis"]            = $postData["basis"];
            $data["game_id"]          = $postData["game_id"];
            $data["target_user_id"]   = element("target_user_id", $postData, null);
        
        	$widgetData = $this->site_model->getWidgetSessionData();
			$data["site_id"]   = $widgetData["siteuser_id"];
			$data["widget_id"] = $widgetData["id"];
			$data["language_id"]  = $widgetData["language_id"];
			
			$apiData = array(nut_api::$PARAM => $data);
			$responseData = nut_api::api('challenge/create_challange', $apiData);
			echo json_encode($responseData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function getNextChallenge()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$offset = nut_session::get("challenge_offset", 0);
			$offset = intval($offset) + 1;
			nut_session::set("challenge_offset", $offset);

			$widgetData = $this->site_model->getWidgetSessionData();
            
            $data = array();
            $data["user_id"]      = $this->site_model->getCurrentPlayerId();
            $data["language_id"]  = $widgetData["language_id"];
            $data["widget_id"]    = $widgetData["id"];
            $data["offset"]       = $offset;

			$apiData = array(nut_api::$PARAM => $data);
			$responseData = nut_api::api('challenge/get_public_challenge', $apiData);
			if(array_key_exists('block', $responseData)) {
				nut_session::set("challenge_offset", 0);
				$data["offset"] = 0;
				$apiData = array(nut_api::$PARAM => $data);
				$responseData = nut_api::api('challenge/get_public_challenge', $apiData);
			}
			echo json_encode($responseData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function acceptChallenge()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$postData  = $this->input->post();

			// v::arr()->key('opponent_user_id', v::numeric())
			// 	    ->key('challenge_id',	  v::numeric())
			// 	    ->key('event_id', 		  v::numeric())
			// 	    ->key('coefficient', 	  v::float())
			// 	    ->key('basis', 		      v::float())
			// 	    ->check($postData);

			$widgetData = $this->site_model->getWidgetSessionData();
			//TODO validation
            $data["opponent_user_id"] = $this->site_model->getCurrentPlayerId();
            $data["challenge_id"]     = $postData["challenge_id"];
            $data["event_id"]         = $postData["event_id"];
            $data["coefficient"]      = $postData["coefficient"];
            $data["basis"]            = $postData["basis"];

            $data["widget_id"]        = $widgetData["id"];
            $data["language_id"]  	  = $widgetData["language_id"];
			
			$apiData = array(nut_api::$PARAM => $data);
			$responseData = nut_api::api('challenge/accept_challenge', $apiData);
			echo json_encode($responseData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function getOpponents()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

			$postData  = $this->input->post();

			$widgetData = $this->site_model->getWidgetSessionData();

			$data = array();
			$data["site_id"]    = $widgetData["siteuser_id"];
			$data["widget_id"]  = $widgetData["id"];
            $data["user_id"]    = $this->site_model->getCurrentPlayerId();
            $offset 			= element("offset",  $postData, 0);
            $data["limit"]      = USERS_LIMIT;
            $data["offset"]     = $offset * USERS_LIMIT;
            $data["search"]     = element("search",  $postData, null);

            //TODO:: 
            $data["game_id"]    = element("game_id",  $postData, null);
            $data["event_id"]   = element("event_id", $postData, null);
			
			$apiData = array(nut_api::$PARAM => $data);
			$responseData = nut_api::api('users/filter_opponents', $apiData);
			echo json_encode($responseData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}

	public function getLeaderboard()
	{
		try
		{
			nut_session::init();
			Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');
			
			$widgetData = $this->site_model->getWidgetSessionData();

			$postData  = $this->input->post();
			$page      = element("page", $postData, 0);
			$page      = intval($page);
			$limit = TOPS_LIMIT;

			$data = array();
			$data["widget_id"]  = $widgetData["id"];
            $data["user_id"]    = $this->site_model->getCurrentPlayerId();
            $data["limit"]      = $limit;
            $data["offset"]     = $page * $limit;
            // $data["site_id"]    = $widgetData["siteuser_id"];
			
			$apiData = array(nut_api::$PARAM => $data);
			$responseData = nut_api::api('widgetdata/get_leaderboard', $apiData);
			echo json_encode($responseData);
		}
		catch(Api_exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
		}		
		catch(Exception $e)
		{
			nut_utils::print_error($e->getMessage(), $e->getCode());
		}
	}
}

function activityHistorySortByDate($a, $b)
{
    return intval($b["sort_stamp"]) - intval($a["sort_stamp"]);
}

