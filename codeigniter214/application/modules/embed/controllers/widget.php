<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

header("P3P: CP = ADM DEV PSA COM OUR STP IND");
// header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

use Respect\Validation\Validator as v;

class widget extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(MODULE_EMBED_FOLDER . "/site_model");
		$this->load->model(MODULE_EMBED_FOLDER . "/css_model");
	}

	public function cookiefix(){
		nut_session::init();
		$url = isset($_GET["fixuri"]) ? $_GET["fixuri"] : null;
		header("Location:" . $url);
	}

	// LOADER OF INITIAL JAVASCRIPT LOADER, WHICH CREATES AN IFRAME 
	public function loader($widgetId = 0)
	{
		try
		{
			$sid = isset($_COOKIE["PHPSESSID"]) ? $_COOKIE["PHPSESSID"] : null;

			if ($sid) 
			{
				session_id($sid);
				session_start();
			}
			else
			{
				 $headers = getallheaders();
				 $referer = isset($headers["Referer"]) ?  urlencode($headers["Referer"]) : null;
    			 echo "top.location = '" . BASE_INDEX .  MODULE_EMBED_FOLDER . '/widget/cookiefix?fixuri=' . $referer . "';";
    			 exit();
			}

			$widgetId = intval($widgetId);
			v::numeric()->check($widgetId);
			$apiData    = nut_api::wrapData(array("id" => $widgetId));
			$widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);

			$data = array();
			$data['lang']             = 1;
			$data['width']            = 800;
			$data['height']           = 600;
			$data["widgetSelector"]   = ".chili_widgets";
			$data["widgetUrlDefault"] = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/siteCheater";

			if(isset($widgetData["id"]))
			{
				$siteID     =  $widgetData["siteuser_id"];
				$domain     =  $widgetData["domain"];
				$langId     =  $widgetData["language_id"]; 
				$width      =  $widgetData["width"];
				$height     =  $widgetData["height"];
				$widgetType =  intval($widgetData["widget_type"]);

				$widgetSelector  = ".chili_widgets";

				$data['lang']   = $langId;
				$data['width']  = $width;
				$data['height'] = $height;

				$ob       = array("id" => $siteID);
				$apiData  = array("chili" => $ob);
				$siteData = nut_api::api('siteusers/get_by_id', $apiData);
				
				$isConfirmed  = $siteData["confirmed"] == 1;
				$isNotBlocked = $siteData["blocked"]   == 0;

				if(!$isNotBlocked)
				{
					$data["widgetSelector"]   = $widgetSelector;
					$data["widgetUrlDefault"] = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/siteBlocked";
					$this->load->view(MODULE_EMBED_FOLDER . "/js/load_error_js.php", $data);
					exit();
				}

				if(!$isConfirmed)
				{
					$data["widgetSelector"]   = $widgetSelector;
					$data["widgetUrlDefault"] = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/siteNotConfirmed";
					$this->load->view(MODULE_EMBED_FOLDER . "/js/load_error_js.php",  $data);
					exit();
				}

				switch($widgetType)
				{
					case WIDGET_GAME:
						$this->site_model->setWidgetSessionData($widgetData);
						$widgetSelector = "#chili_game_" . $widgetId;
						break;	
					default:
						//EXIT CASE
						$data["widgetSelector"]   = $widgetSelector;
						$data["widgetUrlDefault"] = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/siteCheater";
						$this->load->view(MODULE_EMBED_FOLDER . "/js/load_error_js.php", $data);
						exit();
				}
				
				$data["siteData"]          = $siteData;
				$data["widgetSelector"]    = $widgetSelector;
				$data["widgetUrlDefault"]  = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/originError";
				$data["widgetUrlOrigin"]   = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/render";
				$data['originValidateURL'] = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/publisher/" . $widgetId . "/" . rand(1,100000);
				
				$this->load->view(MODULE_EMBED_FOLDER . "/js/load_widget_js.php", $data);
			}
			else
			{
				// NO WIDGET WITH AN ID IS FOUND
				$this->load->view(MODULE_EMBED_FOLDER . "/js/load_error_js.php", $data);
				return;
			}
		}
		catch(InvalidArgumentException $e)
		{
			$data["widgetSelector"]       = '.chili_widgets';
			$data["widgetUrlDefault"]     = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/error";
			$this->load->view(MODULE_EMBED_FOLDER . "/js/load_error_js.php", $data);
		}
		catch(Exception $e)
		{
			$data["widgetSelector"]       = '.chili_widgets';
			$data["widgetUrlDefault"]     = BASE_INDEX . MODULE_EMBED_FOLDER . "/widget/error";
			$this->load->view(MODULE_EMBED_FOLDER . "/js/load_error_js.php", $data);
		}
	}

	//$randUrl is just for generating new urls for non caching
	// HAVE NO SESSION HERE 
    public function publisher($widgetId = 0, $randUrl = 1)
    {
        try
        {
        	$apiData    = nut_api::wrapData(array("id" => $widgetId));
			$widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);

        	$domain = $widgetData["domain"];
    		$headers = getallheaders();

    		if(isset($headers["Origin"]) &&   strpos($headers["Origin"], $domain) !== FALSE)
            {
                // header("Access-Control-Allow-Origin: *");
                header("Access-Control-Allow-Origin: " .  $headers["Origin"]);
                header("Access-Control-Request-Method: POST, GET");
                // header("Access-Control-Max-Age: 3600");
                header("Content-type: text/plain");
            }
            else
            {
            	nut_log::log("error", "Origin not validated " . $widgetId);
            }
        }
        catch(Exception $e)
        {
        	nut_log::log("error", "embed/widget/publisher");
        }
    }

	//RENDER THE PARTICULAR WIDGET
	// $lang 1 is for English
	public function render($tz = 0)
	{
		try
		{
			$sid = isset($_COOKIE["PHPSESSID"]) ? $_COOKIE["PHPSESSID"] : null;
			
			if($sid)
			{
				session_id($sid);
				session_start();
			}
			else
			{
				nut_session::init();
			}
			
			nut_session::set("user_time_zone", $tz);

			// $apiData    = nut_api::wrapData(array("id" => 35));
			// $widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);

			// #### VALIDATIONS START ####	
			$widgetData = $this->site_model->getWidgetSessionData();

			$siteId      = element("siteuser_id", $widgetData, NULL);
			$height      = element("height",      $widgetData, NULL);
			$width       = element("width",       $widgetData, NULL);
			$lang        = element("language_id", $widgetData, NULL);
			$widgetType  = element("widget_type", $widgetData, NULL);
			
	        if($siteId && $height && $width && $lang && $widgetType)
	        { 
	        	//here we also expect to get if player is new user
	        	// so DURING SETTING DATA SET IT ALSO
				
				$playerId         = $this->site_model->getCurrentPlayerId();
				$playerPlatformId = $this->site_model->getCurrentPlayerPlatformId();
				
				$playerData = array();
				$isPlayerNewbie = null;
				
				if($playerId && $playerPlatformId)
				{
					$widgetId = $widgetData["id"];
					$apiData = array(nut_api::$PARAM => array("site_id" => $siteId, "widget_id" => $widgetId, "user_id" => $playerId));
					$playerData = nut_api::api('users/get_player_profile', $apiData);
					// TODO
					$playerData["player_id"]   = $playerId;
					$playerData["platform_id"] = $playerPlatformId;
					$isPlayerNewbie  = element("is_newbie", $playerData, FALSE);
				}

				switch(intval($widgetType))
				{
						case WIDGET_GAME :
							$this->renderGame($playerData, $widgetData);
							break;	
						default:
							//exit case
							$this->load->view(MODULE_EMBED_FOLDER . "/error/error_cheater_site.php");
							exit();
				}
	        }
	        else
	    	{
	    		$this->load->view(MODULE_EMBED_FOLDER . "/error/error_cheater_site.php");
	    	}
		}
		catch(Exception $e)
		{
			$this->load->view(MODULE_EMBED_FOLDER . "/error/error_exception.php");
		}
	}

	//in case someone wants to cheat
	public function siteNotConfirmed()
	{
		$this->load->view(MODULE_EMBED_FOLDER . "/error/error_notconfirmed.php");
	}

	public function siteBlocked()
	{
		$this->load->view(MODULE_EMBED_FOLDER . "/error/error_blocked.php");
	}

	public function originError()
	{
		$this->load->view(MODULE_EMBED_FOLDER . "/error/error_origin.php");
	}

	public function siteCheater()
	{
		$this->load->view(MODULE_EMBED_FOLDER . "/error/error_cheater_site.php");
	}

	public function error()
	{
		$this->load->view(MODULE_EMBED_FOLDER . "/error/error_exception.php");
	}

	private function renderGame($playerData, $widgetData)
	{
		try
		{
			$data = array();
			$siteId           = element("siteuser_id", $widgetData, NULL);
			$playerId         = element("player_id",   $playerData, NULL);	
			$widgetId         = $widgetData["id"];
			$widgetLanguageId = $widgetData["language_id"];
		
			$gameCount = GAME_QTY_UNAUTHORIZED;
			$widgetData["playerData"] = array();
			if($playerId)
			{
				$gameCount = GAME_QTY_AUTHORIZED;
				$widgetData["playerData"] = $playerData;
				nut_session::set("challenge_offset", 0);
			}

			// GETING GAME DATA
			// competiton vars
			$leaguesVars = array();
			$leaguesVars["widget_id"]   = $widgetData["id"];
			$leaguesVars["language_id"] = $widgetLanguageId;

			$competitionVars = array();
			$competitionVars["widget_id"]   = $widgetData["id"];
			$competitionVars["language_id"] = $widgetLanguageId;

			//TODO create config for sport types	
			$eventsVars = array("sport_id" => 1, "language_id" => $widgetLanguageId);
			
			//GAMES VARS
			$timezone = intval(nut_session::get('user_time_zone'));

			$from =nut_utils::adjust_timestamp_to_tz(time(), $timezone, TRUE);
			$to   =nut_utils::adjust_timestamp_to_tz(time(), $timezone, FALSE, TRUE);

			$gamesVars  = array();
            $gamesVars["widget_id"]    = $widgetData["id"];
            $gamesVars["language_id"]  = $widgetLanguageId;
            $gamesVars["bookmaker_id"] = $widgetData["bookmaker_id"];
            $gamesVars["league_id"]    = null;
            //$gamesVars["competitions"] 	   = null;
            $gamesVars["user_id"]      = $playerId;
            $gamesVars["from"]   = $from;
            $gamesVars["to"]     = $to;
            $gamesVars["limit"]  = 0;
            $gamesVars["offset"] = $gameCount;
            $gamesVars["user_timezone"] = $timezone;

			$challengeVars = array();
			$challengeVars["widget_id"]   = $widgetData["id"];
			$challengeVars["user_id"]     = $playerId;
			$challengeVars["language_id"] =  $widgetLanguageId;
			$challengeVars["offset"]      = 0;

			$apiData = array();
			$apiData["leagues"]		   = array("url"  => "widgetdata/get_widget_leagues",  "vars"  => $leaguesVars);
			$apiData["competitions"]   = array("url"  => "widgetdata/get_widget_competitions",  "vars"  => $competitionVars);
			$apiData["eventTemplates"] = array("url"  => "chilievents/get_all_event_templates", "vars"  => $eventsVars);
			$apiData["games"]          = array("url"  => "games/get_widget_games", "vars" => $gamesVars);
			$apiData["challenges"]     = array("url"  => "challenge/get_public_challenge", "vars"  => $challengeVars);

			$batchData = nut_api::api('batch/action', array("chili" => $apiData));


			$widgetData["games"] 		  = $batchData["games"];
			$widgetData["leagues"]   	  = $batchData["leagues"];
			$widgetData["competitions"]   = $batchData["competitions"];
			$widgetData["eventTemplates"] = $batchData["eventTemplates"];
			$widgetData["challenges"]     = $batchData["challenges"];

			//merge user css with default ones
			$widgetData["cssdata"] = $this->css_model->mergeCSS($widgetData["css"]); 


			$langCode = "en";
			switch($widgetData["language_id"])
			{
				case 3:
					$langCode = "am";
					break;				
				case 2:
					$langCode = "ru";
					break;
				default:
					$langCode = "en";
			}

			
			$translationData = chili_translations::load_translations('embed/game_view', $langCode);
			$translationData["WIDGET_LANG_CODE"] = $langCode;

			$widgetData = array_merge($widgetData, $translationData);

			$data["JSON_GAME_DATA"]         = $this->load->view(MODULE_EMBED_FOLDER . "/game/game_json_data.php",      $widgetData, true);
			$data["SOCIAL_LOGIN_CONTENT"]   = $this->load->view(MODULE_EMBED_FOLDER . "/game/social_login_view.php",   $widgetData, true);
			$data["PROFILE_HEADER_CONTENT"] = $this->load->view(MODULE_EMBED_FOLDER . "/game/profile_header_view.php", $widgetData, true);
			$data["CHALLENGES_CONTENT"]     = $this->load->view(MODULE_EMBED_FOLDER . "/game/challenges_view.php",     $widgetData, true);
			$data["GAMES_CONTENT"]          = $this->load->view(MODULE_EMBED_FOLDER . "/game/game_list_view.php",      $widgetData, true);
			$data["ACTIVITY_CONTENT"]       = $this->load->view(MODULE_EMBED_FOLDER . "/game/activity_view.php",       $widgetData, true);
			$data["LEADERBOARD_CONTENT"]    = $this->load->view(MODULE_EMBED_FOLDER . "/game/leaderboard_view.php",    $widgetData, true);
			$data["HELP_CONTENT"]    		= $this->load->view(MODULE_EMBED_FOLDER . "/game/help_view.php",    	   $widgetData, true);
			
			$this->load->view(MODULE_EMBED_FOLDER . "/widget_main_view.php", $data);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}


