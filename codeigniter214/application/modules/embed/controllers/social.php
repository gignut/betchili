<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Social extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
	}

	public function betshared($platformId = null, $widgetId = null, $betId = null)
	{
		try
		{
			validator::numeric()->check($platformId);
			validator::numeric()->check($widgetId);
			validator::numeric()->check($betId);

			$apiData    = nut_api::wrapData(array("id" => $widgetId));
            $widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);
            $widgetData["imageLink"] = BASE_INDEX . MODULE_EMBED_FOLDER . "/social/betsharedphoto/" . $platformId . "/" . $widgetId  . "/" . $betId;
            $widgetData["link"]      = BASE_INDEX . MODULE_EMBED_FOLDER . "/social/betshared/"      . $platformId . "/" . $widgetId  . "/" . $betId;
         	
            switch ($platformId) {
            	case 1:
            		$widgetData["title"]  = "I have placed a bet | " . $widgetData["domain"];
            		$widgetData["description"] = "Do you think I'll win ?";
            		break;
            	case 3:
            		$widgetData["title"] = "Я сделал ставку |" . $widgetData["domain"];
            		$widgetData["description"] = "Как вы думайте я виграю ?";
            		break;
            }

            $widgetData["width"]  = $this->getPlatformImgWidth($platformId);
            $widgetData["height"] = $this->getPlatformImgHeight($platformId);

         	$this->load->view(MODULE_EMBED_FOLDER . "/social/betshared_link_view.php", $widgetData);
		}
		catch(InvalidArgumentException $e)
		{
		
		}
		catch(Exception $e)
		{

		}
	}

	public function betsharedphoto($platformId = null, $widgetId = null, $betId = null)
	{
		$widgetData = array();

		$apiData = nut_api::wrapData(array("id" => $betId));
		$betData = nut_api::api('bets/get_by_id', $apiData);

		$eventChain = $betData["event_chain"];
		
		$gameIds = array();
		foreach ($eventChain as $chainItem) {
			$gameIds[] = $chainItem["game_id"]; 
		}

		$apiData   = nut_api::wrapData(array("ids" => $gameIds, "aux_info" => array("participants") ));
		$gamesData = nut_api::api('games/get_by_ids', $apiData);

		$pictureData = array();
		$pictureData["gamesData"] = $gamesData;
		$pictureData["bets"]      = $betData;

		$langCode = "en";
		$translationData = chili_translations::load_translations(MODULE_EMBED_FOLDER .'/game_view', $langCode);
		$pictureData = array_merge($pictureData, $translationData);

		$betCardHtml = null;

		switch ($platformId) {
        	case 1:
        		$betCardHtml = $this->load->view(MODULE_EMBED_FOLDER . "/social/fb_betshared_photo_view.php", $pictureData, true);
        		break;
        	case 3:
        		$betCardHtml = $this->load->view(MODULE_EMBED_FOLDER . "/social/odn_betshared_photo_view.php", $pictureData, true);
        		break;
        }

        $w = $this->getPlatformImgWidth($platformId);
        $h = $this->getPlatformImgHeight($platformId);

		//$this->load->view(MODULE_EMBED_FOLDER . "/social/fb_betshared_photo_view.php", $pictureData);
		
		html2imgwin($betCardHtml, $w, $h);
	}

	private function getPlatformImgWidth($platformId)
	{
		switch ($platformId) 
		{
			case 1:
				return 1200;
			case 3:
				return 1200;
		}
	}

	private function getPlatformImgHeight($platformId)
	{
		switch ($platformId) 
		{
			case 1:
				return intval(1200 /1.91);
			case 3:
				return 800;
		}
	}
}
