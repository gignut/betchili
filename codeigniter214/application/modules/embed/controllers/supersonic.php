<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Supersonic extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(MODULE_EMBED_FOLDER . "/supersonic_model");
	}

	public function rewardBoints()
	{
		nut_session::init();
		Modules::run(MODULE_EMBED_FOLDER . '/wauth/widgetAuthorizedUser');

		$widgetData = $this->site_model->getWidgetSessionData();
		$postData = $this->input->post();

		$pData = array();
		$pData["widget_id"] = $widgetData["id"];
		$pData["site_id"]   = $widgetData["siteuser_id"];
		$pData["user_id"]   = $this->site_model->getCurrentPlayerId();
		$pData["change"]    = element("reward", $postData, 0);

		$apiData     = nut_api::wrapData($pData);
		$widgetCss   = nut_api::api('users/change_user_balance', $apiData);
		unset($pData["change"]);
		$apiData     = nut_api::wrapData($pData);
		$newBalance  = nut_api::api('users/get_player_balance', $apiData);
		$retBalance  = array('balance' => $newBalance);
        echo json_encode($retBalance);
	}

	
	public function css($widgetId = null)
	{
		if($widgetId)
		{
			$this->load->model(MODULE_EMBED_FOLDER . "/css_model");
			$apiData   = nut_api::wrapData(array("widget_id" => $widgetId));
			$widgetCss = nut_api::api('widgetdata/get_widget_css', $apiData);
			$cssData = array();
			$cssData["css"] = $this->css_model->mergeCSS($widgetCss);
			$cssTxt = $this->load->view(MODULE_EMBED_FOLDER . "/supersonic/style.php", $cssData, true);
		
			echo $cssTxt;
		}
	}
	
	public function getoffers()
	{
		$this->load->model(MODULE_EMBED_FOLDER . "/site_model");
		
		nut_session::init();
		
		/*
		$widgetData = $this->site_model->getWidgetSessionData();
		$widgetId   = $widgetData["id"];
		$siteId     = $widgetData["siteuser_id"];
		*/
		
		$userIP = get_ip_address();
		$userId = $this->site_model->getCurrentPlayerId();
		$responseData = array("error" => 1);
		if($userIP && $userId)
		{
			$getCampaignUrl = SUPERSONIC_API_URL . "?applicationUserId=" . $userId . 
         									       "&applicationKey=" . SUPERSONIC_APP_ID . 
         									       "&ip=" . $userIP . 
         									       "&accessKey=" . SUPERSONIC_ACCESS_KEY .
											       "&secretKey=" . SUPERSONIC_SECRET_KEY;

			$ch = curl_init($getCampaignUrl);
			curl_setopt($ch, CURLOPT_POST, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);
			$responseData = json_decode($result, true);
			
			echo json_encode($responseData);
		}
		else
		{
			echo json_encode($responseData);
			nut_log::log("error", "SUPERSONIC hasoffer error: " . " userid = " . $userIP . " IP: " . $userIP);
		}
	}

	public function grant()
	{
		$getParams = $_GET;
		nut_log::log("error", $getParams);
		
		// get the variables 
		
		$userId     = $_GET['applicationUserId']; 
		$eventId    = $_GET['eventId']; 
		$rewards    = $_GET['rewards']; 
		$signature  = $_GET['signature']; 
		$timestamp  = $_GET['timestamp']; 
		$privateKey = SUPERSONIC_PRIVATE_KEY; 
		 
		// validate the call using the signature 
		if (md5($timestamp.$eventId.$userId.$rewards.$privateKey) != $signature){ 
		 nut_log::log("error", "SUPERSONIC ERROR: " . json_encode($getParams) . "\n");
		 echo "Signature doesn’t match parameters"; 
		 return; 
		} 
		 
		// check that we haven't processed the very same event before 
		if (!$this->supersonic_model->alreadyProcessed($eventId)){ 
		 
		 // grant the rewards 
		 $this->supersonic_model->doProcessEvent($eventId, $userId, $rewards); 
		} 
		 
		// return ok 
		echo $eventId.":OK"; 
		
	}
	
	public function revoke()
	{

	}
}


function get_ip_address() {
    $ip_keys = array('HTTP_CLIENT_IP', 
					 'HTTP_X_FORWARDED_FOR', 
					 'HTTP_X_FORWARDED', 
					 'HTTP_X_CLUSTER_CLIENT_IP', 
					 'HTTP_FORWARDED_FOR', 
					 'HTTP_FORWARDED', 
					 'REMOTE_ADDR');
    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                // trim for safety measures
                $ip = trim($ip);
                // attempt to validate IP
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        }
    }
 
    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}
 
 
/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
        return false;
    }
    return true;
}

