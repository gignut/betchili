<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'libraries/google-api-php-client/src/Google_Client.php';  
require_once APPPATH . 'libraries/google-api-php-client/src/contrib/Google_PlusService.php';  
 
use Respect\Validation\Validator as v;

class Wauth extends MX_Controller
{
    function __construct()
    {
        nut_session::init();
        parent::__construct();
        $this->load->model(MODULE_EMBED_FOLDER . "/site_model");
        $this->load->helper('array');
    }

    // CHECK IF WIDGET USER IS AUTHORIZED
    public function widgetAuthorizedUser()
    {
        $userId = $this->site_model->getCurrentPlayerId();
        //$widgetData = $this->site_model->getWidgetSessionData();
        if($userId)
        {
            return $userId;
        }
        else
        {
            echo json_encode(array("not_authorized_user" => 1));
            exit();
        }
    }

    //avar 16 JUNE 2013
    public function player($paramWidgetId = null)
    {
        $retData = array("error"=> 1, "action" => "", "message" => "");

        $platformId   = intval($this->input->post("platform_id"));
        $platformData = $this->input->post("data");

        if(!$platformId || !$platformData) {echo json_encode($retData); exit(); }

        $userData = array();
        switch($platformId)
        {
            case PLATFORM_FACEBOOK:
                $userData = $this->getUserDataFacebook($platformData);
                break;
            case PLATFORM_GOOGLE:
                $userData = $this->getUserDataGoogle($platformData);
                break;
        }

        $gameData = $this->socialUser($paramWidgetId, $platformId , $userData);

        echo $gameData;
    }

    // authorizes odnoklassniki
    public function odnoklassniki($paramWidgetId = null)
    {
        require_once APPPATH . 'libraries/socialoauth2/odnoklassniki.php'; 

        $AUTH_CALLBACK = BASE_INDEX . "auth/odnoklassniki/" . $paramWidgetId . "/";
        $config = array("app_id" => ODN_APP_ID, "app_secret" => ODN_APP_PRIVATE_KEY, "app_public_key" => ODN_APP_PUBLIC_KEY, "redirect_uri" => $AUTH_CALLBACK );
        $odn = new Odnoklassniki($config);
        $authPath =  $odn->getAuthPath();

        $code = element("code", $_GET, null);   
        if($code) 
        {
            try
            {
                $authData = $odn->init($code);
                $responseData = $odn->api("users.getCurrentUser"); 

                $userData = $odn->transformUserData($responseData);
                $gameData = $this->socialUser($paramWidgetId, PLATFORM_ODNOKLASSNIKI, $userData);

                $userDataJson = json_encode($userData);
                echo "<script> window.opener.authorized_VK_ODN_MAILRU(" . PLATFORM_ODNOKLASSNIKI . ", '" . $gameData . "'); window.close();</script>";
            }
            catch(Exception $e){
                nut_log::log("error", "Exception from wauth/odnoklassniki");
            }
        } 
        else 
        {
            header('Location: ' .  $authPath);
        }
    } 

    public function vkontakte($paramWidgetId = null)
    {
        require_once APPPATH . 'libraries/socialoauth2/vkontakte.php'; 

        $AUTH_CALLBACK = BASE_INDEX . "auth/vkontakte/" . $paramWidgetId . "/";
        $config = array("app_id" => VK_APP_ID, "app_secret" => VK_APP_SECRET, "redirect_uri" => $AUTH_CALLBACK , "scope" => "friends,email");

        $vk = new Vkontakte($config);
        $authPath =  $vk->getAuthPath();
        
        $code = element("code", $_GET, null);   
        if($code) 
        {
            try
            {
                $authData = $vk->init($code);
                $uid      = $vk->getUserId();
               
                $responseData = $vk->api("users.get", array("user_ids" =>  $uid, "fields" => "uid, first_name, last_name, photo_50, sex")); 
                $userData     = $vk->transformUserData($responseData);
                $gameData     = $this->socialUser($paramWidgetId, PLATFORM_VKONTAKTE, $userData);

                echo "<script> window.opener.authorized_VK_ODN_MAILRU(" . PLATFORM_VKONTAKTE .", '" . $gameData . "'); window.close();</script>";
            }
            catch(Exception $e)
            {
                nut_log::log("error", "Exception from wauth/vkontakte");
            }
        } 
        else 
        {
            header('Location: ' . $authPath);
        }
    } 

    public function mailru($paramWidgetId = null)
    {
        require_once APPPATH . 'libraries/socialoauth2/mailru.php'; 

        $AUTH_CALLBACK = BASE_INDEX . "auth/mailru/" . $paramWidgetId . "/";
        $config = array("app_id" => MAILRU_APP_ID, "app_secret" => MAILRU_APP_SECRET, "redirect_uri" => $AUTH_CALLBACK , "scope" => "");

        $mr = new Mailru($config);
        $authPath =  $mr->getAuthPath();
        
        $code = element("code", $_GET, null);   
        if($code) 
        {
            try
            {
                $authData = $mr->init($code);
                $uid      = $mr->getUserId();
                $responseData = $mr->api("users.getInfo", array("uids" =>  $uid)); 
                $userData =  $mr->transformUserData($responseData);
                $gameData = $this->socialUser($paramWidgetId, PLATFORM_MAILRU, $userData);

                echo "<script> window.opener.authorized_VK_ODN_MAILRU(" . PLATFORM_MAILRU .", '" .  $gameData  . "'); window.close();</script>";
            }
            catch(Exception $e)
            {
                nut_log::log("error", "Exception from wauth/mailru");
            }
        } 
        else 
        {
            header('Location: ' . $authPath);
        }
    } 

    private function getUserDataFacebook($data)
    {
        try
        {
            $token = $data['accessToken'];
            $userID = $data['userID'];
            $config =  array('appId'  =>  FB_APP_ID , 'secret' => FB_APP_SECRET);
            
            $this->load->library(LIB_FACEBOOK . 'Facebook', $config, 'facebook');
            
            $userProfile = $this->facebook->api('/'. $userID . "?access_token=" . $token);

            $data = array();
            //TODO::artak PLATFORM_FACEBOOK
            $data['platform_id']  = PLATFORM_FACEBOOK; 
            $data['platform_uid'] = $userID; 
            $data['first_name']   = element('first_name', $userProfile, ""); 
            $data['last_name']    =  element('last_name', $userProfile, ""); 
            $data['image']        = "https://graph.facebook.com/" . $userID . "/picture";
            $data['gender']       = element('gender', $userProfile, "");
            $data['email']        =  element('email', $userProfile, ""); 
            $data['timezone']     = element('timezone', $userProfile, "");
            $data['blocked']      = 0; 
            return $data;
         }
        catch(Api_exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode());
        }
    }

    private function getUserDataGoogle($gData)
    {
        $data = array();

        $data['platform_id']  = PLATFORM_GOOGLE; 
        $data['platform_uid'] = $gData["id"]; 
        $data['first_name']   = $gData["name"]["familyName"];
        $data['last_name']    = $gData["name"]["givenName"];
        $data['image']        = $gData["image"]["url"]; 
        $data['gender']       = $gData["gender"]; 
        $data['email']        = $gData["emails"][0]["value"];
        $data['blocked']      = 0; 
        return $data;
    }

    private function socialUser($paramWidgetId, $platformId, $userData)
    {
        try
        {
            $retData = array("error"=> 1, "action" => "", "message" => "");

            $widgetData = $this->site_model->getWidgetSessionData();
            
            if(!$widgetData)
            {
                    $paramWidgetId = intval($paramWidgetId);
                    v::numeric()->check($paramWidgetId);
                    $apiData    = nut_api::wrapData(array("id" => $paramWidgetId));
                    $widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);
                    $this->site_model->setWidgetSessionData($widgetData);
            }

            $widgetId    = element("id", $widgetData, NULL);
            $siteId      = element("siteuser_id", $widgetData, NULL);

            if(!$siteId) {echo json_encode($retData); exit(); }

            $siteBalance = $this->site_model->getSiteBalanceConfig();

            $ob = array("site_id" => $siteId, "widget_id" => $widgetId);
            $ob["user_id"]     =  $userData["platform_uid"];
            $ob["platform_id"] =  $userData["platform_id"];
            $ob["language_id"] =  $widgetData["language_id"];

            $chiliPost = array(nut_api::$PARAM => $ob);
            $data      = nut_api::api('users/init_player', $chiliPost);
               
            //PLAYER DATA AFTER INIT 
            $playerData       = $data["player_data"];
            $chiliPlatformUID =   element("id",  $playerData, NULL);

            if(!$chiliPlatformUID)
            {
                //need to get data and register user
                $userData['site_id']   = $siteId;
                $userData['widget_id'] = $widgetId;
                $userData['balance']   = PLAYER_INITIAL_BALANCE;
                $userData['blocked']   = 0;
            
                $chiliPost  = array(nut_api::$PARAM => $userData);
                //chili user id of registered user
                $playerData = nut_api::api('users/add' , $chiliPost);
                $chiliPlatformUID =   element("id",  $playerData, NULL);
            }

            //TODO
            $playerData["platform_id"] =  $platformId;
            $playerData["player_id"]   =  $playerData["id"];

            $timezone = intval(nut_session::get('user_time_zone'));
            $from = mktime(0,0,0);
            $to   = mktime(23,59,59);

            $gamesVars  = array();
            $gamesVars["widget_id"]    = $widgetData["id"];
            $gamesVars["language_id"]  = $widgetData["language_id"];
            $gamesVars["bookmaker_id"] = $widgetData["bookmaker_id"];
            $gamesVars["league_id"]    = null;
            $gamesVars["user_id"]      = $chiliPlatformUID; 
            
            $gamesVars["from"]   = $from;
            $gamesVars["to"]     = $to;
            
            $gamesVars["limit"]  = 0;
            $gamesVars["offset"] = GAME_QTY_AUTHORIZED;
            $gamesVars["user_timezone"] = $timezone;

            $challengeVars = array();
            $challengeVars["widget_id"] = $widgetData["id"];
            $challengeVars["user_id"]   = $chiliPlatformUID;
            $challengeVars["offset"]    = 0;

            $apiData["games"]          = array("url"  => "games/get_widget_games", "vars" => $gamesVars);
            $apiData["challenges"]     = array("url"  => "challenge/get_public_challenge", "vars"  => $challengeVars);

            $retData = nut_api::api('batch/action', array("chili" => $apiData));
            //appending player data
            $retData["player_data"] = $playerData;

            //IMPORTANT CHILI PLAYER SESSION  
            $this->site_model->loginPlayer($chiliPlatformUID, $platformId);

            return json_encode($retData);
        }
        catch(Api_exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
        }
    }
}
