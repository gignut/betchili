<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Demo extends MX_Controller {

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
	}

	public function render($widgetData = array())
	{
		$data = array();
		$data["JSON_GAME_DATA"]         = $this->load->view(MODULE_EMBED_FOLDER . "/demo/game_json_demo_data.php", $widgetData, true);
		$data["SOCIAL_LOGIN_CONTENT"]   = $this->load->view(MODULE_EMBED_FOLDER . "/game/social_login_view.php",   $widgetData, true);
		$data["PROFILE_HEADER_CONTENT"] = $this->load->view(MODULE_EMBED_FOLDER . "/game/profile_header_view.php", $widgetData, true);
		$data["CHALLENGES_CONTENT"]     = $this->load->view(MODULE_EMBED_FOLDER . "/game/challenges_view.php",     $widgetData, true);
		$data["GAMES_CONTENT"]          = $this->load->view(MODULE_EMBED_FOLDER . "/game/game_list_view.php",      $widgetData, true);
		$data["ACTIVITY_CONTENT"]       = $this->load->view(MODULE_EMBED_FOLDER . "/game/activity_view.php",       $widgetData, true);
		$data["LEADERBOARD_CONTENT"]    = $this->load->view(MODULE_EMBED_FOLDER . "/game/leaderboard_view.php",    $widgetData, true);
		
		return $this->load->view(MODULE_EMBED_FOLDER . "/game/demo_view.php", $data, true);
	}
}
