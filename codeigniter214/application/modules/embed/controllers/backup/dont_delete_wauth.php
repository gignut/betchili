
<?php
//
exit();
require_once APPPATH . 'libraries/google-api-php-client/src/Google_Client.php';  
require_once APPPATH . 'libraries/google-api-php-client/src/contrib/Google_PlusService.php';  
require_once APPPATH . 'libraries/socialoauth2/odnoklassniki.php';  
require_once APPPATH . 'libraries/socialoauth2/vkontakte.php'; 
require_once APPPATH . 'libraries/socialoauth2/mailru.php'; 

use Respect\Validation\Validator as v;

class Wauth extends MX_Controller
{
    function __construct()
    {
        nut_session::init();
        parent::__construct();
        $this->load->model(MODULE_EMBED_FOLDER . "/site_model");
        $this->load->helper('array');
    }

    /* ####  HILI USER POPUP START

    public function goauthcallback(){
        log_message("error", "goauthcallback");
    }

    //call ::ajax
    //chili account signup from popup
    public function register()
    {
        try
        {
            $userRegistrationData = array("chili" => $this->input->post());
            //should be the registered user id or error data
            $data = nut_api::api('users/register_chili_account',  $userRegistrationData);

            if(!isset($data["error"]))
            {
               nut_utils::evalRedirect(BASE_INDEX. MODULE_EMBED_FOLDER . "/oauth/signin_page");
            }
            else
            {
                switch(intval($data["code"]))
                {
                    case 101:
                         nut_utils::print_error("User with same email exists!");
                        break;
                    case 102:
                        nut_utils::print_error("Password mismatch!");
                        break;
                }
            }
        }
        catch(Api_exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode());
        }
    }

    // call:ajax
    // chili account signin from popup
    public function singin()
    {
        try
        {
            $data = nut_api::api('users/signin_chili_account',  array("chili" => $this->input->post()));
            if(!isset($data["error"]))
            {
                 $userId =  $data["user_id"];
                 $this->site_model->loginPlayer($userId);
            }
            else
            {
                nut_utils::print_error("Username or Password is incorrect!");
            }
        }
        catch(Api_exception $e)
        {
            nut_utils::print_error("Username or Password is incorrect!");
            //nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::print_error("Username or Password is incorrect!");
            //nut_utils::print_error($e->getMessage(), $e->getCode());
        }
    }

    //#############
    //avar 17 JUL 2013 , if the chili user is signed this will close the popup window and reload iframe
    public function signin_page()
    {
        $playerId = $this->site_model->getCurrentPlayerId();

        $signedUser = $playerId ? 1 :  0;
        $data = $this->load->view(MODULE_EMBED_FOLDER . "/chili_popup/chili_signin.php", array("signedUser" => $signedUser), true);
        $this->load->view(MODULE_EMBED_FOLDER . "/chili_popup/chili_main_popup.php", array("CONTENT"=>$data));
    }

    public function forgotpassword_page()
    {
        $data=$this->load->view(MODULE_EMBED_FOLDER . "/chili_popup/chili_forgotpassword.php", '', true);
        $this->load->view(MODULE_EMBED_FOLDER . "/chili_popup/chili_main_popup.php", array("CONTENT"=>$data));
    }

    public function registration_page()
    {
        $data = $this->load->view(MODULE_EMBED_FOLDER . "/chili_popup/chili_register.php", '', true);
        $this->load->view(MODULE_EMBED_FOLDER . "/chili_popup/chili_main_popup.php", array("CONTENT"=>$data));
    }

    public function chiliuserautorize()
    {
        $playerId = $this->site_model->getCurrentPlayerId();
        $data = array("chili_user" =>  $playerId);
        echo json_encode($data);
    }

    //################
    */

    //avar 16 JUNE 2013
    public function player($paramWidgetId = null)
    {
        $retData = array("error"=> 1, "action" => "", "message" => "");

        try
        {
            $widgetData = $this->site_model->getWidgetSessionData();
            
            if(!$widgetData)
            {
                $paramWidgetId = intval($paramWidgetId);
                v::numeric()->check($paramWidgetId);
                $apiData    = nut_api::wrapData(array("id" => $paramWidgetId));
                $widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);
                $this->site_model->setWidgetSessionData($widgetData);
            }

            $widgetId    = element("id", $widgetData, NULL);
            $siteId      = element("siteuser_id", $widgetData, NULL);

            if(!$siteId) {echo json_encode($retData); exit(); }

            $siteBalance = $this->site_model->getSiteBalanceConfig();

            $platform = $this->input->post("platform");
            $platformData = $this->input->post("data");

            if(!$platform || !$platformData) {echo json_encode($retData); exit(); }
            
            $ob = array("site_id" => $siteId, "widget_id" => $widgetId);
            $userId = NULL;
            $platformId = NULL;

            switch($platform)
            {
                case "chili":
                    //Firelog::log( $chiliData);
                    $userId = $platformData['chili_user'];
                    $platformId = PLATFORM_CHILI;
                    break;
                case "facebook":
                    $userId = $platformData['userID'];
                    $platformId = PLATFORM_FACEBOOK;
                    break;
                case "google":
                    $userId = $platformData['id'];
                    $platformId = PLATFORM_GOOGLE;
                    break;
            }

            $ob["user_id"]     =  $userId;
            $ob["platform_id"] =  $platformId;

            $chiliPost = array("chili" => $ob);
            $data = nut_api::api('users/init_player', $chiliPost);
           
            //PLAYER DATA AFETR INIT 
            $playerData = $data["player_data"];
            
            $chiliPlatformUID =   element("id",  $playerData, NULL);
            
            if(!$chiliPlatformUID)
             {
                //need to get data and register user
                $userData = array();
                switch($platform)
                {
                    case "facebook":
                        $userData = $this->getUserDataFacebook($platformData);
                        break;
                    case "google":
                        $userData = $this->getUserDataGoogle($platformData);
                        break;
                }

                $userData['site_id']   = $siteId;
                $userData['widget_id'] = $widgetId;
                $userData['balance']   = PLAYER_INITIAL_BALANCE;
                $userData['blocked']   = 0;

                $chiliPost  = array("chili" => $userData);
                //chili user id of registered user
                $playerData = nut_api::api('users/add' , $chiliPost);
                $chiliPlatformUID =   element("id",  $playerData, NULL);
             }
            
            $timezone = intval(nut_session::get('user_time_zone'));
            $from = mktime(0,0,0);
            $to = mktime(23,59,59);

            $gamesVars  = array();
            $gamesVars["widget_id"]    = $widgetData["id"];
            $gamesVars["language_id"]  = $widgetData["language_id"];
            $gamesVars["bookmaker_id"] = $widgetData["bookmaker_id"];
            $gamesVars["league_id"]    = null;
            $gamesVars["user_id"]      = $chiliPlatformUID; 
            
            $gamesVars["from"]   = $from;
            $gamesVars["to"]     = $to;
            
            $gamesVars["limit"]  = 0;
            $gamesVars["offset"] = GAME_QTY_AUTHORIZED;
            $gamesVars["user_timezone"] = $timezone;

            $challengeVars = array();
            $challengeVars["widget_id"] = $widgetData["id"];
            $challengeVars["user_id"]   = $chiliPlatformUID;
            $challengeVars["offset"]    = 0;

            $apiData["games"]          = array("url"  => "games/get_widget_games", "vars" => $gamesVars);
            $apiData["challenges"]     = array("url"  => "challenge/get_public_challenge", "vars"  => $challengeVars);

            $retData = nut_api::api('batch/action', array("chili" => $apiData));
            //appending player data
            $retData["player_data"] = $playerData;

            //IMPORTANT CHILI PLAYER SESSION  
            $this->site_model->loginPlayer($chiliPlatformUID);

            echo json_encode($retData);
        }
        catch(Exception $e)
        {
            echo json_encode($retData);
        }

    }

    private function getUserDataFacebook($data)
    {
        try
        {
            $token = $data['accessToken'];
            $userID = $data['userID'];
            $config =  array('appId'  =>  FB_APP_ID , 'secret' => FB_APP_SECRET);
            $this->load->library(LIB_FACEBOOK . 'Facebook', $config, 'facebook');
            $userProfile = $this->facebook->api('/'. $userID . "?access_token=" . $token);

            $data = array();
            //TODO::artak PLATFORM_FACEBOOK
            $data['platform_id'] = PLATFORM_FACEBOOK; 
            $data['platform_uid'] = $userID; 
            $data['first_name'] = element('first_name', $userProfile, ""); 
            $data['last_name'] =  element('last_name', $userProfile, ""); 
            $data['image'] = "https://graph.facebook.com/" . $userID . "/picture";
            $data['gender'] = element('gender', $userProfile, "");
            $data['email'] =  element('email', $userProfile, ""); 
            $data['timezone'] = element('timezone', $userProfile, "");
            $data['blocked'] = 0; 
            return $data;
         }
        catch(Api_exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::print_error($e->getMessage(), $e->getCode());
        }
    }

    private function getUserDataGoogle($gData)
    {
        $data = array();

        $data['platform_id']  = PLATFORM_GOOGLE; 
        $data['platform_uid'] = $gData["id"]; 
        $data['first_name']   = $gData["name"]["familyName"];
        $data['last_name']    = $gData["name"]["givenName"];
        $data['image']        = $gData["image"]["url"]; 
        $data['gender']       = $gData["gender"]; 
        $data['email']        = $gData["emails"][0]["value"];
        $data['blocked']      = 0; 
        return $data;
    }

    //call ::ajax
    public function forgetpassword()
    {
        try
        {
            $email = $this->input->post('email');
            //todo::rafo add email validation
            if($email)
            {
                $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");
                $userData = $this->user_model->getUserByEmail($email);
                $id = $userData["id"];
                if($id)
                {
                    $data = array("id" => $id);
                    $hash = $this->shortlink_model->generateUniqueString(12, $id);
                    $type = 'forgetpassword';
                    $data = json_encode($data);
                    $start_date = date("Y-m-d H:i:s");
                    $end_date = "2013-12-31 23:59:59";
                    $count = 0;
                    $this->shortlink_model->insert($hash, $type, $data, $start_date, $end_date, $count);
                    $this->sendRegistrationMail($email, "splitdb team", $hash);

                    $path = BASE_URL . INDEX_PHP . "/" . MODULE_ACCOUNT_FOLDER . "/host/create";
                    nut_utils::noty_success("Please check your email to reset password");
                }
                else
                {
                    nut_utils::noty_error("There is no account with such email...");
                }
            }
        }
        catch(Exception $e)
        {
            nut_utils::noty_error();
        }
    }

    //call ::ajax
    public function changepassword()
    {
        try
        {
            $email = $this->input->post('email');
            $pass = $this->input->post('pass');
            $confirmPass = $this->input->post('confirmpass');
            
            if($email && $pass)
            {
                $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");
                $this->shortlink_model->changePass($email, $pass);
                $path = BASE_URL . INDEX_PHP . "/" . MODULE_ACCOUNT_FOLDER . "/profile/signup";
                nut_utils::js_redirect($path);
                //nut_utils::noty_success("Please login with new password");
            }
            else
            {
                nut_utils::noty_error("Please try again");
            }
        }
        catch(Exception $e)
        {
             nut_utils::noty_error();
        }
    }

    //call:direct
    public function logout()
    {
        try
        {
            $this->user_model->logoutUser();
            $path = BASE_INDEX . MODULE_ACCOUNT_FOLDER . "/profile";
            nut_utils::locationRedirect($path);
        }
        catch(Exception $e)
        {
            nut_utils::error_page("account/profile/logout");
        }
    }

    private function sendRegistrationMail($email, $frname, $hash)
    {
        try
        {
            $link = BASE_URL . INDEX_PHP . "/" . MODULE_ACCOUNT_FOLDER . "/shortlink/confirm/" . $hash;
            $data = array(
                            'to_email' => $email,
                            'from_name'=> $frname,
                            'subject'  => "Thanks", 
                            'message'  => $link);  

            $email = new nut_email();
            $email->initialize("noreply");
            $email->send($data); 
        }
        catch(Exception $e)
        {
             nut_utils::error_page("account/profile/sendRegistrationMail");
        }        
    }


    // authorizes odnoklassniki
    public function odnoklassniki($paramWidgetId = null)
    {

        $AUTH_CALLBACK = BASE_INDEX . "auth/odnoklassniki/" . $paramWidgetId . "/";
        $config = array("app_id" => ODN_APP_ID, "app_secret" => ODN_APP_PRIVATE_KEY, "app_public_key" => ODN_APP_PUBLIC_KEY, "redirect_uri" => $AUTH_CALLBACK );
        $odn = new Odnoklassniki($config);
        $authPath =  $odn->getAuthPath();

        $code = element("code", $_GET, null);   
        if($code) 
        {
            try
            {
                $authData = $odn->init($code);
                $responseData = $odn->api("users.getCurrentUser"); 

                $userData = $odn->transformUserData($responseData);
                $gameData = $this->loginRussianSocials($paramWidgetId, PLATFORM_MAILRU, $userData);

                $userDataJson = json_encode($userData);
                echo "<script> window.opener.authorizedByOdnoklassniki('" . $gameData . "'); window.close();</script>";
            }
            catch(Exception $e){
                nut_log::log("error", "Exception from wauth/odnoklassniki");
            }
        } 
        else 
        {
            header('Location: ' .  $authPath);
        }
    } 

    public function vkontakte($paramWidgetId = null)
    {
        $AUTH_CALLBACK = BASE_INDEX . "auth/vkontakte/" . $paramWidgetId . "/";
        $config = array("app_id" => VK_APP_ID, "app_secret" => VK_APP_SECRET, "redirect_uri" => $AUTH_CALLBACK , "scope" => "friends,email");

        $vk = new Vkontakte($config);
        $authPath =  $vk->getAuthPath();
        
        $code = element("code", $_GET, null);   
        if($code) 
        {
            try
            {
                $authData = $vk->init($code);
                $uid      = $vk->getUserId();
               
                $responseData = $vk->api("users.get", array("user_ids" =>  $uid, "fields" => "uid, first_name, last_name, photo_50, sex")); 
                $userData     = $vk->transformUserData($responseData);
                $gameData     = $this->loginRussianSocials($paramWidgetId, PLATFORM_VKONTAKTE, $userData);

                echo "<script> window.opener.authorizedByVkontakte('" . $gameData . "'); window.close();</script>";
            }
            catch(Exception $e)
            {
                nut_log::log("error", "Exception from wauth/vkontakte");
            }
        } 
        else 
        {
            header('Location: ' . $authPath);
        }
    } 

    public function mailru($paramWidgetId = null)
    {
        $AUTH_CALLBACK = BASE_INDEX . "auth/mailru/" . $paramWidgetId . "/";
        $config = array("app_id" => MAILRU_APP_ID, "app_secret" => MAILRU_APP_SECRET, "redirect_uri" => $AUTH_CALLBACK , "scope" => "");

        $mr = new Mailru($config);
        $authPath =  $mr->getAuthPath();
        
        $code = element("code", $_GET, null);   
        if($code) 
        {
            try
            {
                $authData = $mr->init($code);
                $uid      = $mr->getUserId();
                $responseData = $mr->api("users.getInfo", array("uids" =>  $uid)); 
                $userData =  $mr->transformUserData($responseData);
                $gameData = $this->loginRussianSocials($paramWidgetId, PLATFORM_MAILRU, $userData);

                echo "<script> window.opener.authorizedByMailru('" .  $gameData  . "'); window.close();</script>";
            }
            catch(Exception $e)
            {
                nut_log::log("error", "Exception from wauth/mailru");
            }
        } 
        else 
        {
            header('Location: ' . $authPath);
        }
    } 

    private function loginRussianSocials($paramWidgetId, $platformId, $userData)
    {
        $retData = array("error"=> 1, "action" => "", "message" => "");

        $widgetData = $this->site_model->getWidgetSessionData();
        
        if(!$widgetData)
        {
                $paramWidgetId = intval($paramWidgetId);
                v::numeric()->check($paramWidgetId);
                $apiData    = nut_api::wrapData(array("id" => $paramWidgetId));
                $widgetData = nut_api::api('widgetdata/get_widget_data', $apiData);
                $this->site_model->setWidgetSessionData($widgetData);
        }


        $widgetId    = element("id", $widgetData, NULL);
        $siteId      = element("siteuser_id", $widgetData, NULL);

        if(!$siteId) {echo json_encode($retData); exit(); }

        $siteBalance = $this->site_model->getSiteBalanceConfig();

        $ob = array("site_id" => $siteId, "widget_id" => $widgetId);
        $ob["user_id"]     =  $userData["platform_uid"];
        $ob["platform_id"] =  $userData["platform_id"];

        $chiliPost = array(nut_api::$PARAM => $ob);
        $data = nut_api::api('users/init_player', $chiliPost);
           
        //PLAYER DATA AFETR INIT 
        $playerData       = $data["player_data"];
        $chiliPlatformUID =   element("id",  $playerData, NULL);

        if(!$chiliPlatformUID)
        {
            //need to get data and register user
            $userData['site_id']   = $siteId;
            $userData['widget_id'] = $widgetId;
            $userData['balance']   = PLAYER_INITIAL_BALANCE;
            $userData['blocked']   = 0;
        
            $chiliPost  = array(nut_api::$PARAM => $userData);
            //chili user id of registered user
            $playerData = nut_api::api('users/add' , $chiliPost);
            $chiliPlatformUID =   element("id",  $playerData, NULL);
        }

        $timezone = intval(nut_session::get('user_time_zone'));
        $from = mktime(0,0,0);
        $to   = mktime(23,59,59);

        $gamesVars  = array();
        $gamesVars["widget_id"]    = $widgetData["id"];
        $gamesVars["language_id"]  = $widgetData["language_id"];
        $gamesVars["bookmaker_id"] = $widgetData["bookmaker_id"];
        $gamesVars["league_id"]    = null;
        $gamesVars["user_id"]      = $chiliPlatformUID; 
        
        $gamesVars["from"]   = $from;
        $gamesVars["to"]     = $to;
        
        $gamesVars["limit"]  = 0;
        $gamesVars["offset"] = GAME_QTY_AUTHORIZED;
        $gamesVars["user_timezone"] = $timezone;

        $challengeVars = array();
        $challengeVars["widget_id"] = $widgetData["id"];
        $challengeVars["user_id"]   = $chiliPlatformUID;
        $challengeVars["offset"]    = 0;

        $apiData["games"]          = array("url"  => "games/get_widget_games", "vars" => $gamesVars);
        $apiData["challenges"]     = array("url"  => "challenge/get_public_challenge", "vars"  => $challengeVars);

        $retData = nut_api::api('batch/action', array("chili" => $apiData));
        //appending player data
        $retData["player_data"] = $playerData;

        //IMPORTANT CHILI PLAYER SESSION  
        $this->site_model->loginPlayer($chiliPlatformUID);


        return json_encode($retData);
    }

    // check if widget user is authorized
    public function widgetAuthorizedUser()
    {
        $userId = $this->site_model->getCurrentPlayerId();
        //$widgetData = $this->site_model->getWidgetSessionData();
        if($userId)
        {
            return $userId;
        }
        else
        {
            echo json_encode(array("not_authorized_user" => 1));
            exit();
        }
    }

}
