<?php

class site_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	private function generateUniqueString($length, $id= "")
	{
		$key = '';
        $keyStart = '';
        $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

        $len  = intval($length/2);

        for ($i = 0; $i < $len; $i++)
        {
            $keyStart .= $keys[array_rand($keys)];
        }

        $keyEnd = '';
        $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

        for ($i = 0; $i < $len; $i++)
        {
            $keyEnd .= $keys[array_rand($keys)];
        }

        $key = $keyStart . $id . $keyEnd;
        return $key;
	}

	public function getSiteBalanceConfig()
	{
		return PLAYER_INITIAL_BALANCE;
	}

	public function getSiteId()
	{
		$siteId = nut_session::get("siteuser_id");
		return $siteId ? $siteId : 0;
	}

	//set widget data abtained during loading
	public function setWidgetSessionData($data)
	{
		nut_session::set("widget_data" , $data);
	}

	public function getWidgetSessionData()
	{
		// STORED DATA LIKE THIS ARRAY THROUGH setWidgetSessionData()
		// $data["id"]              
		// $data["siteuser_id"]   
		// $data["domain"]          
		// $data["language_id"]    
		// $data["width"]         
		// $data["height"]   
		// $data["widget_type"]

		$data = nut_session::get("widget_data", NULL);
		return $data;
	}

	public function loginPlayer($playerId, $platformId)
	{
		if($playerId)
		{
			nut_session::set("player_id",   $playerId);
			nut_session::set("platform_id", $platformId);
		}
		else
		{
			nut_session::set("player_id",   null);
			nut_session::set("platform_id", null);
		}
	}

	public function logoutPlayer()
	{
		nut_session::set("player_id", null);
		nut_session::set("platform_id", null);
		//session_destroy();
	}

	public function getCurrentPlayerId()
	{
		$id = nut_session::get("player_id", NULL);
		return $id  ? intval($id) : NULL; 
	}

	public function getCurrentPlayerPlatformId()
	{
		$platformId = nut_session::get("platform_id", NULL);
		return $platformId; 
	}
}

?>