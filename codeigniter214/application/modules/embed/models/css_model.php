<?php

class css_model extends CI_Model {

	// "body_border_color"  =>  "black",
	// "body_border_size"   =>  "2",
	// "h_font"             =>  "",
	// "g_font"             =>  "",
	private $GAME_CSS = array(
		"gh_height"             =>  35,     // events titles, description
		"h_side_margin"         =>  20,
		"g_height"              =>  60,
		"g_width"               =>  280,
		"h_menu_height"		    =>  60,     //header menu height for authorized users 
		"login_header_height"   =>  32,
		"challenge_height"      =>  160,
		"events_right_position" =>  30,
		"body_bg_color" 	    =>  "#ffffff",

		//customazable ones
		"h_bg_color"             =>  "#166BAB",//+
		"h_font_color"           =>  "#FFFFFF",//+
		"h_icon_color"			 =>  "#FFFFFF",

		"challange_bg_color"     =>  "#E8E8E8",//+
		"challange_font_color"	 =>  "#000000", //+
		"challange_team_bg"	      =>  "#FFFFFF", //+

		"challange_option_enabled_bg"	  =>  "#166BAB", //+
		"challange_option_disabled_bg"	  =>  "#FFFFFF", //+
		"challange_option_enabled_font"	  =>  "#FFFFFF", //+
		"challange_option_disabled_font"  =>  "#3B3B3B", //+

		"challange_btn_bg"    =>  "#166BAB", //+
		"challange_btn_font"  =>  "#FFFFFF", //+

		"gh_bg_color"       => "#166BAB",
		"gh_tmp_bg_color"   => "#FFFFFF",
		"gh_tmp_font_color" => "#000000",
		
		"g_border_color"    => "#000000",
		"g_border_size"     => "1",   
		
		"g_left_bg_color"   => '#E8E8E8',
		"g_left_font_color" => "#000000",

		"g_right_bg_color"  => "#FFFFFF",
		"g_img_bg_color"    => "#FFFFFF",

		"g_tmp_bg_color"    => '#DEDCDC',
		"g_tmp_font_color"  => "#000000"
	);

	function __construct()
	{
		parent::__construct();
	}

	public function mergeCSS($siteUserCss)
	{
		if($siteUserCss)
		{
			$siteUserCss = json_decode($siteUserCss, true);

			foreach ($siteUserCss as $key => $value) {
				if(isset($this->GAME_CSS[$key]))
				{
					$this->GAME_CSS[$key] = $value;
				}	
			}
		}

		return $this->GAME_CSS;
	}
}