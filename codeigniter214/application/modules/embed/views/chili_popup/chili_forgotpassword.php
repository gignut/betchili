<script>
  window.resizeTo(500,350);
</script>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="popup-signup-div"  ng-init="init()">
        <form class="well span12" name="forgotPasswordForm" >
        <!-- Forgotten Email -->
        <div>
            <h4 class="textcolor">Write your Email</h4>
            <hr>
          
            <div class="control-group">
              <label class="control-label color" for="inputEmail" name="emailname">Email</label>
              <div class="controls input-prepend">
                 <a href="#" tooltip="text" tooltip-placement="right" data-trig="hover" data-placement="top" data-title="Read your email" class="ng-scope"><span class="add-on"><i class="icon-user"></i></span></a>
                 <input type="text" ng-model="forgetPassModel.email" tabindex="1" class="nut-input input-xlarge" data-nut-form-error="Wrong syntax:write email type" data-nut-form-regexp="^[A-Z,a-z,0-9].{1,200}@[A-Z,a-z]{4,7}\.[A-Z,a-z]{2,3}$" data-nut-form-key="email" placeholder="Email">
              </div>
            </div>
            <div class="control-group">
                  <div class="controls">
                    <button type="button" ng-click="resetPassword()" class="btn btn-success" data-loading-text="Wait...">Send</button>
                    <a href="{{loginHref}}" class="mrgLeft20"><small>Back</small></a>
                  </div>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
