<!DOCTYPE html>
<html id="ng-app" ng-app="ChiliAccountPopup">

<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script>
    var chiliParams = new Object();
    chiliParams.BASE_URL     = <?php echo '"' . BASE_URL  . '"'; ?>;
    chiliParams.INDEX_PHP    = <?php echo '"' . INDEX_PHP . '"'; ?>;
    chiliParams.BASE_INDEX   = <?php echo '"' . BASE_INDEX  . '"'; ?>;
    chiliParams.MEDIA_URI    = <?php echo '"' . MEDIA_URL . '"'; ?>;
</script>

<?php
  chili_asset_loader::jquery(true, true);
  chili_asset_loader::angularjsVersion();
  chili_asset_loader::bootstrap();


  script("modules/js/jquery.blockUI.js");
  script("modules/js/json2.js");
  script("modules/m_embed/js/ng_chili_module.js");
  script("modules/m_embed/js/ng_popup_profile.js");
?>

<style>
body
{
   margin: 0px;
   padding:0px;
}
.popup-signup-div
{
  width:100%;
}
.mrgLeft20
{
  margin-left:20px;
}
</style>
</head>
<body>
  <div ng-controller= "ProfileCtrl">
    <?php echo $CONTENT ?>
  </div>
</body>
</html>