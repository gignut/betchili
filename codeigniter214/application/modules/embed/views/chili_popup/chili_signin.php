<script>
  window.resizeTo(500,450);

  //window.moveTo(100,100);
</script>
<div class="container-fluid">

<div class="row-fluid">
  <div class="popup-signup-div" ng-init="init(<?php echo $signedUser; ?>)">
      <form class="well span12" name="signinForm">
        <div>
          <h4>Sign In</h4>
          <hr>
          <div class="control-group">
            <label class="control-label color" for="inputEmail" name="emailname">Email</label>
            <div class="controls input-prepend">
               <a href="#" tooltip="text" tooltip-placement="right" data-trig="hover" data-placement="top" data-title="Read your email" class="ng-scope"><span class="add-on"><i class="icon-user"></i></span></a>
               <input type="text" ng-model="signinModel.username" tabindex="1" class="nut-input input-xlarge" data-nut-form-error="Wrong syntax:write email type" data-nut-form-regexp="^[A-Z,a-z,0-9].{1,200}@[A-Z,a-z]{4,7}\.[A-Z,a-z]{2,3}$" data-nut-form-key="email" placeholder="Email">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label color" for="inputPass">Password
              <a href="{{forgetpasswordHref}}" class="forgottenPass mrgLeft20"><small>Forgotten your password?</small></a>
            </label>

            <div class="controls input-prepend">
               <a href="#" data-trig="hover" data-placement="top" data-title="Read your password"><span class="add-on"><i class="icon-lock"></i></span></a>
               <input type="password" ng-model="signinModel.password" tabindex="2" class="nut-input input-xlarge" data-nut-form-error="Wrong syntax" data-nut-form-regexp="^[A-Z,a-z].+" data-nut-form-key="password" placeholder="Password">
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <button class="btn btn-success" tabindex="3" ng-click="login()">Sign in</button>
              <a href="{{registrationHref}}" class="registration mrgLeft20"><small>Registration</small></a>
            </div>
         </div>
      </div>

      <hr class="hrcolor">
      <div ng-show="errorTxt != ''"class="alert alert-error">
        <button type="button" class="close" ng-click="errorTxt = ''">×</button>
        <strong>Error: </strong>
        <span>{{errorTxt}}</span>
      </div>
      
      </form>
  </div>
</div>
</div>
