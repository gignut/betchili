<script>
  window.resizeTo(500,650);
</script>

<div class="container-fluid">
<div class="row-fluid">
  <div class="popup-signup-div" ng-init="init()">
      <form class="well span12"  name="registerForm" >
      <div>
        <h4>Registration</h4>
          <hr class="hrcolor">
        <div class="control-group">
          <label class="control-label firstname color" for="frname"><?php echo FIRST_NAME; ?></label>
          <div class="controls input-prepend">
           <a href="#" data-title="Write your name"><span class="add-on"><i class="icon-pencil"></i></span></a>
            <input type="text" ng-model="signupModel.first_name" class="nut-input input-xlarge" data-nut-form-error="Wrong syntax:write only font" data-nut-form-regexp="^[A-Z,a-z]+$" data-nut-form-key="firstname" placeholder="First Name">
        </div>
        </div>
      <div class="control-group">
        <label class="control-label color" for="lastName">Last Name</label>
        <div class="controls input-prepend">
         <a href="#" data-title="Write your lastname" data-placement="top" data-trig="hover "><span class="add-on"><i class="icon-pencil"></i></span></a>
         <input type="text" ng-model="signupModel.last_name" class="nut-input input-xlarge" data-nut-form-error="Wrong syntax:write only font" data-nut-form-regexp="^[A-Z,a-z]+$" data-nut-form-key="lastname" placeholder="Last Name">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label color" for="email">Email</label>
        <div class="controls input-prepend">
         <a href="#" data-title="Write your email" data-placement="top" data-trig="hover "><span class="add-on"><i class="icon-envelope"></i></span></a>
         <input type="text" ng-model="signupModel.email" class="nut-input input-xlarge" data-nut-form-error="Wrong syntax:write email type" data-nut-form-regexp="^[A-Z,a-z,0-9].{1,200}@[A-Z,a-z]{4,7}\.[A-Z,a-z]{2,3}$" data-nut-form-key="email" placeholder="Email">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label color" for="pass">Password</label>
        <div class="controls input-prepend">
         <a href="#" data-title="Write your password" data-placement="top" data-trig="hover "><span class="add-on"><i class="icon-lock"></i></span></a>
         <input type="password" ng-model="signupModel.pass" class="nut-input input-xlarge" data-pass="pass" data-nut-form-error="Wrong syntax" data-nut-form-regexp="^[A-Z,a-z].{6,}" data-nut-form-key="password" placeholder="Password">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label color" for="confirmpass">Confirm password</label>
        <div class="controls input-prepend">
         <a href="#" data-title="Write your password" data-placement="top" data-trig="hover "><span class="add-on"><i class="icon-lock"></i></span></a>
         <input type="password" ng-model="signupModel.confirm_password" class="nut-input input-xlarge" data-nut-confirm="pass" data-nut-form-error="Your passwords do not match. " data-nut-form-regexp="^[A-Z,a-z].{6,}" data-nut-form-key="confirmpassword" placeholder="confirm password">
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
        <button type="button" ng-click="register()" class="btn btn-success" data-loading-text="Wait..." data-nut-form-submmit="submit">Sign up</button>
          <a href="{{loginHref}}" class="mrgLeft20"><small>Back</small></a>
      </div>
      </div>
      </div>
      <hr class="hrcolor">
      <div ng-show="errorTxt != ''"class="alert alert-error">
        <button type="button" class="close" ng-click="errorTxt = ''">×</button>
        <strong>Error: </strong>
        <span>{{errorTxt}}</span>
      </div>

      </form>

  </div>
</div>
</div>
