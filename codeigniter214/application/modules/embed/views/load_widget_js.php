<?php 
	header("content-type: application/javascript");
	$this->load->view(MODULE_EMBED_FOLDER . "js/nut_widget_js.php");
 ?>

(function(w, d, options){
	var wg = new NutWidget();
	wg.init(w, d, options);
})(window, document, {
	height: 				<?php echo  $height 				     ;  ?>,
	width : 				<?php echo  $width 				         ;  ?>,
	url_origin :  			<?php echo '"' . $widgetUrlOrigin   . '"';  ?>,
	url_default: 			<?php echo '"' . $widgetUrlDefault 	. '"';  ?>,
	selector : 				<?php echo '"' . $widgetSelector   	. '"';  ?>,
	origin_validate_url :   <?php echo '"' . $originValidateURL . '"';  ?>
});