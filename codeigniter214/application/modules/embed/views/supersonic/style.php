<?php header("Content-type: text/css"); ?>

/* SUPERSONIC ADS CUSTOM STYLE*/

.ssaInterstitialContent{
	position: fixed;
	top: 50%;
	left: 50%;
	z-index: 9997;
	background: #777777;  /*css["h_bg_color"]*/
	padding: 9px;
	/*font-family: Helvetica,Arial,sans-serif;*/
	border-radius: 14px; 
	-moz-border-radius: 14px;
	/* -webkit-border-radius: 14px; */
	-ms-border-radius: 14px;
	-o-border-radius: 14px;
}

.ssaInterstitialOverlay{
	background:url(http://s.ssacdn.com/inlineDelivery/images/background.png); top left repeat;
	opacity:0.9;filter:alpha(opacity=90);
	width:100%;height:100%;position:fixed;top:0;left:0;
	z-index:9996;
}

.ssaInterstitialTopBar{
	text-align:center;
}

.ssaInterstitialButton{
	cursor:pointer;float:right;
	height:24px;
	line-height:24px;
	border:1px solid #999;
	padding:0px 10px;margin-top:8px;
	text-align:left;
	font-weight:bold;
	font-size:12px;
}
.ssaInterstitialButton:hover{
}

.ssaInterstitialPagination{
	cursor:pointer;
	margin-left:5px;border:1px solid #244380;
	color:#fff;
	background: rgb(73, 165, 105);
	text-shadow: none;
	border: 1px solid rgb(253, 253, 253);
}
.ssaInterstitialPagination:hover{
  	cursor:pointer;
  	margin-left:5px;border:1px solid #244380;
	color:#fff;
	background: rgb(73, 165, 105);
	text-shadow: none;
	border: 1px solid rgb(253, 253, 253);
}
.ssaInterstitialNotification{
	display:none;
	z-index:9998;
	position:absolute;top:0;left:0;
	line-height:26px;
	height:50px;
	width:500px;
	background: rgb(73, 165, 105);
	font-size:15px;
	padding-top:16px;
	color:#fff;
	text-align:center;
}

.ssaEngagementWrap{
	background:#000 url(http://s.ssacdn.com/inlineDelivery/images/background2.png) left bottom no-repeat;
	color:#fff;
}

.ssaEngagementTitle{
	color:#fff;
	font-weight:bold;
	text-shadow:-1px -1px 3px #000, 1px 1px 1px #000;
}

.ssaEngagementFooter,.ssaEngagementVideo{
	background:transparent;
}

.ssaEngagementLogo{
	float:none;
	margin:0 auto 5px auto;
}

.ssaInterstitialBottomBar {
	line-height: 42px;
	height: 42px;
	display: block;
	clear: both;
	padding: 0 10px;
	background: #d8d8d8;
	border-radius: 0 0 10px 10px;
}

