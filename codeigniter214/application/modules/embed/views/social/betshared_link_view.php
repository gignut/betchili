<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
	<title><?php echo $title;?></title>
	<meta charset="utf-8" /> 
	<meta property="og:locale" content="en_US" />
	
	<meta property="og:title"     content="<?php echo $title;?>" />
	<meta property="og:site_name" content="<?php echo $title;?>" />
	<meta property="og:type"      content="website" />     
	<meta property="og:url"       content="<?php echo $link;?>" />

	<!-- image -->
	<meta property="og:image"        content="<?php echo $imageLink;?>" />
	<meta property="og:image:type"   content="image/jpeg" />
	<meta property="og:image:width"  content="<?php echo intval($width);?>"  />
	<meta property="og:image:height" content="<?php echo intval($height);?>" />
	
	<!-- description -->
	<meta name="description"        content="<?php echo $description;?>" />
	<meta property="og:description" content="<?php echo $description;?>" />

	<script>
		window.location.href = "<?php echo 'http://' . $domain; ?>";
	</script>
</head>
<body>
</body>	
</html>