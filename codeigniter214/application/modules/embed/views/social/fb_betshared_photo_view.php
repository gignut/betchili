<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"> 
<style>

html {
  font-size: 100%;
  -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
}

a:focus {
  outline: thin dotted #333;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}

a:hover,
a:active {
  outline: 0;
}

img {
  width: auto\9;
  height: auto;
  max-width: 100%;
  vertical-align: middle;
  border: 0;
  -ms-interpolation-mode: bicubic;
}

body {
  margin: 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  line-height: 20px;
  color: #333333;
  background-color: #ffffff;
}

a {
  color: #0088cc;
  text-decoration: none;
}

.img-rounded {
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
}

.img-polaroid {
  padding: 4px;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
     -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
          box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
}

.img-circle {
  -webkit-border-radius: 500px;
     -moz-border-radius: 500px;
          border-radius: 500px;
}

p {
  margin: 0 0 10px;
}

small {
  font-size: 85%;
}

strong {
  font-weight: bold;
}

em {
  font-style: italic;
}

cite {
  font-style: normal;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  margin: 5px 0;
  font-family: inherit;
  font-weight: bold;
  line-height: 20px;
  color: inherit;
  text-rendering: optimizelegibility;
}

h1 small,
h2 small,
h3 small,
h4 small,
h5 small,
h6 small {
  font-weight: normal;
  line-height: 1;
  color: #999999;
}

h1,
h2,
h3 {
  line-height: 40px;
}

h1 {
  font-size: 38.5px;
}

h2 {
  font-size: 31.5px;
}

h3 {
  font-size: 24.5px;
}

h4 {
  font-size: 17.5px;
}

h5 {
  font-size: 14px;
}

h6 {
  font-size: 11.9px;
}

h1 small {
  font-size: 24.5px;
}

h2 small {
  font-size: 17.5px;
}

h3 small {
  font-size: 14px;
}

h4 small {
  font-size: 14px;
}

hr {
  margin: 20px 0;
  border: 0;
  border-top: 1px solid #eeeeee;
  border-bottom: 1px solid #ffffff;
}

table {
  max-width: 100%;
  background-color: transparent;
  border-collapse: collapse;
  border-spacing: 0;
}

.table {
  width: 100%;
  margin-bottom: 20px;
}

.table th,
.table td {
  padding: 8px;
  line-height: 20px;
  text-align: left;
  vertical-align: top;
  border-top: 1px solid #dddddd;
}

.table th {
  font-weight: bold;
}

.table thead th {
  vertical-align: bottom;
}

.table caption + thead tr:first-child th,
.table caption + thead tr:first-child td,
.table colgroup + thead tr:first-child th,
.table colgroup + thead tr:first-child td,
.table thead:first-child tr:first-child th,
.table thead:first-child tr:first-child td {
  border-top: 0;
}

.table tbody + tbody {
  border-top: 2px solid #dddddd;
}

.table .table {
  background-color: #ffffff;
}

.table-condensed th,
.table-condensed td {
  padding: 4px 5px;
}

.table-bordered {
  border: 1px solid #dddddd;
  border-collapse: separate;
  *border-collapse: collapse;
  border-left: 0;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
}

.table-bordered th,
.table-bordered td {
  border-left: 1px solid #dddddd;
}

.table-bordered caption + thead tr:first-child th,
.table-bordered caption + tbody tr:first-child th,
.table-bordered caption + tbody tr:first-child td,
.table-bordered colgroup + thead tr:first-child th,
.table-bordered colgroup + tbody tr:first-child th,
.table-bordered colgroup + tbody tr:first-child td,
.table-bordered thead:first-child tr:first-child th,
.table-bordered tbody:first-child tr:first-child th,
.table-bordered tbody:first-child tr:first-child td {
  border-top: 0;
}

.table-bordered thead:first-child tr:first-child > th:first-child,
.table-bordered tbody:first-child tr:first-child > td:first-child,
.table-bordered tbody:first-child tr:first-child > th:first-child {
  -webkit-border-top-left-radius: 4px;
          border-top-left-radius: 4px;
  -moz-border-radius-topleft: 4px;
}

.table-bordered thead:first-child tr:first-child > th:last-child,
.table-bordered tbody:first-child tr:first-child > td:last-child,
.table-bordered tbody:first-child tr:first-child > th:last-child {
  -webkit-border-top-right-radius: 4px;
          border-top-right-radius: 4px;
  -moz-border-radius-topright: 4px;
}

.table-bordered thead:last-child tr:last-child > th:first-child,
.table-bordered tbody:last-child tr:last-child > td:first-child,
.table-bordered tbody:last-child tr:last-child > th:first-child,
.table-bordered tfoot:last-child tr:last-child > td:first-child,
.table-bordered tfoot:last-child tr:last-child > th:first-child {
  -webkit-border-bottom-left-radius: 4px;
          border-bottom-left-radius: 4px;
  -moz-border-radius-bottomleft: 4px;
}

.table-bordered thead:last-child tr:last-child > th:last-child,
.table-bordered tbody:last-child tr:last-child > td:last-child,
.table-bordered tbody:last-child tr:last-child > th:last-child,
.table-bordered tfoot:last-child tr:last-child > td:last-child,
.table-bordered tfoot:last-child tr:last-child > th:last-child {
  -webkit-border-bottom-right-radius: 4px;
          border-bottom-right-radius: 4px;
  -moz-border-radius-bottomright: 4px;
}

.table-bordered tfoot + tbody:last-child tr:last-child td:first-child {
  -webkit-border-bottom-left-radius: 0;
          border-bottom-left-radius: 0;
  -moz-border-radius-bottomleft: 0;
}

.table-bordered tfoot + tbody:last-child tr:last-child td:last-child {
  -webkit-border-bottom-right-radius: 0;
          border-bottom-right-radius: 0;
  -moz-border-radius-bottomright: 0;
}

.table-bordered caption + thead tr:first-child th:first-child,
.table-bordered caption + tbody tr:first-child td:first-child,
.table-bordered colgroup + thead tr:first-child th:first-child,
.table-bordered colgroup + tbody tr:first-child td:first-child {
  -webkit-border-top-left-radius: 4px;
          border-top-left-radius: 4px;
  -moz-border-radius-topleft: 4px;
}

.table-bordered caption + thead tr:first-child th:last-child,
.table-bordered caption + tbody tr:first-child td:last-child,
.table-bordered colgroup + thead tr:first-child th:last-child,
.table-bordered colgroup + tbody tr:first-child td:last-child {
  -webkit-border-top-right-radius: 4px;
          border-top-right-radius: 4px;
  -moz-border-radius-topright: 4px;
}

.table-striped tbody > tr:nth-child(odd) > td,
.table-striped tbody > tr:nth-child(odd) > th {
  background-color: #f9f9f9;
}

.table-hover tbody tr:hover > td,
.table-hover tbody tr:hover > th {
  background-color: #f5f5f5;
}

table td[class*="span"],
table th[class*="span"],
.row-fluid table td[class*="span"],
.row-fluid table th[class*="span"] {
  display: table-cell;
  float: none;
  margin-left: 0;
}

.table td.span1,
.table th.span1 {
  float: none;
  width: 44px;
  margin-left: 0;
}

.table td.span2,
.table th.span2 {
  float: none;
  width: 124px;
  margin-left: 0;
}

.table td.span3,
.table th.span3 {
  float: none;
  width: 204px;
  margin-left: 0;
}

.table td.span4,
.table th.span4 {
  float: none;
  width: 284px;
  margin-left: 0;
}

.table td.span5,
.table th.span5 {
  float: none;
  width: 364px;
  margin-left: 0;
}

.table td.span6,
.table th.span6 {
  float: none;
  width: 444px;
  margin-left: 0;
}

.table td.span7,
.table th.span7 {
  float: none;
  width: 524px;
  margin-left: 0;
}

.table td.span8,
.table th.span8 {
  float: none;
  width: 604px;
  margin-left: 0;
}

.table td.span9,
.table th.span9 {
  float: none;
  width: 684px;
  margin-left: 0;
}

.table td.span10,
.table th.span10 {
  float: none;
  width: 764px;
  margin-left: 0;
}

.table td.span11,
.table th.span11 {
  float: none;
  width: 844px;
  margin-left: 0;
}

.table td.span12,
.table th.span12 {
  float: none;
  width: 924px;
  margin-left: 0;
}

.table tbody tr.success > td {
  background-color: #dff0d8;
}

.table tbody tr.error > td {
  background-color: #f2dede;
}

.table tbody tr.warning > td {
  background-color: #fcf8e3;
}

.table tbody tr.info > td {
  background-color: #d9edf7;
}

.table-hover tbody tr.success:hover > td {
  background-color: #d0e9c6;
}

.table-hover tbody tr.error:hover > td {
  background-color: #ebcccc;
}

.table-hover tbody tr.warning:hover > td {
  background-color: #faf2cc;
}

.table-hover tbody tr.info:hover > td {
  background-color: #c4e3f3;
}

.pull-right {
  float: right;
}

.pull-left {
  float: left;
}

.hide {
  display: none;
}

.show {
  display: block;
}

.invisible {
  visibility: hidden;
}

.affix {
  position: fixed;
}


.betRow .betItem{
	position:relative;
	height: 105px;
	margin-right: 50px;
}

.betTableHeader th, td{
	padding: 0px !important;
}

.betTableHeader .part2 {
	width:250px;
}

.betTableHeader .part3 {
	width: 150px;
}

.betGameTable th {
	text-align: left !important;
}

.betGameTable .part2 {
	width: 250px;
}

.betGameTable .part3 {
	width: 150px;
}

.betRow  .teamImg1{
	width:  70px;
	height: 70px;
	border-radius: 4px;
	/* customizable*/
	display: inline-block;
	position: absolute;
	top:28px;
	left:9px;
	background-color: white;
}

.betRow .teamImg2{
	width: 70px;
	height: 70px;
	border-radius: 4px;
	/* customizable*/
	display: inline-block;
	position: absolute;
	top:28px;
	right:9px;
	background-color: white;
}

.betRow .teamImg1 > img, .betRow  .teamImg2 > img{
	width: 64px;
	height:64px;
	margin:3px;
}


.betRow  .teamName1{
	position: absolute;
	left:8px;
}

.betRow .teamName2{
	position: absolute;
	right:8px;
}

.betRow .gameItemTime {
	position: absolute; 
	right: 0px;
	left: 0px;
	text-align: center; 
	top: 48px;
	font-size: 18px;
}
</style>
</head>
<body style="width:1200px;">
	<image src= "<?php echo MEDIA_URL . '/imagestmp/share_bg1.jpg' ?>" style="position:absolute; top:0px; width:1200px;" />
	
	<div style="text-align:left; padding:15px;position:relative;height:100%;background:white; opacity:0.93; margin-top:50px;">
			<table style="margin-bottom:20px;" class="table betTableHeader">
				 	<thead>
				 		<th><h3><?php echo $TR_BET_AMOUNT; ?> </h3></th>
				 		<th><h3><?php echo $TR_COEFFICIENT; ?></h3></th>
				 		<th><h3><?php echo $TR_WIN; ?>        </h3></th>
				 	</thead>	
				 	<tbody>
				 		<tr>
				 			<td class="part1">
					 			<span>
							    	<h3><?php echo $bets["amount"]?></h3>
							    </span>	
							</td>
							<td class="part2"><h3><?php echo $bets["coefficient"]?></h3></td>
					 		<td class="part3"><h3><?php echo round($bets["amount"] * $bets["coefficient"], 2); ?></h3></td>
				 		</tr>
				 	</tbody>	
			</table>
			<table style="width:100%;" class="betGameTable">
				<thead>
					<th><small><?php echo $TR_GAME; ?></small></th>
		            <th><small><?php echo $TR_ODD; ?></small></th>
		            <th><small><?php echo $TR_SELECTION; ?></small></th>
				</thead>
				<tbody>
					<?php foreach ($bets["event_chain"] as $chainItem) :?>
					<?php 
						$gameId            = $chainItem["game_id"];
						$gameItem          = $gamesData[$gameId];
						$gameStartDate     = date('F j, Y H:i', $gameItem["start_date"]);
						$gameCompetitionId = $gameItem["competition_id"];
						$gameParticipants  = $gameItem["aux_info"]["participants"];

						$name1 = $gameParticipants[0]["tname"];
						$name2 = $gameParticipants[1]["tname"];
						$logo1 = MEDIA_URL . "assets/participants/" . $gameParticipants[0]["logo"];
						$logo2 = MEDIA_URL . "assets/participants/" . $gameParticipants[1]["logo"];
					?>
					<tr  class="betRow" style="background-color: rgb(215, 215, 215);">
		        		<td>			
		        			<div>
		            			<div class="betItem">
										<div class="gameItemTime">
											<span><small><b><?php echo $gameStartDate; ?> </b></small></span>
										</div>	
										<!-- PARTICIPANT 1 -->
										<div class="teamImg1">
											<img src = "<?php echo $logo1; ?>" />
										</div>
										<div class="teamName1">
											<h5><?php echo $name1; ?></h5>
										</div>	
										<!-- PARTICIPANT2 -->
										<div class="teamImg2">
											<img src = "<?php echo $logo2; ?>"/>
										</div>
										<div class="teamName2">
											<h5><?php echo $name2; ?></h5>
										</div>	
								</div>
							</div>
						</td>
		        		<td class="part2">
		        			<h3><?php echo $chainItem["event_coefficient"] ?></h3>
		        		</td>
		        		<td class="part3"><h3><?php echo $chainItem["event_name"] ?></h3></td>
		        	</tr>
		        	<?php endforeach;  ?>
		        </tbody>	
			</table>	
	</div>

</body>
</html>