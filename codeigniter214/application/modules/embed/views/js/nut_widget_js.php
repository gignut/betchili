if (typeof console === "undefined" || typeof console.log === "undefined") {
       window.console = {};
       window.console.log = function(msg) {};
}

function NutWidget(){
	var self = this;
	/*
	var libs = {
		jquery: "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
		labjs: "//cdnjs.cloudflare.com/ajax/libs/labjs/2.0.3/LAB.min.js"
	};
	*/

	var libs = {
		jquery: "<?php echo MEDIA_URL . 'modules/m_embed/js/jquery.min.js'; ?>",
		labjs:  "<?php echo MEDIA_URL . 'modules/m_embed/js/LAB.min.js'; ?>"
	};
	
	var nutWindow 	= null;
	var nutDocument = null;
	
	var widgetContainer   = null;
	
	var widgetHeight;
	var widgetWidth;
	var widgetSelector    = null;
	var widgetUrlOrigin   = null;
	var widgetUrlDefault  = null;
	var validateOriginUrl = null;

	var params = {};

	this.init = function(w, d, options){
		var self = this;
	
		nutWindow    = w;
		nutDocument  = d;

		widgetSelector = options.selector
		
		//widget
		widgetHeight      	= options.height;
		widgetWidth       	= options.width;
		widgetUrlOrigin 	= options.url_origin          || null; 
		widgetUrlDefault	= options.url_default         || null;
		validateOriginUrl 	= options.origin_validate_url || null;

		var renderWidget = function(url){
				//loading labjs
				self.loadDynamicScript(libs.labjs, function(){
						if(self.jqueryExists())
						{
							$(document).ready(function(){
								self.loadContent(url);	
							});
						}
						else
						{
							$LAB
							.script({ src: libs.jquery, type: "text/javascript" })
							.wait(function(){ 
								// wait for all scripts to execute first
								$(document).ready(function(){
									self.loadContent(url);				
								});
							});
						}
				});
		};

		if(validateOriginUrl)
		{
			self.checkOrigin(function(){
				renderWidget(widgetUrlOrigin);
			}, function(){
				renderWidget(widgetUrlDefault);
			});
		}
		else
		{
			renderWidget(widgetUrlDefault);
		}
	};

	this.jqueryExists = function(){
		if (typeof nutWindow.jQuery != 'undefined') {
			return true;  
		} else {
			return false;
		}
	};

	//nutWindow = window, nutDocument = document
	this.loadDynamicScript =  function(scriptPath, callback){
		var head = nutDocument.head || nutDocument.getElementsByTagName("head");
		    // loading code borrowed directly from LABjs itself
	    setTimeout(function () {
	        if ("item" in head) { // check if ref is still a live node list
	            if (!head[0]) { // append_to node not yet ready
	                setTimeout(arguments.callee, 25);
	                return;
	            }
	            head = head[0]; // reassign from live node list ref to pure node ref -- avoids nasty IE bug where changes to DOM invalidate live node lists
	        }
	        var scriptElem = nutDocument.createElement("script");
	        var scriptdone = false;

	        scriptElem.onload = scriptElem.onreadystatechange = function () {
	            if ((scriptElem.readyState && scriptElem.readyState !== "complete" && scriptElem.readyState !== "loaded") || scriptdone) {
	                return false;
	            }
	            scriptElem.onload = scriptElem.onreadystatechange = null;
	            scriptdone = true;
	            callback();
	        };
	        
	        scriptElem.src = scriptPath;
	        head.insertBefore(scriptElem, head.firstChild);

	    }, 0);

	    // required: shim for FF <= 3.5 not having document.readyState
	    if (nutDocument.readyState == null && nutDocument.addEventListener) {
	        nutDocument.readyState = "loading";
	        nutDocument.addEventListener("DOMContentLoaded", handler = function () {
	            nutDocument.removeEventListener("DOMContentLoaded", handler, false);
	            nutDocument.readyState = "complete";
	        }, false);
	    }
	};

	this.appendIframe = function(url){
		var tz = (new Date()).getTimezoneOffset();
		var iframe = widgetContainer.children(".chiliWidgetIframe");				 
		if(iframe.length > 0)
		{
			iframe = iframe[0];
			iframe.src = url + '/'+ tz;
		}
		else
		{
			iframe =    '<iframe  style="position:relative; border: none !important; overflow-y: hidden !important;" scrolling="no" horizontalscrolling="no" verticalscrolling="no" frameborder = "0" class="chiliWidgetIframe" ' + 
							    ' height="'  + widgetHeight + 'px"' +
							    ' width="'   + widgetWidth + 'px"' + 
							    ' src="'     + url + '/'+ tz +'"'+ '>' +
					    '</iframe>';

			widgetContainer.append(iframe);
		}
	};

	this.loadContent = function(url){
		var self = this;
		widgetContainer = $(widgetSelector);
		 self.appendIframe(url);
	};

	this.ieCallback = function(callbackFn, response){
		if(typeof callbackFn != "undefined")
		{
			callbackFn(response);
	  	}
	};

	this.checkOrigin = function(successFn, errorFn){
			var self = this;
			var xhr = null;
			if(nutWindow.XDomainRequest){
				xhr = new XDomainRequest(); 
				xhr.onprogress = function() {};
				xhr.onerror = function(){self.ieCallback(errorFn);};
				xhr.onload = function(){self.ieCallback(successFn);};
			}
			else {
				if(nutWindow.XMLHttpRequest){
					xhr = new XMLHttpRequest();
					
					xhr.onreadystatechange = function (){
						if (xhr.readyState == 4 && xhr.status == 200) {
							if(typeof successFn != "undefined"){
								successFn(xhr.responseText);
						  	}
						}
					};
		
					xhr.onerror = function (){errorFn();};
				}
			}
			
			if(xhr){
				xhr.open("GET", validateOriginUrl , true);
				setTimeout(function () {
				    xhr.send();
				}, 0);
			}
	};

	return this;
}


function post_to_url(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}