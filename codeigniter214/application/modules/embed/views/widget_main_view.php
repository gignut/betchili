<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="chilisiteApp">
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta charset="utf-8" /> 

<script>
  if (typeof console === "undefined" || typeof console.log === "undefined") {
       window.console = {};
       window.console.log = function(msg) { };
  }
</script>

 <!--[if lte IE 7]>
   <script src="<?php echo  MEDIA_URL . '/modules/js/json2.js'; ?>"></script>
<![endif]-->


<script>
    window.BASE_INDEX   = <?php echo '"' . BASE_INDEX  . '"'; ?>;
    window.BASE_DOMAIN  = <?php echo '"' . BASE_DOMAIN  . '"'; ?>;
    window.MEDIA_URL    = <?php echo '"' . MEDIA_URL . '"'; ?>;
    window.BASE_URL     = <?php echo '"' . BASE_URL  . '"'; ?>;
    window.INDEX_PHP    = <?php echo '"' . INDEX_PHP . '"'; ?>;
</script>

<!-- GAME JSON DATA -->
<?php echo $JSON_GAME_DATA; ?>


<?php
  chili_asset_loader::jquery(true, false);
  chili_asset_loader::angularjsVersion(true, "1.2.15", $WIDGET_LANG_CODE);
  chili_asset_loader::bootstrapNotResponsive();
  //chili_asset_loader::angularstrap();
  chili_asset_loader::uibootstrap();
  chili_asset_loader::scrollBar();
  // chili_asset_loader::momentJs();
  
  script("nutron/js/ng-nutron.js", true);
  script("modules/js/jquery.blockUI.js");

  css("modules/m_embed/css/widget_game.css", true);

  script("modules/m_embed/js/ng_chili_module.js", true);
  script("modules/m_embed/js/ng_widget_api.js"  , true);
  script("modules/m_embed/js/ng_widget_game.js" , true);


  // ** introjs tours ** //
  chili_asset_loader::introjs();

  //** ## ICONMOON ## **//
  css("modules/m_embed/iconmoon/style.css", true);
?>

<!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>
(function(){
  var uv=document.createElement('script');
  uv.type='text/javascript';
  uv.async=true;uv.src='//widget.uservoice.com/ChKZHHK02UA6O2nJy4wEg.js';
  var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)
})();

UserVoice = window.UserVoice || [];
// A function to launch the Classic Widget
function showClassicWidget(data) {
  UserVoice.push(['showLightbox', 'classic_widget', {
    mode: 'full',
    primary_color: data.gh_bg_color,
    link_color: data.gh_bg_color,
    default_mode: 'feedback',
    forum_id: 251440
  }]);
}

</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  //ga('create', 'UA-43392137-2', {'cookieDomain': 'none'});
  ga('create', 'UA-43392137-2','betchili.com');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics -->
</head>

<body ng-cloak class="ng-cloak">

<!-- ClickTale Bottom part -->
<script type='text/javascript'>
  // The ClickTale Balkan Tracking Code may be programmatically customized using hooks:
  // function ClickTalePreRecordingHook() { /* place your customized code here */  }
  // For details about ClickTale hooks, please consult the wiki page http://wiki.clicktale.com/Article/Customizing_code_version_2
  /*    
  if(CHILI_ENVIRONMENT) {  
    var WRInitTime=(new Date()).getTime();  
    document.write(unescape("%3Cscript%20src='"+
    (document.location.protocol=='https:'?
    "https://cdnssl.clicktale.net/www07/ptc/e087a2ec-a348-4385-9e40-2e67f5cc5ee5.js":
    "http://cdn.clicktale.net/www07/ptc/e087a2ec-a348-4385-9e40-2e67f5cc5ee5.js")+"'%20type='text/javascript'%3E%3C/script%3E"));
  }
  */
</script>
<!-- ClickTale end of Bottom part -->

  <div>
      <div>
        <div ng-controller = "MainCtrl">
          <div ng-init="initData()">

            <!-- LOADER -->
            <div id="gameLoader" style="background-color:transparent;height:0px;border:0px;display:none;">
                <img style="width: 64px;" src = "<?php echo MEDIA_URL;?>modules/m_embed/img/loader_circle.gif" />
            </div>  


             <div id="notificationDialog" style="display:none; text-align:left; padding: 20px 5px 5px 15px; padding-left: 32px; ">
                <a  class="blockui_close modalCloseBtn" ng-click="closeModal()" 
                    style="cursor:pointer" 
                    tooltip="<?php echo $TR_CLOSE; ?>" tooltip-placement="left">
                  <img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>" />
                </a>
                <div style="text-align:center;">
                  <b><small>{{notification.message}}</small></b>
                </div>  
            </div>

            <div id = "supersonicReminderDialog" style="display:none;  padding: 20px 5px 5px 15px; padding-left: 32px;">
               <a  class="blockui_close modalCloseBtn" ng-click="closeModal()" 
                   style="cursor:pointer"
                   tooltip="<?php echo $TR_CLOSE; ?>" tooltip-placement="left">
                  <img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>" />
                </a>
                <div>
                  
                    <h3>
                        <?php echo $TR_EARN_BOINTS; ?>
                    </h3> 
                      <span aria-hidden="true" class="chili-play-circle iconmoon earnExtraCoins" 
                            ng-click="playSupersonic()" 
                            style="font-size: 110px;
                                   cursor: pointer;
                                   color: rgb(85, 83, 83);">
                    </span>
                 </div> 
            </div>  

            <!-- OPPONENTS MODAL -->
            <div id="choosePlayerModal" style="display:none; text-align:left; padding: 15px; padding-left: 32px; ">
              <a  class="blockui_close modalCloseBtn" ng-click="closeModal()" style="cursor:pointer">
                <img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>" />
              </a>

              <img ng-show = "opponentsLoading" 
                 style="width: 48px;
                    position: absolute;
                    margin-left: -24px;
                    top: 218px;
                    left: 50%;" 
                 src = "<?php echo MEDIA_URL . 'modules/m_embed/img/loader_circle.gif'; ?>"  />

              <h4 style="margin:0px;"><?php echo $TR_CHOOSE_OPPONENT; ?></h4>

              <div class="opponentSearch">
                <i class="icon-search"></i>
                <input class="input" placeholder="<?php echo $TR_TYPE_NAME; ?>" 
                       ng-model="searchInput.userText" 
                       nut-enter="searchOpponent()">
                </input>
              </div>

              <div style="margin-top:20px;"> 
                <div class="activityChallangeOpponent" ng-click="selectOpponent()" style="width:97.2%;">
                  <img style= "width:36px;" src= "<?php echo MEDIA_URL . 'modules/m_embed/img/any_opponent.png'; ?>" />
                    <small><b><?php echo $TR_ANY_OPPONENT; ?></b></small>
                </div>
              </div>

              <div style="margin-top:20px; clear:both" ng-hide = "opponentsLoading"  nut-scroll-bar="370">
                  <div ng-repeat = "opItem in opponentList">
                    <div class="activityChallangeOpponent" ng-click="selectOpponent(opItem)">
                      <img style= "width:36px;" ng-src= "{{opItem.image}}" />
                      <small><b>{{opItem.first_name + ' ' + opItem.last_name }}</b></small>
                    </div>  
                  </div>

                 <div ng-hide = "opponentsLoading || opponentList.length < 20"
                   class="activityChallangeMoreOpponents"
                   ng-click="loadMoreUsers()">
                   <small><b><?php echo $TR_LOAD_MORE; ?></b></small>
                 </div>  
              </div>                          
            </div>  
            <!-- OPPONENTS MODAL -->

            <div style="overflow:hidden; border-style:solid; border:0px;
                  height: <?php echo $height . 'px'; ?>;
                  max-height: <?php echo $height . 'px'; ?>;  
                  width: <?php echo $width . 'px'; ?>;"
                  ng-style="{'background-color': cssModel.body_bg_color}">

                  <!-- HEADER -->
                  <div>
                      <div>
                        <div ng-show = "isAuthorized">
                          <!-- PLAYER UNAUTHORIZED MENU START-->
                          <?php echo $PROFILE_HEADER_CONTENT;?>
                        </div>
                        <div ng-hide = "isAuthorized">
                           <!-- PLAYER AUTHORIZED MENU START-->
                           <?php echo $SOCIAL_LOGIN_CONTENT;?>
                        </div>
                      </div>  
                  </div>
                  <!-- HEADER END -->

                  <!-- GAME LIST ROW START-->
                  <div ng-show = "viewmode == 'MODE_GAME'">
                    <?php echo $CHALLENGES_CONTENT; ?>
                    <?php echo $GAMES_CONTENT; ?>
                  </div>
                  <div ng-show = "viewmode == 'MODE_LEADERBOARD'">
                    <?php echo $LEADERBOARD_CONTENT; ?>
                  </div>
                  <div ng-show = "viewmode == 'MODE_ACTIVITY'">
                    <?php echo $ACTIVITY_CONTENT; ?>
                  </div>
                  <div ng-show = "viewmode == 'MODE_HELP'">
                    <?php echo $HELP_CONTENT; ?>
                  </div>

                  <!-- GAME LIST ROW END-->
            </div>  

          </div>

        </div>
      </div>
  </div>

</body>
</html>