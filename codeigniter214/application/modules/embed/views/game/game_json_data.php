<script>
  <?php
      if (defined('ENVIRONMENT'))
      {
        switch (ENVIRONMENT)
        {
          case 'development':
            echo 'var CHILI_ENVIRONMENT = false;';
            break;
          case 'testing':
          case 'production':
            echo 'var CHILI_ENVIRONMENT = true;';
          break;
          default:
        }
      }
  ?>
</script>

<script>
  // current loaded widget id
  var widgetId          = "<?php echo $id; ?>";
  var widgetLangId      = "<?php echo $language_id; ?>";
  var widgetBookmakerId = "<?php echo $bookmaker_id; ?>";
  var widgetSiteId      = "<?php echo $siteuser_id; ?>";

  var challenges     = <?php echo json_encode($challenges); ?>;
  var cssdata        = <?php echo json_encode($cssdata); ?>;
  var games          = <?php echo json_encode($games); ?>;
  var eventTemplates = <?php echo json_encode($eventTemplates); ?>;
  var leagues	       = <?php echo json_encode($leagues); ?>;
  var competitions   = <?php echo json_encode($competitions); ?>;
  var playerData     = <?php echo json_encode($playerData); ?>;
</script>