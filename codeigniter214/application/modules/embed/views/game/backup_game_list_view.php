<!-- TODO REFACTOR THE JS DATA BY VIEWS -->
<div ng-controller= "GameCtrl"> 

		<!-- BET MODAL  -->
		<div id="betModal" style="display:none; text-align:left; padding: 15px;position:relative;height:100%;">
			<a  ng-hide = "currentBetStatus.state == 'success'"
				class="blockui_close modalCloseBtn chili-clock3 iconmoon" 
				ng-click="closeModal()" 
				style="cursor: pointer; font-size: 18px; top:8px; color: rgb(35, 29, 29);" 
				tooltip="<?php echo $TR_HOLD; ?>" tooltip-placement="left">
			</a>
			
			<!-- max-width:650px; -->
			<div ng-show="currentBetStatus.state =='success'" 
			     style="border-bottom: 2px solid rgb(129, 129, 129);
                        height: 50px;
                        margin-top: 10px;
                        margin-bottom: 10px;
                        padding-bottom:10px;">
				<a  class="blockui_close modalCloseBtn" 
				    ng-click="closeBetModal()" 
				    style="cursor: pointer" 
					tooltip="<?php echo $TR_CLOSE; ?>" tooltip-placement="left">
					<img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png'; ?>" />
				</a>
				
				<!-- SHARE BET -->
				<div  ng-show = "playerData.platform_id == 1" >
					<img ng-click="shareBet()" 
						 style="width:90px;cursor:pointer;" tooltip="<?php echo $TR_SHARE; ?>" tooltip-placement="right" 
						 src = "<?php echo MEDIA_URL .  'modules/m_embed/img/fbShareButton.png'; ?>" />
				</div>

				<div ng-show = "playerData.platform_id == 3"   id="shareBetOdnoklassniki">
				</div>
				<div>
					<span class="pull-left">
						<p style="margin-top:5px;"><?php echo $TR_SHARE_YOUR_BET; ?></p>
					</span>	
					<span class="pull-left" style="width: 32px; margin-top: -10px;">
						<img src="<?php echo MEDIA_URL . 'modules/m_embed/img/shield.png'; ?>" />
					</span>	
					<span class="pull-left">
						<p style="margin-top:5px;"><small style = "font-style: italic;"><?php echo $TR_SHIELD_TIP; ?></small></p>
					</span>	
				
				</div>	
			</div>	

			<table style="margin-bottom:20px;" class="table betTableHeader">
				 	<thead>
				 		<th><h4><?php echo $TR_BET_AMOUNT; ?></h4></th>
				 		<th><h4><?php echo $TR_COEFFICIENT; ?></h4></th>
				 		<th><h4><?php echo $TR_WIN; ?></h4></th>
				 	</thead>	
				 	<tbody>
				 		<tr>
				 			<td class="part1">
							 <div ng-form = "betForm" class="betForm"
							 	  style="margin-top:10px; 
							 			 position:relative; 
							 			 width: <?php echo $cssdata['g_width'] . 'px'; ?>;" >
								<input ng-hide="currentBetStatus.state == 'success'" 
								       name="betAmountInput" 
								       ng-model = "betAmount" 
								       chili-digit maxlength="5" 
								       placeholder="<?php echo $TR_AMOUNT; ?>" 
							   		   class="betAmountInput"
							   		   bet-min="<?php echo MIN_BET_AMOUNT; ?>" bet-max="<?php echo MAX_BET_AMOUNT; ?>">
							    
							    <span  ng-show="currentBetStatus.state =='success'" >
							    	<h4>{{betAmount}}</h4>
							    </span>	
							    
							    <div class="betAmountErrors">
						    		<div ng-show = "betForm.betAmountInput.$dirty && ( betForm.betAmountInput.$error.betMax || betForm.betAmountInput.$error.betMin) ">
							    	  <small><b><?php echo $TR_MINMAX_BET_AMOUNT; ?></b></small>
							    	</div>	
							    </div>	
							
								<div ng-show="currentBetStatus.state == 'initial'" 
								     class="btn-group" style="margin-top: -12px;float: right; width:160px;">
								  <!-- 	<button class="btn" ng-click="closeBetModal()" style="margin-top: 12px;" 
								  		ng-class="{disabled : (betchain.length == 0) }"
								  		ng-disabled="(betchain.length == 0)">
										<i class=" icon-time"></i>
										<?php //echo $TR_HOLD; ?>
									</button> -->
								 	<button class="btn" 
											ng-click="setBet()" 
											style="margin-top: 12px;" 
											ng-class="{disabled : (betchain.length == 0 || !betForm.$valid  || betForm.$pristine) }"
											ng-disabled="(betchain.length == 0 || !betForm.$valid  || betForm.$pristine)">
										<i class="icon-star"></i>
										<?php echo $TR_BET; ?>
									</button>
								</div>

								<img  ng-show="currentBetStatus.state =='betting'" 
									      src = "<?php echo MEDIA_URL;?>modules/m_embed/img/loader_betting.gif" 
									      style="margin-top: 2px;" />

							</div>
							</td>
							<td class="part2"><h4>{{totalCoeff}}</h4></td>	
					 		<td class="part3"><h4>{{(totalCoeff * betAmount).toFixed(2)}}</h4></td>	
				 		</tr>
				 	</tbody>	
			</table>

			<!-- max-width:650px; -->
			<table style="width:100%;" class="betGameTable">
				<thead>
					<th><small><?php echo $TR_GAME; ?></small></th>
		            <th><small><?php echo $TR_ODD; ?></small></th>
		            <th><small><?php echo $TR_SELECTION; ?></small></th>
		            <th><small></small></th>	
				</thead>
				<tbody>
		        	<tr ng-repeat = "betitem in betchain" 
		        		class="betRow"
		        		style="background-color: rgb(215, 215, 215);">
		        		<td>			
		        			<div>
		            			<div class="betItem"
									 style = "height: 50px; width: <?php echo $cssdata['g_width'] . 'px'; ?>;" >
										<div class="gameItemTime">
											<span><small><b>{{betitem.game.start_date * 1000 | date : 'dd MMM H:mm'}}</b></small></span>
										</div>	
										<!-- PARTICIPANT 1 -->
										<div class="teamImg1">
											<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{betitem.game.aux_info.participants[0].logo}}" />	
										</div>
										<div class="teamName1"><small><b>{{betitem.game.aux_info.participants[0].tname}}</b></small></div>	
										<!-- PARTICIPANT2 -->
										<div class="teamImg2" picker-click="g_img_bg_color">
											<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{betitem.game.aux_info.participants[1].logo}}" />	
										</div>
										<div class="teamName2"><small><b>{{betitem.game.aux_info.participants[1].tname}}</b></small></div>	
								</div>
							</div>
						</td>
		        		<td class="part2">
		        			<h4>{{betitem.coeff}}</h4>
		        		</td>
		        		<!-- <td><h4>{{betitem.basis}}</h4></td> -->
		        		<td class="part3"><h4>{{eventTemplatesData[betitem.eventTemplateId].tname}}</h4></td>
		        		<td class="part4">
		        			<span ng-hide = "currentBetStatus.state == 'success'"
		        				  class="removeBet" style="cursor:pointer" 
		        			      tooltip="<?php echo $TR_REMOVE; ?>" tooltip-placement="left">
								 <a ng-click="removeBetItem($index)">	
								 	<img src="<?php echo MEDIA_URL . 'modules/m_embed/img/removebet.png' ?>">
								 </a>
							</span>	
		        		</td>
		        	</tr>	
		        </tbody>	
			</table>	
		</div>

		<!-- REPLACE BET DIALOG START -->
		<div id="betReplaceDialog" style="display:none; text-align:left; padding: 15px;position:relative;height:100%;">
			<a  class="blockui_close modalCloseBtn" ng-click="closeModal()" style="cursor: pointer">
				<img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>">
			</a>

			<h4  ng-show="gamePrevBetItem.eventTemplateId != gameNextBetItem.eventTemplateId"><?php echo $TR_BET_ON_SAME_GAME; ?></h4>
			<h4  ng-show="gamePrevBetItem.eventTemplateId == gameNextBetItem.eventTemplateId"><?php echo $TR_EXISTING_BET; ?></h4>
			<!-- max-width:650px; -->
			<table style="width:100%;margin-bottom:35px;" class="betGameTable">
				<thead>
					<th><small><?php echo $TR_GAME; ?></small></th>
		            <th><small><?php echo $TR_ODD; ?></small></th>
		            <th><small><?php echo $TR_SELECTION; ?></small></th>
		            <th></th>	
				</thead>
				<tbody>
		        	<tr class="betRow"
		        		style="background-color: rgb(215, 215, 215);" >
		        		<td>			
		        			<div >
		            			<div class="betItem" picker-click="g_left_font_color"
									 style="height: 50px;
									 		width: <?php echo $cssdata['g_width'] . 'px'; ?>;" >
										<div class="gameItemTime">
											<span><small><b>{{gamePrevBetItem.game.start_date * 1000 | date : 'dd MMM H:mm'}}</b></small></span>
										</div>	
										<!-- PARTICIPANT 1 -->
										<div class="teamImg1" picker-click="g_img_bg_color">
											<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{gamePrevBetItem.game.aux_info.participants[0].logo}}" />	
										</div>
										<div class="teamName1"><small><b>{{gamePrevBetItem.game.aux_info.participants[0].tname}}</b></small></div>	
										<!-- PARTICIPANT2 -->
										<div class="teamImg2" picker-click="g_img_bg_color">
											<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{gamePrevBetItem.game.aux_info.participants[1].logo}}" />	
										</div>
										<div class="teamName2"><small><b>{{gamePrevBetItem.game.aux_info.participants[1].tname}}</b></small></div>	
								</div>
							</div>
						</td>
		        		<td class="part2">
		        			<h4>{{gamePrevBetItem.coeff}}</h4>
		        		</td>

		        		<td class="part3"><h4>{{eventTemplatesData[gamePrevBetItem.eventTemplateId].tname}}</h4></td>
		        		<td class="part4">
		        		</td>
		        	</tr>	
		        </tbody>	
			</table>
			
			<div ng-show="gamePrevBetItem.eventTemplateId != gameNextBetItem.eventTemplateId">
				<h4><?php echo $TR_WANT_TO_REPLACE_BET; ?></h4>
				<table style="width:100%;" class="betGameTable">
					<thead>
						<th><small><?php echo $TR_GAME; ?></small></th>
			            <th><small><?php echo $TR_ODD; ?></small></th>
			            <th><small><?php echo $TR_SELECTION; ?></small></th>
			            <th></th>	
					</thead>
					<tbody>
			        	<tr class="betRow"
			        		style="background-color: rgb(215, 215, 215);">
			        		<td>			
			        			<div >
			            			<div class="betItem" picker-click="g_left_font_color"
										 style="height: 50px;
										 		width: <?php echo $cssdata['g_width'] . 'px'; ?>;" >
											
											<div class="gameItemTime">
												<span><small><b>{{gameNextBetItem.game.start_date * 1000 | date : 'dd MMM H:mm'}}</b></small></span>
											</div>	
											<!-- PARTICIPANT 1 -->
											<div class="teamImg1">
												<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{gameNextBetItem.game.aux_info.participants[0].logo}}" />	
											</div>
											<div class="teamName1"><small><b>{{gameNextBetItem.game.aux_info.participants[0].tname}}</b></small></div>	
											<!-- PARTICIPANT2 -->
											<div class="teamImg2">
												<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{gameNextBetItem.game.aux_info.participants[1].logo}}" />	
											</div>
											<div class="teamName2"><small><b>{{gameNextBetItem.game.aux_info.participants[1].tname}}</b></small></div>	
									</div>
								</div>
							</td>
			        		<td class="part2">
			        			<h4>{{gameNextBetItem.coeff}}</h4>
			        		</td>

			        		<td class="part3"><h4>{{eventTemplatesData[gameNextBetItem.eventTemplateId].tname}}</h4></td>
			        		<td class="part4">
			        		</td>
			        	</tr>	
			        </tbody>	
				</table>	
			</div>

			<div style="text-align:center;margin-top:30px;">
					<button class="widgetBtn" ng-click="replaceBet();"  ng-show="gamePrevBetItem.eventTemplateId != gameNextBetItem.eventTemplateId">
						<?php echo $TR_BET_REPLACE; ?>
					</button>
					<button class="widgetBtn" ng-click="closeModal();">
						<?php echo $TR_CANCEL; ?>
					</button>
			</div>	
		</div>
		<!-- REPLACE BET DIALOG END -->

		<div id="maxBetCountDialog" style="display:none; text-align:left; padding: 15px;position:relative;height:100%;">
			<a  class="blockui_close modalCloseBtn" ng-click="closeModal()" style="cursor: pointer">
				<img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>">
			</a>
		</div>

		<div id="leaguesModal" style="display:none; text-align:left; padding: 15px;position:relative;height:100%;">
			<a  class="blockui_close modalCloseBtn" ng-click="closeLeaguesModal()" style="cursor: pointer">
				<img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>" />
			</a>

			<h4 style="margin: 0px;margin-left: 9px;">
				<?php echo $TR_SELECT_COMPETITION; ?>
			</h4>

			<div class="competitionItem" style="text-align:center;" 
				 ng-click="filterByLeague({id: null})"
				 tooltip="<?php echo $TR_ALL_LEAGUES; ?>" tooltip-placement="bottom">
				<!-- <img src = "<?php // echo MEDIA_URL;?>modules/m_embed/img/world1.png" /> -->
				<span aria-hidden="true" 
					  class = "chili-globe iconmoon"
					  style = "color: #444444; margin-top: 4px; font-size: 56px; display: inline-block;">
				</span>
			</div>	

			<div ng-repeat = "leagueItem in leagues" 
				 class="competitionItem" 
				 ng-click="filterByLeague(leagueItem)"
				 tooltip="{{leagueItem.tname}}" tooltip-placement="bottom">
				<img ng-src = "<?php echo MEDIA_URL;?>assets/leagues/{{leagueItem.logo}}"/>
			</div>	
		</div>	


		<div ng-init="initGameData()">
			<!-- GAME EVENT TEMPLATES -->
			<div class="clearfix chiliGamesTitle"  picker-click="gh_bg_color"
				 style="height: <?php echo $cssdata['gh_height']. 'px'; ?>; border-bottom-width: <?php echo $cssdata['g_border_size']. 'px'; ?>; "
				 ng-style = "{ 'border-bottom-color' : cssModel.g_border_color,
						       'background-color': cssModel.gh_bg_color }">

					<div class="gameFilterPicker">

						<div class="pickCompetition" ng-click="openLeaguesModal();" tooltip="<?php echo $TR_SELECT_LEAGUE;?>" tooltip-placement="right"
							 ng-style="{ 'background-color' : cssModel.gh_tmp_bg_color}" picker-click="gh_tmp_bg_color">
							<!-- <img ng-show = "!gameFilterData.league.id" src = "<?php // echo MEDIA_URL;?>modules/m_embed/img/world1.png" /> -->
							<span ng-show = "!gameFilterData.league.id" aria-hidden="true" 
								  class = "chili-globe iconmoon"
								  style="font-size: 21px;"
								  ng-style = "{ color: cssModel.gh_tmp_font_color }" picker-click="gh_tmp_font_color">
							</span>
							<img ng-if = "gameFilterData.league.id" ng-src = "<?php echo MEDIA_URL;?>assets/leagues/{{gameFilterData.league.logo}}" />
						</div>

						
						<div class="datePicked" ng-repeat = "dateItem in dateRange" 
							 ng-class="{datePickedActive: dateItem == gameFilterData.date}"
							 ng-style="{ 'background-color' : cssModel.gh_tmp_bg_color}" picker-click="gh_tmp_bg_color">
							<div ng-click= "filterByTime(dateItem)" picker-click="gh_tmp_font_color" class="pickerDate"  ng-style="{ color : cssModel.gh_tmp_font_color}">{{dateItem  | date : 'd'}}</div>
							<div ng-click= "filterByTime(dateItem)" picker-click="gh_tmp_font_color" class="pickerMonth" ng-style="{ color : cssModel.gh_tmp_font_color}">{{dateItem | date : 'MMM' }}</div>
						</div>
						<!-- <div class="dateAction"><img src= "<?php //echo MEDIA_URL?>modules/m_embed/img/icon-chevron-right-16.png" /></div>	 -->
					</div>	
				
					<!-- EVENT TEMPLATES -->
					<!-- style="right: <?php //echo $cssdata['events_right_position'] . 'px'; ?>;" -->
					<div class="eventTemplatesContainer">
						<div ng-repeat="template in eventTemplatesData" class="pull-left">
							<div ng-switch on = "template.id <= 11 ">
								<div ng-switch-when ="true">
									<div class="odd">
										<div class="coeff" picker-click="gh_tmp_bg_color"
											 ng-style="{ 'background-color' : cssModel.gh_tmp_bg_color }">
											<span picker-click="gh_tmp_font_color" ng-style="{ color : cssModel.gh_tmp_font_color }">{{template.tname}}</span>
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
			</div>	
			
			<!-- MESSAGE -->
			<div ng-show="showMessage" style="position:relative;text-align:center;">
				<div style="padding-top:10px; padding-bottom:10px; top:0px; font-style:italic; color:gray; font-size: 12px;">
					<?php echo $TR_GAMESMESSAGE; ?>{{showMessage * 1000 | date : 'dd MMM'}}
				</div>
				 <a  class="blockui_close modalCloseBtn" ng-click="showMessage=null" style="cursor:pointer">
                    <img src="<?php echo MEDIA_URL . 'modules/m_embed/img/close24.png' ?>" />
                 </a>
			</div>

			<!-- GAMES LIST -->
			<div class="gameListContainer" nut-scroll-bar="<?php echo (intval($height) - $cssdata['h_menu_height'] - $cssdata['challenge_height'] - $cssdata['gh_height']); ?>" >
				<div ng-repeat = "(cmpId, gameGroup) in games track by $index" class="chiliGameRow chRow" repeat-complete="documentReady()">
							<div class="league_games" picker-click="gh_bg_color" 
								style="border-bottom-width: <?php echo $cssdata['g_border_size']. 'px'; ?>;  border-bottom-style: solid;"
								ng-style=" { 'background-color': cssModel.gh_bg_color, 'border-bottom-color' :  cssModel.g_border_color }"
								ng-click="showList[cmpId] = ! (showList[cmpId])">

									<span ng-show="!showList[cmpId]" class="cmpGroup chili-angle-right iconmoon" style="left:17px;" ng-style ="{ color: cssModel.gh_tmp_bg_color}"> </span>
									<span ng-show="showList[cmpId]"  class="cmpGroup chili-angle-down iconmoon"  style="left:13px;" ng-style ="{ color: cssModel.gh_tmp_bg_color}"> </span>

									<img class="league_img" ng-src="<?php echo MEDIA_URL . 'assets/competitions/';?>{{getCompetitionDataById(cmpId, 'logo')}}"></img>
									<span picker-click="gh_tmp_bg_color" ng-style=" { color: cssModel.gh_tmp_bg_color}" class="league_text">{{getCompetitionDataById(cmpId, 'tname')}}</span>
									
							</div>
					<div  ng-repeat="game in gameGroup" ng-show="showList[cmpId]" class="animate-show">
						<div class="chSection" 
							   style="border-bottom-width: <?php echo $cssdata['g_border_size']. 'px'; ?>;  border-bottom-style: solid;"
							   ng-style= "{ 'background-color' : cssModel.g_right_bg_color,  'border-bottom-color' : cssModel.g_border_color}"
							   picker-click="g_right_bg_color">
							<div class="game" picker-click="g_left_bg_color"
								 ng-style=" { 'background-color' : cssModel.g_left_bg_color, 
								 		      color : cssModel.g_left_font_color }"
								 style="height: <?php echo $cssdata['g_height'] . 'px'; ?>;
								 		width:  <?php echo $cssdata['g_width'] . 'px'; ?>;" >

									<div ng-if="!(game.result_text)" class="gameItemStatus">
										<div ng-show="isGamePassed(game.start_date)">
											<i class="gameWait icon-info-sign"  tooltip="<?php echo $TR_RESULT_SOON; ?>" tooltip-placement="bottom"></i>
										</div>	
									</div>	
									<div ng-if="game.result_text" class="gameItemStatus">
										{{game.result_text}}
									</div>

									<!-- GAME ITEM TIME  -->
									<div class="gameItemTime">
										<span picker-click="g_left_font_color" ng-style="{ color : cssModel.g_left_font_color}"><small><b>{{game.start_date * 1000 | date : 'dd MMM H:mm'}}</b></small></span>
									</div>	
									<!-- PARTICIPANT 1 -->
									<div class="teamImg1" picker-click="g_img_bg_color"
										 ng-style="{ 'background-color' : cssModel.g_img_bg_color }">
										<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{game.aux_info.participants[0].logo}}" />	
									</div>
									<div class="teamName1"><small picker-click="g_left_font_color" ng-style="{ color : cssModel.g_left_font_color}"><b>{{game.aux_info.participants[0].tname}}</b></small></div>	
									<!-- PARTICIPANT2 -->
									<div class="teamImg2" picker-click="g_img_bg_color"
										 ng-style="{ 'background-color' : cssModel.g_img_bg_color}">
										<img ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{game.aux_info.participants[1].logo}}" />	
									</div>
									<div class="teamName2"><small picker-click="g_left_font_color" ng-style="{ color : cssModel.g_left_font_color}"><b>{{game.aux_info.participants[1].tname}}</b></small></div>	
							</div>
						</div>

						<div class="chSection" id="gameEvents" 
							   style =" border-bottom-style:solid; border-bottom-width: <?php echo $cssdata['g_border_size']. 'px'; ?>;"
							   ng-style ="{ 'background-color' : cssModel.g_right_bg_color, 'border-bottom-color' : cssModel.g_border_color }"
							   picker-click="g_right_bg_color">
							
							<div  picker-click="g_tmp_font_color" 
								ng-style="{ color : cssModel.g_tmp_font_color}" 
								style="width: <?php echo (intval($width) - intval($cssdata['g_width'])) . 'px'; ?>;
									   height: <?php echo $cssdata['g_height'] . 'px'; ?>;">
								<div class="events" style="right: <?php echo $cssdata['events_right_position'] . 'px'; ?>;"
									 ng-class="{gameItemDisabled : isGamePassed(game.start_date)}">

									<div  ng-repeat = " odd in game.aux_info.events.base_events" class="eventItem" ng-click="openBetcart(game, odd)" style="cursor:pointer;">
										<div ng-switch on = "odd.template_id <= 6 " >
											<div ng-switch-when ="true" >
												<div ng-show = "odd.coefficient > 1 && odd.blocked != '1' ">
													<div class="odd" >
														<div class="coeff"  picker-click="g_tmp_bg_color" 
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color}">
															 {{odd.coefficient}}
														</div>
													</div>
												</div>
												<div ng-show = "odd.coefficient <= 1 || odd.blocked == '1'">
													<div class="odd">
														<div class="coeff" picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">
															 -
														</div>
													</div>
												</div>
											</div>	
										</div>	
										<!-- CSE H1 H2 -->
										<div ng-switch on = " odd.template_id == 7 ||  odd.template_id == 8" >
											<div ng-switch-when ="true">
												<div ng-show = "odd.coefficient > 1 && odd.blocked != '1' ">
													<div class="odd">
														<div class="coeff" picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">
															{{odd.coefficient}}
														</div>
														<div class="basis" picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">
															{{odd.basis}}
														</div>
													</div>
												</div>
												<div ng-show = "odd.coefficient <= 1 || odd.blocked == '1' ">
													<div class="odd">
														<div class="coeff" picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">
															-
														</div>	
														<div class="basis" picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">	
															 - 
														</div>	
													</div>
												</div>
											</div>	
										</div>

										<!-- CASE TOTALS -->
										<!-- 
											TODO::CHILI IMPORTANT  game.aux_info.events.base_events[9] is T = total 
											we need to change events to be not array but object with template id
										-->
										<div ng-switch on = "odd.template_id == 9 ||  odd.template_id == 10 || odd.template_id == 11  ">
											<div ng-switch-when ="true">
												<div ng-show = "odd.coefficient > 1 && odd.blocked != '1' && game.aux_info.events.base_events[9].blocked != '1'">
													<div class="odd">
														<div class="coeff"  picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">
															{{odd.coefficient}}
														</div>
													</div>
												</div>
												<div ng-show = "odd.coefficient <= 1 || odd.blocked == '1' || game.aux_info.events.base_events[9].blocked == '1'">
													<div class="odd">
														<div class="coeff" picker-click="g_tmp_bg_color"
															 ng-style="{ 'background-color' : cssModel.g_tmp_bg_color }">
															 -
														</div>
													</div>
												</div>
											</div>	
										</div>	
									</div>	
								</div>	
								
								<div id="chall" class="challengeAction" ng-show="!isGamePassed(game.start_date)" >
									<a href="" ng-click="prepareChallenge(game);"><small><?php echo $TR_CHALLENGE; ?></small></a>
								</div>
							</div>

						</div>
					</div>
				</div>	
			</div>	

		</div>	
</div>