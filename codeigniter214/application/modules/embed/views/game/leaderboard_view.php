<div ng-controller="LeaderboardCtrl">

      <div  class="activityEven"
                ng-style="{ 'border-color': cssModel.h_bg_color }"
                style="border-style:solid; border-width:4px;">
            <span style= "margin-left:10px; display: inline-block; width: 32px;">
              <h4 style="display:inline-block;">#{{currentUserData.rank}}</h4>
            </span>  
            <span style= "margin-left:10px;">
              <img style= "width:32px;"ng-src = "{{currentUserData.image}}"/>
            </span>    
            <span style= "margin-left:10px;width:200px;display:inline-block;">
              <small><b>{{currentUserData.first_name + ' ' + currentUserData.last_name}}</b></small>
            </span>
            <span>
              <img style="width:16px;margin-top: -4px;" src = "<?php echo MEDIA_URL;?>modules/m_embed/img/bcoin.png" />
            </span> 
            <span style="width:100px;display:inline-block;">
              <h5 style="display:inline-block;">{{currentUserData.balance}}</h5>
            </span>
            <span>
              <img style="width:16px;margin-top: -4px;" src = "<?php echo MEDIA_URL;?>modules/m_embed/img/target.png" tooltip="<?php echo $TR_ACCURACY;?>" tooltip-placement="bottom"/>
            </span> 
            <span>
              <h5 style="display:inline-block;">{{currentUserData.accuracy}}%</h5>
            </span>     
       </div>


   <div nut-scroll-bar="<?php echo (intval($height) - $cssdata['h_menu_height'] - 50); ?>" >
      <div  ng-repeat="userData in leaderboardData"
                ng-class-odd="'activityOdd'"  
                ng-class-even="'activityEven'"
                style="border-bottom:1px dashed gray;"
                ng-class="{activityUserRank : userData.id == player_id}">
            <span style= "margin-left:10px; display: inline-block; width: 32px;">
            	<h4 style="display:inline-block;">#{{$index+1}}</h4>
            </span>  
            <span style= "margin-left:10px;">
            	<img style= "width:32px;"ng-src = "{{userData.image}}"/>
            </span>    
            <span style= "margin-left:10px;width:200px;display:inline-block;">
            	<small><b>{{userData.first_name + ' ' + userData.last_name}}</b></small>
            </span>
            <span>
            	<img style="width:16px;margin-top: -4px;" src = "<?php echo MEDIA_URL;?>modules/m_embed/img/bcoin.png" />
            </span> 
            <span style="width:100px;display:inline-block;">
            	<h5 style="display:inline-block;">{{userData.balance}}</h5>
            </span>
            <span>
            	<img style="width:16px;margin-top: -4px;" src = "<?php echo MEDIA_URL;?>modules/m_embed/img/target.png" tooltip="<?php echo $TR_ACCURACY;?>" tooltip-placement="bottom"/>
            </span> 
            <span>
            	<h5 style="display:inline-block;">{{userData.accuracy}}%</h5>
            </span>     
       </div>
       <div class="activityBottomActions" ng-show="leaderboardData.length > 14"
               ng-style="{ 'background-color' : cssModel.h_bg_color, color : cssModel.h_font_color }"
               ng-click = "loadLeaderboard()">
              <small><b><?php echo $TR_LOAD_MORE; ?></b></small>
       </div>
   </div>
</div>       
          
