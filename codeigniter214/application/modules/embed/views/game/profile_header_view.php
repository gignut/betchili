<div ng-controller="ProfileHeaderCtrl" 
	 style="position: relative; height: <?php echo  $cssdata['h_menu_height'] . 'px' ; ?>;" 
	 ng-style ="{ 'background-color': cssModel.h_bg_color, color: cssModel.h_font_color }" 
	 picker-click="h_bg_color">
		
	<div>	
		<img class="chiliProfileImg" ng-src = "{{playerData.image}}" picker-click="stop"/>
		<span class="headerTxtContent" 
			  ng-style="{color: cssModel.h_font_color}">
			
			<div picker-click="h_font_color">
				{{playerData.first_name}} {{playerData.last_name}}
			</div>
			<div style="text-align:left;">
				<span aria-hidden="true" class="chili-money27 iconmoon"  picker-click="h_font_color"> </span>
				<span  picker-click="h_font_color">{{playerData.balance}}</span>
			</div>	
			<!-- <span aria-hidden="true" class="chili-diamond iconmoon"> </span> -->
			
			<!-- SUPERSONIC -->
			<span aria-hidden="true" class="chili-play-circle iconmoon offerIcon" 
			      ng-click="playSupersonic()" 
			      ng-show="supersonic.hasOffer"
				  picker-click="h_icon_color" 
				  tooltip="<?php echo $TR_EARN_BOINTS; ?>" 
				  tooltip-placement="bottom" 
				  style="font-size: 41px;
						 position: absolute;
						 cursor: pointer;
						 top: 8px;
						 left: 185px;">
			</span>

		</span>
	</div>

	<span class="pull-right headerMenu" ng-style="{color: cssModel.h_icon_color}">
		<div class="headerMenuItem" ng-click="setViewMode('MODE_GAME');" picker-click="h_icon_color" tooltip="<?php echo $TR_MENU_HOME; ?>" tooltip-placement="bottom">
			<span aria-hidden="true" class="chili-dwelling1 iconmoon" ></span>
		</div>	
		<div id="menuActivityId" class="headerMenuItem" ng-click="setViewMode('MODE_ACTIVITY');" picker-click="h_icon_color" tooltip="<?php echo $TR_MENU_ACTIVITY; ?>" tooltip-placement="bottom">
			<span aria-hidden="true" class="chili-clipboard52 iconmoon"></span>
		</div>	
		<div class="headerMenuItem" ng-click="setViewMode('MODE_LEADERBOARD');" picker-click="h_icon_color" tooltip="<?php echo $TR_MENU_USERS; ?>" tooltip-placement="bottom">
			<span aria-hidden="true" class="chili-competition iconmoon"></span>
		</div>	

		<div class="headerMenuItem" picker-click="h_icon_color" ng-click="openBetcart();" style="position:relative;" ng-class="{disabledBetCart : betCartItemsQty == 0}"
				tooltip="<?php echo $TR_MENU_CART; ?>" tooltip-placement="bottom">
		     	<span class="betCartCountLabel" ng-style="{ color: cssModel.h_bg_color}">
					  <small><b>{{betCartItemsQty}}</b></small>
				</span>
				<span aria-hidden="true" class="chili-cart4 iconmoon"></span>
		</div>	

	<!-- USERVOICE -->
	<!-- 	<div class="headerMenuItem" tooltip="<?php //echo $TR_FEEDBACK; ?>" tooltip-placement="bottom" id="chiliUserVoice">
			<span aria-hidden="true" class="chili-bullhorn iconmoon" ng-click="showFeedbackModal()"></span>
		</div>	 -->


		<div class="headerMenuItem" tooltip="<?php echo $TR_MENU_HELP; ?>" tooltip-placement="bottom" id="chiliUserVoice">
			<span aria-hidden="true" class="chili-faq iconmoon" ng-click="setViewMode('MODE_HELP');" ></span>
		</div>	

		<div class="headerMenuItem" tooltip="<?php echo $TR_MENU_EXIT; ?>" tooltip-placement="bottom" id="chiliUserVoice">
			<span aria-hidden="true" class="chili-power18 iconmoon" ng-click="logoutPlayer();"></span>
		</div>	

<!-- DROP DOWN MENU -->
<!-- 		<div class="headerMenuItem headerMenuOption"  ng-mouseover = "toggleMenu(true);" ng-mouseleave = "toggleMenu(false);" picker-click="h_icon_color">
			<span aria-hidden="true" class="chili-menu10 iconmoon" style="font-size:20px;"></span>
			
			<div ng-show="showMenu"  
				 ng-mouseleave = "toggleMenu(false);"
				 class="headerMenuDropdown"
				 ng-style="{ 'background-color' : cssModel.h_bg_color}">

				<div class="dropdownItem" ng-click="openHelpDialog();" picker-click="h_icon_color" tooltip="<?php //echo $TR_MENU_HELP; ?>" tooltip-placement="left">
					<span aria-hidden="true" class="chili-faq iconmoon"></span>
				</div>	

				<div class="dropdownItem" ng-click="logoutPlayer();" picker-click="h_icon_color" tooltip="<?php //echo $TR_MENU_EXIT; ?>" tooltip-placement="left">
					<span aria-hidden="true" class="chili-power18 iconmoon"></span>
				</div>	
			</div>	
		</div>	 
-->
	</span>

</div>	

<!-- MENU -->