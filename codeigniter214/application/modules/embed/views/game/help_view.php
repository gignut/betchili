<style>
.content-help {
	width:100%;
}
.block-head {
	background: #c1c1c1 url("<?php echo MEDIA_URL; ?>modules/m_embed/img/block-head-bg.jpg"); 
	padding: 5px 10px; 
	border-radius: 5px 5px 0 0; 
	font-weight: bold; 
	font-size: 12px;
}
.leftbar {
	margin: 0px 0px 0 0px;
}
.content {
	background:#FFFAF0; 
	padding: 5px 10px; 
	border-radius: 0 0 5px 5px; 
	font-size: 12px;
}
.nav-tabs > .active > a, .nav-tabs > .active > a:hover {
	background-color: #FFFAF0;
}
.nav > li > a:hover {
	background-color: #DBDBDB;
}
.nav {
	margin-bottom: 10px; margin-top:10px;
}
</style>

<div class="content-help">
	<div class="leftbar">
		<div class="block-head" style="position:relative">
			<i class="icon-question-sign"></i> <b><?php echo $TR_HELP_TITLE; ?></b>
			<div style="position:absolute; right:5px; top:5px;"><a href="#" ng-click="showIntroTour()"><i class="icon-info-sign"></i> <b><?php echo $TR_G_HELP_POINT8; ?></b></a></div>
		</div>
		<div class="content" style="height: <?php echo (intval($height) - $cssdata['h_menu_height']) . 'px'; ?>">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab-main" data-toggle="tab"><?php echo $TR_NAV_HELP_GEN; ?></a></li>
				<li><a href="#tab-money" data-toggle="tab"><?php echo $TR_NAV_HELP_MONEY; ?></a></li>
				<li><a href="#tab-bet" data-toggle="tab"><?php echo $TR_NAV_HELP_BETS; ?></a></li>
				<li><a href="#tab-multibet" data-toggle="tab"><?php echo $TR_NAV_MULTI_BETS; ?></a></li>
				<li><a href="#tab-bet-odds" data-toggle="tab"><?php echo $TR_NAV_HELP_BETO; ?></a></li>
				<li><a href="#tab-challenge" data-toggle="tab"><?php echo $TR_NAV_HELP_CHAL; ?></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab-main">
					<ul>
						<li><?php echo $TR_G_HELP_POINT1; ?></li>
						<li><?php echo $TR_G_HELP_POINT2; ?></li>
						<li><?php echo $TR_G_HELP_POINT3; ?></li>
						<li><?php echo $TR_G_HELP_POINT4; ?></li>
						<li><?php echo $TR_G_HELP_POINT5; ?></li>
						<li><?php echo $TR_G_HELP_POINT6; ?></li>
					</ul>
				</div>
				<div class="tab-pane" id="tab-bet">
					<ul>
						<li><?php echo $TR_B_HELP_POINT1; ?></li>
						<li><?php echo $TR_B_HELP_POINT2; ?></li>
						<li><?php echo $TR_B_HELP_POINT3; ?><span class="activityBet"><b>Bet</b></span></li>
						<li><?php echo $TR_B_HELP_POINT4; ?></li>
						<li><?php echo $TR_B_HELP_POINT5; ?></li>
						<li><?php echo $TR_B_HELP_POINT6; ?></li>
						<li>
							<?php echo $TR_B_HELP_POINT7; ?>
							
									<ul>
										<li><?php echo $TR_B_HELP_POINT8; ?> - <b><?php echo MIN_BET_AMOUNT;?></b></b>.</li>
										<li><?php echo $TR_B_HELP_POINT9; ?> - <b><?php echo MAX_BET_AMOUNT;?></b></b>.</li>
									</ul>
							</ul>
						</li>
					</ul>
				</div>
				<div class="tab-pane" id="tab-multibet">
					<ul>
						<li><?php echo $TR_MB_HELP_POINT1; ?></li>
						<li><?php echo $TR_MB_HELP_POINT2; ?></li>
						<li><?php echo $TR_MB_HELP_POINT3; ?></li>
						<li><?php echo $TR_MB_HELP_POINT4; ?><span class="activityBet"><b>Bet</b></span></li>
						<li><?php echo $TR_MB_HELP_POINT5; ?></li>
					</ul>
				</div>				
				<div class="tab-pane" id="tab-money">
					<ul>
						<li><?php echo $TR_M_HELP_POINT1;?><span aria-hidden="true" class="chili-money27 iconmoon" picker-click="h_font_color"> </span><?php echo $TR_M_HELP_POINT2;?></li>
						<li><?php echo $TR_M_HELP_POINT3;?></li>
						<li><?php echo $TR_M_HELP_POINT4;?></li>
					</ul>
				</div>

				<div class="tab-pane" id="tab-bet-odds">
					<div><span class="label label-warning"><?php echo $TR_ATTENTION; ?></span> <?php echo $TR_B_HELP_POINT5; ?></div>
					<br>
					<div >
						<div style="float:left">
		                    <div ng-show="et.id <= 11" class="helpodd" ng-repeat="et in eventTemplatesData">
		                    	<div class="helpcoeff">{{et.tname}}</div>
		                	</div>
	                	</div>
		                <div style="float:left">
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_0; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_1; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_2; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_3; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_4; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_5; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_6; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_7; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_8; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_9; ?></div>
		                  <div class="helpodd"><?php echo $TR_ODD_EXP_10; ?></div>
		                </div>
	            	</div>
				</div>				

				<div class="tab-pane" id="tab-challenge">
					<div >
		                <div>
		                  <div style="font-size:17px; font-style:italic; font-weight:900"> <?php echo $TR_CH_HELP_MAIN; ?> </div>
		                    <div style="margin-top:15px"> <span class="label label-warning"><?php echo $TR_ATTENTION; ?></span> <span style="font-style:italic; font-size: 12px"><?php echo $TR_CH_HELP_SUB; ?> </span></div>
		                    <ul style="color:gray">
		                     <li> <?php echo $TR_CH_HELP_POINT1?> </li>
		                     <li> <?php echo $TR_CH_HELP_POINT2?> </li>
		                     <li> <?php echo $TR_CH_HELP_POINT3?> </li>                     
		                   </ul>
		                   <?php echo $TR_CH_HELP_POINT4; ?><span class="activityBet"><b>VS</b></span><br>
		                   <br>
		                  <div><a href="#" ng-click="introFlow('challenge', 'start')"><?php echo $TR_HELP_DIALOG_POINT1?></a></div>
		                </div>
              		</div>
				</div>				
			</div>
		</div>
		<div class="footer">&nbsp;</div>
	</div>
</div>
