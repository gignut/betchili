<div ng-controller="MyActivityCtrl">

    <div ng-show="activityHistory.length == 0">
      <h4 style="padding-top: 120px;
                 text-align: center;
                 color: rgb(155, 146, 146);
                 text-shadow: 2px 1px white;">
          <?php echo $TR_NO_MORE_ACTIVITY; ?>
      </h4>
    </div> 

  <div ng-show="activityHistory.length > 0"  class="activityLeftStats" 
       style="border-style:solid; border-width:4px;"
       ng-style ="{ 'border-color': cssModel.h_bg_color }">
   
    <div class= "activityStatsBox" style="background:white;">
      <div style="float:left;width:100px;">
          <h4><?php echo $TR_ACT_BETS; ?></h4>
          <img src = "<?php echo MEDIA_URL . 'modules/m_embed/img/bet_icon.png';?>" style="width:40px;margin-left:5px;"/>
      </div>  
      
      <div style="float:left;width:240px;margin-top:5px;">
        <h5><?php echo $TR_ACT_WIN; ?>: {{userStats.bets_stat.wonCount}}</h5>
        <h5><?php echo $TR_ACT_LOOSE; ?>:   {{userStats.bets_stat.loosCount}}</h5>
        <h5><?php echo $TR_ACT_WAITING; ?>: {{userStats.bets_stat.waitingCount}}</h5>
      </div>
    </div>

    <div  class= "activityStatsBox" style="background:white;">
      <div style="float:left;width:100px;">
          <h4><?php echo $TR_ACT_CHALLANGES; ?></h4>
          <img src = "<?php echo MEDIA_URL . 'modules/m_embed/img/challenge_icon.png';?>" style="width:40px;margin-left:5px;"/>
      </div> 
      <div style="float:left;width:170px;margin-top:5px;">
        <h5><?php echo $TR_ACT_WIN; ?>: {{userStats.challenges_stat.wonCount}}</h5>
        <h5><?php echo $TR_ACT_LOOSE; ?>:   {{userStats.challenges_stat.loosCount}}</h5>
        <h5><?php echo $TR_ACT_WAITING; ?>: {{userStats.challenges_stat.waitingCount}}</h5>
      </div>
    </div>

  </div>  

   <div nut-scroll-bar="<?php echo (intval($height) - $cssdata['h_menu_height'] - 88); ?>"  class="clearfix">

      <div ng-show="activityHistory.length > 0">
          

          <!-- BET status waiting: -1 returned: 0 loose: 1 won: 2 -->
          <!-- event  status waiting: -1 returned: 0 loose: 1 won: 2 -->

          <div  ng-repeat="activityItem in activityHistory"
                ng-class-odd="'activityOdd'"  
                ng-class-even="'activityEven'"
                ng-class = "{ activityWin    : activityItem.result_status == 2, 
                              activityLoose  : activityItem.result_status == 1, 
                              activityReturn : activityItem.result_status == 0, 
                              activityWait   : activityItem.result_status == -1}"   
                style="padding-left: 10px;
                       padding-right: 10px;
                       padding-top: 5px;
                       padding-bottom: 5px;">
           
            <div ng-switch = "activityItem.type">
               <div ng-switch-when="b">
                    <!-- BET ITEM START -->
                      <div class="activityTxt">
                          <span ><h5 class="activityBet">Bet</h5></span>
                          <span ng-show="activityItem.bet_type==1 || ctivityItem.bet_type==3" style="margin-top: -10px;" tooltip="{{(activityItem.shield_percent*activityItem.amount)/100|number:0}}$ <?php echo $TR_ACT_CASHBACK; ?>" tooltip-placement="right">
                            <img src="<?php echo MEDIA_URL . 'modules/m_embed/img/shield.png'; ?>" style="width: 32px; height:32px;"/>
                          </span>
                          <span> <h5>${{activityItem.amount}} |</h5></span>
                          <span ><h5><?php echo $TR_ODD; ?>: </h5></span><span> <h5>{{activityItem.coefficient}}</h5></span>
                          
                          <!-- WON AMOUNT OF BET -->
                          <span ng-switch on = "activityItem.result_status" style="float:right; margin-right:5px;margin-top: 3px;">
                            <span ng-switch-when = "2">
                                 <span><h5 class="statusGreen statusTip"><?php echo $TR_WIN; ?>:</h5></span>
                                 <span><h5 class="statusGreen fontItalic">${{activityItem.won_amount}}</h5></span>
                            </span>
                            <span ng-switch-when = "1">
                                 <span><h5 class="statusRed statusTip"><?php echo $TR_WIN; ?>:</h5></span>
                                 <span ng-hide="activityItem.bet_type==1 || ctivityItem.bet_type==3"><h5 class="statusRed fontItalic">${{activityItem.won_amount}}</h5></span>
                                 <span ng-show="activityItem.bet_type==1 || ctivityItem.bet_type==3"><h5 class="statusRed fontItalic">$0</h5><h5 class="statusGreen fontItalic">+${{activityItem.won_amount}}</h5></span>
                            </span>
                            <span ng-switch-when = "0">
                                <span><h5 class="statusReturn statusTip"><?php echo $TR_ACT_RETURNED; ?>:</h5></span>
                                <span><h5 class="statusReturn fontItalic">${{activityItem.won_amount}}</h5></span>
                            </span>
                            <span ng-switch-when = "-1">
                               <span><h5 class="statusYellow statusTip"><?php echo $TR_ACT_ESTIMATED; ?>:</h5></span>
                               <span><h5 class="statusYellow fontItalic">${{activityItem.coefficient * activityItem.amount | number : 2}}</h5></span>
                            </span> 
                          </span>

                          <!-- {{activityItem.sort_stamp * 1000 | date : 'dd MMM H:mm'}} -->
                          <!-- <span class="label label-info betItemHeaderLabel">{{activityItem.result_status}}</span>  -->
                      </div>

                    <div class="betItemGames">
                      <table style="width:100%;">
                          <thead>
                            <th></th>
                            <th style="width:60px;"><b><small><?php echo $TR_SELECTION; ?></b></small></th>
                            <th style="width:60px;"><b><small><?php echo $TR_ODD; ?></b></small></th>
                            <th style="width:60px;"><b><small><?php echo $TR_ACT_STATUS; ?></b></small></th>
                          </thead>
                          <tbody>
                              <tr ng-repeat="(betEventItemID, betEventItem) in activityItem.event_chain">
                                  <td>
                                    <span style="display:inline-block; width:140px; text-align:right;" class="activityGameParticipant">
                                      <b><small>{{betEventItem.pname1}}</small></b>
                                    </span> 
                                    <span>
                                      <img class="img18" ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{betEventItem.plogo1}}" />  
                                    </span>
                                   
                                    <span style="font-size: 10px; width:70px; display:inline-block; text-align:center;">
                                      <b ng-show = "betEventItem.game_result" class="activityGameResult">
                                        {{betEventItem.game_result}}
                                      </b>
                                       <small  ng-hide = "betEventItem.game_result" style="color: gray;">
                                        <b>{{betEventItem.game_stamp *1000 | date : 'dd MMM H:mm' }}</b>
                                      </small>
                                    </span>
                                   
                                    <span>
                                      <img class="img18" ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{betEventItem.plogo2}}" />  
                                    </span>
                                    <span style="width:140px; text-align:left;" class="activityGameParticipant">
                                      <b><small>{{betEventItem.pname2}}</small></b>
                                    </span> 
                                  </td>
                                  <td class="text-center"><b><small>{{eventTemplatesData[betEventItem.etmp_id].tname}}</small></b></td>
                                  <td class="text-center">
                                    <small ng-switch on="betEventItem.etmp_id">
                                      <b ng-switch-when="7"> {{betEventItem.event_coefficient}}/{{betEventItem.basis}}</b>
                                      <b ng-switch-when="8"> {{betEventItem.event_coefficient}}/{{betEventItem.basis}}</b>
                                      <b ng-switch-when="9"> {{betEventItem.event_coefficient}}/{{betEventItem.basis}}</b>
                                      <b ng-switch-when="11">{{betEventItem.event_coefficient}}/{{betEventItem.basis}}</b>
                                      <b ng-switch-default>{{betEventItem.event_coefficient}}</b>
                                    </small>  
                                  </td>
                                  <td class="text-center">
                                      <span ng-switch on="betEventItem.event_result_status" class="eventStatusSpan">
                                          <img ng-switch-when = "2" src = "<?php echo MEDIA_URL; ?>modules/m_embed/img/status_green.png"/>
                                          <img ng-switch-when = "1" src = "<?php echo MEDIA_URL; ?>modules/m_embed/img/status_red.png"/>
                                          <img ng-switch-when = "-1" src = "<?php echo MEDIA_URL; ?>modules/m_embed/img/status_wait.png"/>
                                          <img ng-switch-when = "0" src = "<?php echo MEDIA_URL; ?>modules/m_embed/img/status_return.png"/>
                                      </span> 
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                    </div>
                      <!-- BET ITEM END -->
               </div>

               <div ng-switch-when = "ch" >
               <!--    challenge {{activityItem.sort_stamp * 1000 | date : 'dd MMM H:mm'}} -->

                      <div class="activityTxt">
                         <span><h5 class="activityVS">VS</h5></span><span> <h5>${{activityItem.ch_amount}}</h5></span>
                       
                         <span ng-switch on = "activityItem.result_status" style="float:right;margin-right:5px;margin-top: 3px;">
                          <span ng-switch-when = "2">
                               <span><h5 class="statusGreen statusTip"><?php echo $TR_WIN; ?>:</h5></span>
                               <span><h5 class="statusGreen fontItalic">${{activityItem.bet_wonamount*1| number : 2}}</h5></span>
                          </span>
                          <span ng-switch-when = "1">
                               <span><h5 class="statusRed statusTip"><?php echo $TR_WIN; ?>:</h5></span>
                               <span><h5 class="statusRed fontItalic">$0.00</h5></span>
                          </span>
                          <span ng-switch-when = "0">
                              <span><h5 class="statusReturn statusTip"><?php echo $TR_ACT_RETURNED; ?>:</h5></span>
                              <span><h5 class="statusReturn fontItalic">${{activityItem.ch_amount}}</h5></span>
                          </span>
                          <span ng-switch-when = "-1">
                             <span><h5 class="statusYellow statusTip"><?php echo $TR_ACT_ESTIMATED; ?>:</h5></span>
                             <span><h5 class="statusYellow fontItalic">${{activityItem.ch_amount*2 | number : 2}}</h5></span>
                          </span> 
                        </span> 

                      </div>
                      <div class= "betItemGames" style="height:50px;position:relative;">
                          <div style="float:left; margin-top:8px;">
                            <div class="challangeParticipant">
                                <img  tooltip="{{activityItem.publisher_name}}" tooltip-placement="right" ng-show = "activityItem.publisher_id" style="width:36px;" ng-src="{{activityItem.publisher_image}}" />
                            </div>
                          </div>

                          <div style="float:left; margin: 7px 0px 0px 4px;">
                            <div ng-switch on = "activityItem.publisher_etid">
                                <span class="label" ng-switch-when="1" style="font-size:12px;"><?php echo $TR_WIN_1; ?></span>
                                <span class="label" ng-switch-when="2" style="font-size:12px;"><?php echo $TR_DRAW; ?></span>
                                <span class="label" ng-switch-when="3" style="font-size:12px;"><?php echo $TR_WIN_2; ?></span>
                            </div>
                          </div>

                          <div style="margin-top:8px; float:right;">
                           <div class="challangeParticipant">
                               <!-- Targeted challenge -->
                               <img tooltip="{{activityItem.target_opname}}"  tooltip-placement="left" ng-show = "activityItem.target_user_id" style="width:36px;" ng-src="{{activityItem.target_opimage}}" />
                               <!-- Public challange accepted -->
                               <img  tooltip="{{activityItem.opponent_name}}" tooltip-placement="left" ng-show = "!activityItem.target_user_id && activityItem.opponent_id" style="width:36px;"  ng-src="{{activityItem.opponent_image}}" />
                               <!-- Public challange NOT accepted -->
                               <img tooltip="<?php echo $TR_ALL_USERS; ?>"    tooltip-placement="left" ng-show = "!activityItem.target_user_id && !activityItem.opponent_id" style="width:36px;" src="<?php echo MEDIA_URL;?>/modules/m_embed/img/any_opponent.png" />
                            </div>
                          </div> 

                          <div style="float:right; margin:7px 4px 0px 0px;">
                            <div>
                                <span ng-show = "activityItem.opponent_etid == 1"  style="font-size:12px;" class="label"><?php echo $TR_WIN_1; ?></span>
                                <span ng-show = "activityItem.opponent_etid == 2"  style="font-size:12px;" class="label"><?php echo $TR_DRAW; ?></span>
                                <span ng-show = "activityItem.opponent_etid == 3"  style="font-size:12px;" class="label"><?php echo $TR_WIN_2; ?></span>
                                <span ng-show = "activityItem.opponent_etid == -1" style="font-size:12px;" class="label">
                                  <div ng-hide="activityItem.game_result"><?php echo $TR_ACT_WAITING; ?></div>
                                  <div ng-show="activityItem.game_result"><?php echo $TR_ACT_IGNORED; ?></div>
                                </span>
                            </div>
                          </div>

                          <!-- GAME -->
                          <div style="position: absolute; top: 24px; left: 50%; margin-left: -235px;">
                                <span style="display:inline-block; width:178px; text-align:right;" class="pull-left activityGameParticipant">
                                  <b><small>{{activityItem.pname1}}</small></b>
                                </span> 
                                <span class="pull-left" style="width:22px;">
                                  <img class="img18"  ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{activityItem.plogo1}}"  />  
                                </span> 
                                
                                <span style="font-size: 10px; width:70px; display:inline-block; text-align:center;" class="pull-left">
                                    <b ng-show = "activityItem.game_result" class="activityGameResult" >
                                      {{activityItem.game_result}}
                                    </b>
                                     <small  ng-hide = "activityItem.game_result" style="color: gray;">
                                      <b>{{activityItem.game_stamp *1000  | date : 'dd MMM H:mm' }}</b>
                                    </small>
                                </span>

                                <span class="pull-left" style="width:22px;">
                                  <img class="img18" ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{activityItem.plogo2}}" />  
                                </span>
                                <span style="display:inline-block; width:178px; text-align:left;" class="pull-left activityGameParticipant">
                                  <b><small>{{activityItem.pname2}}</small></b>
                                </span> 
                          </div>   
                      </div>
               </div>
               <!-- Challange end -->
            </div>   
    

          </div>
          <!-- ngrepeat_end -->

          <div class="activityBottomActions" ng-show="activityHistory.length > 6"
               ng-style="{ 'background-color' : cssModel.h_bg_color, color : cssModel.h_font_color }"
               ng-click = "loadMoreActivity()">
              <small><b><?php echo $TR_LOAD_MORE; ?></b></small>
          </div>

      </div> 
  </div>
</div>  
