
<div ng-controller="ChallengeCtrl">
	
	<div class="challengeRow" ng-class="{slideChallange : challangeLoaded }" 
		 picker-click="challange_bg_color"
		 style=" height: <?php echo $cssdata['challenge_height']. 'px'; ?>; "
		 ng-style="{ 'background-color' : cssModel.challange_bg_color}">

			<div ng-show="blockChallengeView != true">
					
					
					<div  style="position: absolute; top: 17px; left: 50%;">
						
						 	<div ng-show="isNew" style="margin-left: -82px;" id="challengeButtons">
								<span >
									<button class="widgetBtn"  ng-style="{ 'background-color': cssModel.challange_btn_bg,  color: cssModel.challange_btn_font }"
										ng-class="{disabled : (optionIndex == null || !createChallangeForm.$valid) }" 
										ng-click="createChallenge();"
										ng-disabled = "optionIndex == null || !createChallangeForm.$valid">
										<?php echo $TR_CHALLENGE; ?>
									</button>
								</span>
								<span>
									<button class="widgetBtn" ng-click="nextChallenge('cancel');"
										    ng-style="{ 'background-color': cssModel.challange_btn_bg, color: cssModel.challange_btn_font}">
										<?php echo $TR_CANCEL; ?>
									</button>
								</span>
							</div>

							<div ng-show="!isNew" style="margin-left: -82px;" >
								<span>
									<button  id = "acceptBtn" class="widgetBtn" 
										ng-style="{ 'background-color': cssModel.challange_btn_bg,  color: cssModel.challange_btn_font}"
										ng-class="{disabled : optionIndex == null ||  opponent2.id == null}" 
										ng-click="acceptChallenge();"
										ng-disabled = "optionIndex == null || opponent2.id == null"
										picker-click="challange_btn_bg">
										<span picker-click="challange_btn_font"><?php echo $TR_ACCEPT; ?></span>
								    </button>
								</span>
								<span>
									<button class="widgetBtn" ng-click="nextChallenge();"
											ng-style="{ 'background-color': cssModel.challange_btn_bg, color: cssModel.challange_btn_font}"
											picker-click="challange_btn_bg">
											<span picker-click="challange_btn_font"><?php echo $TR_NEXT; ?></span>
									</button>
								</span>
							</div>
					</div>	

					<!-- GAME -->
					<div style="position: absolute; top: 101px; left: 50%; margin-left: -186px; z-index:1;" id="challengeGame">
						<span picker-click="challange_font_color" 
							  style="display:inline-block; width:150px; text-align:right;"
							  ng-style ="{ color: cssModel.challange_font_color }"> 
							<b><small>{{gameData.aux_info.participants[0].tname}}</small></b>
						</span> 
		                <span class="challangeTeam" ng-style="{ 'background-color': cssModel.challange_team_bg }">
		                  <img picker-click="challange_team_bg" class="img24"  ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{gameData.aux_info.participants[0].logo}}" />  
		                </span>	
		                <div picker-click="challange_font_color" class="gameItemTime" style="top:28px;" ng-style= "{ color: cssModel.challange_font_color }">
							<span><small><b>{{gameData.start_date * 1000 | date : 'dd MMM H:mm' }}</b></small></span>
						</div>
						<span class="challangeTeam"  ng-style="{ 'background-color': cssModel.challange_team_bg }">
		                  <img picker-click="challange_team_bg" class="img24" ng-src = "<?php echo MEDIA_URL . 'assets/participants/'; ?>{{gameData.aux_info.participants[1].logo}}" />  
		                </span>
		                <span picker-click="challange_font_color" 
		                	  style="display:inline-block; width:150px; text-align:left;" 
		                	  ng-style="{ color: cssModel.challange_font_color}">
		                	  <b><small>{{gameData.aux_info.participants[1].tname}}</small></b>
		                </span> 
					</div>	

					<div ng-show="!isNew" style="position: absolute; left: 80px; top: 14px;">
						<div ng-show="optionIndex!=null">
							<h3 picker-click="challange_font_color" 
							    style="text-align:left; margin:0px; font-size: 30px; line-height: 30px;" 
							    ng-style="{color: cssModel.challange_font_color}">
							    {{challengeData.amount}}
							</h3>
							<small picker-click="challange_font_color" ng-style="{ color: cssModel.challange_font_color}" style="font-style: italic;">
								<?php echo $TR_ESTIMATED_WIN; ?>: {{challengeAmount*2 |number:2}}
							</small>
						</div>	
						<div ng-show="optionIndex ==null" ng-style="{ color: cssModel.challange_font_color}" style="font-style: italic;">
							<small>{{opponent2.name}}</small> 
							<br/>
							<small><?php echo $TR_CHALLANGES_YOU; ?></small>
							<br/>
							<small><?php echo $TR_SELECT_OPTION_AND_FIGHT; ?></small> 
						</div>	
					</div>	


					<div ng-show="!isNew" style="position: absolute; right: 80px; top: 14px;">
						<h3 picker-click="challange_font_color" 
							style="text-align:right; margin:0px; font-size: 30px; line-height: 30px;" 
							ng-style="{ color: cssModel.challange_font_color}">
							{{challengeData.amount}}
						</h3>
						<small picker-click="challange_font_color" ng-style="{ color: cssModel.challange_font_color }" style="font-style: italic;">
							<?php echo $TR_ESTIMATED_WIN; ?>: {{challengeAmount*2 |number:2 }}
						</small>
					</div>	


					<div ng-form = "createChallangeForm" ng-show="isNew">
						 		<input name = "challangeAmountInput" 
								   ng-model="challengeAmount" chili-digit maxlength="5" placeholder=""
								   class="challengeAmountInput"
								   bet-min="<?php echo MIN_CHALLANGE_AMOUNT; ?>" bet-max="<?php echo MAX_CHALLANGE_AMOUNT; ?>" >

							<div picker-click="challange_font_color" class="challangeAmountHint" ng-style="{ color: cssModel.challange_font_color}">
						    		<div ng-show = "createChallangeForm.challangeAmountInput.$dirty && (createChallangeForm.challangeAmountInput.$error.betMax || createChallangeForm.challangeAmountInput.$error.betMin) ">
							    	  <small><b><?php echo $TR_MINMAX_CH_AMOUNT; ?></b></small>
							    	</div>	
							    	<div ng-show="createChallangeForm.challangeAmountInput.$valid && optionIndex!= null" id="estimatedWon">
							    		<small ng-style="{ color: cssModel.challange_font_color }" style = "font-style: italic; font-size:12px;">
							    			<?php echo $TR_ESTIMATED_WIN; ?>: {{challengeAmount*2 |number:2}}
							    		</small>
							    	</div>	
							    	<div ng-show="createChallangeForm.challangeAmountInput.$valid && optionIndex == null">
							    		 <small><b><?php echo $TR_PLEASE_SELECT_OPTION; ?></b></small>
							    	</div>
							    	
							</div>	
					</div>	

					<div style="float:left; margin:10px;" class="opAvatarContainer">
						<div picker-click="stop">
							<img ng-show="opponent1.id" class="oppAvatar" ng-src="{{opponent1.avatar}}" tooltip="{{opponent1.name}}" tooltip-placement="right"/>
							<img ng-hide="opponent1.id" class="oppAvatar" ng-src="<?php echo MEDIA_URL;?>modules/m_embed/img/any_opponent.png" tooltip="<?php echo $TR_ALL_USERS; ?>" tooltip-placement="right"/>
						</div>
					</div>	
						
					<div style="margin-top: 95px;z-index: 2; position: absolute; left: 79px;">
						<div  id="challengeOptions">
						<div >
							<span ng-show="isNew || opponent2.event_template_id != 1">
								  <span  ng-click="selectOption(0);" picker-click="challange_option_enabled_bg" class="challangeOption" ng-show="optionIndex == 0" 
								         ng-style = "{ 'background-color': cssModel.challange_option_enabled_bg }">
								  	  <span ng-click="selectOption(0);" picker-click="challange_option_enabled_font" 
								  	        ng-style="{ color: cssModel.challange_option_enabled_font}">
								  	        <?php echo $TR_WIN_1; ?>
								  	  </span>
								  </span>
								  <span  ng-click="selectOption(0);" picker-click="challange_option_disabled_bg" class="challangeOption" ng-hide="optionIndex == 0" 
								         ng-style = "{ 'background-color': cssModel.challange_option_disabled_bg }">
								  	<span ng-click="selectOption(0);" picker-click="challange_option_disabled_font" 
								  	      ng-style="{ color: cssModel.challange_option_disabled_font}">
								  	      <?php echo $TR_WIN_1; ?>
								  	</span>
								  </span>	
							</span>
				
							<span ng-click="selectOption(1);" 
								  ng-show="isNew || opponent2.event_template_id != 2">
								  <span ng-click="selectOption(1);"  picker-click="challange_option_enabled_bg" class="challangeOption" ng-show="optionIndex == 1" 
								        ng-style = "{ 'background-color': cssModel.challange_option_enabled_bg }">
								  	  <span ng-click="selectOption(1);" picker-click="challange_option_enabled_font" 
								  	        ng-style="{ color: cssModel.challange_option_enabled_font}">
								  	        <?php echo $TR_DRAW; ?>
								  	  </span>
								  </span>
								  <span ng-click="selectOption(1);" picker-click="challange_option_disabled_bg" class="challangeOption" ng-hide="optionIndex == 1" 
								        ng-style = "{ 'background-color': cssModel.challange_option_disabled_bg }">
								  	  <span ng-click="selectOption(1);" picker-click="challange_option_disabled_font" 
								  	        ng-style="{ color: cssModel.challange_option_disabled_font }">
								  	        <?php echo $TR_DRAW; ?>
								  	  </span>
								  </span>	
							</span>
					
							<span ng-click="selectOption(2);"
								   ng-show="isNew || opponent2.event_template_id != 3">
								  <span ng-click="selectOption(2);" picker-click="challange_option_enabled_bg" class="challangeOption" ng-show="optionIndex == 2" 
								        ng-style = "{ 'background-color': cssModel.challange_option_enabled_bg }">
								  	  <span ng-click="selectOption(2);" picker-click="challange_option_enabled_font" 
								  	        ng-style="{ color: cssModel.challange_option_enabled_font }">
								  	        <?php echo $TR_WIN_2; ?>
								  	  </span>
								  </span>
								  <span ng-click="selectOption(2);" picker-click="challange_option_disabled_bg" class="challangeOption" ng-hide="optionIndex == 2" 
								        ng-style = "{ 'background-color': cssModel.challange_option_disabled_bg }">
								  	 <span ng-click="selectOption(2);" picker-click="challange_option_disabled_font" 
								  	       ng-style="{ color: cssModel.challange_option_disabled_font }">
								  	       <?php echo $TR_WIN_2; ?>
								  	 </span>
								  </span>	
							</span>
						</div>	

						<div picker-click="challange_font_color" style="text-align:center; font-style: italic; " 
						     ng-style= "{ color: cssModel.challange_font_color}">
							<small><?php echo $TR_AVAILABLE_BETS; ?></small>
						</div>
						</div>
					</div>
					
					<!-- OPPONENT 2 PART -->
					<div style="margin:10px; float:right;" id="opponent2" class="opAvatarContainer" >
						<div ng-show  ="opponent2.id" class="chooseChallangeOpponent" tooltip="{{opponent2.name || '<?php echo $TR_ALL_USERS;?>'}}" tooltip-placement="left">
							<img ng-if="opponent2.id != 'any'" class="oppAvatar" ng-src="{{opponent2.avatar}}" picker-click="stop"  />
							<img ng-if="opponent2.id == 'any'" class="oppAvatar" src="<?php echo MEDIA_URL;?>modules/m_embed/img/any_opponent.png" picker-click="stop"/>
							<div class="chooseOpponentOverlay" ng-show="isNew" ng-click="openUsersModal()" picker-click="stop" >
								<img style="margin-left: 10px; margin-top: 14px; width: 32px;" src="<?php echo MEDIA_URL;?>modules/m_embed/img/change_opponent.png" />
							</div>	
						</div>	
							
						<span ng-hide ="opponent2.id">
							<div>
								<img style="width:51px; cursor:pointer;" ng-click="openUsersModal()" tooltip="<?php echo $TR_ALL_USERS; ?>" tooltip-placement="left" src="<?php echo MEDIA_URL;?>modules/m_embed/img/icon_invite.png"/>
							</div>	
						</span>
					</div>

					<div ng-show="!isNew" style="position:absolute; right:75px; margin-top:95px; text-align:center;">
						<div ng-switch= "opponent2.event_template_id">
							<span ng-switch-when= "1" class="challangeOption" picker-click="challange_option_enabled_bg"
								  style = "cursor:default;" 
								  ng-style="{ 'background-color': cssModel.challange_option_enabled_bg, color: cssModel.challange_option_enabled_font }">
								<span picker-click="challange_option_enabled_font"><?php echo $TR_WIN_1; ?></span>
							</span>
							<span ng-switch-when= "2" class="challangeOption" picker-click="challange_option_enabled_bg"
							      style = "cursor:default;" 
							      ng-style="{ 'background-color': cssModel.challange_option_enabled_bg, color: cssModel.challange_option_enabled_font }">
								<span picker-click="challange_option_enabled_font"><?php echo $TR_DRAW; ?></span>
							</span>
							<span ng-switch-when= "3" class="challangeOption" picker-click="challange_option_enabled_bg"
								  style = "cursor:default;" 
								  ng-style="{ 'background-color': cssModel.challange_option_enabled_bg,  color: cssModel.challange_option_enabled_font }">
								<span picker-click="challange_option_enabled_font"><?php echo $TR_WIN_2; ?></span>
							</span>
						</div>	
						<div picker-click="challange_font_color" style="font-style: italic;" 
						     ng-style="{ color: cssModel.challange_font_color }">
							<small><?php echo $TR_OPPONENT; ?></small></div>
					</div>
			</div>
			
			<!-- BLOCK VIEW  -->
			<div ng-show="blockChallengeView">
				<h3  picker-click="challange_font_color" 
				     style="padding-top: 50px; text-align: center; margin: 0px;"
					 ng-style="{ color: cssModel.challange_font_color }">
					<?php echo $TR_NO_MORE_CHALLANGES; ?>
				</h3>
			</div>

	</div>
</div>	

