<!-- DEMO GAME JSON DATA -->
<?php echo $JSON_GAME_DATA; ?>

<?php
  // chili_asset_loader::jquery(true, false);
  chili_asset_loader::uibootstrap();  
  css("modules/m_embed/css/widget_game.css");
  // script("modules/m_embed/js/ng_chili_module.js");
  script("modules/m_embed/js/ng_widget_demo_api.js");
  script("modules/m_embed/js/ng_widget_game.js");

   //** ## ICONMOON ## **//
  css("modules/m_embed/iconmoon/style.css");
  
?>

  <div>
    <div>
        <div ng-controller = "MainCtrl">
          <div ng-init="initData()">

                  <!-- LOADER -->
                  <div id="gameLoader" style="background-color:transparent;height:0px;border:0px;display:none;">
                      <img style="width: 64px;" src = "<?php echo MEDIA_URL;?>modules/m_embed/img/loader_circle.gif" />
                  </div>  

                  <div style="overflow:hidden; border-style:solid; border:0px;
                        height: <?php echo $height . 'px'; ?>;
                        max-height: <?php echo $height . 'px'; ?>;  
                        width: <?php echo $width . 'px'; ?>;
                        background-color: {{cssModel.body_bg_color}};">

                              <!-- HEADER -->
                              <div>
                                  <div>
                                    <div ng-show = "isAuthorized">
                                      <!-- PLAYER UNAUTHORIZED MENU START-->
                                      <?php echo $PROFILE_HEADER_CONTENT;?>
                                    </div>
                                    <div ng-hide = "isAuthorized">
                                       <!-- PLAYER AUTHORIZED MENU START-->
                                       <?php echo $SOCIAL_LOGIN_CONTENT;?>
                                    </div>
                                  </div>  
                              </div>
                              <!-- HEADER END -->

                              <!-- GAME LIST ROW START-->
                              <div ng-show = "viewmode == 'MODE_GAME' || viewmode == 'MODE_HELP' ">
                                <?php echo $CHALLENGES_CONTENT; ?>
                                <?php echo $GAMES_CONTENT; ?>
                              </div>
                              <div ng-show = "viewmode == 'MODE_LEADERBOARD'">
                                <?php echo $LEADERBOARD_CONTENT; ?>
                              </div>
                              <div ng-show = "viewmode == 'MODE_ACTIVITY'">
                                <?php echo $ACTIVITY_CONTENT; ?>
                              </div>
                  </div>  
          </div>
     </div>
  </div>
</div>

