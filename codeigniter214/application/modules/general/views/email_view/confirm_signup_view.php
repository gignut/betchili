<!DOCTYPE html>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8"> 

	<body style = "font-size:100%;">
		<div style = "background: #F3F3F3;">
			<div style = "margin: 0 auto;
						  width: 500px;
						  padding-top:50px;
						  padding-bottom:50px;">
				<div style = "background: #D1202E;
							  color: white;
							  text-align: center;
		                      font-size:2.1em;">
					Betchili
				</div  
				<div style = "background: white;
							  color:black;
							  padding:10px 10px 10px 10px;">
					<h2>Hello <?php echo $userFirstName;?>,</h2>
					<p>We're ready to activate your account. All we need to do is make sure this is your email address.</p>
					<div style = "text-align:center;">
						<button style = "	color:white;
										width: 11em;
										font-size: 1.3em;
										background-color: #D1202E;
										text-align: center;
										border: none;
										padding: 9px 12px 10px;
										line-height: 22px;
										text-decoration: none;
										border-radius: 6px;
										-webkit-transition: 0.25s linear;
										-moz-transition: 0.25s linear;
										-o-transition: 0.25s linear;
										transition: 0.25s linear;
										-webkit-backface-visibility: hidden;
										-webkit-user-select: none;
										-moz-user-select: none;
										-ms-user-select: none;
										-o-user-select: none;
										user-select: none;">
							<a style = "text-decoration:none;color:white" href="<?php echo $link;?>">Verify Address</a>
						</button>
					</div>
					<p style = "color: #777;">If you didn't create a Betchili account, just delete this email and everything will go back to the way it was.</p>
				</div>
			</div>
		</div>
	</body>
</html>

