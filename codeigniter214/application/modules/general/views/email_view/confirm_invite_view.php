<!DOCTYPE html>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8"> 

	<body style = "font-size:100%;">
		<div style = "background: #F3F3F3;">
			<div style = "margin: 0 auto;
						  width: 500px;
						  padding-top:50px;
						  padding-bottom:50px;">
				<div style = "background: #2fbfbf;
							  color: white;
							  text-align: center;
		                      font-size:3em;">
					Betchili
				</div  
				<div style = "background: white;
							  color:black;
							  padding:10px 10px 10px 10px;">
					<h2>Hello <?php echo $userFirstName;?>,</h2>
					<p>You have invitation, if you want confirm it,click button below.</p>
					<div style = "text-align:center;">
						<button style = "	color:white;
										width: 11em;
										font-size: 1.5em;
										background-color: #00C69D;
										text-align: center;
										border: none;
										padding: 9px 12px 10px;
										line-height: 22px;
										text-decoration: none;
										border-radius: 6px;
										-webkit-transition: 0.25s linear;
										-moz-transition: 0.25s linear;
										-o-transition: 0.25s linear;
										transition: 0.25s linear;
										-webkit-backface-visibility: hidden;
										-webkit-user-select: none;
										-moz-user-select: none;
										-ms-user-select: none;
										-o-user-select: none;
										user-select: none;">
							<a style = "text-decoration:none;color:white" href="<?php echo $link;?>">Confirm</a>
						</button>
					</div>
					<p style = "color: #777;">If you don't want to confirm invitation, just ignore this message.</p>
				</div>
			</div>
		</div>
	</body>
</html>

