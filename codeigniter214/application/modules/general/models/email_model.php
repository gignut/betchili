<?php
class Email_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        
    }

    public function sendInvitionMail($userFirstName, $userEmail, $fname, $link)
    {
        try
        {
            $email = new nut_email();
            $email->initialize("noreply");
            $msgData = array('link'          => $link,
                             'userFirstName' => $userFirstName);
            $msg = $this->load->view(MODULE_GENERAL_FOLDER . "/email_view/confirm_invite_view.php", $msgData, true);
            $data = array(
                            'to_email' => $userEmail,
                            'from_name'=> $fname,
                            'subject'  => "Invitation to join a host", 
                            'message'  =>  $msg
                            );
            $email->send($data);
        }
        catch(Exception $e)
        {
            nut_log::log("error" , "[Error: email_model->sendInvitionMail]");
            return false;
        }
    }

    public function sendRegistrationMail($userFirstName,$userEmail, $frname, $link)
    {
        try
        {
          

            $email = new nut_email();
            $email->initialize("noreply");
            $msgData = array('link'          => $link,
                             'userFirstName' => $userFirstName);
            
                             
            $msg = $this->load->view(MODULE_GENERAL_FOLDER . "/email_view/confirm_signup_view.php", $msgData, true);

            $data = array(
                            'to_email' => $userEmail,
                            'from_name'=> $frname,
                            'subject'  => "Verify Email", 
                            'message'  => $msg
                        );
                           
            $email->send($data); 
        }
        catch(Exception $e)
        {
             nut_utils::error_page("account/profile/sendRegistrationMail");
        }        
    }

    public function sendRegistrationRequestMail($name, $mail, $domain)
    {
        try
        {
            $email = new nut_email();
            $email->initialize("noreply");
            $msgData = array('userName'           => $name,
                             'userEmail'          => $mail,
                             'userDomain'         => $domain);
            
                             
            $msg = $this->load->view(MODULE_GENERAL_FOLDER . "/email_view/registration_request_view.php", $msgData, true);

            $data = array(
                            'to_email' => "arayik@betchili.com, artak@betchili.com, artashes@betchili.com",
                            'from_name'=> "sales@betchili.com",
                            'subject'  => "Betchili request", 
                            'message'  => $msg
                        );
                           
            $email->send($data); 
        }
        catch(Exception $e)
        {
             nut_utils::error_page("account/profile/sendRegistrationMail");
        }        
    }

    public function sendPasswordResetEmail($userFirstName,$userEmail, $frname, $link)
    {
        try
        {
            $email = new nut_email();
            $email->initialize("noreply");
            $msgData = array('link'          => $link,
                             'userFirstName' => $userFirstName);
            
                             
            $msg = $this->load->view(MODULE_GENERAL_FOLDER . "/email_view/password_reset_view.php", $msgData, true);

            $data = array(
                            'to_email' => $userEmail,
                            'from_name'=> $frname,
                            'subject'  => "Reset password", 
                            'message'  => $msg
                        );
                           
            $email->send($data); 
        }
        catch(Exception $e)
        {
             nut_utils::error_page("account/profile/sendForgetpasswordMail");
        }        
    }
    
}
?>