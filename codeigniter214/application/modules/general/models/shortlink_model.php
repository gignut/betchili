<?php
class Shortlink_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function generateUniqueString($length, $id= "")
	{
        try
        {
            $key = '';
            $keyStart = '';
            $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

            for ($i = 0; $i < $length/2; $i++)
            {
                $keyStart .= $keys[array_rand($keys)];
            }

            $keyEnd = '';
            $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

            for ($i = 0; $i < $length/2; $i++)
            {
                $keyEnd .= $keys[array_rand($keys)];
            }

            $key = $keyStart . $id . $keyEnd;
            return $key;
        }
        catch(Exception $e)
        {
            throw $e;
        }
		
	}

    public function getDataByHash($hash)
    {
        try
        {
            $data = null;
            $sql = "SELECT * FROM "  . TB_LINKS .   " WHERE hash =  ? " ; 
            $query = nut_db::query($sql, $hash);
            
            return $query->num_rows() == 1 ? $query->row_array() : array();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

    public function insert($hash, $type, $data, $start_date, $end_date, $count)
    {
        try
        {
            $list = array($hash, $type, $data, $start_date, $end_date, $count);
            $sql = "INSERT INTO ". TB_LINKS . " (hash, type, data, start_date, end_date, count) VALUES (?,?,?,?,?,?)";
            $query = nut_db::query($sql, $list);
            return true;
        }
        catch(Exception $e)
        {
            throw $e;
        }       
    }
    
    public function addCount($hash)
    {
        try
        {
        	$linkData = $this->getDataByHash($hash);
        	$count = $linkData["count"];
            $count++;
            $list = array($count, $hash);
            $sql = "UPDATE " . TB_LINKS . " SET count = ? WHERE hash = ?";
            $query = nut_db::query($sql, $list);
            return $count;
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
}
?>