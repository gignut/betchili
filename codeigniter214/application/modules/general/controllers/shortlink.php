<?php

class Shortlink extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function confirm($hash)
    {
        try
        {
            $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");

            $data = $this->shortlink_model->getDataByHash($hash);
            $type = $data['type'];
            $start_date  = strtotime($data['start_date']);
            $end_date = strtotime($data['end_date']);
            $count = $data['count'];
            $linkData =  $data['data'];
            $current_date = time();
           
            $in_time = false;
            if($current_date > $start_date)
            {
                $in_time = $end_date ? ($current_date < $end_date) : true;
            }

            if($in_time)
            {
                $this->shortlink_model->addCount($hash);
                switch($type)
                {
                    case SIGNUP:
                        $this->confirmSignup($hash,  $linkData);
                        break;
                    case RESSET_PASSWORD:
                        $this->resetPassword($hash);
                        break;
                }
            }
        }
        catch(Api_exception $e)
        {
            nut_utils::echo_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
    }

    private function confirmSignup($hash , $linkData)
    {
        try
        {
            // during signup we put user id as data
            // check profile/signup
            $userId = $linkData;
            $count = $this->shortlink_model->addCount($hash);
            $apiParams = array("chili" => array("id" => $userId));

            $result = nut_api::api('siteusers/confirm',  $apiParams);
            if(intval($result["confirmed"]) == 1)
            {
                nut_utils::locationRedirect(BASE_INDEX . 'login');
            }
        }
        catch(Api_exception $e)
        {
            nut_utils::echo_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
        
    }

    private function resetPassword($hash)
    {
        try
        {
            nut_utils::locationRedirect(BASE_INDEX . 'resetpassword/' . $hash);
        }
        catch(Api_exception $e)
        {
            nut_utils::echo_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
    }

    private function setCurrentUser($hash,$type)
    {
        try
        {
            $linkData = $this->shortlink_model->getDataByHash($hash);
            $id = $linkData["data"];
            $this->user_model->setCurrentUser($id);
            nut_utils::locationRedirect(BASE_INDEX . 'resetpassword');
        }
        catch(Api_exception $e)
        {
            nut_utils::echo_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
        
    }

    private function confirmInvitation($hash, $userId, $hostId)
    {
        try
        {
            $this->load->model(MODULE_ACCOUNT_FOLDER . "/profile_model");
            $this->load->model(MODULE_GENERAL_FOLDER . "/user_model");
            $count = $this->shortlink_model->addCount($hash);

            $this->profile_model->confirmInvitation($userId, $hostId);

            $this->user_model->loginUser($userId);
            $this->user_model->setCurrentHost($hostId);
            nut_utils::locationRedirect(BASE_INDEX . MODULE_ACCOUNT_FOLDER."/show/index");
        }
        catch(Api_exception $e)
        {
            nut_utils::echo_error($e->getMessage(), $e->getCode(), $e->getData());
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
    }

}
