<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Show extends MX_Controller {
	
	private $siteUserId;
	private $currentLang;
	private $allLanguages;
	private $mainViewData;

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		
		$this->load->model(MODULE_SITE_FOLDER . "/account_model");
		$this->currentLang = $this->account_model->getCurrentLang();
		//IMPROVE LANG GETTING WITH SESSION
		$this->allLanguages = $this->getLanguageData();
	}

	private function getLanguageData()
	{
		$apiData       = nut_api::wrapData(array("language" => "en"));
		$data = nut_api::api('auxdata/get_languages', $apiData);

		return $data;
	}

// ##START RENDER UNAUTHOEIZED VIEWS
 	public function index($lang = null)
	{
		if($lang) 
		{
			$this->account_model->setCurrentLang($lang);
			$this->currentLang = $this->account_model->getCurrentLang();
		}

		$viewData = chili_translations::load_translations('site/landing_view', $this->currentLang);

		$languagesData = $this->getLanguageData();
		$viewData['allLanguages'] = $languagesData;

		foreach ($languagesData as $langId => $langItem) 
		{
			if($langItem["code"] == $this->currentLang)
			{
				$viewData['currentLanguage'] = $langItem;
			}
		}
		
		$this->load->view(MODULE_SITE_FOLDER . "/landing_view.php", $viewData);
	}

	public function login()
	{
		$viewData = chili_translations::load_translations('dialog/login_view', $this->currentLang);
		$dialogContent = $this->load->view(MODULE_SITE_FOLDER . "/dialog/login_view.php", $viewData, true);
		$this->renderDialog($dialogContent);
	}

	// request for using betchili
	public function request()
	{
		$viewData = chili_translations::load_translations('dialog/request_view', $this->currentLang);
		$dialogContent = $this->load->view(MODULE_SITE_FOLDER . "/dialog/request_view.php", $viewData, true);
		$this->renderDialog($dialogContent);
	}

	public function signup()
	{
		$viewData = chili_translations::load_translations('dialog/signup_view', $this->currentLang);
		$dialogContent = $this->load->view(MODULE_SITE_FOLDER . "/dialog/signup_view.php", $viewData, true);
		$this->renderDialog($dialogContent);
	}

	public function forgotpassword()
	{
		$viewData = chili_translations::load_translations('dialog/forgetpassword_view', $this->currentLang);
		$dialogContent = $this->load->view(MODULE_SITE_FOLDER . "/dialog/forgetpassword_view.php", $viewData, true);
		$this->renderDialog($dialogContent);
	}

	public function resetpassword($hash = null)
    {
        try
        {
            $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");

            $linkData = $this->shortlink_model->getDataByHash($hash, 10);
            $id = $linkData["data"];
            //TODO change with id
            
            $this->shortlink_model->addCount($hash);
            $this->account_model->setAuxData($id);
            $dialogContent = $this->load->view(MODULE_SITE_FOLDER . '/dialog/reset_pass_view.php', '', true);
            $this->renderDialog($dialogContent);
        }
        catch(Exception $e)
        {
        	
        }
    }

	private function renderDialog($content)
    {
    	$data = array("CONTENT" => $content);
    	$this->load->view(MODULE_SITE_FOLDER . '/dialog/dialog_view.php', $data );
    }

    public function changeLanguage($lang){
    	try
    	{
			$this->account_model->setCurrentLang($lang);
			nut_utils::reloadPage();
		}
		catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/show/changeLanguage. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'GeneralException: site/show/changeLanguage. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }

// ##END RENDER UNAUTHOEIZED VIEWS 

	private function preRenderAuthorized(){
		$this->siteUserId   = Modules::run(MODULE_SITE_FOLDER . '/authorize/siteuser');

		$this->mainViewData = chili_translations::load_translations('home/home_view', $this->currentLang);
		$this->mainViewData['currentLanguageCode'] = $this->currentLang;
		$this->mainViewData['allLanguages']        = $this->allLanguages;
	}

    public function widgetsDashboard()
    {
    	try
    	{
    		$this->preRenderAuthorized();

    		$language = $this->currentLang;
    		$pageData = chili_translations::load_translations('home/widget_dashboard_view', $language);

    		//using domain to show in "how to integrate"
    		$domain = $this->account_model->getAuthorizedUserDomain();
    		$pageData["domain"] = $domain ;
			
			$pageData['siteUserId'] = $this->siteUserId;

			$apiPostData = array();
			$apiPostData["language"] = $language;
			$apiPostData["id"] = $this->siteUserId;
			$apiPostData["widget_types"] = array(WIDGET_GAME);

			$apiData = nut_api::wrapData($apiPostData); 
			$siteUserWidgets = nut_api::api('siteusers/get_all_widgets', $apiData);
			$pageData['siteUserWidgets'] = json_encode($siteUserWidgets);
			
			$this->mainViewData['CONTENT'] = $this->load->view(MODULE_SITE_FOLDER. "/home/widgets_dashboard_view.php", $pageData, true);

			$this->load->view(MODULE_SITE_FOLDER . "/home/home_view.php", $this->mainViewData);
		}
		catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/dashboard. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/dashboard. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }

    public function game($widgetId = null, $page = 'main')
    {
    	try
    	{
    		$this->preRenderAuthorized();

    		$this->load->model(MODULE_EMBED_FOLDER . "/css_model");
    		//add some validation 
    		$language = $this->currentLang;

			$pageData = chili_translations::load_translations('home/widgets_game_view', $language);
			$pageData['DESIGN'] = '';

			//BATCH  API CALLS PROCESSING
			$langVar  = array("language" => $language);
			
			$apiData  = array();
			$apiData["sizeList"]    = array("url"  => "auxdata/get_sizes"     ,  "vars"  => $langVar);
			$apiData["sportData"]   = array("url"  => "sports/get_all"        ,  "vars"  => $langVar);
			$apiData["leaguesData"] = array("url"  => "leagues/get_all"       ,  "vars"  => $langVar);
			$batchData = nut_api::api('batch/action', array( nut_api::$PARAM => $apiData));

			$sportData   = $batchData["sportData"];
			$leaguesList = $batchData["leaguesData"];

			foreach ($leaguesList as $key => &$leagueItem) {
				$sportId = $leagueItem["sport_id"];
				$leagueItem["sport_name"] = $sportData[$sportId]["name"];
			}

			//TODO 
			$pageData['sizeList']    = json_encode($batchData["sizeList"]);
			$pageData['leaguesList'] = json_encode($leaguesList);

			if($widgetId && $widgetId != "new")
			{
				$apiData    = nut_api::wrapData(array("language" => $language, "widget_id" => $widgetId));
				
				//TODO
				$widgetData = nut_api::api('siteusers/get_game_widget_data', $apiData);
				$widgetLangId      = $widgetData["lang_id"];
				$widgetBookmakerId = $widgetData["bookmaker_id"];

				$pageData['gameWidgetData'] = json_encode($widgetData);
			
				// UNAUTHORIZED VIEW OF GAME
				$wData            = array();
				$wData["id"]      = $widgetId;
				$wData["lang_id"] = $widgetLangId;
				$wData["height"]  = 600; //$widgetData["height"];
				$wData["width"]   = 800; //$widgetData["width"];

				// GETING GAME DATA
				// competiton vars
				$competitionVars = array();
				$competitionVars["widget_id"]   = $widgetId;
				$competitionVars["language_id"] = $widgetLangId;

				//TODO create config for sport types	
				$eventsVars = array("sport_id" => 1, "language_id" => $widgetLangId);

				$from = mktime(0,0,0);
            	$to   = mktime(23,59,59);

				$gamesVars  = array();
	            $gamesVars["widget_id"]    = $widgetId;
	            $gamesVars["language_id"]  = $widgetLangId;
	            $gamesVars["bookmaker_id"] = $widgetBookmakerId;
	            $gamesVars["league_id"]    = null;
	            $gamesVars["user_id"]      = 1;
	            $gamesVars["from"]   = $from;
	            $gamesVars["to"]     = $to;
	            $gamesVars["limit"]  = 0;
	            $gamesVars["offset"] = GAME_QTY_UNAUTHORIZED;

				$apiData = array();
				//$apiData["competitions"]   = array("url"  => "widgetdata/get_widget_competitions",  "vars"  => $competitionVars);
				$apiData["eventTemplates"] = array("url"  => "chilievents/get_all_event_templates", "vars"  => $eventsVars);
				
				//GETTING GAME LIVE DATA
				// $apiData["games"]          = array("url"  => "games/get_widget_games", "vars" => $gamesVars);

				$batchData = nut_api::api('batch/action', array("chili" => $apiData));

				//$wData["games"] 		   = $batchData["games"];
				//$wData["competitions"]   = $batchData["competitions"];
				$wData["eventTemplates"] = $batchData["eventTemplates"];

				//merge user css with default ones
				$wData["cssdata"] = $this->css_model->mergeCSS($widgetData["css"]);

				$langCode = "en";

				switch($wData["lang_id"])
				{
					case 2:
						$langCode = "ru";
						break;
					default:
						$langCode = "en";
				}

				$translationData = chili_translations::load_translations(MODULE_EMBED_FOLDER . '/game_view', $langCode);
				$translationData["WIDGET_LANG_CODE"] = $langCode;

				$wData = array_merge($wData, $translationData);

				//loading translation of design
				$designPageData = chili_translations::load_translations('home/widget_design_view', $language);

				$designPageData['WIDGET_CONTENT'] =  Modules::run(MODULE_EMBED_FOLDER . '/demo/render', $wData);
				// $designPageData['w_id']   = $widgetId;
				$designPageData['width']  = 800;
				$designPageData['height'] = 600;

				$pageData['DESIGN'] = $this->load->view(MODULE_SITE_FOLDER. "/home/widget_design_view.php", $designPageData, true);
			} 

			$pageData['page'] = $page;
			$this->mainViewData['CONTENT'] = $this->load->view(MODULE_SITE_FOLDER. "/home/widgets_game_view.php", $pageData, true);
			$this->load->view(MODULE_SITE_FOLDER . "/home/home_view.php", $this->mainViewData);
		}
		catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/show/game. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/show/game. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }

    public function accountSettings()
    {
    	try
        {
        	$partialData = chili_translations::load_translations('home/account_settings_view', $this->currentLang);
        	$this->preRenderAuthorized();

        	$apiData = nut_api::wrapData(array("id" => $this->siteUserId));
			$accountData = nut_api::api('siteusers/get_by_id', $apiData);

			$partialData["accountData"]    =  json_encode($accountData);
			$this->mainViewData['CONTENT'] = $this->load->view(MODULE_SITE_FOLDER. "/home/account_settings_view.php", $partialData, true);

			$this->load->view(MODULE_SITE_FOLDER . "/home/home_view.php", $this->mainViewData);
        }
        catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/show/accountSettings. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/show/accountSettings. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }


    public function accountPassword()
    {
    	try
        {
        	$partialData = chili_translations::load_translations('home/account_password_view', $this->currentLang);
        	$this->preRenderAuthorized();
        	
			$this->mainViewData['CONTENT'] = $this->load->view(MODULE_SITE_FOLDER. "/home/account_password_view.php", $partialData , true);
			$this->load->view(MODULE_SITE_FOLDER . "/home/home_view.php", $this->mainViewData);
        }
        catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/show/accountPassword. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/show/accountPassword. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }
}

