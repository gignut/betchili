<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

// Account ctrl purpose is serving ajax calls to create, update site user profile
class Account extends MX_Controller {
	
	public function __construct()
	{
		nut_session::init();
		parent::__construct();
	}

	//ajax
	public function signup()
	{
		try
		{
			$result = nut_api::api('siteusers/create',  $this->input->post());

			$id            = $result['id'];
			$userFirstName = $result['first_name'];
			$email         = $result['email'];

            if($id)
            {
                $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");
                $this->load->model(MODULE_GENERAL_FOLDER . "/email_model");


                $hash       = $this->shortlink_model->generateUniqueString(80, $id);
                $type       = SIGNUP;
                $tmpTime    = time();
                $start_date = date("Y-m-d H:i:s", $tmpTime);
                $end_date   = date("Y-m-d H:i:s", $tmpTime + 86400);
                $count      = 0;
                $this->shortlink_model->insert($hash, $type, $id, $start_date, $end_date, $count); 
                $link       = BASE_URL . INDEX_PHP . "/" . MODULE_GENERAL_FOLDER . "/shortlink/confirm/" . $hash;
                $this->email_model->sendRegistrationMail($userFirstName, $email, "Betchili Team", $link);
            }
            else
            {
                nut_utils::noty_error(SOMETHING_WENT_WRONG);
            }   

			echo json_encode($result);
		}
		catch(Api_exception $e)
		{
			$code = $e->getCode();
			switch($code)
			{
				case Api_exception::$SITEUSER_SIGNUP_SAME_EMAIL :
					nut_utils::print_error("Email is already used." , $code);	
					break;
				default:
				nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/error/error.php");
			}
		}		
		catch(Exception $e)
		{
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/error/error.php");
		}
	}

	public function login()
	{
		try
		{
			$data       = nut_api::api('siteusers/is_registered',  $this->input->post());
			$siteUserId = $data["id"]; 
			$domain     = $data["domain"];
			$this->load->model(MODULE_SITE_FOLDER . "/account_model");
		
			$this->account_model->login($siteUserId, $domain);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$code = $e->getCode();
			$tmp = $e->getData();
			
			switch($code)
			{
				case Api_exception::$SITEUSER_EMAIL_NOT_CONFIRMED:
					$tmpEmail = $tmp["email"];
					$ret = array("error" => $e->getCode(), "email" => $tmpEmail);
					$ret["message"] = "Pleae verify your email.";
					break;
				case Api_exception::$SITEUSER_BLOCKED:
					$tmpEmail = $tmp["email"];
					$ret = array("error" => $e->getCode(), "email" => $tmpEmail);
					$ret["message"] = "Your account is blocked.";	
					break;
				case Api_exception::$SITEUSER_INVALID_USERNAME_OR_PASSWORD:
					$ret = array("error" => $e->getCode());
					$ret["message"] = "Invalid username or password.";
					break;
				default:
				nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/error/error.php");
			}

			echo json_encode($ret);
		}		
		catch(Exception $e)
		{
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/error/error.php");
		}
	}

	public function logout()
    {
    	Modules::run(MODULE_SITE_FOLDER . '/authorize/siteuser');

    	$this->load->model(MODULE_SITE_FOLDER . "/account_model");
    	$this->account_model->logout();
	    $path = BASE_INDEX;
	    header('Location: ' . $path);
	    exit();
    }

	public function resendVerificationEmail()
	{
		try
		{
			$result = nut_api::api('siteusers/get_by_email',  $this->input->post());
			$id            = $result['id'];
			$userFirstName = $result['first_name'];
			$email         = $result['email'];

			if($id)
	        {
	            $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");
	            $this->load->model(MODULE_GENERAL_FOLDER . "/email_model");

	            $hash       = $this->shortlink_model->generateUniqueString(80, $id);
	            $type       = SIGNUP;
	            $tmpTime    = time();
                $start_date = date("Y-m-d H:i:s", $tmpTime);
                $end_date   = date("Y-m-d H:i:s", $tmpTime + 86400);
	            $count      = 0;
	            $this->shortlink_model->insert($hash, $type, $id, $start_date, $end_date, $count); 
	            $link       = BASE_URL . INDEX_PHP . "/" . MODULE_GENERAL_FOLDER . "/shortlink/confirm/" . $hash;
	            $this->email_model->sendRegistrationMail($userFirstName, $email, "Betchili Team", $link);
	        }
	        else
	        {
	            nut_utils::noty_error(SOMETHING_WENT_WRONG);
	        } 
		}
		catch(Api_exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
	}

	public function sendPasswordResetEmail()
	{
		try
		{
			$result = nut_api::api('siteusers/get_by_email',  $this->input->post());
			$id            = element("id", $result, NULL);

			if($id)
	        {
	        	$userFirstName = $result['first_name'];
				$email         = $result['email'];

	            $this->load->model(MODULE_GENERAL_FOLDER . "/shortlink_model");
	            $this->load->model(MODULE_GENERAL_FOLDER . "/email_model");


	            $hash       = $this->shortlink_model->generateUniqueString(80, $id);
	            $type       = RESSET_PASSWORD;
	            $tmpTime    = time();
                $start_date = date("Y-m-d H:i:s", $tmpTime);
                $end_date   = date("Y-m-d H:i:s", $tmpTime + 86400);
	            $count      = 0;
	            $this->shortlink_model->insert($hash, $type, $id, $start_date, $end_date, $count); 
	            $link       = BASE_URL . INDEX_PHP . "/" . MODULE_GENERAL_FOLDER . "/shortlink/confirm/" . $hash;
	            $this->email_model->sendPasswordResetEmail($userFirstName, $email, "Betchili Team", $link);
	        }
	        else
	        {
	           // just to show we have error	
	           // message is dummy one
	           echo json_encode(array("error"=> "not register email"));
	        } 
		}
		catch(Api_exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }

	}

	public function updatePassword()
	{
		try
		{
			Modules::run(MODULE_SITE_FOLDER . '/authorize/siteuser');
			
			validator::arr()->key('pass',    		validator::regex('/^[a-zA-Z0-9._]{5,}$/')->length(6,24))
							->key('confirm_pass',   validator::regex('/^[a-zA-Z0-9._]{5,}$/')->length(6,24))
					        ->check($this->input->post());

			$pass = $this->input->post("pass");
			$confirmPass = $this->input->post("confirm_pass");	
			if($pass === $confirmPass)
			{
				$this->load->model(MODULE_SITE_FOLDER . "/account_model");
				// we store pass in aux data during shoertlink click
				$userId =  $this->account_model->getAuxData();
				$params = array(nut_api::$PARAM => array("user_id" => $userId , "pass" => $pass));
				$result = nut_api::api('siteusers/change_password', $params);
				echo json_encode(array("password_reseted" => 1));
			}	
			else
			{
				nut_utils::noty_error(SOMETHING_WENT_WRONG . ". Passwords don't match.");
			}
		}
		catch(Api_exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
	}

	public function updateAccountPassword()
	{
		try
		{
			$siteUserId   = Modules::run(MODULE_SITE_FOLDER . '/authorize/siteuser');
			
			validator::arr()->key('pass',    		validator::regex('/^[a-zA-Z0-9._]{5,}$/')->length(6,24))
							->key('confirm_pass',   validator::regex('/^[a-zA-Z0-9._]{5,}$/')->length(6,24))
					        ->check($this->input->post());

			$pass        = $this->input->post("pass");
			$confirmPass = $this->input->post("confirm_pass");	
			if($pass === $confirmPass)
			{
				$apiData = array(nut_api::$PARAM => array("user_id" => $siteUserId , "pass" => $pass));
				$result = nut_api::api('siteusers/change_password', $apiData);
				echo json_encode(array("password_updated" => 1));
			}	
			else
			{
				nut_utils::noty_error(SOMETHING_WENT_WRONG . ". Passwords don't match.");
			}
		}
		catch(Api_exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
	}

	public function updateAccountSettings()
	{
		try
		{
			$siteUserId = Modules::run(MODULE_SITE_FOLDER . '/authorize/siteuser');
			$postdata   = $this->input->post();
    		$apiData    = nut_api::wrapData($postdata);

			$result = nut_api::api('siteusers/update', $apiData);		        
			echo json_encode(array("info_updated" => 1));
		}
		catch(Api_exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
	}

	public function sendrequest()
	{
		try
		{
			$result = nut_api::api('siteusers/sendrequest',  $this->input->post());
			$postData = $this->input->post('chili');
			$name        = $postData["name"];
            $email       = $postData["email"];
            $domain      = $postData["domain"];
			$this->load->model(MODULE_GENERAL_FOLDER . "/email_model");
			$this->email_model->sendRegistrationRequestMail($name, $email, $domain);
			echo json_encode(array("thanks" => 1));
		}
		catch(Api_exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }       
        catch(Exception $e)
        {
            nut_utils::noty_error(SOMETHING_WENT_WRONG);
        }
	}
	// END SITE USER
}

