<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Widgets extends MX_Controller {
	
	private $data;
	private $siteUserId;
	private $currentLang;
	private $allLanguages;
	private $mainViewData;

	public function __construct()
	{
		nut_session::init();
		parent::__construct();
		$this->siteUserId = Modules::run(MODULE_SITE_FOLDER . '/authorize/siteuser');
		$this->load->model(MODULE_SITE_FOLDER . "/account_model");
		
    	$this->currentLang = $this->account_model->getCurrentLang();
	}
	
    //ajax call
    public function createGameWidget()
    {
    	try
    	{
    		$postdata = $this->input->post();
    		$postdata["siteuser_id"] = $this->siteUserId;

    		$apiData = nut_api::wrapData($postdata);
			$data    = nut_api::api('siteusers/create_game_widget', $apiData);

			nut_utils::evalRedirect(BASE_INDEX . "widget/" . $data['id']. "/design");
    	}
    	catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/createGameWidget. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/createGameWidget. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }

    //ajax call
    public function updateGameWidget()
    {
    	try
    	{
    		$postdata = $this->input->post();
    		$postdata["siteuser_id"] = $this->siteUserId;

    		$apiData = nut_api::wrapData($postdata);
			
			$data = nut_api::api('siteusers/update_game_widget', $apiData);
			
			echo json_encode($data);
    	}
    	catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/updateGameWidget. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/updateGameWidget. siteUserID: '. $this->siteUserId);
			nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }

    //ajax
    public function removeWidget()
    {
    	try
    	{
    		$postdata = $this->input->post();
    		$postdata["siteuser_id"] = $this->siteUserId;

    		$apiData = nut_api::wrapData($postdata);
			
			$data = nut_api::api('siteusers/remove_widget', $apiData); 
			echo json_encode($data);
    	}
    	catch(Api_exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/removeWidget. siteUserID: '. $this->siteUserId);
			//nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}		
		catch(Exception $e)
		{
			nut_log::log('error', 'APIException: site/widgets/removeWidget. siteUserID: '. $this->siteUserId);
			//nut_utils::evalRedirect(BASE_INDEX . MODULE_SITE_FOLDER . "/account/error_page");
		}
    }

}

