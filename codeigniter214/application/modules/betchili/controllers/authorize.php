<?php
class Authorize extends MX_Controller
{
    function __construct()
    {
        nut_session::init();
        parent::__construct();
        $this->load->model(MODULE_SITE_FOLDER . "/account_model");
    }

    public function siteuser()
    {
        $siteUserId = $this->account_model->getAuthorizedUser();
        if($siteUserId)
        {
            Firelog::log("Authorized " . $siteUserId);
            return  $siteUserId;
        }
        else 
        {
            $path = BASE_INDEX . 'login';
            Firelog::log("Not Authorized");
            header('Location: ' . $path);
            exit();
        }
    }
}