<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demo extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function game()
	{
		$this->load->view(MODULE_SITE_FOLDER . "/demo/demo_view.php");
	}

}