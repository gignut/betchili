<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">


  <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
  <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ 'Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic:latin' ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); 
  </script>

<style>
	.chili_home_link{
		margin:0 auto;
		width:400px;
		text-align: center !important;
	}
	.chili_home_link > a{
		color: #666 !important;
		text-decoration: none !important;
		margin:0 auto !important;
	}
	.chili_not_confirmed{
		width:400px;
		margin:0 auto;
		background: #F3F3F3;
		border: 3px solid #DDD !important;
		padding: 0px 10px 15px 10px;
		margin-top: 30px !important;
	}
	
	@media screen and (max-width: 480px) {
		body { font-size: 90%;}	
		.chili_not_confirmed{
			width:90% !important;
		}
	}
</style>

<?php
  chili_asset_loader::flatstrap();
  css("modules/m_site/css/chilisite.css");
?>

</head>
<body>
    	<div class = "chili_not_confirmed">
				<div>
					<h4><?php echo $email ; ?> is not verified.</h4>
					<h5><a href=""><b>Resend</b></a> email for verification.</h5>
				</div>
		</div>
		<div class = "chili_home_link">
				<a href="<?php echo BASE_INDEX . 'show';?>"><h4>BETCHILI</h4></a>
		</div>
  </body>
</html>


