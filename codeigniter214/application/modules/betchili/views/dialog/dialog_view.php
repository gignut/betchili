<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" href="<?php echo MEDIA_URL . 'modules/m_site/img/favico.png';?>" />
  <script>
      window.BASE_URL     = "<?php echo BASE_URL;   ?>";
      window.BASE_INDEX   = "<?php echo BASE_INDEX; ?>";
      window.MEDIA_URL    = "<?php echo MEDIA_URL;  ?>";
  </script>

  <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
  <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ 'Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic:latin' ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); 
  </script>

  <!--[if lte IE 8]>
    <script type="text/javascript" src="<?php echo MEDIA_URL. 'modules/js/json2.js'; ?>"></script>
    <script>
      //document.createElement('ng-include');
      // Optionally these for CSS
      //document.createElement('ng:include');
    </script>
  <![endif]-->

  <?php
    chili_asset_loader::jquery(true, true);
    chili_asset_loader::angularjsVersion();
    chili_asset_loader::angularstrap();
    chili_asset_loader::flatstrap();
    chili_asset_loader::colorPicker();
    
    script("nutron/js/ng-nutron.js");
    script("modules/js/css3-mediaqueries.js");
    script("modules/js/jquery.blockUI.js");

    css("modules/m_site/css/chilisite.css");
    css("modules/m_site/css/dialog.css");
    script("modules/m_site/js/ng_chilisite.js");
  ?>

</head>

<div class="bg-div"></div>
<body ng-app="chilisiteApp" id="ng-app" class="ng-cloak" >
		<?php  echo $CONTENT; ?>
		<div class = "chili_home_link">
				<a href="<?php echo BASE_INDEX ;?>"><h4 style="color:white"><span style="color:black">BET</span><span style="color:red">CHILI</span></h4></a>
		</div>
  </body>
</html>


