  <?php
    css("modules/m_site/css/login.css");
  ?>

<script>
	
	function LogInModel(){

		this.email       = null;
		this.pass        = null;
		return this;
	}  

	function LogInCtrl($scope, $http, $window, ChiliUtils, ChiliConfigs) {

		$scope.loginModel = new LogInModel();
		$scope.regiteredUser;
		$scope.errorData;

		$scope.resetLogInForm = function(){
			$scope.logInSubmited = false;
			$scope.logInForm.email.$dirty  = false;
			$scope.logInForm.pass.$dirty   = false;
		};

		$scope.logIn = function(){
				$scope.logInSubmited = true;
				if($scope.logInForm.$valid){
					var ob  = {};
					ob[ChiliConfigs.API_KEY] = $scope.loginModel;
					
					ChiliUtils.block();
					$http({ method: "POST",
						url:  ChiliConfigs.BASE_INDEX + "betchili/account/login" ,
						data :  $.param(ob),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					 }).success(function(data, status, headers, config){
					 		$scope.errorData = new ErrorData();
							switch(parseInt(data.error))
							{
								case 101:
								case 102:
								case 103:
									$scope.errorData = data;
									ChiliUtils.unblock();
									break;
								default:
									$window.location.href = ChiliConfigs.BASE_INDEX + "widgets";
							}
				  		}).error(function(data, status, headers, config) {
				        	console.log("js::Error LoginCtrl logIn");
			 	    	});
				}
		};

		$scope.resendVerificationEmail = function(emailVal){
			ChiliUtils.block();
			var ob  = {};
			ob[ChiliConfigs.API_KEY] = {'email' : emailVal};
			$http({ method: "POST",
				url:  ChiliConfigs.BASE_INDEX + "betchili/account/resendVerificationEmail" ,
				data :  $.param(ob),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config){
					$scope.errorData = new ErrorData();
					ChiliUtils.unblock();
			}).error(function(data, status, headers, config) {
			        console.log("js::Error LoginCtrl resendVerificationEmail");
		 	});
		};
	}

</script>

<div class="bg-div"></div>
<div ng-controller = "LogInCtrl" class = "chili_login_body" ng-form = "logInForm">
	<div>
		<h3><?php echo $TR_LOGIN;?></h3>
		<div class="row-fluid">
			<input type="email" placeholder = "<?php echo $TR_EMAIL;?>" class = "chili_email_input"  ng-model = "loginModel.email" name = "email" nut-blurmodel required nut-valid-email>
			<span class = "chili_validation">
				<small ng-show="(logInForm.email.$dirty || logInSubmited) && logInForm.email.$invalid">
					<?php echo $TR_TIPEMAIL;?><b>hi@betchili.com.</b></small>
			</span>
		</div>
		<div class="row-fluid">
			<input type="password" placeholder = "<?php echo $TR_PASSWORD;?>" class = "chili_pass_input"  ng-model = "loginModel.pass" name = "pass" 
										nut-blurmodel required ng-minlength="6" ng-maxlength="256" ng-pattern=" /^[a-zA-Z0-9._]+$/" nut-enter="logIn()">
			<span class = "chili_validation">
				<small ng-show="(logInForm.pass.$dirty || logInSubmited) && logInForm.pass.$error.required"><?php echo $TR_REQUIRED;?></small>
				<small ng-show="(logInForm.pass.$dirty || logInSubmited) && logInForm.pass.$error.minlength"><?php echo $TR_TIPPASS;?></small>
				<small ng-show="(logInForm.pass.$dirty || logInSubmited) && logInForm.pass.$error.pattern">Enter latin character, dight, `.` or `_`.</small>
			</span>
		</div>

		<div ng-show ="errorData.error != null" class="alert alert-error">
			<div ng-switch on = "errorData.error" >
			    <div ng-switch-when = "101">
			   		<?php echo $TR_BLOCKED;?>.<br/>Contact <a href="mailto:support@betchili.com ?subject=Account is blocked."> <b> support@betchili.com </b></a>
			    </div>
		   		<div ng-switch-when = "102">
		   			 <b>{{errorData.email}}</b> <?php echo $TR_NOT_VERIF;?>.<br/>
		   			<a href="" ng-click = "resendVerificationEmail(errorData.email)"><b><?php echo $TR_RESEND;?></b></a>  <?php echo $TR_VERIFEMAIL;?>.
		   		</div>
		   		<div ng-switch-when = "103">
		   			 <?php echo $TR_INVALID;?>.
		   		</div>
		   	</div>
		</div>

		<hr/>
		<div class="row-fluid">
			<div  class="span12" style="position:relative;">
				<div><a href="<?php echo BASE_INDEX . 'forgotpassword';?>"><small><?php echo $TR_FORGOT;?>?</small></a></div>
				<!-- DISABLING SIGNUPS NOW START  -->
				<!-- <div><a href="<?php //echo BASE_INDEX . 'signup';?>"><small><?php //echo $TR_NOTHAVE;?>?</small></a></div> -->
				<!-- DISABLING SIGNUPS NOW END  -->
				<button ng-click = "logIn()" class = "loginBtn btn btn-large btn-danger pull-right"><?php echo $TR_LOGIN;?></button>
			</div>
		</div>
	</div>
</div>
