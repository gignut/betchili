  <?php
    css("modules/m_site/css/signup.css");
  ?>

<script>

function SignupModel(){

	this.first_name   = null;
	this.last_name    = null;
	this.email       = null;
	this.domain      = null;
	this.pass        = null;
	this.confirm_pass = null;
	return this;
}  

function SignupCtrl($scope, $http, ChiliUtils, ChiliConfigs) {

	$scope.signinModel = new SignupModel();
	$scope.signUpView = "userSignUpStart";
	$scope.errorData;

	$scope.resetSignUpForm = function(){

		$scope.signUpSubmited = false;

		$scope.signUpForm.firstName.$dirty   = false;
		$scope.signUpForm.lastName.$dirty    = false;
		$scope.signUpForm.email.$dirty       = false;
		$scope.signUpForm.domain.$dirty      = false;
		$scope.signUpForm.pass.$dirty        = false;
		$scope.signUpForm.confirmPass.$dirty = false;

	};

	$scope.signUp = function(){
		$scope.signUpSubmited = true;
		var ob  = {};
		ob[ChiliConfigs.API_KEY] = $scope.signinModel;
		if($scope.signUpForm.$valid){
			ChiliUtils.block();
			$scope.errorData = new ErrorData();
			$http({ method: "POST",
				url:  ChiliConfigs.BASE_INDEX + "betchili/account/signup",
				data :  $.param(ob),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			 }).success(function(data, status, headers, config){
						switch(parseInt(data.error))
						{
							case 100:
								$scope.errorData = data;
								break;
							default:
								$scope.signUpView = "userSignUpEnd";
								$scope.resetSignUpForm();
						}
						ChiliUtils.unblock();
			  		}).error(function(data, status, headers, config) {
			        	console.log("js::Error SignupCtrl signUp");
		 	    	});
		}
	};
}
</script>
<div class="bg-div"></div>
		<div ng-controller = "SignupCtrl" class = "chili_signup_body" ng-form = "signUpForm">
			<div ng-show = "signUpView == 'userSignUpStart'">
				<h3><?php echo $TR_SIGNUP;?></h3>
				<div class="row-fluid">
					<input type="text" placeholder = "<?php echo $TR_FIRSTNAME;?>" class = "chili_firstName_input"  ng-model = "signinModel.first_name" name = "firstName" nut-blurmodel required>
					<span class = "chili_validation">
						<small ng-show = "(signUpForm.firstName.$dirty || signUpSubmited) && signUpForm.firstName.$invalid"><?php echo $TR_REQUIRED;?></small>
					</span>
				</div>
				<div class="row-fluid">
					<input type="text" placeholder = "<?php echo $TR_LASTNAME;?>" class = "chili_lastName_input"  ng-model = "signinModel.last_name" name = "lastName" nut-blurmodel required>
					<span class = "chili_validation">
						<small ng-show="(signUpForm.lastName.$dirty || signUpSubmited) && signUpForm.lastName.$invalid"><?php echo $TR_REQUIRED;?></small>
					</span>
				</div>
				<div class="row-fluid">
					<input type="email" placeholder = "<?php echo $TR_EMAIL;?>" class = "chili_email_input"  ng-model = "signinModel.email" name = "email" nut-blurmodel required nut-valid-email>
					<span class = "chili_validation">
						<small ng-show="(signUpForm.email.$dirty || signUpSubmited) && signUpForm.email.$invalid">
							<?php echo $TR_TIPEMAIL;?><b>hi@betchili.com.</b>
						</small>
					</span>
				</div>
				<div class="row-fluid">
					<div style="position:relative;">
						<b style = "position:absolute; top:18px; left:11px;">http://</b>
						<input style="display:inline;" type="text" placeholder = "<?php echo $TR_DOMAIN;?>" class = "chili_domain_input"  ng-model = "signinModel.domain" 
						name = "domain" nut-blurmodel  
						nut-valid-domain ng-maxlength = "256" required ></input>
					</div>
					<span class = "chili_validation">
						<small ng-show="(signUpForm.domain.$dirty || signUpSubmited) && signUpForm.domain.$invalid"><?php echo $TR_TIPDOMAIN;?><b>sport.am.</b></small>
					</span>
				</div>
				<div class="row-fluid">
					<input type="password" placeholder = "<?php echo $TR_PASSWORD;?>" class = "chili_password_input"  ng-model = "signinModel.pass" name = "pass" nut-blurmodel required ng-minlength="6" ng-maxlength="256" ng-pattern=" /^[a-zA-Z0-9._]+$/" >
					<span class = "chili_validation">
						<small ng-show="(signUpForm.pass.$dirty || signUpSubmited) && signUpForm.pass.$error.required"><?php echo $TR_REQUIRED;?></small>
						<small ng-show="(signUpForm.pass.$dirty || signUpSubmited) && signUpForm.pass.$error.minlength"><?php echo $TR_TIPPASS;?></small>
						<small ng-show="(signUpForm.pass.$dirty || signUpSubmited) && signUpForm.pass.$error.pattern">Enter latin character, dight, `.` or `_`.</small>
					</span>
				</div>
				<div class="row-fluid">
					<input type="password" placeholder = "<?php echo $TR_CONFIRMPASS;?>" class = "chili_confirmPassword_input"  ng-model = "signinModel.confirm_pass" name = "confirmPass" nut-blurmodel nut-confirm-password = "signUpForm.pass">
					<span class = "chili_validation">
						<small ng-show="(signUpForm.confirmPass.$dirty || signUpSubmited) && signUpForm.confirmPass.$invalid"><?php echo $TR_TIPPASS1;?></small>
					</span>
				</div>

				<div ng-show = "errorData.error != null" class="alert alert-error">
				  {{errorData.message}}
				</div>

				<hr/>
				<div class="row-fluid">
					<div class="span8 chili_login">
						<small><?php echo $TR_QUEST;?></small>
						<a href="<?php echo BASE_INDEX . 'login';?>"><small><?php echo $TR_LOGIN;?></small></a>
					</div>
					<div class="span4">
						<button class = "pull-right btn btn-large btn-danger" ng-click = "signUp()"><?php echo $TR_SIGNUP;?></button>
					</div>
				</div>
			</div>
			<div ng-show = "signUpView == 'userSignUpEnd'">
				<div class = "chili_confirm_email">
					<h4><?php echo $TR_TIP1;?></h4><?php echo $TR_TIP2;?> <b>{{signinModel.email}}</b>.
				</div>
			</div>
		</div>
