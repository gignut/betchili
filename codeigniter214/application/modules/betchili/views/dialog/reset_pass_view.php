  <?php
    css("modules/m_site/css/reset_pass.css");
  ?>
<script>

function ResetPassModel(){

	this.pass = null;
	this.confirmPass = null;
	return this;
}  

function ResetPassCtrl($scope, $http, ChiliUtils, ChiliConfigs) {

	$scope.resetPassModel = new ResetPassModel();
	$scope.passwordReseted = 0;

	$scope.cleanPassForm = function(){

		$scope.resetPassSubmited                 = false;
		$scope.resetPassForm.pass.$dirty         = false;
		$scope.resetPassForm.confirmPass.$dirty  = false;
	};

	$scope.resetPass = function(){
			$scope.resetPassSubmited = true;
			var ob = {'pass' : $scope.resetPassModel.pass  ,'confirm_pass' : $scope.resetPassModel.confirmPass};

			if($scope.resetPassForm.$valid){
				ChiliUtils.block();
				$http({ method: "POST",
					url:  ChiliConfigs.BASE_INDEX + "betchili/account/updatePassword" ,
					data :  $.param(ob),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				 }).success(function(data, status, headers, config){
							ChiliUtils.action(data);
							$scope.passwordReseted = data.password_reseted;
							$scope.cleanPassForm();
							ChiliUtils.unblock();
				  		}).error(function(data, status, headers, config) {
				        	console.log("js::Error ResetPassCtrl resetPass");
			 	    	});
			}
	};
}
</script>
<div class="bg-div"></div>
	<div ng-controller = "ResetPassCtrl" class = "chili_resetPass_body" ng-form = "resetPassForm">
			<div ng-show = "passwordReseted == 0" style="padding-bottom:20px;">
				<h3>Reset Password</h3>
				<div class="row-fluid">
					<input type="password" placeholder = "New Password" class = "chili_pass_input"  ng-model = "resetPassModel.pass" name = "pass" nut-blurmodel required ng-minlength="6" ng-maxlength="256" ng-pattern=" /^[a-zA-Z0-9._]+$/">
					<span class = "chili_validation">
						<small ng-show="(resetPassForm.pass.$dirty || resetPassSubmited) && resetPassForm.pass.$error.required">
							Required.
						</small>
						<small ng-show="(resetPassForm.pass.$dirty || resetPassSubmited) && resetPassForm.pass.$error.minlength">
							Enter at least 6 characters.
						</small>
						<small ng-show="(resetPassForm.pass.$dirty || resetPassSubmited) && resetPassForm.pass.$error.pattern">
							Enter latin characters, digit, `.` or `_`.
						</small>	
					</span>
				</div>
				<div class="row-fluid">
					<input type="password" placeholder = "Confirm new password" class = "chili_pass_input" ng-model = "resetPassModel.confirmPass" name = "confirmPass" 
					nut-blurmodel nut-confirm-password = "resetPassForm.pass">
					<span class = "chili_validation">
						<small ng-show="(resetPassForm.confirmPass.$dirty || resetPassSubmited) && resetPassForm.confirmPass.$invalid">
							Passwords don't match.
						</small>
					</span>
				</div>
				<hr />
				<div class="row-fluid">
					<div class="span12" style="position:relative;">
						<small>Back to </small>
						<a href="<?php echo BASE_INDEX . 'login';?>"><small> Log In</small></a>
						<button class = "resetBtn btn btn-large btn-danger pull-right" ng-click = "resetPass()">Save</button>
					</div>
				</div>
			</div>
			<div ng-show = "passwordReseted == 1">
				<div class = "chili_pass_changed">
					<h4>Your password changed successfully!</h4>Now you can <a href="<?php echo BASE_INDEX . 'login';?>"><b>Log In</b></a> with new password.
				</div>
			</div>
		</div>
