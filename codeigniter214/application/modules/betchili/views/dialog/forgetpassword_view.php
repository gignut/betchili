  <?php
    css("modules/m_site/css/forgot.css");
  ?>

<script>

function ForgotCtrl($scope, $http, ChiliUtils, ChiliConfigs) {
	$scope.emailAddress;
	$scope.emailSent = false;
	$scope.errorTxt = null;

	$scope.resetForgotForm = function(){
		$scope.forgotSubmited = false;
		$scope.forgotForm.email.$dirty  = false;
	};

	$scope.forgot = function(){
			$scope.forgotSubmited = true;
			if($scope.forgotForm.$valid){
				var ob  = {};
				ob[ChiliConfigs.API_KEY] = {'email' : $scope.emailAddress};
				ChiliUtils.block();

				$scope.errorTxt = null;
				
				$http({ method: "POST",
					url:  ChiliConfigs.BASE_INDEX + "betchili/account/sendPasswordResetEmail" ,
					data :  $.param(ob),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				 }).success(function(data, status, headers, config){
							if(ChiliUtils.action(data))
							{
								if(data.error)
								{
									$scope.errorTxt = data.error;
								}
								else
								{
									$scope.emailSent = true;
									$scope.resetForgotForm();
								}
								
							}
							ChiliUtils.unblock();
				  		}).error(function(data, status, headers, config) {
				        	console.log("js::Error ForgotCtrl forgot");
			 	    	});
			}
	};
}
</script>
<div class="bg-div"></div>
		<div ng-controller = "ForgotCtrl" class = "chili_forgot_body" ng-form = "forgotForm">
				<div ng-show = "!emailSent" style="padding-bottom:20px;">
					<h3><?php echo $TR_FORGOT;?>?</h3>
					<div class="row-fluid">
						<input type="email" placeholder = "<?php echo $TR_EMAIL;?>" class = "chili_email_input"  ng-model = "emailAddress" name = "email" nut-blurmodel required nut-valid-email>
						<span class = "chili_validation"><small ng-show="(forgotForm.email.$dirty || forgotSubmited) && forgotForm.email.$invalid"><?php echo $TR_TIPEMAIL;?>hi@betchili.com.</b></small></span>
					</div>
					
					<div  ng-show ="errorTxt != null" class=" alert alert-error">
						 <?php echo $TR_NOT_REGISTERED_EMAIL; ?>
					</div>

					<div class="row-fluid">
						<div class="span12" style="position:relative;">
							<small><?php echo $TR_BACK;?></small>
							<a href="<?php echo BASE_INDEX . 'login';?>"><small><?php echo $TR_LOGIN;?></small></a>
							<button class = "resetBtn btn btn-large btn-danger pull-right" ng-click = "forgot()"><?php echo $TR_RESET;?></button>
						</div>
					</div>

					

				</div>

				<!-- PASS RESET EMAIL SENT -->
				<div ng-show = "emailSent">
					<div class = "chili_forgot_password">
						<?php echo $TR_CHECK;?>.
						<br/>
						<?php echo $TR_RESETLINK;?>.
					</div>
				</div>
		</div>
