  <?php
    css("modules/m_site/css/request.css");
  ?>

<script>
	
	function RequestModel(){
		this.name = null;
		this.email    = null;
		this.domain   = null;
		return this;
	}  

	function RequestCtrl($scope, $http, $window, ChiliUtils, ChiliConfigs) {
		$scope.sayThanks = false;
		$scope.requestModel = new RequestModel();

		$scope.resetRequestForm = function(){
			$scope.requestSubmitted = false;
			$scope.requestForm.name.$dirty   = false;
			$scope.requestForm.email.$dirty  = false;
			$scope.requestForm.domain.$dirty = false;
		};

		$scope.request = function(){
				$scope.sayThanks = false;
				$scope.requestSubmitted = true;
				if($scope.requestForm.$valid){
					var ob  = {};
					ob[ChiliConfigs.API_KEY] = $scope.requestModel;
					
					ChiliUtils.block();
					$http({ method: "POST",
						url:  ChiliConfigs.BASE_INDEX + "betchili/account/sendrequest",
						data :  $.param(ob),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					 }).success(function(data, status, headers, config){
							if(ChiliUtils.action(data))
							{
								$scope.sayThanks = true;
							}
							ChiliUtils.unblock();
				  		}).error(function(data, status, headers, config) {
				        	console.log("js::Error RequestCtrl request");
			 	    	});
				}
		};
	}

</script>
<div class="bg-div"></div>
    	<div ng-controller = "RequestCtrl" class = "chili_request_body" ng-form = "requestForm">
			<div>
				<h3><?php echo $TR_REQUEST_BETCHILI;?></h3>
				<small><b><?php echo $TR_REQUEST_TEXT; ?></b></small>
				<hr/>
				<!-- NAME -->
				<div class="row-fluid">
					<input type="text" placeholder = "<?php echo $TR_NAME;?>" class = "chili_name_input"  ng-model = "requestModel.name" name = "name" nut-blurmodel required>
					<span class = "chili_validation">
						<small ng-show = "(requestForm.name.$dirty || requestSubmitted) && requestForm.name.$invalid"><?php echo $TR_REQUIRED;?></small>
					</span>
				</div>
				<!-- EMAIL -->
				<div class="row-fluid">
					<input type="email" placeholder = "<?php echo $TR_EMAIL;?>" class = "chili_email_input"  ng-model = "requestModel.email" name = "email" nut-blurmodel required nut-valid-email>
					<span class = "chili_validation">
						<small ng-show="(requestForm.email.$dirty || requestSubmitted) && requestForm.email.$invalid"><?php echo $TR_TIPEMAIL;?><b>hi@betchili.com.</b></small>
					</span>
				</div>

				<!-- DOMAIN -->
				<div class="row-fluid">
					<div style="position:relative;">
						<b style = "position:absolute; top:18px; left:11px;">http://</b>
						<input style="display:inline;" type="text" placeholder = "<?php echo $TR_DOMAIN;?>" class = "chili_domain_input"  ng-model = "requestModel.domain" 
						name = "domain" nut-blurmodel  
						nut-valid-domain ng-maxlength = "256" required ></input>
					</div>
					<span class = "chili_validation">
						<small ng-show="(requestForm.domain.$dirty || requestSubmitted) && requestForm.domain.$invalid"><?php echo $TR_TIPDOMAIN;?><b>sport.am.</b></small>
					</span>
				</div>
				<hr style = "margin-bottom:3px;"/>
				
				<div class="alert  alert-success" ng-show="sayThanks">
					<?php echo $TR_THANKS_FOR_REQUEST; ?>
				</div>

				<div class="row-fluid">
					<div  class="span12" style="position:relative;">
						<button ng-click = "request()" class = "requestBtn btn btn-large btn-danger pull-right"><?php echo $TR_REQUEST;?></button>
					</div>
				</div>
			</div>
		</div>
