<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="landing page for betchili">
    <meta name="betchili" content="">
    <link rel="icon" type="image/png" href="<?php echo MEDIA_URL . 'modules/m_site/img/favico.png';?>" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title> Betchili </title>
    <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ 'Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic:latin' ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); 
  </script>

  <?php
    chili_asset_loader::jquery(true, true);
    // chili_asset_loader::angularjsVersion();
    // chili_asset_loader::angularstrap();
    // chili_asset_loader::flatstrap();
    // chili_asset_loader::colorPicker();
    // for background strech photo
    //chili_asset_loader::backstretch();
    
    // script("nutron/js/ng-nutron.js");
    // script("modules/js/css3-mediaqueries.js");
    // script("modules/js/jquery.blockUI.js");

    // old site
    // css("modules/m_site/css/chilisite.css");
    script("modules/m_site/js/ng_chilisite.js");

    // new site
    css("modules/m_site/assets/css/bootstrap.css");
    css("modules/m_site/assets/css/font-awesome.min.css");
    css("modules/m_site/assets/plugins/vegas/jquery.vegas.min.css");
    css("modules/m_site/assets/css/style.css");

    script("modules/m_site/assets/plugins/bootstrap.js");
    script("modules/m_site/assets/plugins/vegas/jquery.vegas.min.js");
    script("modules/m_site/assets/plugins/jquery.easing.min.js");
    script("modules/m_site/assets/js/custom.js");
  ?>  

<!-- start Mixpanel -->
  <script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
  typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
  b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
  mixpanel.init("be531de8966dee828adc28fc11f630e2");
  </script>
<!-- end Mixpanel -->
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body >
     <div class="navbar navbar-inverse navbar-fixed-top scrollclass" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <b><a class="navbar-brand" href="#">BET<span style="color:red;">CHILI</span></a></b>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home">HOME</a></li>
                     <li><a href="#about">ABOUT</a></li>
                    <li><a href="#clients-testimonial">TESTIMONIALS</a></li>
                    <li><a href="#contact">CONTACT</a></li>
                     <li><a href="#social-section">SOCIAL</a></li>
                     <li><a href="<?php echo BASE_INDEX . 'login';?>" onclick="mixpanel.track('loginBtn_click');">LOGIN</a></li>
                </ul>
            </div>
           
        </div>
    </div>
   
    
   
    <!--HOME SECTION-->
    <div class="container" id="home">
        <div class="row text-center scrollclass">
            <div class="col-md-12">
                <span class="head-main"> BETCHILI </span>
                <h3 class="head-last">Connecting brands with sports fans in digital world</h3>
                <a href="<?php echo BASE_INDEX . 'request';?>" class="btn btn-primary btn-lg " onclick="mixpanel.track('requestBtn_click');">Request Account</a> 
            </div>
        </div>
    </div>
    <!--END HOME SECTION-->

    <!--ABOUT SECTION-->
    <section class="for-full-back color-bg-one" id="about">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-8 col-md-offset-2 ">
                    <h1>Product Tour</h1>
                </div>
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2 ">
                        <h5>
                            -                            
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="for-full-back color-white" id="about-team">
        <div class="container">
                   <div class="col-md-3 col-sm-3 col-xs-6 about-box">
                        <div class="panel-danger">
                            <div class="panel-heading">
                             <h4>What is Betchili</h4> 
                            </div>
                            <div class="alert alert-danger">
                               Content strategy is crucial for brand marketers to engage with sports fans. Betchili is a social sports entertaining online solution (as a 3-th party widget) which will help brands to connect with sports fans and as a result build strong customer loyalty, brand image and drive the business metrics.
                            </div>
                            
                        </div>
                    </div>
                <div class="col-md-3 col-sm-3 col-xs-6 about-box">
                        <div class="panel-warning">
                            <div class="panel-heading">
                             <h4>Your Benefits</h4> 
                            </div>
                            <div class="alert alert-warning">
                               Digital solution to engage sports fans with your brand will increase your social appearance, make sports fans happy and connected with your brand by creating strong emotional contact between sports fans and your brands.
                            </div>
                            
                        </div>
                    </div>
                <div class="col-md-3 col-sm-3 col-xs-6 about-box">
                        <div class="panel-success">
                            <div class="panel-heading">
                             <h4>How to use</h4> 
                            </div>
                            <div class="alert alert-success">
                               You can integrate Betchili widget everywhere – websites, social network pages, in a separate microsite, etc. No need to do any programing. Integration takes only 2 minutes and then you can manage all design related aspects of the integrated plugin.
                            </div>
                           
                        </div>
                    </div>
                <div class="col-md-3 col-sm-3 col-xs-6 about-box">
                        <div class="panel-info ">
                            <div class="panel-heading">
                             <h4>Pricing</h4> 
                            </div>
                            <div class=" alert alert-info">
                               We have both free packages and advanced premium options. For premium options you can contact us.
                            </div>
                            
                        </div>
                    </div>
        </div>
    </section>
    <!--END ABOUT SECTION-->

     <!--CLIENT TESTIMONIALS SECTION-->
       <section id="clients-testimonial">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 ">
                    <h1>Clients Testimonials</h1>
                    <div id="carousel-example" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example" data-slide-to="1"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="container center">
                                <div class="col-md-6 col-md-offset-3 slide-custom">
                                   
                                    <h4><i class="fa fa-quote-left"></i> Very innovative customer engagement solution. It increased our visibility through social networks by 30% in 1 month and helped us to improve our ranking by 8 among the top 50 websites in Armenia.
                                    <i class="fa fa-quote-right"></i></h4>
                                    <h5 class="pull-right"><strong class="c-black">Karen S. </strong></h5>
                                    <h5 class="pull-right" style="font-style:italic">CEO at armfootball.com</h5><br><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container center">
                                <div class="col-md-6 col-md-offset-3 slide-custom">
                                    <h4> <i class="fa fa-quote-left"></i> Betchili is a cool when sports funs, our brand and our customers meet together in one place. <i class="fa fa-quote-right"></i></h4>
                                    <h5 class="pull-right"><strong class="c-black">Maria L.</strong></h5>
                                    <h5 class="pull-right" style="font-style:italic">Marketing manager at Pepsi Macedonia</h5>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
           </section>
     <!--END CLIENT TESTIMONIALS SECTION-->

<!--     <section class="for-full-back color-bg-one" id="about">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-8 col-md-offset-2 ">
                    <h1>Betchili In Action</h1>
                </div>
            </div>

        </div>
    </section>


       <section id="clients-testimonial" class="for-full-back color-white" style="padding-top:50px; padding-bottom:50px">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-5 col-sm-5" style="color:white; opacity:0.8; font-weight:900">
                    <h4>From your customer perspective they can easily login through a social accounts and start to predict the upcoming game results and do other prediction activities on upcoming real sports events. They will have the opportunity to do this for FREE as we are not a bookmaker. Your customers will be able to share their predictions in social networks, compete with their friends, feel the excitement of sports predictions ,just have fun and as a result engage new sports fans with your brand.</h4>
                </div>
                <div class="col-md-5 col-sm-5" style="color:white; opacity:0.8; font-weight:900">
                  <img src="<?php echo MEDIA_URL .'/modules/m_site/assets/img/action.jpg';?>"></img>
                </div>
            </div>
        </div>
        </section> -->

    <!--CONTACT SECTION-->
    <section class="for-full-back color-bg-one" id="contact">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-8 col-md-offset-2 ">
                    <h1>Contacts</h1>
                </div>
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2 ">
                        <h5>
                            <div class="chili_mail_phone" style="clear:both;">
                                <div class="chili_mail">
                                  <img src="<?php echo MEDIA_URL .'/modules/m_site/img/chili_mail.png';?>">
                                  hi@betchili.com
                                </div>
                                <!-- <div class="chili_phone">
                                  <img src="<?php echo MEDIA_URL .'/modules/m_site/img/chili_phone.png';?>">
                                  +374 94 36 00 85
                                </div> -->
                            </div>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
    <!--SOCIAL SECTION-->
    <section id="social-section">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4">
                </div>
                <div class="col-md-2 scl">
          <h4>FACEBOOK</h4>
          <p class="text-center"><a href="https://www.facebook.com/betchili" target="_blank"><i class="fa fa-facebook fa-5x"></i></a></p>
        </div>
        <div class="col-md-2 scl">
          <h4 >TWITTER</h4>
          <p class="text-center"><a href="https://twitter.com/Betchili" target="_blank"><i class="fa fa-twitter fa-5x"></i></a></p>
        </div>
            </div>
            </div>
        </section>
  
    <!--END SOCIAL SECTION-->
    <!--FOOTER SECTION -->
    <div class="for-full-back" id="footer">
        2014 www.betchili.com | All Right Reserved          
    </div>
    <!-- END FOOTER SECTION -->
</body>
</html>
