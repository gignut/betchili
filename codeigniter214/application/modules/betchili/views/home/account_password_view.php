  <?php
    css("modules/m_site/css/profile.css");
  ?>

<script>

function AccountPasswordModel(){
	this.pass         = null;
	this.confirm_pass = null;
	return this;
}  

AccountPasswordModel.prototype.setData = function(data){
	this.pass         = data.pass;
	this.confirm_pass = data.confirm_pass;
}

AccountPasswordModel.prototype.getData = function(){
	var ob  = {};
	ob.pass           = this.pass;
	ob.confirm_pass   = this.confirm_pass;
	return ob;
}

function AccountPasswordCtrl($scope, $http, ChiliUtils, ChiliConfigs) {

	$scope.accountModel = new AccountPasswordModel();

	$scope.resetForm = function(){
		$scope.formSubmited = false;
		$scope.accountForm.pass.$dirty        = false;
		$scope.accountForm.confirmPass.$dirty = false;
	};

	$scope.updateAccountPassword = function(){
		$scope.formSubmited = true;
		var ob = $scope.accountModel.getData();
		if($scope.accountForm.$valid){
			ChiliUtils.block();
			$scope.errorData = new ErrorData();
			$http({ method: "POST",
				url:  ChiliConfigs.BASE_INDEX + "betchili/account/updateAccountPassword",
				data :  $.param(ob),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			 }).success(function(data, status, headers, config){
						$scope.resetForm();
						ChiliUtils.unblock();
			  		}).error(function(data, status, headers, config) {
			        	console.log("js::Error AccountCtrl updateAccountPassword");
		 	    	});
		}
	};
}

</script>

		<div ng-controller = "AccountPasswordCtrl" class = "chili_signup_body" ng-form = "accountForm">
			
			<h3><?php echo $TR_CHANGE_PASSWORD;?> </h3>
			
			<div>
				<div class="row-fluid">
					<input  type="password" placeholder = "<?php echo $TR_NEW_PASSWORD;?>" class = "chili_password_input"  
							ng-model = "accountModel.pass" name = "pass" nut-blurmodel required ng-minlength="6" ng-maxlength="256" ng-pattern=" /^[a-zA-Z0-9._]+$/" >
					<span class = "chili_validation">
						<small ng-show="(accountForm.pass.$dirty || formSubmited) && accountForm.pass.$error.required">
							<?php echo $TR_REQUIRED;?>
						</small>
						<small ng-show="(accountForm.pass.$dirty || formSubmited) && accountForm.pass.$error.minlength">
							<?php echo $TR_TIPPASS;?>
						</small>
						<small ng-show="(accountForm.pass.$dirty || formSubmited) && accountForm.pass.$error.pattern">
							Enter latin character, dight, `.` or `_`.
						</small>
					</span>
				</div>
				<div class="row-fluid">
					<input  type="password" placeholder = "<?php echo $TR_CONFIRM_PASSWORD;?>" class = "chili_confirmPassword_input"  
							ng-model = "accountModel.confirm_pass" name = "confirmPass" nut-blurmodel nut-confirm-password = "accountForm.pass">
					<span class = "chili_validation">
						<small ng-show="(accountForm.confirmPass.$dirty || formSubmited) && accountForm.confirmPass.$invalid">
							<?php echo $TR_TIPPASS1;?>
						</small>
					</span>
				</div>

				<div ng-show = "errorData.error != null" class="alert alert-error">
				  {{errorData.message}}
				</div>

				<hr/>
				<div class="row-fluid">
					<div class="span12">
						<button class = "pull-right btn btn-large btn-danger" ng-click = "updateAccountPassword()">
							<?php echo $TR_UPDATE;?>
						</button>
					</div>
				</div>

			</div>

		</div>
