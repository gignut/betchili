<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8" />
  <meta name="google" value="notranslate">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
  <script>
    window.BASE_INDEX   = <?php echo '"' . BASE_INDEX  . '"'; ?>;
    window.BASE_DOMAIN  = <?php echo '"' . BASE_DOMAIN  . '"'; ?>;
    window.MEDIA_URL    = <?php echo '"' . MEDIA_URL . '"'; ?>;
    window.BASE_URL     = <?php echo '"' . BASE_URL  . '"'; ?>;
    window.INDEX_PHP    = <?php echo '"' . INDEX_PHP . '"'; ?>;
  </script>

  <!--[if lte IE 8]>
    <script type="text/javascript" src="<?php echo MEDIA_URL. 'modules/js/json2.js'; ?>"></script>
    <script>
      //document.createElement('ng-include');
      // Optionally these for CSS
      //document.createElement('ng:include');
    </script>
  <![endif]-->

  <?php
    chili_asset_loader::jquery(true, false);
    chili_asset_loader::angularjsVersion();
    chili_asset_loader::angularstrap();
    chili_asset_loader::flatstrap();
    chili_asset_loader::chosen();
    chili_asset_loader::colorPicker();

    chili_asset_loader::momentJs();
    chili_asset_loader::scrollBar();
    // for background strech photo
    
    script("nutron/js/ng-nutron.js");
    script("modules/js/css3-mediaqueries.js");
    script("modules/js/jquery.blockUI.js");

    // css("modules/m_site/css/chilisite.css");
    css("modules/m_site/css/header.css");
    script("modules/m_site/js/ng_chilisite.js");
    script("modules/m_site/js/ng_widgets.js"); 
    script("modules/m_site/js/ng_header.js");
  ?>

  <!-- START Mixpanel -->
  <script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
  typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
  b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
  mixpanel.init("be531de8966dee828adc28fc11f630e2");
  </script>
  <!-- END Mixpanel -->

</head>

  <script>
    var currentLanguageCode = "<?php echo $currentLanguageCode;?>";
    var allLanguages = <?php echo json_encode($allLanguages);?>;
  </script>

<body ng-app="chilisiteApp" id="ng-app" class="ng-cloak football" >

<div class="navbar navbar-fixed-top chili_navbar" ng-controller = "HeaderCtrl">
              <div class="navbar-inner chili_navbar_inner" ng-init="init()">
                <div class="chili_header">
                      <div class="chili_left_menu">
                        <div class="chili_logo">
                          <?php echo $TR_BETCHILI;?>
                        </div>

                        <!-- WIDGETS -->
                        <div class="chili_menu_item">
                            <a  href="<?php echo BASE_INDEX . "widgets"; ?>">
                              <i> <img src="<?php echo MEDIA_URL . 'modules/m_site/img/widgets.png';?>" style = "height:48px; width:48px;pointer-events:none;"></i>
                              <span class = "chili_account_title"><?php echo $TR_WIDGETS;?></span>
                            </a>  
                        </div>

                        <!-- CREATE WIDGET -->
                         <div class="dropdown chili_menu_account_item">
                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i> <img src="<?php echo MEDIA_URL . 'modules/m_site/img/add_widget.png';?>" style = "height:48px; width:48px;pointer-events:none;"></i>
                                    <span class = "chili_account_title"><?php echo $TR_CREATE_WIDGET;?></span>
                                    <i class="chili_caret"></i>
                                  </a>
                                  <ul class="dropdown-menu chili_dropdown">
                                    <li>
                                      <a href="<?php echo BASE_INDEX . 'create'; ?>"><?php echo $TR_GAME;?></a>
                                    </li>
                                  </ul>
                            </div>  

                      </div>
 
                      <div class="chili_right_menu">
                        <div>
                            <div class="chili_menu_left_item"></div>
                            <div class="dropdown chili_menu_account_item">
                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i> <img src="<?php echo MEDIA_URL . 'modules/m_site/img/globe.png';?>" style = "height:48px; width:48px;pointer-events:none;"></i>
                                    <span class = "chili_account_title">{{currentLanguageName}}</span>
                                    <i class="chili_caret"></i>
                                  </a>
                                  <ul class="dropdown-menu chili_dropdown">
                                    <li ng-repeat="lang in allLanguages">
                                       <!-- ng-show="lang.id < 3" added to have only russian and usa -->
                                      <a ng-show="lang.id < 3" href="#" ng-show="lang.code != currentLanguageCode" ng-click="changeLang(lang.code)">{{lang.name}}</a>
                                    </li>
                                  </ul>
                            </div>  

                            <div class="dropdown chili_menu_account_item">
                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i> <img src="<?php echo MEDIA_URL . 'modules/m_site/img/chili_user.png';?>" style = "height:48px; width:48px;pointer-events:none;"></i>
                                    <span class = "chili_account_title"><?php echo $TR_ACCOUNT;?></span>
                                    <i class="chili_caret"></i>
                                  </a>
                                  <ul class="dropdown-menu chili_dropdown">
                                    <li>
                                      <a href="<?php echo BASE_INDEX . "account/settings"; ?>" ><?php echo $TR_SETTINGS; ?></a>
                                    </li>
                                    <li>
                                      <a href="<?php echo BASE_INDEX . "account/password"; ?>" ><?php echo $TR_CHANGE_PASSWORD; ?></a>
                                    </li>
                                     <li>
                                       <a href="<?php echo BASE_INDEX . "logout"; ?>" ><?php echo $TR_LOGOUT;?></a>
                                    </li>
                                  </ul>
                            </div>  


                            <!-- <div class="chili_menu_right_item"></div> -->
                           <!--  <div class="chili_menu_item">
                                  <a  href="<?php //echo BASE_INDEX . "logout"; ?>">
                                    <img src="<?php //echo MEDIA_URL . 'modules/m_site/img/logout_white.png';?>" style = "height:48px; width:48px;pointer-events:none;"></i>
                                    <span class = "chili_account_title"><?php //echo $TR_LOGOUT;?></span>
                                  </a>  
                            </div>   -->
                        </div>
                      </div>
                </div>
              </div>
</div>

<!-- BODY OF ACCOUNT -->
<div>
  <?php echo $CONTENT;?>
</div>
</body>
</html>