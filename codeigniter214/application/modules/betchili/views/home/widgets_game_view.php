<?php css("modules/m_site/css/account.css"); ?>

<script>
    var sizeList         = <?php echo $sizeList;?>;
    var leaguesList      = <?php echo $leaguesList;?>;
    var gameWidgetData   = <?php echo isset($gameWidgetData) ? $gameWidgetData : 'undefined' ;?>;
    var gameWidgetStep   = '<?php echo $page;?>';
</script>

<script id="gameWidgetRemovePopover" type="text/ng-template">
	<div id="popoverCtrl">
	    <h5><?php echo $TR_SURE;?>?</h5>
	    <button class="btn btn-primary" type = "button" ng-click = "removeGameWidget(); hide()"><?php echo $TR_YES;?></button>
		<button class="btn" type = "button" ng-click = "hide()"><?php echo $TR_NO;?></button>
	</div>
</script>

<div ng-controller="GameWidgetCtrl" id="gameWidgetView">
	<div ng-init ="init()">
		<div class="chili_dashboard_row">

			<h3 ng-show = "!widgetModel.id" class="text-center" style="margin-top: 60px; color: rgb(102, 102, 102);"><?php echo $TR_NEW;?></h3>
			<h3 ng-show = "widgetModel.id"  class="text-center" style="margin-top: 60px; color: rgb(102, 102, 102);">{{widgetModel.name}}</h3>

			<div class="tabbable tabbable-custom" style="position:relative;">
	          <ul class="nav nav-tabs">
	            <li  ng-class="{chili_custom_active_tab: gameWidgetStep == 'main'}" 
	            	 ng-click = " gameWidgetStep = 'main';">
	            	<a href="" ><b><?php echo $TR_CONFIG;?></b></a>
	            </li>

	            <li  ng-show = "widgetModel.id" ng-class="{chili_custom_active_tab: gameWidgetStep == 'design'}" 
	            	 ng-click = " gameWidgetStep = 'design';">
	            	<a href="" ><b><?php echo $TR_DESIGN;?></b></a>
	            </li>
	          </ul>

	          <span style="display:inline-block; position:absolute; top:0px;right:0px;">
	          		<span ng-show = "widgetModel.id">
	          			<span   data-unique="1" 
	          					bs-popover = "'gameWidgetRemovePopover'" 
	          					data-container="#gameWidgetView"
	          					data-placement="bottom"
	          					data-trigger = "click"
	          					class="badge widgetBadge">
	          				<?php echo $TR_REMOVE;?>
	          			</span>
	          		</span>

	          		<span style="margin-left:10px;">
	          			<a href="" ng-click="saveGameWidget()">
	          				<span class="badge widgetBadge">
		          			   <span ng-show="gameWidgetStep == 'main'   && !widgetModel.id"><?php echo $TR_CREATE;?></span>
		          			   <span ng-show="gameWidgetStep == 'main'   && widgetModel.id"> <?php echo $TR_UPDATE;?></span>
		          			   <span ng-show="gameWidgetStep == 'design' && widgetModel.id"> <?php echo $TR_UPDATE;?></span>
	          			   </span>
	          			</a>
	          		</span>
	          </span>	

	          <div>
		      
		          <div class="tab-content" ng-show="gameWidgetStep == 'main'">
		            <div class="tab-pane">
		            	<div ng-form = "gameWidgetForm" style="padding:10px;">

		            		<div class="clearfix" style="position:relative;">
		            			

			            		<!-- NAME -->
			            		<div class="pull-left chili_inline_block" style="margin-right:15px;">
				            		<h5><?php echo $TR_NAME;?></h5>
									<input type="text" placeholder = "<?php echo $TR_WNAME;?>" ng-model = "widgetModel.name" name = "name" nut-blurmodel required>
									<div class = "chiliWidgetCreateValidation">
										<small ng-show = "(gameWidgetForm.name.$dirty || gameWidgetFormSubmited) && gameWidgetForm.name.$invalid"><?php echo $TR_REQUIRED;?>.</small>
									</div>
								</div>
								<!-- SIZE -->
								<div class="pull-left chili_inline_block" style="margin-right:15px;">
									<h5><?php echo $TR_SIZE;?></h5>
									<select   name="size" ng-model = "widgetModel.size_id" required 
						         			  ng-options="size.id as size.label for size in  sizeList" 
						         			  style="width:185px;">
						         			  <option value = ""><?php echo $TR_CHOOSE_SIZE;?></option>
						            </select>
						            <div class = "chiliWidgetCreateValidation">
										<small ng-show = "(gameWidgetForm.size.$dirty || gameWidgetFormSubmited) && gameWidgetForm.size.$invalid"><?php echo $TR_REQUIRED;?>.</small>
									</div>
					       		 </div>
					       		 <!-- LANG -->
					       		<div class="pull-left chili_inline_block" style="margin-right:15px;">
						            <h5><?php echo $TR_LANGUAGE;?></h5>
									<select   name="language" ng-model = "widgetModel.lang_id"  required 
						         			  ng-options="lang.id as lang.name for lang in  langList" 
						         			  style="width:185px;">
						         			  <option value = ""><?php echo $TR_CHOOSE_LANGUAGE;?></option>
						            </select>
						            <div class = "chiliWidgetCreateValidation">
										<small ng-show = "(gameWidgetForm.language.$dirty || gameWidgetFormSubmited) && gameWidgetForm.language.$invalid"><?php echo $TR_REQUIRED;?>.</small>
									</div>
					        	</div>
				        	</div>
				        	<hr/>

				        	<!-- LEAGUES -->
				        	<div>
					        	<h5><?php echo $TR_LEAGUES;?></h5>
					        	<select name = "leagues" required
					        			multiple nutchosen = "leaguesList" 
					        			data-placeholder="<?php echo $TR_SELECTL;?>"  
					        			ngwidth="100%" 
					        			ng-model   = "selectedLeagues"
					        			chosen-change-event = "leaguesChanged"
					         			ng-options ="league.id as league.tname  group by league.sport_name for league in  leaguesList" 
					         			 >
					         			<option></option>
							    </select>
							    <div class = "chiliWidgetCreateValidation" style="margin-top:4px !important;">
									<small ng-show = "(gameWidgetForm.leagues.$dirty || gameWidgetFormSubmited) && gameWidgetForm.leagues.$invalid"><?php echo $TR_REQUIRED;?>.</small>
								</div>
							</div>
						</div>
		            </div>
		          </div>   
		          
		          <div class="tab-content" ng-show="gameWidgetStep == 'design' && widgetModel.id">
		            <div class="tab-pane">
		              <?php echo $DESIGN; ?>
		            </div>
		          </div>
	          
	          </div>  

			</div>			
		</div>	
	</div>
</div>
	