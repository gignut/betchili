<style>
	.colorPicker {
		width:24px;
		height:24px;
		float:left;
		border: 1px solid gray;
		margin:1px;
		border-radius: 2px;
	}
	.colorPicker:hover {
		cursor:pointer;
	}
	.colorRow {
		clear: both;
	}

	.colorHiddenEdit {
		width: 30px;
		border: solid #ffffff 1px !important;
		background-color: transparent;
		cursor: pointer;
		vertical-align: middle;
		cursor: text;
		width: 70px;
		font-style: bold !important;
		font-size: 14px !important;
		line-height: 14px !important;
		height: 24px !important;
		padding: 0px 0px 0px 5px !important;
		margin-top: 1px;
	}

	.colorHiddenEdit:hover{
	  border:solid #777777 1px !important;
	}	

	.colorHiddenEdit:focus{
	  border:solid #777777 1px !important;
	}

	.cp-cursor {
	  cursor: url(<?php echo MEDIA_URL .'modules/m_site/img/color-picker.png'; ?>), pointer !important;
	}

</style>

<div>
		<div style="width:131px; float:left;" ng-init="togglePart1 = true; ">
			<!-- HEADER -->
			<div style="margin-top:25px;">
				<span style="cursor:pointer;" ng-click="togglePart1 = !togglePart1"><h5><?php echo $TR_HEADER;?></h5></span>
				<div ng-show="togglePart1">
						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.body_bg_color"
									ng-style="{ 'background-color' : designModel.body_bg_color }"
									nut-tooltip title="Background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.body_bg_color" color-edit>
						</div>
						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.h_bg_color" picker-open="h_bg_color"
									ng-style="{'background-color': designModel.h_bg_color}" nut-tooltip title="Header background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.h_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.h_font_color" picker-open="h_font_color"
									ng-style="{ 'background-color' : designModel.h_font_color }"  
									nut-tooltip title="Header font color." placement="right" >
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.h_font_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.h_icon_color" picker-open="h_icon_color"
									ng-style="{ 'background-color' : designModel.h_icon_color }"  
									nut-tooltip title="Header icon color." placement="right" >
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.h_icon_color" color-edit>
						</div>

						<!-- ####  CHALLANGES START #### -->
						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_bg_color" picker-open="challange_bg_color"
									ng-style="{ 'background-color' : designModel.challange_bg_color}"
									nut-tooltip title="Challange background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_font_color" picker-open="challange_font_color"
									ng-style="{ 'background-color' : designModel.challange_font_color}"
									nut-tooltip title="Challange font color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_font_color" color-edit>
						</div>


						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_team_bg" picker-open="challange_team_bg"
									ng-style="{ 'background-color' : designModel.challange_team_bg }"
									nut-tooltip title="Challange team's background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_team_bg" color-edit>
						</div>


						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_option_enabled_bg" picker-open="challange_option_enabled_bg"
									ng-style="{ 'background-color' : designModel.challange_option_enabled_bg }"
									nut-tooltip title="Challange's enabled option background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_option_enabled_bg" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_option_enabled_font" picker-open="challange_option_enabled_font"
									ng-style="{ 'background-color' : designModel.challange_option_enabled_font }"
									nut-tooltip title="Challange's enabled option font color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_option_enabled_font" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_option_disabled_bg" picker-open="challange_option_disabled_bg"
									ng-style="{ 'background-color' : designModel.challange_option_disabled_bg }"
									nut-tooltip title="Challange's disabled option background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_option_disabled_bg" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_option_disabled_font" 
							     picker-open="challange_option_disabled_font"
								 ng-style="{ 'background-color' : designModel.challange_option_disabled_font }"
								 nut-tooltip title="Challange's disabled option font." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_option_disabled_font" color-edit>
						</div>


						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_btn_bg" picker-open="challange_btn_bg"
									ng-style="{ 'background-color' : designModel.challange_btn_bg }"
									nut-tooltip title="Challange's button background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_btn_bg" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.challange_btn_font"  picker-open="challange_btn_font"
									ng-style="{ 'background-color' : designModel.challange_btn_font }"
									nut-tooltip title="Challange's button font color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.challange_btn_font" color-edit>
						</div>
						<!-- #### CHALLANGES END  ####-->
				</div>
			</div>

			<!-- GAME -->
			<div style="clear:both; padding-top:15px;" ng-init="togglePart2 = true;">
				<span style="cursor:pointer;" ng-click="togglePart2 = !togglePart2"><h5><?php echo $TR_GAME;?></h5></span>
				<div  ng-show="togglePart2">
						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.gh_bg_color" picker-open="gh_bg_color"
									ng-style="{ 'background-color' : designModel.gh_bg_color }"
									nut-tooltip title="Header background." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.gh_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.gh_tmp_bg_color" picker-open="gh_tmp_bg_color"
									ng-style="{ 'background-color' : designModel.gh_tmp_bg_color }"
									nut-tooltip title="Event background." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.gh_tmp_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.gh_tmp_font_color" picker-open="gh_tmp_font_color"
									ng-style="{ 'background-color' : designModel.gh_tmp_font_color }" 
									nut-tooltip title="Event font color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.gh_tmp_font_color" color-edit>
						</div>


						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_left_bg_color" picker-open="g_left_bg_color"
									ng-style="{ 'background-color' : designModel.g_left_bg_color }"
									nut-tooltip title="Game row left part." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_left_bg_color" color-edit>
						</div>


						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_right_bg_color" picker-open="g_right_bg_color"
									ng-style="{ 'background-color' : designModel.g_right_bg_color }"
									nut-tooltip title="Game row right part." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_right_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_border_color" picker-open="g_border_color"
									ng-style="{ 'background-color' : designModel.g_border_color }" 
									nut-tooltip title="Border color" placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_border_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_img_bg_color" picker-open="g_img_bg_color"
									ng-style="{ 'background-color' : designModel.g_img_bg_color }"
									nut-tooltip title="Team background color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_img_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_left_font_color" picker-open="g_left_font_color"
									ng-style="{ 'background-color' : designModel.g_left_font_color }"
									nut-tooltip title="Left font color." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_left_font_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_tmp_bg_color" picker-open="g_tmp_bg_color"
									ng-style="{ 'background-color' : designModel.g_tmp_bg_color }"
									nut-tooltip title="Event background." placement="right">
							</div>

							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_tmp_bg_color" color-edit>
						</div>

						<div class="colorRow">
							<div colorpicker="hex" class="colorPicker" ng-model="designModel.g_tmp_font_color" picker-open="g_tmp_font_color"
									ng-style="{ 'background-color' : designModel.g_tmp_font_color }"
									nut-tooltip title="Event font." placement="right">
							</div>
							<input type="text" class="input-small colorHiddenEdit" ng-model="designModel.g_tmp_font_color" color-edit>
						</div>
				</div>
			</div>

		</div>

		<!-- DESIGN TAB -->
		<div class="pull-left">
			<div style="margin-left:125px; margin-top:55px;">
				<h4 style="color: rgb(148, 146, 146); font-style: italic;text-align:center;"><?php echo $TR_GAME_PREVIEW;?></h4>
				
				<div class="gameWidgetView" 
					 style="max-height: <?php echo $height . 'px';?>; height: <?php echo $height . 'px';?>; width: <?php echo $width . 'px';?>; 
					 border: 2px dashed gray;
					 padding: 5px;">
					<?php echo $WIDGET_CONTENT; ?>
				</div>
			</div>	
		</div>
</div>
