  <?php
    css("modules/m_site/css/profile.css");
  ?>

<script>

var accountData = <?php echo $accountData; ?>

function AccountSettingsModel(){
	this.id;
	this.first_name   = null;
	this.last_name    = null;
	this.email        = null;
	this.domain       = null;
	return this;
}  

AccountSettingsModel.prototype.setData = function(data){
	this.id           = data.id;
	this.first_name   = data.first_name;
	this.last_name    = data.last_name;
	this.email        = data.email;
	this.domain       = data.domain;
}

AccountSettingsModel.prototype.getData = function(){
	var ob  = {};
	ob.id           = this.id;
	ob.first_name   = this.first_name;
	ob.last_name    = this.last_name;
	ob.email        = this.email;
	ob.domain       = this.domain;

	return ob;
}

function AccountSettingsCtrl($scope, $http, ChiliUtils, ChiliConfigs, $window) {

	$scope.accountModel = new AccountSettingsModel();

	$scope.resetForm = function(){
		$scope.formSubmited = false;
		$scope.accountForm.firstName.$dirty   = false;
		$scope.accountForm.lastName.$dirty    = false;
		$scope.accountForm.email.$dirty       = false;
		$scope.accountForm.domain.$dirty      = false;
	};

	$scope.updateAccountSettings = function(){
		$scope.formSubmited = true;
		var ob = $scope.accountModel.getData();
		if($scope.accountForm.$valid){
			ChiliUtils.block();
			$scope.errorData = new ErrorData();
			$http({ method: "POST",
				url:  ChiliConfigs.BASE_INDEX + "betchili/account/updateAccountSettings",
				data :  $.param(ob),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			 }).success(function(data, status, headers, config){
						$scope.resetForm();
						ChiliUtils.unblock();
			  		}).error(function(data, status, headers, config) {
			        	console.log("js::Error AccountCtrl updateAccountSettings");
		 	    	});
		}
	};

	$scope.init = function(){
		$scope.accountModel.setData($window.accountData);
	};
}

</script>

		<div ng-controller = "AccountSettingsCtrl" class = "chili_signup_body" ng-form = "accountForm">
			
			<h3><?php echo $TR_ACCOUNT_SETTINGS;?> </h3>
			<div ng-init = "init()">
				<div class="row-fluid">
					<input type="text" placeholder = "<?php echo $TR_FIRSTNAME;?>" class = "chili_firstName_input"  
						   ng-model = "accountModel.first_name" name = "firstName" nut-blurmodel required>
					<span class = "chili_validation">
						<small ng-show = "(accountForm.firstName.$dirty || formSubmited) && accountForm.firstName.$invalid">
							<?php echo $TR_REQUIRED;?></small>
					</span>
				</div>
				<div class="row-fluid">
					<input  type="text" placeholder = "<?php echo $TR_LASTNAME;?>" class = "chili_lastName_input"  
							ng-model = "accountModel.last_name" name = "lastName" nut-blurmodel required>
					<span class = "chili_validation">
						<small ng-show="(accountForm.lastName.$dirty || formSubmited) && accountForm.lastName.$invalid"><?php echo $TR_REQUIRED;?></small>
					</span>
				</div>
				<div class="row-fluid">
					<input  type="email" placeholder = "<?php echo $TR_EMAIL;?>" class = "chili_email_input"  
							ng-model = "accountModel.email" name = "email" nut-blurmodel required nut-valid-email>
					<span class = "chili_validation">
						<small ng-show="(accountForm.email.$dirty || formSubmited) && accountForm.email.$invalid">
							<?php echo $TR_TIPEMAIL;?><b>hi@betchili.com.</b>
						</small>
					</span>
				</div>
				<div class="row-fluid">
					<div style="position:relative;">
						<b style = "position:absolute; top:18px; left:11px;">http://</b>
						<input style="display:inline;" type="text" placeholder = "<?php echo $TR_DOMAIN;?>" class = "chili_domain_input"  
							   ng-model = "accountModel.domain" 
							   name = "domain" nut-blurmodel  
							   nut-valid-domain ng-maxlength = "256" required ></input>
					</div>
					<span class = "chili_validation">
						<small ng-show="(accountForm.domain.$dirty || formSubmited) && accountForm.domain.$invalid">
							<?php echo $TR_TIPDOMAIN;?><b>sport.am.</b></small>
					</span>
				</div>

				<div ng-show = "errorData.error != null" class="alert alert-error">
				  {{errorData.message}}
				</div>

				<hr/>
				<div class="row-fluid">
					<div class="span12">
						<button class = "pull-right btn btn-large btn-danger" ng-click = "updateAccountSettings()">
							<?php echo $TR_UPDATE;?>
						</button>
					</div>
				</div>
			</div>
		</div>
