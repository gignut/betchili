<?php css("modules/m_site/css/account.css"); ?>

<?php chili_asset_loader::syntaxHighlighter();?>

<script>
	var siteUserWidgets = <?php echo $siteUserWidgets;?>;
	var siteUserDomain = "<?php echo $domain;?>";
	$(document).ready(function() {
		  SyntaxHighlighter.all();
	});
</script>

<script id="integrationModal" type="text/ng-template" >
   <div id="popoverCtrl">
	   <div class="modal-header">
	  		<h3><?php echo $TR_HTI_HEADER;?></h3>
		</div>
		<div class="modal-body">

			<h4 style="color:rgb(252, 48, 48);"><?php echo $TR_HTI_IMPORTANT;?>!</h4>
			<p><small>
				<?php echo $TR_HTI_NOTE1;?> <b style="color:rgb(252, 48, 48);">{{siteUserDomain}}</b> <?php echo $TR_HTI_NOTE2;?> 
				<a href="<?php echo BASE_INDEX . 'account/settings'; ?>">
					<?php echo $TR_HTI_NOTE3;?>
				</a>
			</small></p>
			<hr />
			<h5><?php echo $TR_STEP;?> 1:</h5>
				<p>
					<small>
					<?php echo $TR_HTI_DESC1;?> <b class="chiliExplainTag">&ltscript src=&quot//betchili.com/index.php/embed/widget/loader/{{currentWidget.id}}&quot&gt&lt/script&gt  </b> <?php echo $TR_HTI_DESC2;?> <b class="chiliExplainTag">&lthead&gt</b> <?php echo $TR_HTI_DESC3;?>.
					</small>
				</p>
			
			<h5><?php echo $TR_STEP;?> 2:</h5>
			<p><small>
				<?php echo $TR_HTI_DESC1;?> <b class="chiliExplainTag"> &ltdiv id=&quotchili_game_{{item.id}}&quot&gt&lt/div&gt</b> <?php echo $TR_HTI_DESC2;?> <b class="chiliExplainTag">&ltbody&gt</b> <?php echo $TR_HTI_DESC3;?>.
			</small></p>
			<hr />
			<h4><?php echo $TR_HTI_DESC4;?></h4>
		    <pre class="brush: xml">
    				&lthead&gt  
    				 ...
    					&ltscript src=&quot//betchili.com/index.php/embed/widget/loader/{{item.id}}&quot&gt&lt/script&gt  
    				 ...	
    				&lt/head&gt  

	    			&ltbody&gt  
	    			 ...
	    		        &ltdiv id=&quotchili_game_{{item.id}}&quot&gt&lt/div&gt
	    		     ... 
	    			&lt/body&gt
    		</pre>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn" ng-click="hide()"><?php echo $TR_CLOSE;?></button>
		</div>
	</div>
</script>





<div ng-controller="WidgetDashboardCtrl" id="widgetDashboardView">
	<div ng-init ="init()">
		<!-- HEADER -->
		<div  class="chili_widget_header_nav">
			<div class="chili_dashboard_row">
					<div style="padding-left:0.8%; padding-right:0.8%;">
						 
					</div>
			</div>			
		</div>

		<!-- ALL WIDGETS  -->
		<div class="row-fluid">
			<div class="chili_dashboard_row">
			<!-- SITE USER WIDGETS START -->
					<div class="span12">
						<div ng-show="widgetCount == 0">
							<h2 class="text-center" style="margin-top:100px;color:rgb(124, 120, 120);"><?php echo $TR_EMPTY;?>.</h2>
						</div>	
						<div ng-repeat="item in siteUserWidgets" style="width:50%; float:left;">
							<div>
								<div class="chili_widget_thumbnail">
										<div ng-show = "!item.showIntegration">
												<h3 class="chili_widget_title">{{item.name}}</h3>
												<span class="label label-info chili_widget_size"><?php echo $TR_SIZE;?>: {{item.width}}x{{item.height}} </span>
												<span class="label label-info chili_widget_language"><?php echo $TR_LANGUAGE;?>: {{item.lang_name}}</span>
												<a href="" bs-modal="'integrationModal'" ng-click= "currentWidget = item">
							          				<small><?php echo $TR_HOWTO;?></small>
							          			</a>
												
												<div>
													<h5 class="chili_widget_chapter"><?php echo $TR_LEAGUES;?></h5>
													<div class="clearfix">
														<img  ng-repeat = "league in item.leagues"  class="chili_widget_league_img img-rounded" ng-src="<?php echo MEDIA_URL . 'assets/leagues/'; ?>{{league.logo}}" >
													</div>	
												</div>

												<div>
													<h5 class="chili_widget_chapter"><?php echo $TR_STATS;?></h5>
												</div>	

												<a class="btn btn-danger chili_widget_editbtn"
													 href="<?php echo BASE_INDEX . 'widget/';?>{{item.id}}">
													<i class="icon-edit"></i> 
													<?php echo $TR_EDIT;?>
												</a>
									    </div>
								</div>	
							</div>	
	
						</div>
					</div>
					<!-- SITE USER WIDGETS END -->
			</div>			
		</div>	

	</div>	
		
</div>
	
