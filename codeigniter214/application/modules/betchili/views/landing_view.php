<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="google" value="notranslate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="landing page for betchili">
    <meta name="betchili" content="">
    <link rel="icon" type="image/png" href="<?php echo MEDIA_URL . 'modules/m_site/img/favico.png';?>" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title> Betchili </title>
    <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ 'Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic:latin' ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); 
  </script>

  <?php
    chili_asset_loader::jquery(true, true);
    // chili_asset_loader::angularjsVersion();
    // chili_asset_loader::angularstrap();
    // chili_asset_loader::flatstrap();
    // chili_asset_loader::colorPicker();
    // for background strech photo
    //chili_asset_loader::backstretch();
    
    // script("nutron/js/ng-nutron.js");
    // script("modules/js/css3-mediaqueries.js");
    // script("modules/js/jquery.blockUI.js");

    // old site
    script("nutron/js/ng-nutron.js");
    css("modules/m_site/css/chilisite.css");
    script("modules/m_site/js/ng_chilisite.js");

    // new site
    css("modules/m_site/assets/css/bootstrap.css");
    css("modules/m_site/assets/css/font-awesome.min.css");
    css("modules/m_site/assets/plugins/vegas/jquery.vegas.min.css");
    css("modules/m_site/assets/css/style.css");

    script("modules/m_site/assets/plugins/bootstrap.js");
    script("modules/m_site/assets/plugins/vegas/jquery.vegas.min.js");
    script("modules/m_site/assets/plugins/jquery.easing.min.js");
    script("modules/m_site/assets/js/custom.js");
  ?>  

<!-- start Mixpanel -->
  <script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
  typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
  b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
  mixpanel.init("be531de8966dee828adc28fc11f630e2");
  </script>
    <script>
          $( document ).ready(function(){
	    if (window.location.host == 'www.betchili.com') {
             mixpanel.track('siteLoad_view');
	    }
          });
	  function demoButtonClick () {
	      if (window.location.host == "www.betchili.com") {
	          mixpanel.track('demoBtn_click');
	      }
	  };
	  function loginButtonClick () {
	      if (window.location.host == "www.betchili.com") {
		  mixpanel.track('loginBtn_click');
	      }
	  };
  </script>
<!-- end Mixpanel -->
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body ng-app="chilisiteApp" id="ng-app" >
     <div class="navbar navbar-inverse navbar-fixed-top scrollclass" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" collaplass="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <b><a class="navbar-brand" href="#">BET<span style="color:red;">CHILI</span></a></b>
                    <div class="btn-group" style="margin-top:10px">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" style="font-size:12px">
                        <?php echo $currentLanguage["name"];?>
                        <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu chili_langs">
                        <?php
                          foreach($allLanguages as $langId => $langItem) {
                            if($langItem["code"] != $currentLanguage["code"] and $langItem["code"] != 'am')
                            {
                               echo '<li ><a href="'. BASE_INDEX .'home/'.$langItem["code"].'">'.$langItem["name"].'</a></li>';
                            }
                          }
                        ?>
                      </ul>
                    </div>
            </div>
            <div class="navbar-collapse collapse">
                 <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home"><?php echo $TR_HOME;?></a></li>
                     <li><a href="#about"><?php echo $TR_ABOUT;?></a></li>
                    <li><a href="#clients-testimonial"><?php echo $TR_TESTIMONIALS;?></a></li>
                    <li><a href="#contact"><?php echo $TR_CONTACT;?></a></li>
                     <li><a href="#social-section"><?php echo $TR_SOCIAL;?></a></li>
                     <li><a href="<?php echo BASE_INDEX . 'login';?>" onclick="loginButtonClick()"><?php echo $TR_LOGIN;?></a></li>
                </ul>
            </div>

           
        </div>
    </div>
       
   
    <!--HOME SECTION-->
    <div class="container" id="home">
        <div class="row text-center scrollclass">
            <div class="col-md-12">
                <span class="head-main"> <?php echo $TR_BETCHILI;?> </span>
                <h3 class="head-last"><?php echo $TR_SUBTITLE;?></h3>
                <a href="<?php echo BASE_INDEX . 'request';?>" class="btn btn-primary btn-lg " onclick="mixpanel.track('requestBtn_click');"><?php echo $TR_ACCOUNT;?></a> 
                <a href="<?php echo BASE_INDEX . 'demo';?>" target="_blank"class="btn btn-primary btn-lg"  onclick="demoButtonClick()" style="color:white; font-weight:900"><?php echo $TR_DEMO;?></a> 
            </div>
        </div>
    </div>
    <!--END HOME SECTION-->

    <!--ABOUT SECTION-->
    <section class="for-full-back color-bg-one" id="about" style="padding-top:30px !important;">
        <div class="container">
            <div class="row text-center scrollclass">
                <div class="col-md-8 col-md-offset-2 ">
                    <h1><?php echo $TR_TOUR;?></h1>
                </div>
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2 ">
                          <span style="display: block; padding: 10px;">
                                <a href="#about"> <img src="<?php echo MEDIA_URL .'/modules/m_site/img/scroll_down.png';?>"></a>
                          </span>   
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="for-full-back color-white" id="about-team">
        <div class="container">
               <div class="col-md-4 col-sm-6 col-xs-12 about-box" style="padding-right:30px;padding-left:30px;">
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/big-icon.png';?>" width="128px" height="128px"> 
                            <h4 class="about-text-header"><?php echo $TR_WHAT;?></h4>
                        </div>
                         <!-- Content strategy is crucial for brand marketers to engage with sports fans.  -->
                        <p class="about-text-body">
                          <?php echo $TR_WHAT_IS;?>
                        </p>
                        
                    </div>
                </div>
               <div class="col-md-4 col-sm-6 col-xs-12 about-box" style="padding-right:30px;padding-left:30px;">
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/speed.png';?>"> 
                            <h4 class="about-text-header"><?php echo $TR_BENEFITS;?></h4>
                        </div>
                        <p class="about-text-body">
                            <?php echo $TR_BENEFITS_IS;?>
                        </p>
                    </div>
                </div>                  
               <div class="col-md-4 col-sm-6 col-xs-12 about-box" style="padding-right:30px;padding-left:30px;">
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/price.png';?>" width="128px" height="128px"> 
                            <h4 class="about-text-header"><?php echo $TR_PRICING;?></h4>
                        </div>
                        <p class="about-text-body">
                           <?php echo $TR_PRICING_IS;?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-md-12">
                    <div style="text-align:center">
                        <h1 class="integration-text"><?php echo $TR_INTEGRATION;?></h1>
                        <h5><?php echo $TR_INTEGRATION_IS;?></h5>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
               <div class="col-md-3 col-sm-6 col-xs-12 about-box" >
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/reg1.jpg';?>" width="128px" height="128px"> 
                            <h4 class="about-text-header"><?php echo $TR_STEP1;?></h4>
                        </div>
                        <p class="about-text-body2">
                            <?php echo $TR_STEP1_IS;?>
                        </p>
                        
                    </div>
                </div>

               <div class="col-md-2 col-sm-6 col-xs-12 about-box" >
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/reg2.png';?>" width="128px" height="128px"> 
                            <h4 class="about-text-header"><?php echo $TR_STEP2;?></h4>
                        </div>
                        <p class="about-text-body2" style="">
                            <?php echo $TR_STEP2_IS;?>
                        </p>
                        
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 about-box" >
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/blueprints.png';?>" width="128px" height="128px"> 
                            <h4 class="about-text-header"><?php echo $TR_STEP3;?></h4>
                        </div>
                        <p class="about-text-body2">
                            <?php echo $TR_STEP3_IS;?>
                        </p>
                        
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 about-box">
                    <div class="panel">
                        <div class="panel-heading" style="text-align:center !important;">
                            <img src="<?php echo MEDIA_URL .'/modules/m_site/img/reg4_.jpg';?>" width="128px" height="128px"> 
                            <h4 class="about-text-header"><?php echo $TR_STEP4;?></h4>
                        </div>
                        <p class="about-text-body2">
                            <?php echo $TR_STEP4_IS;?>
                        </p>
                    </div>
                </div>    
        </div>
    </section>
    <!--END ABOUT SECTION-->

     <!--CLIENT TESTIMONIALS SECTION-->
       <section id="clients-testimonial">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 ">
                    <h1><?php echo $TR_CTESTIMONIALS;?></h1>
                    <div id="carousel-example" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
<!--
                        <li data-target="#carousel-example" data-slide-to="1"></li>
-->
                    </ol>

                    <div class="carousel-inner carusel-height">
                        <div class="item active">
                            <div class="container center carusel-part">
                                <div class="col-md-6 col-md-offset-3 slide-custom">
                                   
                                    <h4><i class="fa fa-quote-left"></i> Very innovative customer engagement solution. It increased our visibility through social networks by 30% in 1 month and helped us to improve our ranking by 8 among the top 50 websites in Armenia.
                                    <i class="fa fa-quote-right"></i></h4>
                                    <h5 class="pull-right"><strong class="c-black">Karen S. </strong></h5>
                                    <h5 class="pull-right" style="font-style:italic">CEO at armfootball.com</h5><br><br><br>
                                </div>
                            </div>
                        </div>
                       <!--
			 <div class="item">
                            <div class="container center carusel-part">
                                <div class="col-md-6 col-md-offset-3 slide-custom">
                                    <h4> <i class="fa fa-quote-left"></i> Betchili is a cool when sports funs, our brand and our customers meet together in one place. <i class="fa fa-quote-right"></i></h4>
                                    <h5 class="pull-right"><strong class="c-black">Maria L.</strong></h5>
                                    <h5 class="pull-right" style="font-style:italic">Marketing manager at Pepsi Macedonia</h5>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
			-->
                    </div>
                </div>
                    </div>
                </div>
            </div>
           </section>
     <!--END CLIENT TESTIMONIALS SECTION-->

    <!--CONTACT SECTION-->
    <section class="for-full-back color-bg-one" id="contact" style="padding-top:0px !important">
        <div class="container">
            <div class="row text-center" >
                <div class="col-md-8 col-md-offset-2 ">
                    <h1><?php echo $TR_CONTACT;?></h1>
                </div>
                <div>
                    <div class="col-md-8 col-md-offset-2 ">
                            <div class="chili_mail_phone" style="clear:both;">
                                <div class="chili_mail">
                                  <img src="<?php echo MEDIA_URL .'/modules/m_site/img/chili_mail.png';?>">
                                  hi@betchili.com
                                </div>
                                <!-- <div class="chili_phone">
                                  <img src="<?php echo MEDIA_URL .'/modules/m_site/img/chili_phone.png';?>">
                                  +374 94 36 00 85
                                </div> -->
                            </div>
                    </div>

                      <div class="row-fluid chili_contact" id="social-section" >
                        <div class="chili_social"> 
                            <a href="https://twitter.com/Betchili" target="_blank"><div class="social twitter"></div></a>
                            <a href="https://www.facebook.com/betchili" target="_blank"><div class="social facebook"></div></a>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </section>
  
 
    <!--FOOTER SECTION -->
    <div class="for-full-back" id="footer">
        2015 www.betchili.com | <?php echo $TR_RIGHT;?>
    </div>
    <!-- END FOOTER SECTION -->
</body>
</html>
