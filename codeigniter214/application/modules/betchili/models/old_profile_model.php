<?php
class Profile_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*

	public function isAutorized()
    {
  	   $siteUserId = nut_session::get('siteuser_id');
       return  $siteUserId ?  $siteUserId : FALSE;
    }

	// create non blocked, non confirmed account
	public function signUp($firstName, $lastName, $email, $domain, $pass)
	{
		try
		{
			$list  = array($firstName, $lastName, $email, $domain, $pass);

			$sql   = "SELECT * FROM " . TB_SITEUSERS . " WHERE email = ?";
			$query = nut_db::query($sql, $email);

			if($query->num_rows() == 0)
			{
				$sql   = "INSERT INTO " .  TB_SITEUSERS . " (first_name, last_name, email, domain, pass, blocked, confirmed) VALUES (?,?,?,?,?, 0, 0)";
				$query = nut_db::query( $sql, $list);
			}
			else 
			{
				throw new Api_exception("This email is already used.", Api_exception::$USER_SIGNUP_SAME_EMAIL);
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function confirmSignUp($id)
	{
		try
		{
			$sql   = " UPDATE " . TB_SITEUSERS . " SET confirmed = 1 WHERE id = ? ";
			$query = nut_db::query($sql, $id);
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function getProfileById($userId)
	{
		try
		{
			$sql = "SELECT id,
						   first_name,
						   last_name,
						   email,
						   password,
						   domain,
						   blocked,
						   confirmed
				   FROM ". TB_SITEUSERS ." 
				   WHERE id = ?";
			$query = nut_db::query($sql, $userId);

			$data = $query->num_rows() == 1 ? $query->row_array() : array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getProfileByEmail($email)
	{
		try
		{
			$sql = "SELECT id,
						   first_name,
						   last_name,
						   email,
						   password,
						   domain,
						   blocked,
						   confirmed
				   FROM ". TB_SITEUSERS ." 
				   WHERE email = ?";
			$query = nut_db::query($sql, $email);

			$data = $query->num_rows() == 1 ? $query->row_array() : array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function updateProfileData($id, $firstName, $lastName, $email, $domain)
	{
		try
		{
			$sql = "UPDATE ". TB_SITEUSERS ." SET first_name = ?, last_name = ? , email = ?, domain = ? WHERE id = ?"
			$query = nut_db::query($sql, array($firstName, $lastName, $email, $domain, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	public function updateProfilePassword($id, $newPassword)
	{
		try
		{
			$sql = "UPDATE ". TB_SITEUSERS ." SET pass = ? WHERE id = ?"
			$query = nut_db::query($sql, array($newPassword, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

 */

}
?>