<?php
class Account_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function isAutorized()
    {
  	   $siteUserId = nut_session::get('siteuser_id');
       return  $siteUserId ?  $siteUserId : FALSE;
    }

    public function getAuthorizedUser()
	{
		try
		{
			$id = nut_session::get('siteuser_id');
			return $id;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAuthorizedUserDomain()
	{
		try
		{
			$domain = nut_session::get('siteuser_domain');
			return $domain;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


    public function login($id, $domain)
	{
		try
		{
			nut_session::set('siteuser_id', $id);
			nut_session::set('siteuser_domain', $domain);
			return TRUE;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function logout()
	{
		try
		{
			nut_session::set('siteuser_id', null);
			return TRUE;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


    // SITE USER FOR EXAMPLE IN RESET PASSWORD 
	public function setAuxData($data)
	{
		try
		{
			nut_session::set('aux_data', $data);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAuxData()
	{
		try
		{
			$data = nut_session::get('aux_data');
			return  $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function resetAuxData()
	{
		try
		{
			nut_session::set('aux_data', null);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getCurrentLang()
	{
		try
		{
			$lang = nut_session::get('language');
			return  $lang ?  $lang : "en";
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function setCurrentLang($lang)
	{
		try
		{
			nut_session::set('language', $lang);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}
?>