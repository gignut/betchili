<?php

use Respect\Validation\Validator as validator;

class events_model extends CI_Model {

	private static $AUX_TEMPLATES = "event_templates";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getEventById($id, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							template_id,
						    game_id,
						    bookmaker_id,
						    coefficient,
						    old_coefficient,
						    basis,
						    create_stamp,
						    data,
						    result,
						    result_status,
						    blocked
							FROM ". TB_EVENTS ."
							WHERE id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): array();

			//$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllBookmakers()
	{
		try
		{
			$sql = "SELECT  id, 
							name,
						    homepage,
						    logo,
						    blocked
							FROM ". TB_BOOKMAKERS;
			
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

public function getAllEventsByGameId($game_id, $bk_id, $aux_info)
	{
		try
		{
			$sql = "SELECT  id,
							template_id,
						    game_id,
						    bookmaker_id,
						    coefficient,
						    old_coefficient,
						    basis,
						    create_stamp,
						    data,
						    result,
						    result_status,
						    blocked
							FROM ". TB_EVENTS ."
							WHERE game_id = ? AND bookmaker_id = ?";
			
			$query = nut_db::query($sql, array($game_id, $bk_id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			//$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	public function getAllEvents($game_id, $bookmaker_id, $aux_info)
	{
		try
		{
			$sql = "SELECT  id,
							template_id,
						    game_id,
						    bookmaker_id,
						    coefficient,
						    old_coefficient,
						    basis,
						    create_stamp,
						    data,
						    result,
						    result_status,
						    blocked
							FROM ". TB_EVENTS ."
							WHERE game_id = ? AND bookmaker_id = ?";
			
			$query = nut_db::query($sql, array($game_id, $bookmaker_id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			//$this->aux_append('', $data, $langid, $aux_info);

			$result = array();
			$result['aux_events'] = array();
			$result['base_events'] = array();
			foreach ($data as $row) {
				if ($row['template_id'] > 11) {
					$result['aux_events'][] = $row;
				} else {
					$result['base_events'][] = $row;
				}
			}

			return $result;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllEventsByIDs($game_ids, $bookmaker_id, $aux_info)
	{
		try
		{
			$values = implode(',', array_fill(0, count($game_ids), "?"));
			$sql = "SELECT  id,
							template_id,
						    game_id,
						    bookmaker_id,
						    coefficient,
						    old_coefficient,
						    basis,
						    create_stamp,
						    data,
						    result,
						    result_status,
						    blocked
							FROM ". TB_EVENTS ."
							WHERE game_id IN (" . $values .") AND bookmaker_id = ?";
			
			array_push($game_ids, $bookmaker_id);
			$query = nut_db::query($sql, $game_ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			//$this->aux_append('', $data, $langid, $aux_info);
			
			$result = array();
			$result['aux_events'] = array();
			$result['base_events'] = array();
			
			foreach ($data as $row) {
				if ($row['template_id'] > 11) {
					$result['aux_events'][] = $row;
				} else {
					$result['base_events'][] = $row;
				}
			}

			return $result;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function insertEvents($events)
	{
		try
		{
			nut_db::transactionStart("insertEvents");

			$dbdata = array();
			for ($i = 1; $i < 12; $i++) {
				$dbdata[] = array('template_id'  => $i,
								  'game_id' => $events['game_id'],
								  'bookmaker_id' => $events['bookmaker_id'],
								  'coefficient'  => 0,
								  'old_coefficient'  => 0,
								  'basis'  => 0,
								  'data'   => '',
								  'result' => 0,
								  'result_status'  => -1,
								  'blocked' => 1
								  );
			}

			nut_db::insert_batch(TB_EVENTS, $dbdata);
			
			$ids = nut_db::getLastID(TB_EVENTS, 'id', count($dbdata));

			$ids = !is_array($ids) ? array($ids) : $ids;

			//$this->aux_insert($ids, $events);
			
			nut_db::transactionCommit("insertEvents");

			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertEvents");
			throw $e;
		}
	}

	public function updateEvents($events)
	{
		try
		{
			nut_db::transactionStart("updateEvents");
			$values = nut_utils::sanitize_keys($events, array('id', 'game_id', 'bookmaker_id', 'coefficient', 'old_coefficient',
													'basis', 'data', 'result', 'result_status',	'blocked'), FALSE, NULL);
			nut_db::update_batch(TB_EVENTS, $values, 'id');

			nut_db::transactionCommit("updateEvents");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateEvents");
			throw $e;
		}
	}

	public function getAllEventGroups($sport_id)
	{
		try
		{
			$sql = "SELECT  id,
							name,
						    priority,
						    sport_id,
						    blocked

							FROM ". TB_EVENTS_GROUPS ."
							WHERE sport_id = ? ORDER BY priority";
			
			$query = nut_db::query($sql, array($sport_id));

			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// 2014 FEB 16
	// avar
	public function getAllEventTemplates($sportId, $langid = 1)
	{
		try
		{
			$sql = "SELECT  ET.id,
							ET.group_id,
							ET.name,
							GetTranslation(". $langid .", ET.id, ". chili_translate::$EVENT_TEMPLATES.") AS tname,
							GetTranslation(". $langid .", ET.id, ". chili_translate::$EVENT_TEMPLATES_LONG.") AS tlongname,
						    ET.priority,
						    ET.result_text,
						    ET.operation,
						    ET.in_result,
						    ET.blocked
							FROM ". TB_EVENTS_TEMPLATES ." AS ET 
							INNER JOIN " . TB_EVENTS_GROUPS . " AS EG  ON ET.group_id = EG.id 
							WHERE sport_id = ? ORDER BY EG.priority ASC , ET.priority ASC";
			
			$query = nut_db::query($sql, array($sportId));
			
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}