<?php

use Respect\Validation\Validator as validator;

class siteusers_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	/* SITE USER SPECIFIC MODEL START */
	public function getSiteUserDataById($id)
	{
		try
		{
			$sql = "SELECT  id,
							first_name,
							last_name,
						    email,
						    -- password,
						    domain,
						    blocked,
						    confirmed
					FROM ". TB_SITEUSERS ." 
					WHERE id = ? ";
			$query = nut_db::query($sql, $id);
			$data =  $query->num_rows() == 1 ? $query->row_array() : array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// save request for using betchili
	public function	createClientRequest($name, $email, $domain)
	{
		try
		{
			$list  = array($name, $email, $domain);
			$sql   = "INSERT INTO " .  TB_CLIENT_REQUEST . " (name, email, domain, stamp) VALUES (?,?,?, CURRENT_TIMESTAMP)";
			$query = nut_db::query( $sql, $list);
			return array("requested" => 1);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// create non blocked, non confirmed account
	public function createSiteUser($firstName, $lastName, $email, $domain, $pass)
	{
		try
		{
			$list  = array($firstName, $lastName, $email, $domain, $pass);

			$sql   = 'SELECT * FROM ' . TB_SITEUSERS . ' WHERE email = ? ';
			$query = nut_db::query($sql, array($email));

			if($query->num_rows() == 0)
			{
				$sql   = "INSERT INTO " .  TB_SITEUSERS . " (first_name, last_name, email, domain, password, blocked, confirmed) VALUES (?,?,?,?,?, 0, 0)";
				$query = nut_db::query( $sql, $list);
			}
			else 
			{
				throw new Api_exception("This email is already used.", Api_exception::$SITEUSER_SIGNUP_SAME_EMAIL);
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function confirmSiteUserCreation($id)
	{
		try
		{
			$sql   = " UPDATE " . TB_SITEUSERS . " SET confirmed = 1 WHERE id = ? ";
			$query = nut_db::query($sql, $id);
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function isSiteUserRegistered($email, $pass)
	{
		try
		{
			$sql = "SELECT id,
						   first_name,
						   last_name,
						   email,
						   domain,
						   blocked,
						   confirmed
				   FROM ". TB_SITEUSERS ." 
				   WHERE email = ? AND password = ?";
			$query = nut_db::query($sql, array($email, $pass));

			$data = $query->num_rows() == 1 ? $query->row_array() : array();
			if($query->num_rows() == 1)
			{
				if(intval($data["blocked"]) == 1)
				{
					throw new Api_exception("Your account is blocked.", Api_exception::$SITEUSER_BLOCKED, array("email" => $data["email"]));
				}
				if(intval($data["confirmed"]) == 0)
				{
					throw new Api_exception("Your account is not verified.", Api_exception::$SITEUSER_EMAIL_NOT_CONFIRMED, array("email" => $data["email"]));
				}
			}
			else
			{
				throw new Api_exception("Invalid username or password.", Api_exception::$SITEUSER_INVALID_USERNAME_OR_PASSWORD);
			}
			
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getSiteUserDataByEmail($email)
	{
		try
		{
			$sql = "SELECT id,
						   first_name,
						   last_name,
						   email,
						   -- password,
						   domain,
						   blocked,
						   confirmed
				   FROM ". TB_SITEUSERS ." 
				   WHERE email = ? ";
			$query = nut_db::query($sql, array($email));

			$data = $query->num_rows() == 1 ? $query->row_array() : array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function updateSiteUserData($id, $first_name, $last_name, $email, $domain, $blocked)
	{
		try
		{
			$setParams = array();

			$sets = array();
			if ($first_name) {
				$setParams[] = " first_name = ? ";
				$sets[] = $first_name;
			}
			if ($last_name) {
				$setParams[] = " last_name = ? ";
				$sets[] = $last_name;
			}
			if ($email) {
				$setParams[] = " email = ? ";
				$sets[] = $email;
			}
			if ($domain) {
				$setParams[] = " domain = ? ";
				$sets[] = $domain;
			}
			if (!is_null($blocked)) {
				$setParams[] = " blocked = ? ";
				$sets[] = $blocked;
			}

			$sets[] = $id;
			$sql = "UPDATE " . TB_SITEUSERS .
				   " SET " . implode("," , $setParams) .
				   " WHERE id = ?";
			$query = nut_db::query($sql, $sets);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	public function updateSiteUserPassword($id, $newPassword)
	{
		try
		{
			$sql = "UPDATE ". TB_SITEUSERS ." SET password = ? WHERE id = ?";
			$query = nut_db::query($sql, array($newPassword, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function setSiteUserBlockState($id, $state)
	{
		try
		{
			$sql = "UPDATE ". TB_SITEUSERS ." SET blocked = ? WHERE id = ?";
			$query = nut_db::query($sql, array($state, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	/* SITE USER SPECIFIC MODEL END */

	public function getSiteUserWidgets($siteUserId, $langid)
	{
		try
		{
			$sql = "SELECT  W.id, W.name, W.widget_type FROM ". TB_WIDGETS . " AS W WHERE siteuser_id = ?";
			
			$query = nut_db::query($sql, array($siteUserId));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$response = array();
			foreach($data as $key => $val) {
				$response[$val['id']] = $val;
			}

			return $response;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	//get site user game widgets with full widget info
	public function getGameWidgets($siteUserId, $langid)
	{
		try
		{
			$siteUserId = intval($siteUserId);
			$sql = "SELECT  W.id,
							W.widget_type as widget_type_id,
							W.name,
						    W.css,
						    SZ.id as size_id,
						    SZ.label as size_label,
						    SZ.height,
						    SZ.width,
						    W.create_stamp,
						    W.update_stamp,
						    LANG.id as lang_id,
						    LANG.name as lang_name,
						    L.id as league_id,
						    L.name as league_name,
						    L.logo as league_logo,
						    GetTranslation(". $langid .", L.id  ,". chili_translate::$LEAGUES .") AS league_tname,
						    L.sport_id,
						    SP.name as sport_name,
						    GetTranslation(". $langid .", SP.id ,". chili_translate::$SPORTS .") AS  sport_tname
					FROM ". TB_WIDGETS 				 ."  AS W
					INNER JOIN " . TB_SIZE           . " as SZ ON SZ.id = W.size 
					INNER JOIN " . TB_LANGUAGES      . " as LANG   ON LANG.id = W.language_id 
					LEFT  JOIN " . TB_WIDGET_LEAGUES . " AS WL ON WL.widget_id = W.id
					INNER JOIN " . TB_LEAGUES        . " AS L ON L.id = WL.league_id
					INNER JOIN " . TB_SPORTS         . " AS SP  ON SP.id = L.sport_id
					WHERE  W.siteuser_id = ? AND W.widget_type = 1  ORDER BY W.update_stamp DESC, L.sport_id DESC";

			$query = nut_db::query($sql, array($siteUserId));
			$data =  $query->num_rows() > 0 ? $query->result_array() : array();

			$widgetsList = array();
			foreach ($data as $key => $row) {
				$widgetId = $row["id"];

				if(!isset($widgetsList[$widgetId]))
				{
					$widgetData = array();
					$widgetData["id"] 				= $row["id"];
					$widgetData["widget_type_id"]   = $row["widget_type_id"];
					$widgetData["name"]             = $row["name"];
					$widgetData["lang_id"]     	    = $row["lang_id"];
					$widgetData["lang_name"]        = $row["lang_name"];
					$widgetData["size_id"]          = $row["size_id"];
					$widgetData["size_label"]       = $row["size_label"];
					$widgetData["height"]           = $row["height"];
					$widgetData["width"]            = $row["width"];
					$widgetData["create_stamp"]     = $row["create_stamp"];
					$widgetData["update_stamp"]     = $row["update_stamp"];
					$widgetData["leagues"]          = array();
					$widgetsList[$widgetId] 		= $widgetData;
				}

				$leagueData = array();
				$leagueData["id"]          = $row["league_id"];
				$leagueData["name"]        = $row["league_name"];
				$leagueData["logo"]        = $row["league_logo"];

				$leagueData["tname"]       = $row["league_tname"];
				$leagueData["sport_id"]    = $row["sport_id"];
				$leagueData["sport_name"]  = $row["sport_name"];
				$leagueData["sport_tname"] = $row["sport_tname"];
			
				$widgetsList[$widgetId]["leagues"][] = $leagueData;
			}
			return $widgetsList;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//get site user game widget with full widget info
	public function getGameWidgetData($widgetId, $langId)
	{
		try
		{
			$widgetId = intval($widgetId);
			$sql = "SELECT  W.id,
							W.widget_type as widget_type_id,
							W.name,
						    W.css,
						    W.bookmaker_id,
						    SZ.id as size_id,
						    SZ.label as size_label,
						    SZ.height,
						    SZ.width,
						    W.create_stamp,
						    W.update_stamp,
						    LANG.id as lang_id,
						    LANG.name as lang_name,
						    L.id as league_id,
						    L.name as league_name,
						    L.logo as league_logo,
						    GetTranslation(". $langId .", L.id  ,". chili_translate::$LEAGUES .") AS league_tname,
						    L.sport_id,
						    SP.name as sport_name,
						    GetTranslation(". $langId .", SP.id ,". chili_translate::$SPORTS .") AS  sport_tname
					FROM ". TB_WIDGETS 				 ."  AS W
					INNER JOIN " . TB_SIZE           . " as SZ ON SZ.id = W.size 
					INNER JOIN " . TB_LANGUAGES      . " as LANG   ON LANG.id = W.language_id 
					LEFT  JOIN " . TB_WIDGET_LEAGUES . " AS WL ON WL.widget_id = W.id
					INNER JOIN " . TB_LEAGUES        . " AS L ON L.id = WL.league_id
					INNER JOIN " . TB_SPORTS         . " AS SP  ON SP.id = L.sport_id
					WHERE  W.id = ? ORDER BY L.sport_id DESC";

			$query = nut_db::query($sql, array($widgetId));
			$data =  $query->num_rows() > 0 ? $query->result_array() : array();

			$widgetData = array();
			
			foreach ($data as $key => $row) {
				$widgetId = $row["id"];

				if(!isset($widgetData["id"]))
				{
					$widgetData["id"] 				= $row["id"];
					$widgetData["widget_type_id"]   = $row["widget_type_id"];
					$widgetData["name"]             = $row["name"];
					$widgetData["bookmaker_id"]     = $row["bookmaker_id"];
 					$widgetData["lang_id"]     	    = $row["lang_id"];
					$widgetData["lang_name"]        = $row["lang_name"];
					$widgetData["size_id"]          = $row["size_id"];
					$widgetData["size_label"]       = $row["size_label"];
					$widgetData["height"]           = $row["height"];
					$widgetData["width"]            = $row["width"];
					$widgetData["css"]              = $row["css"];
					$widgetData["create_stamp"]     = $row["create_stamp"];
					$widgetData["update_stamp"]     = $row["update_stamp"];
					$widgetData["leagues"]          = array();
				}

				$leagueData = array();
				$leagueData["id"]          = $row["league_id"];
				$leagueData["name"]        = $row["league_name"];
				$leagueData["logo"]        = $row["league_logo"];

				$leagueData["tname"]       = $row["league_tname"];
				$leagueData["sport_id"]    = $row["sport_id"];
				$leagueData["sport_name"]  = $row["sport_name"];
				$leagueData["sport_tname"] = $row["sport_tname"];
			
				$widgetData["leagues"][] = $leagueData;
			}
			return $widgetData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllSiteUsers($aux_info)
	{
		try
		{
			// $sql = "SELECT  SU.id,
			// 				SU.name,
			// 				SU.username,
			// 			    SU.blocked,
			// 			    SUP.domain,
			// 			    SUP.email,
			// 			    SUP.register_date
			// 				FROM ". TB_SITEUSERS ." AS SU INNER JOIN " . TB_SITEUSERSPROFILES ." AS SUP ON SU.id = SUP.siteuser_id";

			$sql = "SELECT  * FROM ". TB_SITEUSERS;
			
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$this->aux_appender($data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function insertSiteUsers($siteuser)
	{
		try
		{
			$pass = md5($siteuser['password']);
			nut_db::transactionStart("insertSiteUsers");
			
			$sql = "INSERT INTO ".TB_SITEUSERS." (name, username, password, blocked)	VALUES (?, ?, ?, ?);";

			$query = nut_db::query($sql, array($siteuser['name'], $siteuser['username'], $pass, $siteuser['blocked']));
			
			$id = nut_db::getLastID(TB_SITEUSERS, 'id');


			$sql = "INSERT INTO ".TB_SITEUSERSPROFILES." (siteuser_id, domain, email) VALUES (?, ?, ?);";

			$query = nut_db::query($sql, array($id, $siteuser['domain'], $siteuser['email']));
			
			nut_db::transactionCommit("insertSiteUsers");

			return $id;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertSiteUsers");
			throw $e;
		}
	}

	public function updateSiteUsers($siteusers)
	{
		try
		{
			nut_db::transactionStart("updateSiteUsers");
			$values = nut_utils::sanitize_keys($siteusers, array('id', 'name', 'username', 'password', 'blocked'));

			nut_db::update_batch(TB_SITEUSERS, $values, 'id');

			if(in_array('domain', $siteusers[0]) or in_array('email', $siteusers[0]))
			{
			
				array_walk($siteusers, 'updateSiteUserId');

				$values = nut_utils::sanitize_keys($siteusers, array('domain', 'siteuser_id', 'email'));
				nut_db::update_batch(TB_SITEUSERSPROFILES, $values, 'siteuser_id');
			}
			//$this->aux_update($siteusers);

			nut_db::transactionCommit("updateSiteUsers");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateSiteUsers");
			throw $e;
		}
	}

	//get all widget types like - game widget, top user widget etc ..
	public function getAllWidgetTypes()
	{
		try
		{
			$sql = "SELECT  id, name FROM ". TB_WIDGET_TYPES;
			
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			//$this->aux_appender($return, $langid, NULL, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//avar
	public function createGameWidget($siteUserId, $name, $sizeId, $langId, $leagues, $bookmakerId = 1)
	{
		try
		{
			nut_db::transactionStart("createGameWidget");
			
			$sql = "INSERT INTO " . TB_WIDGETS . " 
					(name, siteuser_id, widget_type, size, language_id, bookmaker_id, create_stamp, update_stamp)
					VALUES(?, ?, " . WIDGET_GAME . " , ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";

			nut_db::query($sql, array($name, $siteUserId, $sizeId, $langId, $bookmakerId));
			$widgetId = nut_db::getLastID(TB_WIDGETS, "id");

			foreach ($leagues as $key => $leagueId) {
				$sql = "INSERT INTO " . TB_WIDGET_LEAGUES . " 
						(widget_id, league_id, stamp) VALUES (?,?, CURRENT_TIMESTAMP)";

				nut_db::query($sql, array($widgetId, $leagueId));
			}

			nut_db::transactionCommit("createGameWidget");
			return 	$widgetId;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("createGameWidget");
			throw $e;
		}
	}	

	public function updateWidgetDesign($siteUserId, $widgetId, $css)
	{
		try
		{
			nut_db::transactionStart("updateWidgetDesign");
			
			$sql = "UPDATE  " . TB_WIDGETS . " 
					SET css = ?, update_stamp = CURRENT_TIMESTAMP WHERE id = ? AND siteuser_id = ?";
					
			nut_db::query($sql, array($css, $widgetId, $siteUserId));

			nut_db::transactionCommit("updateWidgetDesign");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateWidgetDesign");
			throw $e;
		}
	}

	//avar
	public function updateGameWidgetConfigs($siteUserId, $widgetId, $name, $sizeId, $langId, $leagues)
	{
		try
		{
			nut_db::transactionStart("updateGameWidgetConfigs");
			
			$sql = "UPDATE  " . TB_WIDGETS . " 
					SET name = ?, size = ?, language_id = ?, update_stamp = CURRENT_TIMESTAMP WHERE id = ? AND siteuser_id = ?";
					

			nut_db::query($sql, array($name, $sizeId, $langId, $widgetId, $siteUserId));
			
			$sql = "DELETE FROM " . TB_WIDGET_LEAGUES . " WHERE widget_id = ?";
			nut_db::query($sql, array($widgetId));

			foreach ($leagues as $key => $leagueId) {
				$sql = "INSERT INTO " . TB_WIDGET_LEAGUES . " 
						(widget_id, league_id, stamp) VALUES (?,?, CURRENT_TIMESTAMP)";

				nut_db::query($sql, array($widgetId, $leagueId));
			}

			nut_db::transactionCommit("updateGameWidgetConfigs");
			return 	$widgetId;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateGameWidgetConfigs");
			throw $e;
		}
	}

	// remove game widget
	// avar 10.18.2013
	public function removeGameWidget($id, $siteuserId)
	{
		try
		{
			nut_db::transactionStart("removeGameWidget");
			
			$sql = "DELETE FROM ". TB_WIDGETS ." WHERE id=? AND siteuser_id = ?";
			$query = nut_db::query($sql, array($id, $siteuserId));

			$sql = "DELETE FROM ". TB_WIDGET_LEAGUES ." WHERE widget_id=?";
			$query = nut_db::query($sql, array($id));

			nut_db::transactionCommit("removeGameWidget");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeGameWidget");
			throw $e;
		}
	}	

	public function getWidgetsCountBySite($site_ids)
	{
		try
		{
			$values = implode(',', array_fill(0, count($site_ids), "?"));
			$sql = "SELECT siteuser_id, count(*) as count" .
				   " FROM ". TB_WIDGETS . 
				   " WHERE siteuser_id in (".$values.") ".
				   " GROUP BY siteuser_id" ;

			$query = nut_db::query($sql, $site_ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$data =  setFieldAsKey('siteuser_id', $data);
			return $data;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("getWidgetCountBySite");
			throw $e;
		}
	}

	public function getUsersCountBySite($site_ids)
	{
		try
		{
			$values = implode(',', array_fill(0, count($site_ids), "?"));
			$sql = "SELECT site_id, count(*) as count" .
				   " FROM ". TB_USERSBALANCES . 
				   " WHERE site_id in (".$values.") ".
				   " GROUP BY site_id" ;

			$query = nut_db::query($sql, $site_ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$data =  setFieldAsKey('site_id', $data);
			return $data;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("getWidgetCountBySite");
			throw $e;
		}
	}

	public function getBetsCountBySite($site_ids)
	{
		try
		{
			$values = implode(',', array_fill(0, count($site_ids), "?"));
			$sql = "SELECT site_id, count(*) as count" .
				   " FROM ". TB_BETS . " AS B LEFT JOIN " . TB_CHALLENGES . " AS CH ON (CH.publisher_bet_id = B.id OR CH.opponent_bet_id = B.id)".
				   " WHERE site_id in (".$values.") AND ISNULL(CH.publisher_bet_id) AND ISNULL(CH.opponent_bet_id)".
				   " GROUP BY site_id" ;

			$query = nut_db::query($sql, $site_ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$data =  setFieldAsKey('site_id', $data);
			return $data;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("getWidgetCountBySite");
			throw $e;
		}
	}

	public function getChallengesCountBySite($site_ids)
	{
		try
		{
			$values = implode(',', array_fill(0, count($site_ids), "?"));
			$sql = "SELECT B.site_id, count(*) as count" .
				   " FROM ". TB_CHALLENGES . " AS CH INNER JOIN ". TB_BETS ." AS B ON CH.publisher_bet_id = B.id " . 
				   " WHERE B.site_id in (".$values.")".
				   " GROUP BY B.site_id" ;

			$query = nut_db::query($sql, $site_ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$data =  setFieldAsKey('site_id', $data);
			return $data;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("getWidgetCountBySite");
			throw $e;
		}
	}	

	private function aux_appender(&$data, $aux_info)
	{

		$ids = array();
		foreach($data as $key => $val) {
			$ids[] = $val['id'];
		}

		foreach($data as $ind => $row) {
			if(!array_key_exists("aux_info", $data[$ind])) {
				$data[$ind]["aux_info"] = array();
			}
		}

		if(is_string($aux_info))
		{
			$this->aux_append($ids, $data, $aux_info);
		}

		if(is_array($aux_info) && count($aux_info) > 0)
		{
			foreach($aux_info as $aux)
			{
				$this->aux_append($ids, $data, $aux);
			}
		}		
	}

	private function aux_append(&$ids, &$data, $aux_name)
	{
		switch($aux_name)
		{
			case 'widgetsCount':
				$counts = $this->getWidgetsCountBySite($ids);
				foreach($data as $key => $val) {
					$data[$key]['widgets_count'] = array_key_exists($val['id'], $counts) ? $counts[$val['id']]['count'] : null;
				}

				break;	
			case 'usersCount':
				$counts = $this->getUsersCountBySite($ids);
				
				foreach($data as $key => $val) {
					$data[$key]['users_count'] = array_key_exists($val['id'], $counts) ? $counts[$val['id']]['count'] : null;
				}

				break;
			case 'betsCount':
				$counts = $this->getBetsCountBySite($ids);
				
				foreach($data as $key => $val) {
					$data[$key]['bets_count'] = array_key_exists($val['id'], $counts) ? $counts[$val['id']]['count'] : null;
				}

				break;
			case 'challengesCount':
				$counts = $this->getChallengesCountBySite($ids);
				
				foreach($data as $key => $val) {
					$data[$key]['challenges_count'] = array_key_exists($val['id'], $counts) ? $counts[$val['id']]['count'] : null;
				}

				break;					
		}		
	}


}


function updateSiteUserId(&$arr, $key){
	$id = $arr['id'];
	unset($arr['id']);
	$arr['siteuser_id'] = $id;
}
