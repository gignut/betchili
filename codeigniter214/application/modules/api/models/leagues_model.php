<?php

use Respect\Validation\Validator as validator;

class leagues_model extends CI_Model {

	private static $AUX_TRANSLATION = "translations";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getAllLeagues($langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id,
							name,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$LEAGUES.") AS tname,
						    logo,
						    priority,
						    sport_id,
						    country_id
							FROM ". TB_LEAGUES . " ORDER BY sport_id DESC, priority ASC ";
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$this->aux_append('', $data, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function getAllLeaguesBySportId($skid, $langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$LEAGUES.") AS tname,
						    logo,
							priority,
						    sport_id,
						    country_id					    
							FROM ". TB_LEAGUES
							." WHERE sport_id = ? " ;
			
			$query = nut_db::query($sql, array($skid));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$this->aux_append('', $data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}	

	public function getAllLeaguesBySportIds($skids, $langid, $aux_info)
	{
		try
		{

			$values = implode(',', array_fill(0, count($skids), "?"));
			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$LEAGUES.") AS tname,
						    logo,
							priority,
						    sport_id,
						    country_id					    
							FROM ". TB_LEAGUES
							." WHERE sport_id in (".$values.") ";
			
			$query = nut_db::query($sql, $skids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$this->aux_append('', $data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function getLeagueDataByIds($ids, $langid, $aux_info)
	{
		try
		{
			$qArray = array_fill(0, count($ids), "?");

			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .", id ,". chili_translate::$LEAGUES.") AS tname,
						    logo,
						    priority,
						    sport_id,
						    country_id
							FROM ". TB_LEAGUES ."
							WHERE id in (" . implode(",", $qArray) . " )";
						
			$query = nut_db::query($sql, $ids);
		
			$data =  $query->num_rows() > 0 ? $query->result_array(): NULL;

			$this->aux_append("", $data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function getLeagueById($id, $langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
						    name,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$LEAGUES.") AS tname,
						    logo,
							priority,
						    sport_id,
						    country_id					    
							FROM ". TB_LEAGUES
							." WHERE id = ? " ;
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): NULL;

			$this->aux_append($id, $data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function insertLeagues($leagues)
	{
		try
		{
			nut_db::transactionStart("insertLeagues");
			$values = nut_utils::sanitize_keys($leagues, array('name', 'logo', 'country_id', 'sport_id', 'priority'), TRUE, NULL);
	
			nut_db::insert_batch(TB_LEAGUES, $values);
			$ids = nut_db::getLastID(TB_LEAGUES, 'id', count($leagues));

			$ids = is_array($ids) ? $ids : array($ids);	
			
			foreach ($ids as $k => $v)
			{
				$id = $v;
				$aux_info = element('aux_info', $leagues[$k]);

				$translations = $aux_info ? element('translations', $aux_info) : FALSE;
				
				if ($translations)
				{
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						chili_translate::addTranslation($langId, $id, chili_translate::$LEAGUES, $value);
					}
				}
			}

			nut_db::transactionCommit("insertLeagues");
			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertLeagues");
			throw $e;
		}
	}

	public function updateLeagues($leagues)
	{
		try
		{
			nut_db::transactionStart("updateLeagues");
			$values = nut_utils::sanitize_keys($leagues, array('id', 'name', 'logo', 'country_id', 'sport_id', 'priority'));			
			nut_db::update_batch(TB_LEAGUES, $values, 'id');

			foreach ($leagues as $ldata)
			{
				// update translations
				$id = $ldata['id'];
				$aux_info = element('aux_info', $ldata);
				$translations = $aux_info ? element('translations', $aux_info) : FALSE;
				if ($translations)
				{
					$data = array();
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						$arr    = chili_translate::getTranslation($langId, $id, chili_translate::$LEAGUES);
						if (!is_null($arr))
						{
							$data[] = array('lang' => $langId , 'id' => $id , 'type' => chili_translate::$LEAGUES, 'text' => $value );
						}
						else {
							chili_translate::addTranslation($langId, $id, chili_translate::$LEAGUES, $value);
						}
					}
					chili_translate::updateTranslations($data);
				}				
			}

			nut_db::transactionCommit("updateLeagues");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateLeagues");
			throw $e;
		}
	}

	public function removeLeague($id)
	{
		try
		{
			nut_db::transactionStart("removeLeague");
			$flagFileName = $this->getLogo($id);
			$sql = "DELETE FROM " . TB_LEAGUES  .  " WHERE id = ?";
			$query = nut_db::query($sql, $id);	

			chili_translate::deleteAllTranslations(chili_translate::$LEAGUES, $id);

			$imageFullPath = asset_model::getAssetFolder("leagues") . $flagFileName;
			if($flagFileName)
			{
					asset_model::removeAsset($imageFullPath);
			}
			nut_db::transactionCommit("removeLeague");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeLeague");
			throw $e;
		}
	}	

	private function aux_append($id, &$data, $aux_info)
	{
		if (validator::arr()->each(validator::arr())->validate($data))
		{
			foreach($data as $key => $value) {
				self::aux_appender($value['id'], $data[$key], $aux_info);
			}
		}
		elseif (!is_null($data))
		{
			self::aux_appender($id, $data, $aux_info);
		}
	}


	private function aux_appender($id, &$data, $aux_info)
	{
		if(is_string($aux_info))
		{
			$data["aux_info"] = array() ;
			switch($aux_info)
			{
				
				case self::$AUX_TRANSLATION:
					$data["aux_info"][self::$AUX_TRANSLATION] = chili_translate::getFullTranslations($id, chili_translate::$LEAGUES);
					break;
				
			}
		}

		if(is_array($aux_info) && count($aux_info) > 0)
		{
			$data["aux_info"] = array() ;

			foreach($aux_info as $aux)
			{
				switch($aux)
				{
					case self::$AUX_TRANSLATION:
						$data["aux_info"][self::$AUX_TRANSLATION] = chili_translate::getFullTranslations($id, chili_translate::$LEAGUES);
					break;
				}
			}
		}		
	}

	public  function getLogo($id)
	{
		try
		{
			$sql = "SELECT  logo FROM ". TB_LEAGUES ." WHERE id = ? ";
			$query = nut_db::query($sql, $id);
			$data =  $query->num_rows() > 0 ? $query->row_array(0): NULL;
			$logo = !is_null($data) ? $data['logo'] : null;
			return $logo;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public  function setLogo($id,$filename)
	{
		try
		{
			$filename = trim($filename);
			$sql = "UPDATE " . TB_LEAGUES . " SET logo = ? WHERE id = ?";
			$query = nut_db::query($sql, array($filename, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}