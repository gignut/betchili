<?php

use Respect\Validation\Validator as validator;

class games_model extends CI_Model {

	private static $AUX_TRANSLATION  = "translations";
	private static $AUX_PARTICIPANT  = "participants";
	private static $AUX_EVENT	     = "events";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
		$this->load->model(MODULE_API_FOLDER . "/events_model");
		$this->load->model(MODULE_API_FOLDER . "/participants_model");
	}

	public function getGameDataById($id, $langid, $aux_info, $bookmaker_id)
	{
		try
		{
			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .", id, ". chili_translate::$GAMES.") AS tname,
						    UNIX_TIMESTAMP(start_date) as start_date,
						    UNIX_TIMESTAMP(start_date) as start_utc,
						    competition_id,
						    sport_id,
						    calculated,
						    priority,
						    result_text,
						    result_returned,
						    blocked
							FROM ". TB_GAMES ." 
							WHERE id = ? ";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): NULL;

			if ($data) {
				$return = array($data);	
				$this->aux_appender($return, $langid, $bookmaker_id, $aux_info);
				$data = $return[0];
			}
			
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// used in widget
	public function getTeamsPastGames($id, $langid)
	{
		try
		{
			$sql = "SELECT GP.participant_id FROM ". TB_GAMEPARTICIPANTS ." AS GP WHERE GP.game_id = ?";

			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$retData = array();
			foreach($data as $row) {
				$sql = "SELECT 	G.id,
							UNIX_TIMESTAMP(G.start_date) as game_date,
							GetTranslation(". $langid .", id, ". chili_translate::$GAMES.") AS tname,
							G.result_text, 
							GP.order
						FROM ". TB_GAMES ." AS G 
						INNER JOIN ". TB_GAMEPARTICIPANTS ." AS GP ON G.id = GP.game_id AND GP.participant_id = ? AND G.calculated = 1
						ORDER BY G.start_date ASC LIMIT 5";

				$query = nut_db::query($sql, array($row['participant_id']));
				$games =  $query->num_rows() > 0 ? $query->result_array(): array();
				$retData[$row['participant_id']] = $games;
			}

			return $retData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getGameDataByIds($ids, $langid, $aux_info, $bookmakerId = 1)
	{
		try
		{
			$values = implode(',', array_fill(0, count($ids), "?"));
			$sql = "SELECT  G.id, 
							G.name,
						    GetTranslation(". $langid .", G.id, ". chili_translate::$GAMES .") AS tname,
						    UNIX_TIMESTAMP(G.start_date) as start_date,
						    G.competition_id,
						    G.sport_id,
						    G.priority,
						    G.result_text
							FROM ". TB_GAMES ." AS G 
							WHERE G.id in (".$values.") ORDER BY G.start_date ASC , `priority` DESC ";
			
			$query = nut_db::query($sql, $ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$this->aux_appender($data, $langid, $bookmakerId, $aux_info);
			
			return setFieldAsKey("id", $data);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// 2014 MARCH 29
	// avar
	public function getWidgetGames($widgetId, $userId, $leagueId, $from, $to, $limit, $offset, $langId, $bookmakerId = 1, $utz)
	{
		try
		{
			$sql = "SELECT DISTINCT G.id,
							G.name,
							GetTranslation(". $langId .", G.id, ". chili_translate::$GAMES.") AS tname,
							UNIX_TIMESTAMP(G.start_date) as start_date,
						    G.priority,
						    G.status,
						    G.result_text,
						    G.result_returned,
						    G.competition_id,
						    G.sport_id
					FROM " . TB_WIDGET_LEAGUES . " as WL
					INNER JOIN  " . TB_COMPETITIONS . " AS CMP ON CMP.league_id = WL.league_id
					INNER JOIN  " . TB_GAMES . " AS G ON G.competition_id = CMP.id  
					LEFT JOIN  " . TB_EVENTS . " AS E ON E.game_id = G.id  
					WHERE WL.widget_id = ? 
					AND E.id IS NOT NULL
					AND CMP.blocked = 0
					AND G.blocked = 0 ";
					

			$params = array($widgetId);

			if($leagueId)
			{
				$sql      = $sql . " AND WL.league_id = ? ";
				$params[] = $leagueId;
			}
		
			$sql      = $sql . " AND UNIX_TIMESTAMP(G.start_date) >= ? ";
			$params[] = $from;

			if($to)
			{
				$sql      = $sql . " AND UNIX_TIMESTAMP(G.start_date) <= ? ";
				$params[] = $to;
			}

			if($limit)
			{
				$sql = $sql . " LIMIT " . intval($limit); 
			}

			// if($offset)
			// {
			// 	$sql = $sql . " OFFSET " . intval($offset); 
			// }
			$query = nut_db::query($sql, $params);
			$data = $query->num_rows() > 0 ? $query->result_array() : array();

			if (count($data) == 0) {
				$near = $this->getNearDateGames($widgetId, $userId, $leagueId, $from, $to, $limit, $offset, $langId, $bookmakerId, $utz);
				return $near;
			}

			$this->aux_appender($data, $langId, $bookmakerId, array("participants", "events"));

			$cleanData = array();
			foreach ($data as $key => $value) {
				if (!empty($value['aux_info']['events']['base_events'])) {
					$cleanData[] = $value;
				}
			}

			$newData = $this->groupByCompetition($data);

			return $newData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function getNearDateGames($widgetId, $userId, $leagueId, $from, $to, $limit, $offset, $langId, $bookmakerId, $utz)
	{
		$sql = "SELECT  G.id,
						UNIX_TIMESTAMP(G.start_date) as start_date
					FROM " . TB_WIDGET_LEAGUES . " as WL
					INNER JOIN  " . TB_COMPETITIONS . " AS CMP ON CMP.league_id = WL.league_id
					INNER JOIN  " . TB_GAMES . " AS G ON G.competition_id = CMP.id  
					LEFT  JOIN  " . TB_EVENTS . " AS E ON E.game_id = G.id
					WHERE WL.widget_id = ? 
					AND E.id IS NOT NULL
					AND CMP.blocked = 0
					AND G.blocked = 0 ";

		$params = array($widgetId);

		if($leagueId)
		{
				$sql      = $sql . " AND WL.league_id = ? ";
				$params[] = $leagueId;
		}

		$sql      = $sql . " AND UNIX_TIMESTAMP(G.start_date) >= ? ";
		$params[] = $from;

		// IMPORTANT !!!
		$sql = $sql . " ORDER BY G.start_date ASC LIMIT 1";

		$query = nut_db::query($sql, $params);
		$data = $query->num_rows() > 0 ? $query->row_array() : array();

		if (count($data) > 0) {
			$nearStartDate = $data['start_date'];
			$nearFrom = nut_utils::adjust_timestamp_to_tz($nearStartDate, $utz, TRUE);
			$nearTo   = nut_utils::adjust_timestamp_to_tz($nearStartDate, $utz, FALSE, TRUE);
			return $this->getWidgetGames($widgetId, $userId, $leagueId, $nearFrom, $nearTo, $limit, $offset, $langId, $bookmakerId, $utz);
		} 
		else 
		{
			return $data;
		}
				
	}

	private function groupByCompetition($games_list)
	{
		$newData = array();
		foreach ($games_list as $row => $val) {
			$cid = $val['competition_id'];
			if (array_key_exists($cid, $newData)) {
				$newData[$cid][] = $val;
			} else {
				$newData[$cid] = array();
				$newData[$cid][] = $val;
			}
		}

		return $newData;
	}

	public function getAllGamesBy($id, $sport_id, $competitions, $langid, $aux_info, $from_date, $to_date, 
								  $limit, $offset, $user_id, $participant_id, $state, $result, $bookmaker_id, $count=0)
	{
		try
		{
			// search by ID
			if (!is_null($id) && $id != '') {
				$data = $this->getGameDataById($id, $langid, $aux_info, $bookmaker_id);
				$data = $data ? array($data) : array();
				return $data;
			}

			// search by filters
			$params = array();
			$where_sql = '';
			$from_sql  = '';
			$limit_sql = '';
			$offset_sql = '';
			$order = "ASC";
			$currentTime = time();

			$cids = implode(',', $competitions);
			$params[] = $sport_id;

			if(!is_null($participant_id))
			{
				$from_sql .= " INNER JOIN (SELECT game_id FROM ". TB_GAMEPARTICIPANTS ." AS GPO WHERE GPO.participant_id = ? ) AS GP ON GP.game_id = G.id ";
				$params[] = $participant_id;
			}
			
			if(!is_null($from_date))
			{
				$where_sql .= " AND UNIX_TIMESTAMP(G.start_date) >= UNIX_TIMESTAMP(?)";
				$params[] = $from_date;
				if(time($from_date) >= $currentTime) {
					$order = "ASC";
				}
				else {
					$order = "DESC";
				}
			}
			if(!is_null($to_date))
			{
				$where_sql .= " AND UNIX_TIMESTAMP(G.start_date) <= UNIX_TIMESTAMP(?)";
				$params[] = $to_date;
			}
			
			if(!is_null($state))
			{
				$where_sql .= " AND G.blocked = " . $state;
				$params[] = $state;
			}
			if(!is_null($result))
			{
				if($result == '1') {
					$where_sql .= " AND G.result_text IS NOT NULL";
				} else if ($result == '0') {
					$where_sql .= " AND G.result_text IS NULL";
				}
			}
			if(!is_null($limit))
			{
				$limit_sql = " LIMIT ?";
				$params[] = $limit;
			}
			if(!is_null($offset))
			{
				$offset_sql = " OFFSET ?";
				$params[] = $offset;
			}

			$select = "		G.id,
							G.name,
						    GetTranslation(". $langid .", G.id, ". chili_translate::$GAMES.") AS tname,
						    UNIX_TIMESTAMP(G.start_date) as start_date,
						    G.competition_id,
						    G.sport_id,
						    G.calculated,
						    G.priority,
						    G.blocked,
						    G.result_text,
						    G.result_returned,
						    G.status";
			if($count) {
				$select = " count(*) as count ";
			}

			$sql = "SELECT  ".$select."
							FROM ". TB_GAMES ." AS G 
								 " .$from_sql. "
							WHERE G.competition_id in (".$cids.") AND G.sport_id = ? " . $where_sql 
							. " ORDER BY G.start_date ". $order . $limit_sql . $offset_sql;
			
			$query = nut_db::query($sql, $params);

			if ($count) {
				$data = $query->row_array();
			} else {
				$data = $query->num_rows() > 0 ? $query->result_array() : array();
		   		if (count($data) > 0) {
					$this->aux_appender($data, $langid, $bookmaker_id, $aux_info);
				}
			}

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private  function getEventTemplateById($id, $list)
	{
		foreach($list as $item) {
			if($item['id'] == $id) {
				return $item;
			}
		}
		return NULL;
	}

	public function setGameResultText($game_id, $results, $sport_id)
	{
		try{
			$res_text = '';
			if ($sport_id == 1) { //football
				$res_text = $results[1]." : ".$results[3];
			}
			
			$sql = 	"UPDATE ".TB_GAMES." SET result_text='".$res_text."', result_stamp=CURRENT_TIMESTAMP WHERE id=?";

			nut_db::query($sql, array($game_id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function setGameCalculated($game_id)
	{
		try{
			$sql = "UPDATE ".TB_GAMES." SET calculated=1, result_returned=0 WHERE id=?";

			nut_db::query($sql, array($game_id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function setGameEventsResults($game_id, $results, $sport_id, $bk_id)
	{
		try
		{
			nut_db::transactionStart("setGameEventsResults");

			$game_events = $this->events_model->getAllEventsByGameId($game_id, $bk_id, array());
			$event_templates = $this->events_model->getAllEventTemplates($sport_id);

			$updated_events = array();

			foreach($game_events as $event) {
				$et = $this->getEventTemplateById($event['template_id'], $event_templates);
				$operation = $et['operation'];

				// replace {N} with corresponding event result
				while(preg_match('/(.*)\{(\d+)\}(.*)/', $operation)) {
		 			$operation = preg_replace("/(.*)\{(\d+)\}(.*)/", '$1$results[\'$2\']$3', $operation);
				}

	 			// replace {B} with event basis
	 			$operation = preg_replace("/(.*)\{B\}(.*)/", '$1$event[\'basis\']$2', $operation);

	 			// replace {T} with Total value: 10-nth event coefficient
	 			$tot = $game_events['10']['coefficient'];
	 			$operation = preg_replace("/(.*)\{T\}(.*)/", '$1$tot$2', $operation);

	 			// replace {D} with event data $result[x] - x aux_event template id
	 			$tid = $event['template_id'];
	 			$operation = preg_replace("/(.*)\{D\}(.*)/", '$1$results[$tid]$2', $operation);
				
				$event_value = eval('return '.$operation.';');

				$val = -1;
				switch($event_value) {
					case 0: $val = 1; break; // loose
					case 1: $val = 2; break; // won
				}

				$updated_event = array();
				$updated_event['result_status'] = $val;
				$updated_event['id'] = $event['id'];

				array_push($updated_events, $updated_event);
			}

			nut_db::update_batch(TB_EVENTS, $updated_events, 'id');

			// set result text and set calculated=0 (in-progress)
			$this->setGameResultText($game_id, $results, $sport_id);

			nut_db::transactionCommit("setGameEventsResults");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("setGameEventsResults");
			throw $e;
		}			
	}

	public function get_final_status($statuses, &$returns)
	{
		if (empty($statuses))
		{
			return 0; // return
		}
		if (in_array(-1, $statuses))
		{
			return -1; // waiting
		} 
		if (in_array(1, $statuses))
		{
			return 1; // loose
		}
		if (count(array_unique($statuses)) == 1 && $statuses[0] == 2)
		{
			return 2; // won
		}
		$new = array();

		for($i = 0; $i < count($statuses); ++$i)
		{
			if ($statuses[$i] == 0) // return
			{
				array_push($returns, $i);
			}
			else
			{
				$new[] = $statuses[$i];
			}
		}

		return $this->get_final_status($new, $returns);
	}

	public function updateUsersBalances($game_id)
	{

		$data = $this->bets_model->getAllBetsByGame($game_id);

		if(count($data) == 0) {
			return;
		}

		$bets_status = array();
		foreach($data as $row)
		{
			$bet_statuses = array();
			$bid = $row['id'];
			
			foreach($data[$bid]['event_chain'] as $event)
			{
				$bet_statuses[] = $event['event_result_status'];
			}

			$arr = array();

			$st = $this->get_final_status($bet_statuses, $arr);

			if ($st != -1)
			{
				$bets_status[$bid] = array($st, $arr);
			}
		}

		$res = array();
		$upbets = array();
		foreach($data as $row)
		{
			$id = $row['id'];
			if (!array_key_exists($id, $bets_status))
			{
				continue;
			}
			$bet_prev_st = $row['result_status'];
			$st  = $bets_status[$id][0]; 			// status
			$inds = $bets_status[$id][1];			// returned events indexes
			$userid =  $row['user_id'];
			$widget_id = $row['widget_id'];
			$won_coeff = $row['coefficient'];
			$event_chain = $row['event_chain'];
			$bet_type = $row['bet_type'];
			$shield_percent = $row['shield_percent'];
			$boost_multiplier = $row['boost_multiplier'];

			if (!array_key_exists($userid, $res))
			{
				$res[$userid] = array();
			}
			if (!array_key_exists($widget_id, $res[$userid]))
			{
				$res[$userid][$widget_id] = array();
			}
			$challenges_won_amount = 0;
			if (!is_null($row['pub_id'])) { // in challenge
				if (is_null($row['opp_id'])) { // challenge not accepted => should be returneds
					$st = 0;
				} else { // challenge accepted => won_amount += challenge_amount
					$challenges_won_amount = $row['amount'];
				}
			} 
			$varsh = ($bet_type == '1' || $bet_type == '3');
			$varbo = ($bet_type == '2' || $bet_type == '3');
			// if bet prevoius status is returned then decrement shield/boost count
			$shield_change = ($bet_prev_st == 0 && $varsh) ?  -1 : 0;
			$boost_change  = ($bet_prev_st == 0 && $varbo) ?  -1 : 0;
			// boost or cash back
			$cash_back = $varsh * ceil(($row['amount'] * $shield_percent)/100);
			$boost_mul = $varbo ? $boost_multiplier : 1;
			switch($st) {
				case 0: // return
					//$oldWonAmount = $row['result_status'] == 2 ? $row['won_amount'] + $challenges_won_amount : $row['won_amount'];
					$res[$userid][$widget_id][] = array("id" =>$id, "newWonAmount" =>  $row['amount']-$row['won_amount'], "shield_count" => $varsh,
																	      "boost_count" => $varbo);
					$upbets[] = array('id' => $id, 'won_amount' => $row['amount'], 'result_status' => $st);
					break;
				case 1: // loose
					$res[$userid][$widget_id][] = array("id" =>$id, "newWonAmount" => -$row['won_amount'] + $cash_back, "shield_count" => $shield_change, "boost_count" => $boost_change);
					$upbets[] = array('id' => $id, 'won_amount' => $cash_back, 'result_status' => $st);
					break;
				case 2: // won
					foreach($inds as $ind)
					{
						$won_coeff = $won_coeff / $event_chain[$ind]['event_coefficient'];
					}
					// challenge: only challenge amount should be transferred to winner
					$win_bet = is_null($row['pub_id']) ? ($won_coeff * $row['amount']) * $boost_mul : 2*$challenges_won_amount;
					$win_res = is_null($row['pub_id']) ? ($won_coeff * $row['amount']) * $boost_mul -$row['won_amount'] : -$row['won_amount'] + 2*$challenges_won_amount;
					$upbets[] = array('id' => $id, 'won_amount' => $win_bet, 'result_status' => $st);
					$res[$userid][$widget_id][] = array("id" =>$id, "newWonAmount" =>  $win_res,
														"shield_count" => $shield_change, "boost_count" => $boost_change);
					break;
			}
		}

		$not_calculated_users = array();

		nut_db::update_batch(TB_BETS, $upbets, 'id');

		foreach($res as $user_id => $widget_id) {

			$upbalances = array();
			foreach($widget_id as $widget_id => $bets) {
					//$oldwon = 0;
					$newwon  = 0;
					$shields = 0;
					$boosts  = 0;
					foreach($bets as $k => $v) {
						//$oldwon += $v['oldWonAmount'];
						$newwon  += $v['newWonAmount'];
						$shields += $v['shield_count'];
						$boosts  += $v['boost_count'];
					}

					$upbalances[] = array('user_id' => $user_id, 'widget_id' => $widget_id, 'balance' => 'balance' + $newwon,
										 'boost_count' => 'boost_count' + $boosts, 
										 'shield_count' => 'shield_count' + $shields);
			}
			try{

				if(!nut_db::is_free_lock('U_'.$user_id)) {
					throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
				}

				if(!nut_db::get_lock('U_'.$user_id)) {
					throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
				}

				nut_db::transactionStart("updateUserBalances");

				$sql = "DROP TEMPORARY TABLE IF EXISTS `temp`;";
				nut_db::query($sql);
				// create temp table
				$sql = "CREATE TEMPORARY TABLE `temp` (
				  `user_id` bigint(20) NOT NULL,
				  `widget_id` bigint(20) NOT NULL,
				  `balance` decimal(10) NOT NULL,
				  `boost_count` decimal(10) NOT NULL,
				  `shield_count` decimal(10) NOT NULL,
				  PRIMARY KEY (`user_id`, `widget_id`) 
				  ) ENGINE=MEMORY DEFAULT CHARSET=utf8;";

				nut_db::query($sql);

				$sql="INSERT INTO temp (user_id, widget_id, balance, boost_count, shield_count) VALUES ";
				$b = 0;

				foreach($upbalances as $val) {
					if($b == 0){
						$sql .= "( ".implode(',', $val).")";
					} else {
						$sql .= ", ( ".implode(',', $val).")";
					}
					$b = 1;
				}

				nut_db::query($sql);
				// create temp table
				
				$sql = "UPDATE ".TB_USERSBALANCES." AS UB ".
					   "INNER JOIN temp AS T ON T.widget_id = UB.widget_id AND T.user_id = UB.user_id ".
					   "SET UB.balance=UB.balance+T.balance, UB.shield_count=UB.shield_count+T.shield_count, UB.boost_count=UB.boost_count+T.boost_count";

				nut_db::query($sql);

				$sql = "DROP TEMPORARY TABLE IF EXISTS `temp`;";
				nut_db::query($sql);
				nut_db::transactionCommit("updateUserBalances");

				nut_db::release_lock('U_'.$user_id);
			}
			catch(Exception $e)
			{
				nut_db::transactionRollback("updateUserBalances");
				$not_calculated_users[] = array('user_id' => $user_id, 'widget_id' => $widget_id);
				nut_db::release_lock('U_'.$user_id);
				throw $e;
			}
		}

		return $not_calculated_users;
	}

	public function returnEvents($game_id, $bk_id)
	{
		try{
			// restore participants scores table
			$this->updateScoresTable(null, null, $game_id, $bk_id);

			// set result status to returned
			$sql = "UPDATE ".TB_EVENTS." SET result_status=0 WHERE game_id=? AND bookmaker_id = ?";
			nut_db::query($sql, array($game_id, $bk_id));
		}
		catch(Exception $e)
		{
			throw $e;
		}			
	}

	public function returnGame($game_id, $bk_id)
	{
		try
		{
			if(!nut_db::is_free_lock('G_'.$game_id)) {
				throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
			}

			if(!nut_db::get_lock('G_'.$game_id)) {
				throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
			}

			$sql = "UPDATE ".TB_GAMES." SET calculated=0, result_returned=1 WHERE id=?";
			nut_db::query($sql, array($game_id));

			$this->returnEvents($game_id, $bk_id);

			nut_db::release_lock('G_'.$game_id);

			$this->updateUsersBalances($game_id);

			$sql = "UPDATE ".TB_GAMES." SET calculated=-1 WHERE id=?";
			nut_db::query($sql, array($game_id));

		}
		catch(Exception $e)
		{
			nut_db::release_lock('G_'.$game_id);
			throw $e;
		}	
	}

	public function setGameResult($game_id, $results, $sport_id, $bk_id)
	{
		try
		{

			if(!nut_db::is_free_lock('G_'.$game_id)) {
				throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
			}

			if(!nut_db::get_lock('G_'.$game_id)) {
				throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
			}

			$lid  = $this->chili_translate->getLangId('en');
			$gdata = $this->getGameDataById($game_id, $lid, array(), null);

			if ($gdata['calculated'] == -1) { // -1 waiting, 0 - inprogress, 1 - finished
				$sql = "UPDATE ".TB_GAMES." SET calculated=0 WHERE id=?";
				nut_db::query($sql, array($game_id));

				$this->setGameEventsResults($game_id, $results, $sport_id, $bk_id);

			} else {
				throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
			}

			nut_db::release_lock('G_'.$game_id);

			$gdata = $this->getGameDataById($game_id, $lid, 'events', $bk_id);
			
			if ($gdata['calculated'] == 0) { // calculation in (-1 waiting, 0 - inprogress, 1 - finished)
				$this->updateUsersBalances($game_id);
				$this->setGameCalculated($game_id);
				$this->updateScoresTable($gdata, $results, null, null);
			} else {
				throw new Api_exception('Please try again.', Api_exception::$GAME_ERROR_CODE1);
			}

		}
		catch(Exception $e)
		{
			nut_db::release_lock('G_'.$game_id);
			throw $e;
		}
	}

	public function updateScoresTable($gdata, $goals, $gid, $bkid)
	{
		$gameId = $gdata ? $gdata['id'] : $gid;
		$lid  = $this->chili_translate->getLangId('en');
		$gamedata  = $gdata ? $gdata : $this->getGameDataById($gid, $lid, 'events', $bkid);
		$sql = "SELECT * ".
			   " FROM ". TB_GAMEPARTICIPANTS . 
			   " WHERE game_id = ?";
		$query = nut_db::query($sql, array($gameId));
		$parts =  $query->num_rows() > 0 ? $query->result_array(): NULL;
		
		$scoredata = array(
						'score' => 0,
						'played_games' => 0,
						'wins' => 0,
						'looses' => 0,
						'draws' => 0,
						'goals_out' => 0,
						'goals_in' => 0,
						'participant_id' => 0,
						'competition_id' => 0
						);
		$part1_id = null;
		$part2_id = null;
		foreach ($parts as $key => $row) {
			if($row['order'] == 1) {
				$part1_id = $row['participant_id'];
			} else {
				$part2_id = $row['participant_id'];
			}
		}

		$scores = array($part1_id => $scoredata, $part2_id => $scoredata);

		$events = $gamedata['aux_info']['events']['base_events'];
		$w1 = $events[0]['result_status'];
		$d  = $events[1]['result_status'];
		$w2  = $events[2]['result_status'];
		if ($w1 == $w2) { // draw
			$val = $gdata ? 1 : -1;
			$scores[$part1_id]['draws'] += $val;
			$scores[$part2_id]['draws'] += $val;
			$scores[$part1_id]['score'] += $val;
			$scores[$part2_id]['score'] += $val;
		} elseif ($w1 > $w2) {
			$val = $gdata ? array(1, 1, 3) : array(-1, -1, -3);
			$scores[$part1_id]['wins'] += $val[0];
			$scores[$part2_id]['looses'] += $val[1];
			$scores[$part1_id]['score'] += $val[2];
		} else {
			$val = $gdata ? array(1, 1, 3) : array(-1, -1, -3);
			$scores[$part2_id]['wins'] += $val[0];
			$scores[$part1_id]['looses'] += $val[1];
			$scores[$part2_id]['score'] += $val[2];
		}
		$val = $gdata ? 1 : -1;
		$scores[$part2_id]['played_games'] += $val;
		$scores[$part1_id]['played_games'] += $val;

		$valgoals = $gdata ? $goals : explode(":", $gamedata['result_text']);
		if ($gdata == null) {
			foreach ($valgoals as $key => $value) {
				$valgoals[$key] = -$value;
			}
		}
		$val = $gdata ? array(1, 3) : array(0, 1);

		$scores[$part1_id]['goals_out'] += $valgoals[$val[0]];
		$scores[$part1_id]['goals_in'] += $valgoals[$val[1]];
		$scores[$part2_id]['goals_out'] += $valgoals[$val[1]];
		$scores[$part2_id]['goals_in'] += $valgoals[$val[0]];

		$scores[$part1_id]['participant_id'] = $part1_id;
		$scores[$part2_id]['participant_id'] = $part2_id;
		$scores[$part1_id]['competition_id'] = $gamedata['competition_id'];
		$scores[$part2_id]['competition_id'] = $gamedata['competition_id'];

		foreach ($scores as $key => $row) {
			$sql = "UPDATE ".TB_COMPETITION_PARTICIPANTS." SET score=score+?, played_games=played_games+?, wins=wins+?, looses=looses+?, draws=draws+?, goals_out=goals_out+?, goals_in=goals_in+? ".
			" WHERE participant_id=? AND competition_id=?";
			nut_db::query($sql, $row);
		}

	}

	public function insertGames($games)
	{
		try
		{
			nut_db::transactionStart("insertGames");
			$values = nut_utils::sanitize_keys($games, array('name', 'start_date', 'competition_id', 
													'sport_id', 'result_text', 'calculated', 'priority',
													'blocked', 'status'), TRUE, NULL);

			nut_db::insert_batch(TB_GAMES, $values);
			
			$ids = nut_db::getLastID(TB_GAMES, 'id', count($games));

			$ids = !is_array($ids) ? array($ids) : $ids;

			$this->aux_insert($ids, $games);
			
			nut_db::transactionCommit("insertGames");

			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertGames");
			throw $e;
		}
	}

	public function updateGames($games)
	{
		try
		{
			nut_db::transactionStart("updateGames");
			$values = nut_utils::sanitize_keys($games, array('id', 'name', 'start_date', 'competition_id', 
													'sport_id', 'result_text', 'calculated', 'priority',
													'blocked', 'status'));
			nut_db::update_batch(TB_GAMES, $values, 'id');
			$this->aux_update($games);

			nut_db::transactionCommit("updateGames");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateGames");
			throw $e;
		}
	}

	public function removeGame($id)
	{
		try
		{
			nut_db::transactionStart("removeGame");
			$sql = "DELETE FROM " . TB_GAMES  .  " WHERE id = ?";
			$query = nut_db::query($sql, $id);	

			$sql = "DELETE FROM " . TB_GAMEPARTICIPANTS  .  " WHERE game_id = ?";
			$query = nut_db::query($sql, $id);

			$sql = "DELETE FROM " . TB_EVENTS . " WHERE game_id = ?";
			$query = nut_db::query($sql, $id);

			// $data =  $query->num_rows() > 0 ? $query->row_array(): NULL;
			// $result_id = $data['result_id'];

			// $sql = "DELETE FROM " . TB_RESULTS  .  " WHERE id = ?";
			// $query = nut_db::query($sql, $result_id);

			chili_translate::deleteAllTranslations(chili_translate::$GAMES, $id);

			nut_db::transactionCommit("removeGame");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeGame");
			throw $e;
		}
	}

	private function aux_update($games)
	{
		foreach ($games as $gdata)
		{
			// update translations
			$id = $gdata['id'];
			$aux_info = element('aux_info', $gdata);
			$translations = $aux_info ? element(self::$AUX_TRANSLATION, $aux_info) : FALSE;
			$participants = $aux_info ? element(self::$AUX_PARTICIPANT, $aux_info) : FALSE;
			if ($translations)
			{
				$data = array();
				foreach($translations as $key => $value)
				{
					$langId = chili_translate::getLangId($key);
					$arr    = chili_translate::getTranslation($langId, $id, chili_translate::$GAMES);
					if (!is_null($arr))
					{
						$data[] = array('lang' => $langId , 'id' => $id , 'type' => chili_translate::$GAMES, 'text' => $value );
					}
					else {
						chili_translate::addTranslation($langId, $id, chili_translate::$GAMES, $value);
					}
				}
				chili_translate::updateTranslations($data);
			}
			if ($participants)	
			{
				$values = array();
				$order = 1;
				foreach($participants as $p)
				{
					$values[] = array('game_id' => $id, 'participant_id' => $p, 'order' => $order);
					$order += 1;
				}
				$sql = "DELETE FROM " . TB_GAMEPARTICIPANTS . " WHERE `game_id` = ? ";
				$query = nut_db::query($sql, array($id));
				nut_db::insert_batch(TB_GAMEPARTICIPANTS, $values);
			}
		}		
	}

	private function aux_insert($ids, $data)
	{
		foreach ($ids as $k => $v)
		{
			$id = $v;
			$aux_info = element('aux_info', $data[$k]);

			$translations = $aux_info ? element(self::$AUX_TRANSLATION, $aux_info) : FALSE;
			$participants = $aux_info ? element(self::$AUX_PARTICIPANT, $aux_info) : FALSE;

			if ($translations)
			{
				foreach($translations as $key => $value)
				{
					$langId = chili_translate::getLangId($key);
					chili_translate::addTranslation($langId, $id, chili_translate::$GAMES, $value);
				}
			}
			if ($participants)
			{
				$values = array();
				$order = 1;
				foreach($participants as $p)
				{
					$values[] = array('game_id' => $id, 'participant_id' => $p, 'order' => $order);
					$order += 1;
				}
				nut_db::insert_batch(TB_GAMEPARTICIPANTS, $values);
			}
		}		
	}


	private function aux_appender(&$data, $langid, $bk_id, $aux_info)
	{
		$ids = array();
		foreach($data as $key => $val) {
			$ids[] = $val['id'];
		}

		foreach($data as $ind => $row) {
			if(!array_key_exists("aux_info", $data[$ind])) {
				$data[$ind]["aux_info"] = array();
			}
		}

		if(is_string($aux_info) && count($ids) > 0)
		{
			$this->aux_append($ids, $data, $langid, $bk_id, $aux_info);
		}

		if(is_array($aux_info) && count($aux_info) > 0 && count($ids) > 0)
		{
			foreach($aux_info as $aux)
			{
				$this->aux_append($ids, $data, $langid, $bk_id, $aux);
			}
		}		
	}

	private function aux_append(&$ids, &$data, $langid, $bk_id, $aux_name)
	{
		switch($aux_name)
		{
			case self::$AUX_TRANSLATION:

				$translations = chili_translate::getFullTranslationsByIDs($ids, chili_translate::$GAMES);

				foreach($translations as $key => $val) {
					foreach($data as $ind => $row) {
						if($key == $row['id']) {
							$data[$ind]["aux_info"][self::$AUX_TRANSLATION] = $val;
						}
					}
				}

				break;
				
			case self::$AUX_PARTICIPANT:

				foreach($data as $ind => $row) {
					$data[$ind]["aux_info"][self::$AUX_PARTICIPANT] = array();
				}
				$participants = $this->participants_model->getParticipantsByGameIDs($ids, $langid, NULL);
				foreach($participants as $key => $val) {
					foreach($data as $ind => $row) {
						if($val['game_id'] == $row['id']) {
							$data[$ind]["aux_info"][self::$AUX_PARTICIPANT][] = $val;
						}
					}
				}

				break;

			case self::$AUX_EVENT:
				foreach($data as $ind => $row) {
					$data[$ind]["aux_info"][self::$AUX_EVENT] = array();
					$data[$ind]["aux_info"][self::$AUX_EVENT]['base_events'] = array();
					$data[$ind]["aux_info"][self::$AUX_EVENT]['aux_events'] = array();
				}
				
				$events = $this->events_model->getAllEventsByIDs($ids, $bk_id, array());

				foreach($events['base_events'] as $key => $val) {
					foreach($data as $ind => $row) {
						if($val['game_id'] == $row['id']) {								
							$data[$ind]["aux_info"][self::$AUX_EVENT]['base_events'][] = $val;
						}
					}
				}
				foreach($events['aux_events'] as $key => $val) {
					foreach($data as $ind => $row) {
						if($val['game_id'] == $row['id']) {
							$data[$ind]["aux_info"][self::$AUX_EVENT]['aux_events'][] = $val;
						}
					}
				}
				break;					
		}		
	}

	public function removeGamesBy($tillDate) 
	{
		$sql = "SELECT id ".
		  	   " FROM ". TB_GAMES . 
		  	   " WHERE start_date < ?";
		$query = nut_db::query($sql, array($tillDate));

		$data = $query->num_rows() > 0 ? $query->result_array() : array();

		function value($item) {	return $item['id']; }
		$ids = array_map('value', $data);

		$this->removeGamesByIds($ids);		
	}

	public function removeGamesByIds($ids=null)
	{
		try
		{
			nut_db::transactionStart("removeGames");

			if ($ids) {
				$values = implode(',', array_fill(0, count($ids), "?"));
				$sql = "DELETE FROM " . TB_GAMES  .  " WHERE id in (". $values . ")";
				$query = nut_db::query($sql, $ids);

				$sql = "DELETE FROM " . TB_GAMEPARTICIPANTS  .  " WHERE game_id in (". $values . ")";
				$query = nut_db::query($sql, $ids);

				$sql = "DELETE FROM " . TB_EVENTS . " WHERE game_id in (". $values . ")";
				$query = nut_db::query($sql, $ids);

				chili_translate::deleteAllTranslationsBatch(chili_translate::$GAMES, $ids);
			} 

			nut_db::transactionCommit("removeGames");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeGames");
			throw $e;
		}
	}

}

