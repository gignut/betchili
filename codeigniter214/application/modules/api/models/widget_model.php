<?php

use Respect\Validation\Validator as validator;

class widget_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	// specific for football
	// TODO::CHILI change template ids later
	public function getBetsCountByGame($gameId)
	{
		try
		{
			$sql = "SELECT  E.template_id, 
							count(E.id) as qty
					FROM ". TB_BETS ." AS B INNER JOIN ". TB_EVENT_CHAIN ." AS ECH ON ECH.bet_id = B.id 
					INNER JOIN ". TB_EVENTS ." AS E ON E.id = ECH.event_id 
					WHERE E.game_id = ? AND (E.template_id = 1 OR E.template_id = 2 OR E.template_id = 3)
					GROUP BY E.template_id";
			
			$query = nut_db::query($sql, array($gameId));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//2016 FEB 14
	//avar
	// IMPORTANT TODO:: change competition get by status - current
	public function getCompetitions($wid, $langid = 1) // to Delete
	{
		try
		{
			$sql = "SELECT C.id,
						   C.name,	
						   GetTranslation(". $langid .",  C.id  ,". chili_translate::$COMPETITIONS. ") AS tname ,
						   C.league_id,
						   C.logo,
						   C.start_date,
						   C.end_date,
						   L.priority,
						   L.sport_id,
						   L.country_id
					FROM " . TB_WIDGET_LEAGUES . " as WL
					INNER JOIN  " . TB_LEAGUES . " as L ON L.id = WL.league_id
					INNER JOIN  " . TB_COMPETITIONS . " AS C ON C.league_id = WL.league_id
					WHERE WL.widget_id = ? 
					AND C.blocked = 0";

			$query = nut_db::query($sql, array($wid));
			$data = $query->num_rows() > 0 ? $query->result_array() : array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getLeagues($wid, $langid = 1)
	{
		try
		{
			$sql = "SELECT L.id,
						   L.name,	
						   GetTranslation(". $langid .",  L.id  ,". chili_translate::$LEAGUES. ") AS tname ,
						   L.logo,
						   L.priority,
						   L.sport_id,
						   L.country_id
					FROM " . TB_WIDGET_LEAGUES . " as WL
					INNER JOIN  " . TB_LEAGUES . " as L ON L.id = WL.league_id
					WHERE WL.widget_id = ?";

			$query = nut_db::query($sql, array($wid));
			$data = $query->num_rows() > 0 ? $query->result_array() : array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//2014 FEB 16
	//avar
	public function getCSSData($widgetId)
	{
		try
		{
			$data = array();
			$sql = "Select css FROM " . TB_WIDGETS . " WHERE id = ?";
			$query = nut_db::query($sql, array($widgetId));
			$data = $query->num_rows == 1 ? $query->row_array() :array();

			return element("css", $data, array());
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// 2014 FEB 19
	// avar
	public function getWidgetData($widgetId)
	{
		try
		{
			$widgetId = intval($widgetId);
			$sql = "SELECT  W.id,
							W.name,
							W.siteuser_id,
							W.widget_type,
							W.size as size_id,
							W.language_id,
							W.bookmaker_id,
							W.css,
							SZ.height,
							SZ.width,
							SU.domain
					FROM " . TB_WIDGETS . " AS W 
					INNER JOIN " . TB_SIZE . " AS SZ ON  SZ.id = W.size 
					INNER JOIN " . TB_SITEUSERS . " AS SU ON  SU.id = W.siteuser_id 
					WHERE W.id = ?";
			
			$query = nut_db::query($sql, array($widgetId));
			$data =  $query->num_rows() == 1 ? $query->row_array() : array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getLeaderboard($widgetId, $userId, $limit, $offset)
	{
		try
		{
			$limit_sql="";
			$offset_sql="";
			if($limit)
			{
				$limit = intval($limit);
				$limit_sql = " LIMIT " . $limit; 
			}

			if($offset)
			{
				$offset = intval($offset);
				$offset_sql = " OFFSET " . $offset; 
			}
			$widgetId = intval($widgetId);
			$sql = "SELECT  UP.id,
							UP.first_name,
							UP.last_name,
							UP.image,
							UB.balance,
							GetUserBetAccuracy(UB.widget_id , UB.user_id) as  accuracy,
							@rank := @rank + 1 as rank
					FROM (" . TB_USERSBALANCES . " AS UB
					INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UB.user_id), (SELECT @rank :=0) R
					WHERE UB.widget_id = ? AND UP.blocked = 0 ORDER BY UB.balance DESC ".$limit_sql.$offset_sql;
			
			$query = nut_db::query($sql, array($widgetId));
			$data  =  $query->num_rows() > 0 ? $query->result_array() : array();
			
			$returnData = array("list" => $data, "user" => null);

			$found = false;
			foreach ($data as $key => $row) {
				if ($row['id'] == $userId) {
					$found = true;
					$returnData['user'] = $row;
				}
			}
			if (!$found) {
				$sql = "SELECT * from (SELECT  UP.id,
							UP.first_name,
							UP.last_name,
							UP.image,
							UB.balance,
							GetUserBetAccuracy(UB.widget_id , UB.user_id) as  accuracy,
							@rank := @rank + 1 as rank
					FROM (" . TB_USERSBALANCES . " AS UB
					INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UB.user_id), (SELECT @rank :=0) R
					WHERE UB.widget_id = ? ORDER BY UB.balance DESC) M WHERE M.id = ?";
			
				$query = nut_db::query($sql, array($widgetId, $userId));
				$userData  =  $query->num_rows() > 0 ? $query->row_array() : array();
				$returnData['user'] = $userData;
			}

			return $returnData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}