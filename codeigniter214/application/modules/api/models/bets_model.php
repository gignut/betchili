<?php

use Respect\Validation\Validator as validator;

class bets_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getBetsStatData($user_id, $widget_id, $site_id)
	{
		try
		{
			$sql = "SELECT  B.amount,".
						    "B.won_amount,".
						    "B.result_status".

							" FROM ". TB_BETS ." AS B LEFT JOIN " . TB_CHALLENGES . " AS CH ON (B.id = CH.publisher_bet_id OR B.id = CH.opponent_bet_id)".
							" WHERE B.user_id = ? AND B.widget_id = ? AND B.site_id = ? AND ISNULL(CH.id)";
			
			$query = nut_db::query($sql, array($user_id, $widget_id, $site_id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllSiteUsers()
	{
		try
		{
			$sql = "SELECT  S.id as site_id,
							S.domain as site_domain,
							S.blocked,
							W.id as widget_id,
							W.name as widget_name

							FROM ". TB_SITEUSERS ." AS S INNER JOIN ". TB_WIDGETS ." AS W ON W.siteuser_id = S.id
							WHERE W.widget_type = 1";
			
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$return = array();
			foreach ($data as $row) {
				$id = $row['site_id'];
				if(!array_key_exists($row['site_id'], $return)) {
					$return[$id] = array();
					$return[$id]['widgets'] = array();
					$return[$id]['id'] = $id;
					$return[$id]['site_domain'] = $row['site_domain'];
					$return[$id]['blocked'] = $row['blocked'];
				}
				//$return[$row['site_id']][] = $row;
				$return[$id]['widgets'][] = array('id' => $row['widget_id'], 'name' => $row['widget_name']);
			}

			$result = array();
			foreach($return as $key => $val) {
				$result[] = $val;
			}

			return $result;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getBetById($id, $aux_info)
	{
		try
		{
			$sql = "SELECT  B.id, 
							B.user_id,
						    B.site_id,
						    B.widget_id,
						    B.amount,
						    B.won_amount,
						    B.create_stamp,
						    B.result_status,
						    B.coefficient,
						    ECH.coefficient as event_coefficient,
						    ECH.basis,
						    ECH.event_id,
						    E.game_id,
						    E.result_status as event_result_status,
						    ET.name as event_name,
						    ET.id as template_id

							FROM ". TB_BETS ." AS B INNER JOIN ". TB_EVENT_CHAIN ." AS ECH ON ECH.bet_id = B.id 
								 INNER JOIN ". TB_EVENTS ." AS E ON E.id = ECH.event_id 
								 INNER JOIN ". TB_EVENTS_TEMPLATES." AS ET ON E.template_id = ET.id
							WHERE B.id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$returnData = $this->group_by_bet($data);
			//$this->aux_append('', $data, $langid, $aux_info);
			return isset($returnData[$id]) ? $returnData[$id] : null;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function group_by_bet($data)
	{
		$return = array();
		foreach($data as $row)
		{
			$id = $row['id'];
			$event = array();
			if (!array_key_exists($id, $return))
			{
				$return[$id] = array();
				$return[$id]['event_chain'] = array();
			}
			foreach($row as $key => $val)
			{
				if (in_array($key,array('basis', 'event_coefficient', 'event_id', 'etmp_id', 'game_id', 'game_result', 'game_stamp', 'event_name', 'template_id', 'event_result_status', 'pname1', 'plogo1', 'pname2', 'plogo2')))
				{
					$event[$key] = $val;
				}
				else {
					$return[$id][$key] = $val;
				}
			}
			$return[$id]['event_chain'][] = $event;
		}

		return $return;
	}


	public function getAllBetsByGame($gameId, $aux_info = null)
	{
		try
		{
			$sql = "SELECT  B.id, 
							B.user_id,
						    B.site_id,
						    B.widget_id,
						    B.amount,
						    B.won_amount,
						    B.create_stamp,
						    B.result_status,
						    B.coefficient,
						    B.bet_type,
						    ECH.coefficient as event_coefficient,
						    ECH.basis,
						    ECH.event_id,
						    E.game_id,
						    E.result_status as event_result_status,
						    ET.name as event_name,
						    CH.publisher_bet_id as pub_id,
							CH.opponent_bet_id as opp_id,
							UB.shield_count,
							UB.boost_count,
							UB.shield_percent,
							UB.boost_multiplier

							FROM ". TB_BETS ." AS B INNER JOIN ". TB_EVENT_CHAIN ." AS ECH ON ECH.bet_id = B.id 
								 INNER JOIN ". TB_EVENTS ." AS E ON E.id = ECH.event_id 
								 INNER JOIN ". TB_EVENTS_TEMPLATES." AS ET ON E.template_id = ET.id
								 INNER JOIN ". TB_USERSBALANCES." AS UB ON (B.user_id = UB.user_id AND B.site_id = B.site_id AND B.widget_id = UB.widget_id)
								 LEFT  JOIN ". TB_CHALLENGES." AS CH ON (CH.publisher_bet_id = B.id OR CH.opponent_bet_id = B.id)
							WHERE B.id in (
											SELECT ECH1.bet_id
											FROM ".TB_EVENT_CHAIN." AS ECH1 INNER JOIN ".TB_BETS." AS B1 on B1.id = ECH1.bet_id
											WHERE ECH1.game_id = ?
											)";
						
			$query = nut_db::query($sql, array($gameId));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$retData = $this->group_by_bet($data);

			//$this->aux_append('', $data, $langid, $aux_info);

			return $retData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllBetsByWidget($siteId, $widgetId, $userId, $gameId, $aux_info, $limit)
	{
		try
		{
			$opt = array($siteId);
			$where = "";
			$limitSQL = "";
			if (!IS_NULL($widgetId)) {
				$where = " AND B.widget_id = ?";
				$opt[] = $widgetId;
			}
			if (!IS_NULL($userId)) {
				$where .= " AND B.user_id = ?";
				$opt[] = $userId;
			}
			if (!IS_NULL($limit)) {
				$limitSQL .= " LIMIT " . intval($limit);
			}

			$sql = "SELECT  B.id, 
							B.user_id,
						    B.site_id,
						    B.widget_id,
						    B.amount,
						    B.won_amount,
						    B.create_stamp,
						    B.result_status,
						    B.coefficient,
						    ECH.coefficient as event_coefficient,
						    ECH.basis,
						    ECH.event_id,
						    E.game_id,
						    E.result_status as event_result_status,
						    ET.name as event_name

							FROM ". TB_BETS ." AS B INNER JOIN ". TB_EVENT_CHAIN ." AS ECH ON ECH.bet_id = B.id 
								 INNER JOIN ". TB_EVENTS ." AS E ON E.id = ECH.event_id 
								 INNER JOIN ". TB_EVENTS_TEMPLATES." AS ET ON E.template_id = ET.id
								 LEFT  JOIN ". TB_CHALLENGES." AS CH ON (CH.publisher_bet_id = B.id OR CH.opponent_bet_id = B.id)
							WHERE B.site_id = ? AND ISNULL(CH.publisher_bet_id) AND ISNULL(CH.opponent_bet_id) " . $where . " ORDER BY B.create_stamp DESC" . $limitSQL;
						
			$query = nut_db::query($sql, $opt);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$retData = $this->group_by_bet($data);

			$retData = $this->filterByGameId($retData, $gameId); // help bad solution SQL need
			//$this->aux_append('', $data, $langid, $aux_info);

			return $retData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function filterByGameId($data, $game_id)
	{
		$newData = array();
		if (!IS_NULL($game_id)) {
			foreach ($data as $key => $value) {
				$chain = $value['event_chain'];
				foreach($chain as $k => $v) {
					if ($game_id == $v['game_id']) {
						$newData[$key] = $value;
					}
				}
			}
		} else {
			return $data;
		}
		return $newData;
	}

	public function getAllBetsByUser($userId, $widgetId, $aux_info,  $limit = null, $offset = null, $langId)
	{
		try
		{
			$limit_sql="";
			$offset_sql="";
			if($limit)
			{
				$limit = intval($limit);
				$limit_sql = " LIMIT " . $limit; 
			}

			if($offset)
			{
				$offset = intval($offset);
				$offset_sql = " OFFSET " . $offset; 
			}			
			$sql = "SELECT 
						    ECH.coefficient as event_coefficient,
						    ECH.basis,
						    ECH.event_id,
						    ET.id as etmp_id,
						    E.game_id, 
						    GetGameStamp(E.game_id) as game_stamp,
						    GetGameResultTxt(E.game_id) as game_result,
						    GetParticipantName(E.game_id, 1 , " . $langId . ") as pname1,
						    GetParticipantLogo(E.game_id, 1) as plogo1,
						    GetParticipantName(E.game_id, 2, " . $langId . ") as pname2,
						    GetParticipantLogo(E.game_id, 2) as plogo2,
						    E.result_status as event_result_status,
						    ET.name as event_name,
							MAIN.*
					FROM event_chain AS ECH INNER JOIN events AS E ON E.id = ECH.event_id 
							INNER JOIN eventstemplates AS ET ON E.template_id = ET.id
							INNER JOIN (SELECT  B.id, 
										B.user_id,
									    B.site_id,
									    B.widget_id,
									    B.amount,
									    B.won_amount,
									    UNIX_TIMESTAMP(B.create_stamp) as create_stamp,
									    B.result_status,
									    B.coefficient,
									    B.bet_type,
									    UB.shield_percent

										FROM bets AS B 
											 INNER JOIN usersbalances AS UB ON (UB.user_id = B.user_id AND UB.widget_id = B.widget_id)
											 LEFT  JOIN challenges AS CH ON (CH.publisher_bet_id = B.id OR CH.opponent_bet_id = B.id)
										WHERE B.user_id = ? AND B.widget_id = ? AND ISNULL(CH.publisher_bet_id) AND ISNULL(CH.opponent_bet_id) ORDER BY B.create_stamp DESC ".$limit_sql.$offset_sql." 
							) AS MAIN ON MAIN.id = ECH.bet_id";

			$params = array($userId, $widgetId);

			$query = nut_db::query($sql, $params);

			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$retData = $this->group_by_bet($data);
			//$this->aux_append('', $data, $langid, $aux_info);
			return $retData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	private function checkBalance($site_id, $widget_id, $user_id, $amount, &$bet) {
		// validate balance amount to bet
		$sql =  "SELECT balance, boost_count, shield_count ".
				" FROM " . TB_USERSBALANCES . 
				" WHERE site_id = ? AND widget_id = ? AND user_id = ?" ;

		$query = nut_db::query($sql, array($site_id, $widget_id, $user_id));
		$balance =  $query->num_rows() > 0 ? $query->row_array(): array();
		
		$type = 0;

		switch($bet['bet_type']) {
			// shield
			case 1: 
					$type = ($balance['shield_count'] == 0) ? 0 : $bet['bet_type'];
					break;
			// boost					
			case 2: 
					$type = ($balance['boost_count'] == 0) ? 0 : $bet['bet_type'];
					break;
			// both
			case 3: 
					$type = ($balance['boost_count'] == 0 || $balance['shield_count'] == 0) ? 0 : $bet['bet_type'];
					break;
		}

		$bet['bet_type'] = $type;

		if($balance['balance'] - $amount < 0) {
			throw new Api_exception('No enough balance to accept bet.', Api_exception::$BET_ERROR_CODE1);
		}
	}

	private function checkEventAndGameData($data) {
		$event_ids = array();
		$game_ids = array();
		foreach ($data as $key => $value) {
					$event_ids[]=$value['event_id'];
					$game_ids[]=$value['game_id'];
				}

		// check multi bet rule
		if (count(array_unique($game_ids))<count($game_ids)) {
			throw new Api_exception('Wrong bet chain.', Api_exception::$BET_ERROR_CODE2);
		}

		$values = implode(',', array_fill(0, count($event_ids), "?"));
		$game_values = implode(',', array_fill(0, count($game_ids), "?"));
		
		$sql =  "SELECT id, coefficient, basis, result_status, blocked ".
				" FROM " . TB_EVENTS . 
				" WHERE id in (".$values.")" ;

		$query = nut_db::query($sql, $event_ids);
		$res =  $query->num_rows() > 0 ? $query->result_array(): array();

		$sql =  "SELECT id ".
				" FROM " . TB_GAMES . 
				" WHERE id in (".$game_values.") AND UNIX_TIMESTAMP() > UNIX_TIMESTAMP(start_date) - 600 " ; // start_date - 10 minute
		$query = nut_db::query($sql, $game_ids);
		if ($query->num_rows() > 0 ) {
			throw new Api_exception('Game already started', Api_exception::$BET_ERROR_CODE3);
		}

		foreach ($data as $key => $row) {
			foreach ($res as $key1 => $row1) {
				if ($row['event_id'] == $row1['id']) {
					if($row1['blocked'] == 1) { // event is blocked
						throw new Api_exception('Event is blocked.', Api_exception::$BET_ERROR_CODE4);	
					}
					else if($row1['result_status'] == 1 || $row1['result_status'] == 2) // result was set
					{
						throw new Api_exception('Event already has result.', Api_exception::$BET_ERROR_CODE5);
					}
					//  else if($row1['result_status'] == 0) {
					// 	throw new Api_exception('B6: Event has returned status', 3);
					// }
					else if($row['coefficient'] != $row1['coefficient'] || $row['basis'] != $row1['basis']) {
						throw new Api_exception('Coefficient or balance was updated. Please refresh the game and try again.', Api_exception::$BET_ERROR_CODE6);
					}
				}
			}
		}
	}

	// TODO :: Lock anel bet aneluc
	public function insertBet($bet, $from=null)
	{
		try
		{
			nut_db::transactionStart("insertBet");

			// check bet min/max amount
			$max = MAX_BET_AMOUNT;
			$min = MIN_BET_AMOUNT;
			if(!is_null($from) && $from == 'chall') {
				$max = MAX_CHALLANGE_AMOUNT;
				$min = MIN_CHALLANGE_AMOUNT;
			}
			if (is_null($from) && ($bet['amount'] < $min || $bet['amount'] > $max)) {
				throw new Api_exception('Bet amount should be between '.$min.' and '.$max.'.', Api_exception::$BET_ERROR_CODE7);
			}

			// check balance to accept challenge
			$this->checkBalance($bet['site_id'], $bet['widget_id'], $bet['user_id'], $bet['amount'], $bet);

			$check = array();
			foreach ($bet['event_chain'] as $key => $row) {
				$check[] = $row;
			}
			// check coefficient and basis between server and client sent
			$this->checkEventAndGameData($check);

			$sql = "INSERT INTO ".TB_BETS. " (user_id, site_id, widget_id, amount, won_amount, coefficient, bet_type) ".
				   " VALUES (?, ?, ?, ?, ?, ?, ?) ";

			nut_db::query($sql, array($bet['user_id'], $bet['site_id'], $bet['widget_id'], $bet['amount'], $bet['won_amount'], $bet['coefficient'], $bet['bet_type']));
			
			$id = nut_db::getLastID(TB_BETS, 'id', 1);

			//$this->aux_insert($ids, $bet);
			$eventChain = $bet['event_chain'];
			foreach($eventChain as &$event)
			{
				$event['bet_id'] = $id;
			}
			
			nut_db::insert_batch(TB_EVENT_CHAIN, $eventChain);

			$sql = "UPDATE ".TB_USERSBALANCES." SET balance = balance - ?, shield_count = shield_count - ?, boost_count = boost_count - ?
			        WHERE user_id = ? AND site_id = ? AND widget_id = ?";
			$varsh = ($bet['bet_type'] == 1 || $bet['bet_type'] == 3);
			$varbo = ($bet['bet_type'] == 2 || $bet['bet_type'] == 3);
			nut_db::query($sql, array(intval($bet['amount']), $varsh, $varbo, $bet['user_id'], $bet['site_id'], $bet['widget_id']));

			nut_db::transactionCommit("insertBet");
			return $id;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertBet");
			throw $e;
		}
	}

	public function setBoost($betId, $betType)
	{
		try
		{
			nut_db::transactionStart("setBoost");

			$sql = "UPDATE ". TB_BETS ." SET bet_type = ? WHERE id = ? ";
			nut_db::query($sql, array($betType, $betId));

			nut_db::transactionCommit("setBoost");
			return $betId;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("setBoost");
			throw $e;
		}
	}

}
