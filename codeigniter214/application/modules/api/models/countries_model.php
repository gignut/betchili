<?php

use Respect\Validation\Validator as validator;

class countries_model extends CI_Model {

	private static $AUX_TRANSLATION = "translations";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	//avar 
	//sep 17 2013
	public function getCountryDataById($id, $langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .",". $id . ",". chili_translate::$COUNTRIES.") AS tname,
						    flag,
						    code
							FROM ". TB_COUNTRIES ."
							WHERE id = ? ";
							
			$query = nut_db::query($sql, $id);
			$data =  $query->num_rows() == 1 ? $query->row_array(): array();

			$this->aux_append($id, $data, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	//avar 
	//sep 17 2013
	public function getCountryDataByIds($ids, $langid, $aux_info)
	{
		try
		{
			$qArray = array_fill(0, count($ids), "?");

			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .", id ,". chili_translate::$COUNTRIES.") AS tname,
						    flag,
						    code
							FROM ". TB_COUNTRIES ."
							WHERE id in (" . implode(",", $qArray) . " )"; 
						
			$query = nut_db::query($sql, $ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			$this->aux_append("", $data, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	//avar 
	//sep 17 2013
	public function getCountryDataByCode($code, $langid, $aux_info)
	{
		try
		{
			$countryId = $this->getCountryIdByCode($code);

			$sql = "SELECT  id,
							name,
						    GetTranslation(". $langid .",". $countryId . ",". chili_translate::$COUNTRIES.") AS tname,
						    flag,
						    code
							FROM ". TB_COUNTRIES ."
							WHERE  id = ? ";
							
			$query = nut_db::query($sql, $countryId);
			$data =  $query->num_rows() == 1 ? $query->row_array(): array();

			$this->aux_append($data['id'], $data, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	//avar 
	//sep 17 2013
	public function getAllCountriesData($langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$COUNTRIES.") AS tname,
						    flag,
						    code
							FROM ". TB_COUNTRIES;
							
			$query = nut_db::query($sql, $langid);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$this->aux_append('', $data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	//avar 
	//sep 17 2013
	public function getCountryIdByCode($code)
	{
		try
		{
			$sql = "SELECT  id
							FROM ". TB_COUNTRIES ."
							WHERE  code = ? ";
							
			$query = nut_db::query($sql, $code);
			$data =  $query->num_rows() == 1 ? $query->row_array(0): array();
			return isset($data["id"]) ? $data["id"] : NULL;
		}
		catch(Exception $e)
		{
			throw $e;
		}	
	}

	//avar 
	//sep 17 2013
	public static function getCountryIDByName($name)
	{
		try
		{
			$name = trim($name);
			$sql = "SELECT id FROM ".TB_COUNTRIES." WHERE UCASE(name) = ?";
			$query = nut_db::query($sql, $name);

			$data =  $query->num_rows() == 1 ? $query->row_array(): array();

			return isset($data["id"]) ? $data["id"] : NULL;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}	

	
	public function insertCountries($countries)
	{
		try
		{
			nut_db::transactionStart("insertCountries");
			$values = nut_utils::sanitize_keys($countries, array('name', 'code'), TRUE, NULL);

			nut_db::insert_batch(TB_COUNTRIES, $values);
			$ids = nut_db::getLastID(TB_COUNTRIES, 'id', count($countries));
			
			$ids = is_array($ids) ? $ids : array($ids);	

			foreach ($ids as $k => $v)
			{
				$id = $v;
				$aux_info = element('aux_info', $countries[$k]);
				$translations = $aux_info ? element('translations', $aux_info) : FALSE;

				if ($translations)
				{
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						chili_translate::addTranslation($langId, $id, chili_translate::$COUNTRIES, $value);
					}
				}
			}
			
			nut_db::transactionCommit("insertCountries");
			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertCountries");
			throw $e;
		}
	}

	public function updateCountries($countries)
	{
		try
		{
			nut_db::transactionStart("updateCountries");
			$values = nut_utils::sanitize_keys($countries, array('id', 'name', 'flag', 'code'));
			
			nut_db::update_batch(TB_COUNTRIES, $values, 'id');

			foreach ($countries as $cdata)
			{
				$id = $cdata['id'];
				$aux_info = element('aux_info', $cdata);
				$translations = $aux_info ? element('translations', $aux_info) : FALSE;
				if ($translations)
				{
					$data = array();
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						$arr    = chili_translate::getTranslation($langId, $id, chili_translate::$COUNTRIES);
						if (!is_null($arr))
						{
							$data[] = array('lang' => $langId , 'id' => $id , 'type' => chili_translate::$COUNTRIES, 'text' => $value );
						}
						else {
							chili_translate::addTranslation($langId, $id, chili_translate::$COUNTRIES, $value);
						}
					}
					chili_translate::updateTranslations($data);
				}
			}

			nut_db::transactionCommit("updateCountries");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback();
			throw $e;
		}
	}

	public function removeCountry($id)
	{
		try
		{
			nut_db::transactionStart("removeCountry");
			$flagFileName = $this->getFlag($id);
			$sql = "DELETE FROM " . TB_COUNTRIES  .  " WHERE id = ?";
			$query = nut_db::query($sql, $id);	

			chili_translate::deleteAllTranslations(chili_translate::$COUNTRIES, $id);

			$imageFullPath = asset_model::getAssetFolder("countries") . $flagFileName;
			if($flagFileName)
			{
					asset_model::removeAsset($imageFullPath);
			}
			nut_db::transactionCommit("removeCountry");

			return array("id" => $id, "deleted" => 1);
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeCountry");
			throw $e;			
		}
	}

	public  function getFlag($id)
	{
		try
		{
			$sql = "SELECT  flag FROM ". TB_COUNTRIES ." WHERE id = ? ";
			$query = nut_db::query($sql, $id);
			$data =  $query->num_rows() == 1 ? $query->row_array(0): NULL;
			$flag = !is_null($data) ? $data['flag'] : null;
			return $flag;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public  function setFlag($id, $filename)
	{
		try
		{
			$filename = trim($filename);
			$sql = "UPDATE " . TB_COUNTRIES . " SET flag = ? WHERE id = ?";
			$query = nut_db::query($sql, array($filename, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function aux_append($id, &$data, $aux_info)
	{
		if (validator::arr()->each(validator::arr())->validate($data))
		{
			foreach($data as $key => $value) {
				self::aux_appender($value['id'], $data[$key], $aux_info);
			}
		}
		elseif (!is_null($data))
		{
			self::aux_appender($id, $data, $aux_info);
		}
	}

	private function aux_appender($id, &$data, $aux_info)
	{
		if(is_string($aux_info))
		{
			$data["aux_info"] = array() ;
			switch($aux_info)
			{
				case self::$AUX_TRANSLATION:
					$data["aux_info"][self::$AUX_TRANSLATION] = chili_translate::getFullTranslations($id, chili_translate::$COUNTRIES);
					break;
			}
		}

		if(is_array($aux_info) && count($aux_info) > 0)
		{
			$data["aux_info"] = array() ;

			foreach($aux_info as $aux)
			{
				switch($aux)
				{
					case self::$AUX_TRANSLATION:
						$data["aux_info"][self::$AUX_TRANSLATION] = chili_translate::getFullTranslations($id, chili_translate::$COUNTRIES);
					break;
				}
			}
		}		
	}

}