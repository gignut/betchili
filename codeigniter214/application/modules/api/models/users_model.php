<?php

use Respect\Validation\Validator as validator;

class users_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	//get player widget balance 
	public function getPlayerBalance($siteId, $widgetId, $userId)
	{
		try
		{
			$sql = "SELECT  UB.balance
							FROM ". TB_USERSBALANCES ." as UB 
							WHERE UB.user_id = ? AND UB.site_id = ? AND UB.widget_id = ?";
			
			$query = nut_db::query($sql, array($userId, $siteId, $widgetId));
			$data =  $query->num_rows() == 1 ? $query->row_array(): array();
			return $data["balance"];
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getPlayerProfileAndBalance($siteId, $widgetId, $userId)
	{
		try
		{
			$sql = "SELECT  UP.id,
							UP.first_name,
							UP.last_name,
							UP.image,
							UP.gender,
							UP.register_date,
							UP.blocked,
							UP.mode,
							UB.site_id,
							UB.widget_id,
							UB.balance,
							UB.show_intro
							FROM ". TB_USERSPROFILES ." AS UP 
							INNER JOIN " . TB_USERSBALANCES ." AS UB ON (UB.user_id = UP.id)
							WHERE UB.user_id = ? AND UB.site_id = ? AND UB.widget_id = ?";
			
			$query = nut_db::query($sql, array($userId, $siteId, $widgetId));
			$data =  $query->num_rows() == 1 ? $query->row_array(): array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getUserProfileDataById($id, $aux_info)
	{
		try
		{
			$sql = "SELECT * FROM ". TB_USERSPROFILES ."  WHERE id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): NULL;

			//$this->aux_appender($return, $langid, NULL, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getUserDataByIds($ids, $site_id, $langid, $aux_info)
	{
		try
		{
			$values = implode(',', array_fill(0, count($ids), "?"));
			$sql = "SELECT  UP.*,
							UB.site_id,
							UB.balance
							FROM ". TB_USERSPROFILES ." AS UP 
							INNER JOIN " . TB_USERSBALANCES ." AS UB ON (UB.user_id = UP.id)
							WHERE UP.id in (".$values.")";
			
			array_unshift($ids, $site_id);
			$query = nut_db::query($sql, $ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			//$this->aux_appender($data, $langid, NULL, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//avar 16 JUL 2013
	public function initPlatformUser($platformId, $platformUserId, $siteId, $widgetId)
	{
		try
		{
			$retData = array();
			$query = null;
			switch($platformId)
			{
				case PLATFORM_CHILI:
					$sql = "SELECT UP.*, 
						           UCH.user_id 
							FROM " . TB_USERS_CHILI ." AS UCH 
							INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UCH.user_id
							WHERE UCH.user_id = ? ";
					break;
				case PLATFORM_FACEBOOK:
					$sql = "SELECT UP.*, 
						           UFB.fb_id 
							FROM " . TB_USERS_FACEBOOK ." AS UFB 
							INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UFB.user_id
							WHERE UFB.fb_id = ? ";
					break;
				case PLATFORM_GOOGLE:
					$sql = "SELECT  UP.*, 
						  			UG.google_id 
							FROM " . TB_USERS_GOOGLE ." AS UG 
							INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UG.user_id
							WHERE UG.google_id = ? ";
					break;
				case PLATFORM_ODNOKLASSNIKI:
					$sql = "SELECT  UP.*, 
						  			UODN.odn_id 
							FROM " . TB_USERS_ODN ." AS UODN 
							INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UODN.user_id
							WHERE UODN.odn_id = ? ";
					break;
						break;
				case PLATFORM_VKONTAKTE:
					$sql = "SELECT  UP.*, 
						  			UVK.vk_id 
							FROM " . TB_USERS_VK ." AS UVK 
							INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UVK.user_id
							WHERE UVK.vk_id = ? ";
					break;
				case PLATFORM_MAILRU:
					$sql = "SELECT  UP.*, 
						  			UMR.mr_id 
							FROM " . TB_USERS_MR ." AS UMR 
							INNER JOIN " . TB_USERSPROFILES . " AS UP ON UP.id = UMR.user_id
							WHERE UMR.mr_id = ? ";
					break;
			}

			$query = nut_db::query($sql, array($platformUserId));	
			$data = $query->num_rows() == 1 ? $query->row_array() : array(); 	

			
			// IMPORTANT
			// user id in chili platform for every user, regardless of fb or google... etc	
			$userId = element("id", $data, NULL);
			
			if($userId)
			{
				if(intval($data["blocked"]) === 1)
				{
					throw new Api_exception('User is blocked by administration due to not fair game.', Api_exception::$BLOKED_USER_LOGIN);
				}

				$sql = "SELECT widget_id 
						FROM " . TB_USERSBALANCES . " 
					    WHERE user_id = ? AND site_id = ? and widget_id = ?";
				$query = nut_db::query($sql, array($userId, $siteId, $widgetId));	
				// check if the user is first time vor any site
				$siteData =  $query->num_rows() == 1 ? $query->row_array() : array(); 

				// check if the user is first time in that website	
				$isFirstTime = isset($siteData['widget_id']) ? false : true;
				if($isFirstTime)
				{	
					$this->users_model->createBalanceInSite($siteId, $widgetId, $userId, PLAYER_INITIAL_BALANCE, SHIELD_COUNT, SHIELD_PERCENT, BOOST_COUNT, BOOST_MULTIPLIER);
				}

				$playerData = $this->users_model->getPlayerProfileAndBalance($siteId, $widgetId, $userId);
	
				$retData["autorized"] = true;
				$retData["widget_autorized"] = !$isFirstTime;
				$retData["player_data"] = $playerData;
			}	
			else
			{
				// no user, need to be registered, 
				$retData["autorized"] = false;
				$retData["widget_autorized"] = false;
				$retData["player_data"] = array();
				
			}
			return $retData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//avar 16 JUL 2013
	public function insertSocialPlatformUser($siteId, $widgetId, $platformId, $platform_uid, $first_name, $last_name, $email, $gender, $image, $blocked, $balance, $shield_count, $shield_percent, $boost_count, $boost_multiplier)
	{
		try
		{
			nut_db::transactionStart("insertSocialPlatformUser");

			$sql = "INSERT INTO " . TB_USERSPROFILES . " (first_name, last_name, email, gender, image, blocked, mode) VALUES (?,?,?,?,?,?,0)";
			$query = nut_db::query($sql, array($first_name, $last_name, $email,  $gender, $image, $blocked));
			$uId = nut_db::getLastID( TB_USERSPROFILES);

			switch($platformId)
			{
				case PLATFORM_FACEBOOK:
					$sql = "INSERT INTO " . TB_USERS_FACEBOOK . " (fb_id, user_id) VALUES (?,?)";
					$query = nut_db::query($sql, array($platform_uid, $uId));
				break;
				case PLATFORM_GOOGLE:
					$sql = "INSERT INTO " . TB_USERS_GOOGLE . " (google_id, user_id) VALUES (?,?)";
					$query = nut_db::query($sql, array($platform_uid, $uId));
				break;
				case PLATFORM_ODNOKLASSNIKI:
					$sql = "INSERT INTO " . TB_USERS_ODN . " (odn_id, user_id) VALUES (?,?)";
					$query = nut_db::query($sql, array($platform_uid, $uId));
				break;
				
				case PLATFORM_VKONTAKTE:
					$sql = "INSERT INTO " . TB_USERS_VK . " (vk_id, user_id) VALUES (?,?)";
					$query = nut_db::query($sql, array($platform_uid, $uId));
				break;

				case PLATFORM_MAILRU:
					$sql = "INSERT INTO " . TB_USERS_MR . " (mr_id, user_id) VALUES (?,?)";
					$query = nut_db::query($sql, array($platform_uid, $uId));
				break;
			}

			$this->createBalanceInSite($siteId, $widgetId, $uId, $balance, $shield_count, $shield_percent, $boost_count, $boost_multiplier);
			nut_db::transactionCommit("insertSocialPlatformUser");

			return $uId;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertSocialPlatformUser");
			throw $e;			
		}
	}

	// avar 17 JUL 2013
	public function createBalanceInSite($siteId, $widgetId, $userId, $balance, $shield_count, $shield_percent, $boost_count, $boost_multiplier)
	{
		try
		{
			$sql = "INSERT INTO " . TB_USERSBALANCES . " (user_id, widget_id, site_id, balance, shield_count, shield_percent, boost_count, boost_multiplier, stamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
			$query = nut_db::query($sql, array($userId, $widgetId, $siteId, $balance, $shield_count, $shield_percent, $boost_count, $boost_multiplier));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function addUserId($arr, $ids)
	{
		$return = $arr;
		foreach($ids as $ind => $id) {
			$return[$ind]['user_id'] = $id;
		}
		return $return;
	}

	private function changeIdToUserId($arr)
	{
		$return = array();
		foreach($arr as $key => $value) {
			$return[$key] = array();
			foreach($value as $key1 => $val) {
				if($key1 != 'id') {
					$return[$key][$key1] = $val;
				}
			}
			$return[$key]['user_id'] = $value['id'];
		}

		return $return;
	}

	
	public function registerChiliAccount($email, $password, $confirmPassword, $firstName, $lastName)
	{
		try
		{
			if($password == $confirmPassword)
			{
				nut_db::transactionStart("createChiliAccount");
				$data = $this->createUserProfile($email,  $firstName,  $lastName , 1);
				
				if(!isset($data["error"]))
				{
					// in case tehere is no error its id of user profile
					$userProfileId = $data;
					$sql = "INSERT INTO " . TB_USERS_CHILI . " (user_id, username, password) VALUES (?,?,?)";
					$query = nut_db::query($sql, array($userProfileId, $email,$password));
					
					nut_db::transactionCommit("createChiliAccount");
					return $userProfileId;
				}
				else
				{
					nut_db::transactionRollback("createChiliAccount");
					return $data;
				}
			}
			else
			{
				return array("error"=> true, "message" => "password_mismatch", "code" => 102);
			}
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("createChiliAccount");
			throw $e;
		}
	}

	//avar 17 JUL 2013
	public function createUserProfile($email, $first_name, $last_name, $blocked){
		try
		{
			//check if we have such profile
			$userProfile = $this->getUserProfileByEmail($email);
			$ret = null;
			if($userProfile  === FALSE)
			{
				$sql = "INSERT INTO " . TB_USERSPROFILES . " (first_name, last_name, email, register_date, blocked, mode) VALUES (?,?,?, 'CURRENT_TIMESTAMP',?, 0)";
				$query = nut_db::query($sql, array($first_name, $last_name, $email, $blocked));
				$uId = nut_db::getLastID( TB_USERSPROFILES);
				$ret = $uId;
			}
			else
			{
				$ret = array("error"=> 1, "message" => "User exists", "code" => 101);
			}

			return $ret;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	//avar 17 JUL 2013
	public function getUserProfileByEmail($email){
		try
		{
			$sql = "SELECT * FROM " . TB_USERSPROFILES  .  " WHERE email = ?";
			$query = nut_db::query($sql, array($email));	
			$data = $query->num_rows() > 0 ? $query->row_array() : false;
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}



	public function getChiliUserData($username, $password){
		try
		{
			$sql = "SELECT * FROM " . TB_USERS_CHILI  .  " WHERE username = ? AND password = ?";
			$query = nut_db::query($sql, array($username, $password));	
			$data = $query->num_rows() > 0 ? $query->row_array() : FALSE;
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllUsersBy($site_id, $widget_id, $user_id, $aux_info, $limit, $offset, $state)
	{
		try
		{
			$params = array();
			$where_sql = '';
			$from_sql  = '';
			$limit_sql = '';
			$offset_sql = '';
			$order = "DESC";
			$params[] = $site_id;
			$on_widget = "";

			if(!is_null($site_id))
			{
				$where_sql .= " AND UB.site_id = " . $site_id;
				$params[] = $site_id;
			}
			if(!is_null($widget_id))
			{
				$where_sql .= " AND UB.widget_id = " . $widget_id;
				$params[] = $widget_id;
				$on_widget = " AND B.widget_id = UB.widget_id";
			}
			if(!is_null($user_id))
			{
				$where_sql .= " AND UB.user_id = " . $user_id;
				$params[] = $user_id;
			}			
			if(!is_null($state))
			{
				$where_sql .= " AND UP.blocked = " . $state;
				$params[] = $state;
			}			
			if(!is_null($limit))
			{
				$limit_sql = " LIMIT ?";
				$params[] = $limit;
			}
			if(!is_null($offset))
			{
				$offset_sql = " OFFSET ?";
				$params[] = $offset;
			}

			$sql = "SELECT  UP.*,
							UB.site_id,
							UB.widget_id,
							UB.balance
							FROM ". TB_USERSPROFILES ." AS UP 
							INNER JOIN " . TB_USERSBALANCES ." AS UB ON (UB.user_id = UP.id)
							WHERE TRUE " . $where_sql . "
							ORDER BY UB.balance ". $order . $limit_sql . $offset_sql;
			
			if(!is_null($aux_info)) {
				if (!is_array($aux_info) && 'bets_count' == $aux_info) {
						$sql = "SELECT  UP.*,
						UB.site_id,
						UB.widget_id,
						count(B.id) as bets_count,
						UB.balance
						FROM (". TB_USERSPROFILES ." AS UP 
						INNER JOIN " . TB_USERSBALANCES ." AS UB ON (UB.user_id = UP.id))
						LEFT JOIN (SELECT B1.* 
											 FROM ".TB_BETS." AS B1 LEFT JOIN ".TB_CHALLENGES." AS CH ON (CH.publisher_bet_id = B1.id OR CH.opponent_bet_id = B1.id)
											 WHERE ISNULL(CH.publisher_bet_id) AND ISNULL(CH.opponent_bet_id)
											) AS B ON B.user_id = UP.id AND B.site_id = UB.site_id
						WHERE TRUE " . $where_sql . "
						GROUP BY UP.id ORDER BY UB.balance ". $order . $limit_sql . $offset_sql;
				}
			}

			$query = nut_db::query($sql, $params);

			if ($query->num_rows() == 0) {
				return array();
			}

			$data = $query->result_array();
			
			$uids = array();
			foreach ($data as $key => $row) {
				$uids[] = $row['id'];
			}

			$this->load->model(MODULE_API_FOLDER . "/challenge_model");
			$challs = $this->challenge_model->getChallengesCount($uids, $widget_id, $site_id);
			
			foreach ($data as $key => &$row) {
				$row['challenges_count'] = $challs[$row['id']];
			}
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getOpponents($site_id, $widget_id, $user_id, $limit, $offset, $search)
	{
		try
		{
			$user_id = intval($user_id);
			$params = array();
			$where_sql = '';
			$from_sql  = '';
			$limit_sql = '';
			$offset_sql = '';
			$order = "DESC";
			//$params[] = $site_id;
			$on_widget = "";

			if(!is_null($site_id))
			{
				$where_sql .= " AND UB.site_id = " . $site_id;
				//$params[] = $site_id;
			}
			if(!is_null($widget_id))
			{
				$where_sql .= " AND UB.widget_id = " . $widget_id;
				//$params[] = $widget_id;
			}
			if(!is_null($user_id))
			{
				$where_sql .= " AND UP.id <> " . $user_id;
				//$params[] = $user_id;
			}
			if(!is_null($search))
			{
				$search      = strtoupper($search);
				$searchArray = explode(" ", $search); 
				$tokenQty = 0;
				$whereSqlTokens = array();
				foreach ($searchArray as $tokenValue) {
					$tokenValue = trim($tokenValue);
					// TODO::just compare first 2 tokens
					if(strlen($tokenValue) > 0  && $tokenQty < 2)
					{
						$tokenQty = $tokenQty + 1;
						$tokenValue = "%" . $tokenValue . "%";
						$whereSqlTokens[] = " UPPER(UP.first_name) Like ? OR UPPER(UP.last_name) Like ? ";
						
						$params[] = $tokenValue;
				    	$params[] = $tokenValue;
					}
				}

				$where_sql .= " AND (" . implode(" OR " , $whereSqlTokens) . ") ";
			}	
			if(!is_null($limit))
			{
				$limit_sql = " LIMIT " . $limit;
				//$params[] = $limit;
			}
			if(!is_null($offset))
			{
				$offset_sql = " OFFSET " . $offset;
				//$params[] = $offset;
			}

			$sql = "SELECT  DISTINCT UP.id,
							UP.first_name,
							UP.last_name,
							UP.image
							FROM ". TB_USERSPROFILES ." AS UP 
							INNER JOIN " . TB_USERSBALANCES ." AS UB ON (UB.user_id = UP.id)
							WHERE UP.blocked = 0 " . $where_sql .
							" ORDER BY UB.balance DESC " . $limit_sql . $offset_sql;
			
			$query = nut_db::query($sql, $params);
			
			$data = $query->num_rows() > 0 ? $query->result_array() : array();
			
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}	

	public function getUserActivityStatistics($user_id, $widget_id, $site_id)
	{
		try
		{
			$this->load->model(MODULE_API_FOLDER . "/bets_model");
			$this->load->model(MODULE_API_FOLDER . "/challenge_model");
			$betsData = $this->bets_model->getBetsStatData($user_id, $widget_id, $site_id);
			$challengesData = $this->challenge_model->getChallengesStatData($user_id, $widget_id, $site_id);

			$betsStat = array('allAmount' => 0, 'allWonAmmount' => 0, 'betsCount' => 0, 'wonCount' => 0, 'loosCount' => 0, 'waitingCount' => 0);
			foreach ($betsData as $key => $row) {
				$betsStat['allAmount']      += $row['amount'];
				$betsStat['allWonAmmount']  += $row['won_amount'];
				$betsStat['wonCount']       = $row['result_status'] == 2 ? $betsStat['wonCount']  + 1 : $betsStat['wonCount'];
				$betsStat['loosCount']      = $row['result_status'] == 1 ? $betsStat['loosCount']  + 1 : $betsStat['loosCount'];
				$betsStat['waitingCount']   = $row['result_status'] == -1 ? $betsStat['waitingCount']  + 1 : $betsStat['waitingCount'];
			}
			$betsStat['betsCount'] = count($betsData);

			$challengesStat = array('allAmount' => 0, 'allWonAmmount' => 0, 'challengesCount' => 0, 'wonCount' => 0, 'loosCount' => 0, 'waitingCount' => 0);
			foreach ($challengesData as $key => $row) {
				$challengesStat['allAmount']       += $row['amount'];
				$challengesStat['allWonAmmount']   += $row['result_status'] == 2 ? $row['won_amount'] + $row['amount'] : $challengesStat['allWonAmmount'];
				$challengesStat['wonCount']        = $row['result_status'] == 2 ? $challengesStat['wonCount']  + 1 : $challengesStat['wonCount'];
				$challengesStat['loosCount']       = $row['result_status'] == 1 ? $challengesStat['loosCount']  + 1 : $challengesStat['loosCount'];
				$challengesStat['waitingCount']    = $row['result_status'] == -1 ? $challengesStat['waitingCount']  + 1 : $challengesStat['waitingCount'];
			}
			$challengesStat['challengesCount'] = count($challengesData);

			$data = array('bets_stat' => $betsStat,
						  'challenges_stat' => $challengesStat);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function updateUserBalances($user_id, $site_id, $widget_id, $balance, $shield_count, $shield_percent, $boost_count, $boost_multiplier)
	{
		try
		{
			if (!is_null($balance) || !is_null($shield_count) || 
				!is_null($shield_percent) || !is_null($boost_count) || !is_null($boost_multiplier))
			{
				$set = "";
				$sets = array();
				if ($balance) {
					$set .= "balance = ?";
					$sets[] = $balance;
				}
				if ($shield_count) {
					$set .= "shield_count = ?";
					$sets[] = $shield_count;
				}
				if ($shield_percent) {
					$set .= "shield_percent = ?";
					$sets[] = $shield_percent;
				}
				if ($boost_count) {
					$set .= "boost_count = ?";
					$sets[] = $boost_count;
				}
				if ($boost_multiplier) {
					$set .= "boost_multiplier = ?";
					$sets[] = $boost_multiplier;
				}

				array_push($sets, $user_id, $site_id, $widget_id);
				$sql = "UPDATE " . TB_USERSBALANCES .
					   " SET " . $set .
					   " WHERE user_id = ? AND site_id = ? AND widget_id = ?";
				$query = nut_db::query($sql, $sets);
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function changeUserIntroState($user_id, $site_id, $widget_id, $show)
	{
		try
		{
			$sql = "UPDATE " . TB_USERSBALANCES .
				   " SET show_intro = ? " .
				   " WHERE user_id = ? AND site_id = ? AND widget_id = ?";
			$query = nut_db::query($sql, array($show, $user_id, $site_id, $widget_id));
			return array('show' => $show);
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function changeUserBalance($user_id, $site_id, $widget_id, $change)
	{
		try
		{
			$sql = "UPDATE " . TB_USERSBALANCES .
				   " SET balance = balance + ? " .
				   " WHERE user_id = ? AND site_id = ? AND widget_id = ?";
			$query = nut_db::query($sql, array($change, $user_id, $site_id, $widget_id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function updateUserProfile($id, $first_name, $last_name, $email, $image, $gender, $blocked, $mode)
	{
		try
		{
			$sets = array();
			$setParams = array();
			if ($first_name) {
				$setParams[] = " first_name = ? ";
				$sets[] = $first_name;
			}
			if ($last_name) {
				$setParams[] = " last_name = ? ";
				$sets[] = $last_name;
			}
			if ($email) {
				$setParams[] = " email = ? ";
				$sets[] = $email;
			}
			if ($image) {
				$setParams[] = " image = ? ";
				$sets[] = $image;
			}
			if ($gender) {
				$setParams[] =  " gender = ? ";
				$sets[] = $gender;
			}
			if (!is_null($blocked)) {
				$setParams[] =  " blocked = ? ";
				$sets[] = $blocked;
			}
			if (!is_null($mode)) {
				$setParams[] = " mode = ? ";
				$sets[] = $mode;
			}			

			$sets[] = $id;
			$sql = "UPDATE " . TB_USERSPROFILES .
				   " SET " . implode(",", $setParams) .
				   " WHERE id = ?";
			$query = nut_db::query($sql, $sets);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}
