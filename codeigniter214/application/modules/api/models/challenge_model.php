<?php

use Respect\Validation\Validator as validator;

class challenge_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
		$this->load->model(MODULE_API_FOLDER . "/bets_model");
	}

	public function getChallengesStatData($user_id, $widget_id, $site_id)
	{
		try
		{
			$sql = "SELECT  ".
							"CH.amount,".
						    "B.won_amount AS won_amount,".
						    "B.result_status AS result_status".
						    
							" FROM ". TB_CHALLENGES ." AS CH INNER JOIN " . TB_BETS . " AS B ON (B.id = CH.publisher_bet_id OR B.id = CH.opponent_bet_id)".
							" WHERE (CH.publisher_id = ? OR CH.opponent_id = ?) AND CH.widget_id = ? AND B.user_id = ?";
			$query = nut_db::query($sql, array($user_id, $user_id, $widget_id, $user_id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getPublicChallenge($widgetId, $souceUserId = null, $offset = 0){
		try
		{
			$filterOwnChallenges = $souceUserId ? " AND  CH.publisher_id <> ? AND (CH.target_user_id = ? OR ISNULL(CH.target_user_id)) " : " ";
			$params =  $souceUserId ? array($widgetId, $souceUserId, $souceUserId) : array($widgetId);
			$offsetSql = $offset > 0 ? " OFFSET "  .  $offset . " " : " ";
			
			$sql = "SELECT  CH.id as challenge_id, 
							CH.publisher_id,
						    CH.publisher_bet_id,
						    CH.game_id,
						    CH.amount,
						    CH.create_stamp,
						    GetUserName(CH.target_user_id) as target_user_name,
						    GetUserImage(CH.target_user_id) as target_user_image,
						    CH.target_user_id,
						    UP.first_name,
						    UP.last_name,
						    UP.image
							FROM ". TB_CHALLENGES ."  AS CH 
							INNER JOIN " . TB_USERSPROFILES ." AS UP ON UP.id = CH.publisher_id 
							INNER JOIN " . TB_GAMES         ." AS G ON G.id   =  CH.game_id
							WHERE CH.widget_id = ? 
							AND ISNULL(CH.opponent_id) "  .  $filterOwnChallenges . " 
							AND G.blocked = 0 
							AND (UNIX_TIMESTAMP() < UNIX_TIMESTAMP(G.start_date) - 600)
							ORDER BY CH.create_stamp DESC LIMIT 1 " . $offsetSql ;
							// AND UNIX_TIMESTAMP(G.start_date) > CURRENT_TIMESTAMP" ;

			$query = nut_db::query($sql, $params);

			$retData =  $query->num_rows() > 0 ? $query->row_array(): array();
			return $retData;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getById($id, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							publisher_id,
						    publisher_bet_id,
						    opponent_id,
						    opponent_bet_id,
						    game_id,
						    target_user_id,
						    create_stamp,
						    accept_stamp
						    
							FROM ". TB_CHALLENGES ."
							WHERE id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): array();

			//$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllChallengesBySite($siteId, $widgetId, $userId, $gameId, $aux_info, $limit)
	{
		try
		{
			$opt = array($siteId);
			$where = "";
			$limitSQL = "";
			$opponentId = "";
			if (!IS_NULL($widgetId)) {
				$where = " AND B.widget_id = ?";
				$opt[] = $widgetId;
			}
			if (!IS_NULL($userId)) {
				$where .= " AND B.user_id = ?";
				$opt[] = $userId;
				$opponentId = " OR CH.opponent_bet_id = B.id";
			}
			if (!IS_NULL($gameId)) {
				$where .= " AND CH.game_id = ?";
				$opt[] = $gameId;
			}			
			if (!IS_NULL($limit)) {
				$limitSQL .= " LIMIT " . intval($limit);
			}

			$sql = "SELECT  CH.id, 
							CH.publisher_id,
						    GetEventTemplateIdByBet(CH.publisher_bet_id) as publisher_event,
						    CH.opponent_id,
						    GetEventTemplateIdByBet(CH.opponent_bet_id) as opponent_event,
						    CH.game_id,
						    GetParticipantName(CH.game_id, 1, 1) as participant1_name,
						    GetParticipantName(CH.game_id, 2, 1) as participant2_name,
						    CH.widget_id,
						    CH.target_user_id,
						    CH.create_stamp,
						    CH.accept_stamp,
						    CH.amount

							FROM ". TB_CHALLENGES ." AS CH INNER JOIN ".TB_BETS." AS B ON (CH.publisher_bet_id = B.id " . $opponentId . ")
							WHERE B.site_id = ? " . $where . " ORDER BY CH.create_stamp DESC" . $limitSQL;
						
			$query = nut_db::query($sql, $opt);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllChallengesByGame($game_id, $aux_info=null)
	{
		try
		{
			$sql = "SELECT  id, 
							publisher_id,
						    publisher_bet_id,
						    opponent_id,
						    opponent_bet_id,
						    game_id,
						    target_user_id,
						    create_stamp,
						    accept_stamp

							FROM ". TB_CHALLENGES ." 
							WHERE game_id = ?";
			
			$query = nut_db::query($sql, array($game_id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			//$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllChallengesByGameForPublishers($game_id, $aux_info=null)
	{
		try
		{
			$sql = "SELECT  id, 
							publisher_id,
						    publisher_bet_id,
						    opponent_id,
						    opponent_bet_id,
						    game_id,
						    target_user_id,
						    create_stamp,
						    accept_stamp

							FROM ". TB_CHALLENGES ." 
							WHERE game_id = ?";
			
			$query = nut_db::query($sql, array($game_id));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$return = $this->group_by_publisher($data);

			//$this->aux_append('', $data, $langid, $aux_info);

			return $return;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function group_by_publisher($data)
	{
		$return = array();
		foreach ($data as $row) {
			if(!array_key_exists('publisher_id', $return))
			{
				$return[$row['publisher_id']] = array();
				$return[$row['publisher_id']][] = $row;
			}
			else {
				$return[$row['publisher_id']][] = $row;
			}
		}
	}

	public function getAllChallengesByUser($userId, $widgetId, $limit, $offset, $langId)
	{
		try
		{
			$sql = "SELECT  CH.id, 
							CH.publisher_id,
						    CH.publisher_bet_id,
						    CH.opponent_id,
						    CH.opponent_bet_id,
						    CH.game_id as game_id,
						    B.won_amount as bet_wonamount,
						    B.coefficient as bet_coefficient,
						    CH.amount as ch_amount,
						    B.result_status, 
						    GetGameStamp(CH.game_id) as game_stamp,
						    GetGameResultTxt(CH.game_id) as game_result,
    						GetParticipantName(CH.game_id, 1 , " . $langId . ") as pname1,
						    GetParticipantLogo(CH.game_id, 1) as plogo1,
						    GetParticipantName(CH.game_id, 2, " . $langId . ") as pname2,
						    GetParticipantLogo(CH.game_id, 2) as plogo2,
						    CH.target_user_id,
						    UNIX_TIMESTAMP(CH.create_stamp) as create_stamp,
						    UNIX_TIMESTAMP(CH.accept_stamp) as accept_stamp,
						    GetUserName(CH.publisher_id)    as publisher_name,
						    GetUserImage(CH.publisher_id)   as publisher_image,
						    GetEventTemplateIdByBet(CH.publisher_bet_id) as publisher_etid,
						    GetUserName(CH.opponent_id)      as opponent_name,
						    GetUserImage(CH.opponent_id)     as opponent_image,
						    GetUserName(CH.target_user_id)   as target_opname,
						    GetUserImage(CH.target_user_id)  as target_opimage,
						    GetEventTemplateIdByBet(CH.opponent_bet_id) as opponent_etid
							FROM ". TB_CHALLENGES ." AS CH
							INNER JOIN " . TB_BETS . " AS B ON (B.id = CH.publisher_bet_id OR B.id = CH.opponent_bet_id)  
							WHERE CH.widget_id = ?
							AND B.user_id = ? 
							ORDER BY CH.create_stamp DESC ";

			$params = array($widgetId, $userId);

			if($limit)
			{
				$limit = intval($limit);
				$sql = $sql . " LIMIT " . $limit; 
			}

			if($offset)
			{
				$offset = intval($offset);
				$sql = $sql . " OFFSET " . $offset; 
			}
			
			$query = nut_db::query($sql, $params);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getChallengesCount($user_ids, $widget_id, $site_id)
	{
		try
		{
			$params = array();
			$widgetId = '';
			if ($widget_id) {
				$widgetId .= " AND CH.widget_id = ?";
				$params[] = $widget_id;
			}
			$values = implode(',', array_fill(0, count($user_ids), "?"));
			$params = array_merge($params, $user_ids);
			$sql = "SELECT  UP.id, COUNT(CH.id) as qty ".
							" FROM ". TB_USERSPROFILES ." AS UP INNER JOIN " . TB_USERSBALANCES . " AS UB ON UB.user_id = UP.id".
							" LEFT JOIN " . TB_CHALLENGES . " AS CH ON ((UP.id = CH.publisher_id OR UP.id = CH.opponent_id) ". $widgetId .")" .
							" WHERE UP.id IN (" . $values . ") " .
							" GROUP BY UP.id";
			
			$query = nut_db::query($sql, $params);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$dataIds = array();
			foreach ($data as $key => $row) {
				$dataIds[$row['id']] = $row['qty'];
			}

			return $dataIds;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function insertChallenge($challenge)
	{
		try
		{
			nut_db::transactionStart("insertChallenge");

			// check bet min/max amount
			if ($challenge['amount'] < MIN_CHALLANGE_AMOUNT || $challenge['amount'] > MAX_CHALLANGE_AMOUNT) {
				throw new Api_exception("Challenge amount should be between ".MIN_CHALLANGE_AMOUNT." and ".MAX_CHALLANGE_AMOUNT.".", Api_exception::$CHALLENGE_ERROR_CODE1);
			}

			// check balance to accept challenge
			$this->checkBalance($challenge['site_id'], $challenge['widget_id'], $challenge['source_user_id'], $challenge['amount']);

			// check coefficient and basis between server and client sent
			$this->checkEventData($challenge['event_id'], $challenge['coefficient'], $challenge['basis']);

			// insert Bet
			$basis = 0;
			if (array_key_exists('basis', $challenge)) 
			{
				$basis = $challenge['basis'] != "" ? $challenge['basis'] : 0;
			}

			$betData = array(
						 'user_id'      => $challenge['source_user_id'],
						 'site_id'      => $challenge['site_id'],
						 'widget_id'    => $challenge['widget_id'],
						 'amount'       => $challenge['amount'],
						 'won_amount'   => 0,
						 'coefficient'  => $challenge['coefficient'],
						 'bet_type'		=> 0,
						 'event_chain'  => array()
						 );

			$betData["event_chain"][] = array('game_id'     => $challenge['game_id'],
											  'event_id'    => $challenge['event_id'], 
						 					  'coefficient' => $challenge['coefficient'],
						 					  'basis'       => $basis);

			$bid = $this->bets_model->insertBet($betData, "chall");

			if(array_key_exists('target_user_id', $challenge))
			{
				$sql = "INSERT INTO " . TB_CHALLENGES . " (widget_id, publisher_bet_id, publisher_id, target_user_id, game_id, amount, create_stamp) VALUES (?, ?, ? , ? , ? , ?, CURRENT_TIMESTAMP)";
   				$query = nut_db::query($sql, array($challenge['widget_id'], $bid, $challenge['source_user_id'], $challenge['target_user_id'], $challenge['game_id'], $challenge['amount']));
   			}
   			else 
   			{
   				$sql = "INSERT INTO " . TB_CHALLENGES . " (widget_id, publisher_bet_id, publisher_id, game_id, amount, create_stamp) VALUES (?, ?, ? , ? , ?, CURRENT_TIMESTAMP)";
   				$query = nut_db::query($sql, array($challenge['widget_id'], $bid, $challenge['source_user_id'], $challenge['game_id'], $challenge['amount']));
   			}

			$id = nut_db::getLastID(TB_CHALLENGES, 'id');
			//$this->aux_insert($ids, $bet);
			nut_db::transactionCommit("insertChallenge");
			return $id;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertChallenge");
			throw $e;
		}
	}

	private function checkBalance($site_id, $widget_id, $user_id, $amount) {
		// validate balance amount to bet
		$sql =  "SELECT balance ".
				" FROM " . TB_USERSBALANCES . 
				" WHERE site_id = ? AND widget_id = ? AND user_id = ?" ;

		$query = nut_db::query($sql, array($site_id, $widget_id, $user_id));
		$balance =  $query->num_rows() > 0 ? $query->row_array(): array();
		
		if($balance['balance'] - $amount < 0) {
			throw new Api_exception('No enough balanec to accept the challenge.', Api_exception::$CHALLENGE_ERROR_CODE2);
		}
	}

	private function checkEventData($event_id, $coefficient, $basis) {
		// validate balance amount to bet
		$sql =  "SELECT coefficient, basis, result_status, blocked ".
				" FROM " . TB_EVENTS . 
				" WHERE id = ?" ;

		$query = nut_db::query($sql, array($event_id));
		$data =  $query->num_rows() > 0 ? $query->row_array(): array();

		if($data['blocked'] == 1) { // event is blocked
			throw new Api_exception('Selected event is blocked.', Api_exception::$CHALLENGE_ERROR_CODE3);
		}
		else if ($data['coefficient'] <= 1) {
			throw new Api_exception('Event has no value.', Api_exception::$CHALLENGE_ERROR_CODE4);
		}
		else if($data['result_status'] == 1 || $data['result_status'] == 2) // result was set
		{
			throw new Api_exception('Event already has result.', Api_exception::$CHALLENGE_ERROR_CODE5);
		}
		else if($coefficient != $data['coefficient'] || $basis != $data['basis']) {
			throw new Api_exception('Coefficient or balance was updated. Please refresh the page and try again.', Api_exception::$CHALLENGE_ERROR_CODE6);
		}
	}

	public function acceptChallenge($challenge)
	{
		try
		{
			// check coefficient and basis between server and client sent
			$this->checkEventData($challenge['event_id'], $challenge['coefficient'], $challenge['basis']);

			if(!nut_db::is_free_lock('CH_'.$challenge['challenge_id'])) {
				throw new Api_exception('Please try again.', Api_exception::$CHALLENGE_ERROR_CODE7);
			}

			if(!nut_db::get_lock('CH_'.$challenge['challenge_id'])) {
				throw new Api_exception('Please try again.', Api_exception::$CHALLENGE_ERROR_CODE7);
			}

			nut_db::transactionStart("acceptChallenge");

			$sql = "SELECT  CH.id, 
							CH.publisher_id,
						    CH.publisher_bet_id,
						    CH.opponent_id,
						    CH.opponent_bet_id,
						    CH.game_id,
						    CH.target_user_id,
						    CH.create_stamp,
						    CH.accept_stamp,
						    B.site_id,
						    B.widget_id,
						    B.amount,
						    UNIX_TIMESTAMP(G.start_date) - 600 as game_start
						    ".
						    
							" FROM ". TB_CHALLENGES ." AS CH INNER JOIN ". TB_BETS ." AS B ON CH.publisher_bet_id = B.id ".
							" INNER JOIN ". TB_GAMES . " AS G ON G.id = CH.game_id " .
							" WHERE CH.id = ?";
			
			$query = nut_db::query($sql, array($challenge['challenge_id']));
			$data =  $query->num_rows() > 0 ? $query->row_array(): array();

			if (time() > $data['game_start']) {
				throw new Api_exception('Game already started.', Api_exception::$CHALLENGE_ERROR_CODE8);
			}

			$basis = 0;
			if (array_key_exists('basis', $challenge)) {
				$basis = $challenge['basis'] != "" ? $challenge['basis'] : 0;
			}

			// check balance to accept challenge
			$this->checkBalance($data['site_id'], $data['widget_id'], $challenge['opponent_user_id'], $data['amount']);

			$bet = array(
						 'user_id'      => $challenge['opponent_user_id'],
						 'site_id'      => $data['site_id'],
						 'widget_id'    => $data['widget_id'],
						 'amount'       => $data['amount'],
						 'won_amount'   => 0,
						 'coefficient'  => $challenge['coefficient'],
						 'bet_type'		=> 0,
						 'event_chain'  => array()
						 );

			$bet["event_chain"][] = array('game_id'  => $data['game_id'], 
						 				  'event_id' => $challenge['event_id'], 
						 				  'coefficient' => $challenge['coefficient'],
						 				  'basis' => $basis);

			//$bid = $this->bets_model->insertBet($bet);			

			if($data['target_user_id'] != NULL && $challenge['opponent_user_id'] != $data['target_user_id'])
			{
				throw new Api_exception('Opponent user do not match to target user(opponent).', Api_exception::$CHALLENGE_ERROR_CODE9);
			}
			else if ($data['target_user_id'] != NULL && $data['opponent_id'] != NULL)
			{
				throw new Api_exception('Target opponent already accepted the chellenge.', Api_exception::$CHALLENGE_ERROR_CODE10);
			}
			else if ($data['target_user_id'] != NULL && $challenge['opponent_user_id'] == $data['target_user_id'])
			{
				$bid = $this->bets_model->insertBet($bet, 'chall');
				$sql = "UPDATE " . TB_CHALLENGES . " SET opponent_id = ?, opponent_bet_id = ?, accept_stamp = CURRENT_TIMESTAMP WHERE id = ?";
				$query = nut_db::query($sql, array($challenge['opponent_user_id'], $bid, $challenge['challenge_id']));
			}
			else if ($data['target_user_id'] == NULL)
			{
				if ($data['opponent_id'] == NULL AND $data['publisher_id'] != $challenge['opponent_user_id'])
				{
					$bid = $this->bets_model->insertBet($bet, 'chall');
					$sql = "UPDATE " . TB_CHALLENGES . " SET opponent_id = ?, opponent_bet_id = ?, accept_stamp = CURRENT_TIMESTAMP WHERE id = ?";
					$query = nut_db::query($sql, array($challenge['opponent_user_id'], $bid, $challenge['challenge_id']));
				}
				else if ($data['opponent_id'] == NULL AND $data['publisher_id'] == $challenge['opponent_user_id']) {
   					throw new Api_exception('Challenge created by you.', Api_exception::$CHALLENGE_ERROR_CODE11);
				} else {
					throw new Api_exception('Public challenge already accepted.', Api_exception::$CHALLENGE_ERROR_CODE12);
				}
			}

			$this->load->model(MODULE_API_FOLDER . "/users_model");
			$currentBalance = $this->users_model->getPlayerBalance($data['site_id'], $data['widget_id'], $challenge['opponent_user_id']);

			nut_db::transactionCommit("acceptChallenge");
			nut_db::release_lock('Ch_'.$challenge['challenge_id']);
			return array("id" => $challenge['challenge_id'], "accepted" => true, "balance" => $currentBalance);
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("acceptChallenge");
			nut_db::release_lock('Ch_'.$challenge['challenge_id']);
			throw $e;
		}
	}	

}