<?php

use Respect\Validation\Validator as validator;

class participants_model extends CI_Model {

	private static $AUX_TRANSLATION = "translations";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getParticipantInfoById($id, $language_id, $aux_info)
	{
		try
		{
			$data = NULL;
			$params = array();
			$params[] = $id;

			$sql = "SELECT  id,
							info,
							logo,
							country_id,
							sport_id,
							priority,
							name,
							GetTranslation(". $language_id.",". $id.",". chili_translate::$PARTICIPANTS.") AS tname
					FROM ". TB_PARTICIPANTS." WHERE id = ? ";

			$query = nut_db::query($sql, $params);
			$data = $query->num_rows() > 0 ? $query->row_array() : NULL;

			if($data == NULL) {
				return array();
			}
			
			$result = array($data);
			$this->aux_appender($result, $aux_info);

			$data = $result[0];

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getParticipantsDataByIds($ids, $langid, $aux_info)
	{
		try
		{
			$qArray = array_fill(0, count($ids), "?");

			$sql = "SELECT  id, 
							info,
						    GetTranslation(". $langid .", id ,". chili_translate::$PARTICIPANTS.") AS tname,
						    logo,
						    country_id,
						    sport_id,
						    priority,
						    name
							FROM ". TB_PARTICIPANTS ."
							WHERE id in (" . implode(",", $qArray) . " )"; 
						
			$query = nut_db::query($sql, $ids);
		
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			if (count($data) == 0)  { 
				return $data;
			}

			$this->aux_appender($data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function getParticipantsByCountryId($id, $skid, $language_id, $aux_info)
	{
		try
		{
			$params = array();
			$params[] = $skid;
			$params[] = $id;

			$sql = "SELECT  P.id,
							P.info,
							P.logo,
							P.country_id,
							P.sport_id,
							P.priority,
							name,
							GetTranslation(".$language_id.", P.id,". chili_translate::$PARTICIPANTS.") AS tname
                    FROM ". TB_PARTICIPANTS." AS P WHERE P.sport_id = ?  AND P.country_id = ? 
                    ORDER BY name ";

			$query = nut_db::query($sql, $params);
			$data =  $query->num_rows() > 0 ? $query->result_array():  array();

			if (count($data) == 0)  { 
				return $data;
			}

			$data = $this->changeArrayKeysWithItemID($data);

			$this->aux_appender($data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function changeArrayKeysWithItemID ($arr) {
		$new = array();
		foreach($arr as $key => $val) {
			$new[$val['id']] = $val;
		}
		return $new;
	}

	public function getParticipantsByCompetitionId($id, $language_id, $aux_info)
	{
		try
		{
			$params = array();
			$params[] = $id;

			$sql = "SELECT  P.id,
							P.info,
							P.logo,
							P.country_id,
							P.sport_id,
							P.priority,
							P.name,
							GetTranslation(".$language_id.", P.id,". chili_translate::$PARTICIPANTS.") AS tname,
							S.score,
							S.played_games,
							S.wins,
							S.looses,
							S.draws,
							S.goals_out,
							S.goals_in,
							S.competition_id
			 		FROM 		" . TB_PARTICIPANTS . " AS P 
			 		INNER JOIN  " . TB_COMPETITION_PARTICIPANTS ." AS S ON S.participant_id = P.id 
			 		WHERE S.competition_id = ?  ORDER BY name ";
			
			$query = nut_db::query($sql, $params);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			if (count($data) == 0)  { 
				return $data;
			}

			$data = $this->changeArrayKeysWithItemID($data);

			$this->aux_appender($data, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getParticipantsByGameId($id, $language_id, $aux_info)
	{
		try
		{
			$params = array();
			$params[] = $id;

			$sql = "SELECT  P.id,
							P.info,
							P.logo,
							P.country_id,
							P.sport_id,
							P.priority,
							P.name,
							GetTranslation(".$language_id.", P.id,". chili_translate::$PARTICIPANTS.") AS tname,
							GP.game_id,
							GP.order
					FROM " 		 . TB_PARTICIPANTS ." AS P 
					INNER JOIN " . TB_GAMEPARTICIPANTS ." AS GP ON GP.participant_id = P.id 
					WHERE GP.game_id = ? ORDER BY GP.order";
			
			$query = nut_db::query($sql, $params);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			if (count($data) == 0)  { 
				return $data;
			}

			//$data = $this->changeArrayKeysWithItemID($data);

			$this->aux_appender($data, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getParticipantsByGameIDs($gameIds, $language_id, $aux_info)
	{
		try
		{

			$values = implode(',', array_fill(0, count($gameIds), "?"));
			$sql = "SELECT  P.id,
							P.info,
							P.logo,
							P.country_id,
							P.sport_id,
							P.priority,
							P.name,
							GetTranslation(".$language_id.", P.id,". chili_translate::$PARTICIPANTS.") AS tname,
							GP.game_id,
							GP.order
					FROM " 		 . TB_PARTICIPANTS ." AS P 
					INNER JOIN " . TB_GAMEPARTICIPANTS ." AS GP ON GP.participant_id = P.id 
					WHERE GP.game_id in (".$values.") ORDER BY GP.order";
			
			$query = nut_db::query($sql, $gameIds);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			if (count($data) == 0)  { 
				return $data;
			}

			//$data = $this->changeArrayKeysWithItemID($data);

			$this->aux_appender($data, $aux_info);
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function aux_appender(&$data, $aux_info)
	{
		$ids = array();
		foreach($data as $key => $val) {
			$ids[] = $val['id'];
		}
		
		foreach($data as $ind => $row) {
			if(!array_key_exists("aux_info", $data[$ind])) {
				$data[$ind]["aux_info"] = array();
			}
		}

		if(is_string($aux_info))
		{
			$this->aux_append($ids, $data, $aux_info);
		}

		if(is_array($aux_info) && count($aux_info) > 0)
		{

			foreach($aux_info as $aux)
			{
				$this->aux_append($ids, $data, $aux);
			}
		}		
	}

	private function aux_append(&$ids, &$data, $aux_name)
	{
		switch($aux_name)
		{
			case self::$AUX_TRANSLATION:
				$translations = chili_translate::getFullTranslationsByIDs($ids, chili_translate::$PARTICIPANTS);

				foreach($translations as $key => $val) {
					foreach($data as $ind => $row) {
						if($key == $row['id']) {
							$data[$ind]["aux_info"][self::$AUX_TRANSLATION] = $val;
						}
					}
				}
				break;
		}		
	}

	public function insertParticipants($participants)
	{
		try
		{
			nut_db::transactionStart("insertParticipants");
			$values = nut_utils::sanitize_keys($participants, array('name', 'info', 'logo', 'country_id', 'sport_id', 'priority'), TRUE, NULL);

			nut_db::insert_batch(TB_PARTICIPANTS, $values);
			
			$ids = nut_db::getLastID(TB_PARTICIPANTS, 'id', count($participants));

			$ids = is_array($ids) ? $ids : array($ids);	

			foreach ($ids as $k => $v)
			{
				$id = $v;
				$aux_info = element('aux_info', $participants[$k]);

				$translations = $aux_info ? element('translations', $aux_info) : FALSE;
				
				if ($translations)
				{
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						chili_translate::addTranslation($langId, $id, chili_translate::$PARTICIPANTS, $value);
					}
				}
			}
			
			nut_db::transactionCommit("insertParticipants");
			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertParticipants");
			throw $e;
		}
	}


	public function updateParticipants($participants)
	{
		try
		{
			nut_db::transactionStart("updateParticipants");
			$values = nut_utils::sanitize_keys($participants, array('id', 'name', 'info', 'logo', 'country_id', 'sport_id', 'priority'));
			nut_db::update_batch(TB_PARTICIPANTS, $values, 'id');

			foreach ($participants as $pdata)
			{
				// update translations
				$id = $pdata['id'];
				$aux_info = element('aux_info', $pdata);
				$translations = $aux_info ? element(self::$AUX_TRANSLATION, $aux_info) : FALSE;
				if ($translations)
				{
					$data = array();
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						$arr    = chili_translate::getTranslation($langId, $id, chili_translate::$PARTICIPANTS);
						if (!is_null($arr))
						{
							$data[] = array('lang' => $langId , 'id' => $id , 'type' => chili_translate::$PARTICIPANTS, 'text' => $value );
						}
						else {
							chili_translate::addTranslation($langId, $id, chili_translate::$PARTICIPANTS, $value);
						}
					}
					chili_translate::updateTranslations($data);
				}				
			}

			nut_db::transactionCommit("updateParticipants");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateParticipants");
			throw $e;
		}
	}

	public function removeParticipant($id)
	{
		try
		{
			nut_db::transactionStart("removeParticipant");
			$logoFileName = $this->getLogo($id);
			$sql = "DELETE FROM " . TB_PARTICIPANTS  .  " WHERE id = ?";
			$query = nut_db::query($sql, $id);	

			chili_translate::deleteAllTranslations(chili_translate::$PARTICIPANTS, $id);

			$imageFullPath = asset_model::getAssetFolder("participants") . $logoFileName;
			if($logoFileName)
			{
				asset_model::removeAsset($imageFullPath);
			}
			
			nut_db::transactionCommit("removeParticipant");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeParticipant");
			throw $e;
		}
	}

	public  function getLogo($id)
	{
		try
		{
			$sql = "SELECT  logo FROM ". TB_PARTICIPANTS ." WHERE id = ? ";
			$query = nut_db::query($sql, $id);
			$data =  $query->num_rows() > 0 ? $query->row_array(0): NULL;
			$logo = !is_null($data) ? $data['logo'] : null;
			return $logo;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public  function setLogo($id,$filename)
	{
		try
		{
			$filename = trim($filename);
			$sql = "UPDATE " . TB_PARTICIPANTS . " SET logo = ? WHERE id = ?";
			$query = nut_db::query($sql, array($filename, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}