<?php

use Respect\Validation\Validator as validator;

class news_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getNewsById($id)
	{
		try
		{
			$sql = "SELECT  id, 
							type,
						    link,
						    refer_table,
						    field_id,
						    create_stamp
							FROM ". TB_NEWS ." 
							WHERE id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): NULL;

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getNewsByIds($ids)
	{
		try
		{
			$values = implode(',', array_fill(0, count($ids), "?"));
			$sql = "SELECT  id, 
							type,
						    link,
						    refer_table,
						    field_id,
						    create_stamp,
						    description
							FROM ". TB_NEWS ." 
							WHERE id in (".$values.") ORDER BY create_stamp ASC";
			
			$query = nut_db::query($sql, $ids);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function getAllNewsBy($type, $field_id, $refer_table, $limit, $offset)
	{
		try
		{
			$params = array();
			$where_sql = '';
			$from_sql  = '';
			$limit_sql = '';
			$offset_sql = '';
			$order = "DESC";

			if(!is_null($refer_table))
			{
				$where_sql .= " AND refer_table=? ";
				$params[] = $refer_table;
				if(!is_null($field_id))
				{
					$where_sql .= " AND field_id=? ";
					$params[] = $field_id;
				}
			}

			if(!is_null($type))
			{
				$where_sql .= " AND type=? ";
				$params[] = $type;
			}
			
			if(!is_null($limit))
			{
				$limit_sql = " LIMIT ?";
				$params[] = intval($limit);
			}

			if(!is_null($offset))
			{
				$offset_sql = " OFFSET ?";
				$params[] = $offset;
			}

			$sql = "SELECT  id, 
							type,
						    link,
						    refer_table,
						    field_id,
						    description,
						    create_stamp
							FROM ". TB_NEWS ."
							WHERE TRUE " . $where_sql 
							. " ORDER BY create_stamp ". $order . $limit_sql . $offset_sql;
			
			$query = nut_db::query($sql, $params);

			if ($query->num_rows() <= 0) {
				return array();
			}
			$data = $query->result_array();
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function insertNews($news)
	{
		try
		{
			nut_db::transactionStart("insertNews");
			$values = nut_utils::sanitize_keys($news, array('type', 'field_id', 'refer_table', 'link', 'description'), TRUE, NULL);

			nut_db::insert_batch(TB_NEWS, $values);
			
			$ids = nut_db::getLastID(TB_NEWS, 'id', count($news));

			$ids = !is_array($ids) ? array($ids) : $ids;

			nut_db::transactionCommit("insertNews");

			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertNews");
			throw $e;
		}
	}

	public function updateGames($news)
	{
		try
		{
			nut_db::transactionStart("updateNews");
			$values = nut_utils::sanitize_keys($news, array('type', 'field_id', 'refer_table', 'link', 'description'));
			nut_db::update_batch(TB_NEWS, $values, 'id');

			nut_db::transactionCommit("updateNews");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateNews");
			throw $e;
		}
	}

	public function removeNews($id)
	{
		try
		{
			nut_db::transactionStart("removeNews");

			$sql = "DELETE FROM " . TB_NEWS  .  " WHERE id = ?";
			$query = nut_db::query($sql, $id);	

			nut_db::transactionCommit("removeNews");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeNews");
			throw $e;
		}
	}

}