<?php

use Respect\Validation\Validator as validator;

class competitions_model extends CI_Model {

	private static $AUX_TRANSLATION = "translations";
	private static $AUX_PARTICIPANTS = "participants";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getAllCompetitions($active, $langid, $aux_info)
	{
		try
		{
			// TODO use common date class
			$sql_where = '';
			if($active) 
			{
				$sql_where = $active ? " WHERE C.blocked = 0 " : "";
			}

			$sql = "SELECT  C.id, 
							name,
						    GetTranslation(". $langid .",  C.id  ,". chili_translate::$COMPETITIONS.") AS tname,
						    C.league_id,
						    C.logo,
						    UNIX_TIMESTAMP(C.start_date) as start_date,
						    UNIX_TIMESTAMP(C.end_date) as end_date,
						    L.priority,
						    L.sport_id,
						    L.country_id,
						    C.blocked
							FROM ". TB_COMPETITIONS . " AS C INNER JOIN " . TB_LEAGUES . " AS L ON C.league_id = L.id "
							. $sql_where;
			
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): NULL;

			$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}


	public function getAllCompetitionsBySportId($skid, $active, $langid, $aux_info)
	{
		try
		{
			// TODO use common date class
			$params[] = $skid;
			$sql_where = '';

			if($active) 
			{
				$sql_where = $active ? " AND C.blocked = 0" : "";
			}

			$sql = "SELECT  C.id, 
						    GetTranslation(". $langid .",  C.id  ,". chili_translate::$COMPETITIONS.") AS name,
						    C.league_id,
						    C.logo,
						    UNIX_TIMESTAMP(C.start_date) as start_date,
						    UNIX_TIMESTAMP(C.end_date) as end_date,
							L.priority,
						    L.sport_id,
						    L.country_id,
						    C.blocked					    
							FROM ". TB_COMPETITIONS . " AS C INNER JOIN " . TB_LEAGUES . " AS L ON C.league_id = L.id "
							." WHERE L.sport_id = ? " . $sql_where;
			
			$query = nut_db::query($sql, $params);
			$data =  $query->num_rows() > 0 ? $query->result_array(): NULL;

			$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}


	public function getCompetitionsByLeagueId($lid, $langid, $aux_info, $isActive = false)
	{
		try
		{
			$sql = "SELECT  C.id,
							C.name, 
						    GetTranslation(". $langid .",  C.id  ,". chili_translate::$COMPETITIONS.") AS tname,
						    C.league_id,
						    C.logo,
						    UNIX_TIMESTAMP(C.start_date) as start_date,
						    UNIX_TIMESTAMP(C.end_date) as end_date,
							L.priority,
						    L.sport_id,
						    L.country_id,
						    C.blocked
							FROM ". TB_COMPETITIONS . " AS C INNER JOIN " . TB_LEAGUES . " AS L ON C.league_id = L.id "
							." WHERE C.league_id = ? ";

							if($isActive)	
							{	
								$sql . " AND C.blocked = 0";
							}
			
			$query = nut_db::query($sql, array($lid));
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$this->aux_append('', $data, $langid, $aux_info);

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}	


	public function getCompetitionById($id, $langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  C.id, 
							C.name,
						    GetTranslation(". $langid .",  C.id  ,". chili_translate::$COMPETITIONS.") AS tname,
						    C.league_id,
						    C.logo,
						    UNIX_TIMESTAMP(C.start_date) as start_date,
						    UNIX_TIMESTAMP(C.end_date) as end_date,
							C.priority,
						    L.sport_id,
						    L.country_id,
						    C.blocked					    
							FROM ". TB_COMPETITIONS . " AS C INNER JOIN " . TB_LEAGUES . " AS L ON C.league_id = L.id "
							." WHERE C.id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): NULL;

			$this->aux_append($id, $data, $langid, $aux_info);
			
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

	public function insertCompetitions($competitions)
	{
		try
		{
			nut_db::transactionStart("insertCompetitions");
			$values = nut_utils::sanitize_keys($competitions, array('id', 'name', 'league_id', "priority", "logo",'start_date', 'end_date'), TRUE, NULL);

			nut_db::insert_batch(TB_COMPETITIONS, $values);
			$ids = nut_db::getLastID(TB_COMPETITIONS, 'id', count($competitions));

			$ids = is_array($ids) ? $ids : array($ids);	
			
			foreach ($ids as $k => $v)
			{
				$id = $v;
				$aux_info = element('aux_info', $competitions[$k]);

				$translations = $aux_info ? element('translations', $aux_info) : FALSE;
				
				if ($translations)
				{
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						chili_translate::addTranslation($langId, $id, chili_translate::$COMPETITIONS, $value);
					}
				}
			}
			
			nut_db::transactionCommit("insertCompetitions");
			return $ids;
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("insertCompetitions");
			throw $e;
		}
	}	

	public function updateCompetitions($competitions)
	{
		try
		{
			nut_db::transactionStart("updateCompetitions");
			$values = nut_utils::sanitize_keys($competitions, array('id', 'name', 'league_id', "priority",'start_date', 'end_date', 'blocked'));
			nut_db::update_batch(TB_COMPETITIONS, $values, 'id');
			
			foreach ($competitions as $cdata)
			{
				$id = $cdata['id'];
				$aux_info = element('aux_info', $cdata);
				$translations = $aux_info ? element('translations', $aux_info) : FALSE;
				if ($translations)
				{
					$data = array();
					foreach($translations as $key => $value)
					{
						$langId = chili_translate::getLangId($key);
						$arr    = chili_translate::getTranslation($langId, $id, chili_translate::$COMPETITIONS);
						if (!is_null($arr))
						{
							$data[] = array('lang' => $langId , 'id' => $id , 'type' => chili_translate::$COMPETITIONS, 'text' => $value);
						}
						else 
						{
							chili_translate::addTranslation($langId, $id, chili_translate::$COMPETITIONS, $value);
						}
					}
					chili_translate::updateTranslations($data);
				}
			}
			
			nut_db::transactionCommit("updateCompetitions");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateCompetitions");
			throw $e;
		}
	}


	public function updateCompetitionParticipants($id, $participants)
	{
		try
		{
			nut_db::transactionStart("updateCompetitionParticipants");
			foreach ($participants as $index => $data)
			{
				if($data['action'] == "0") // 0 - remove
				{
					$sql = "DELETE FROM " . TB_COMPETITION_PARTICIPANTS . " WHERE competition_id = ? AND participant_id = ?";
					$query = nut_db::query($sql, array($id, $data['id']));
				}
				elseif($data['action'] == "1") // 1 - add
				{
					$sql = "INSERT INTO " . TB_COMPETITION_PARTICIPANTS . " SET competition_id = ?, participant_id = ?, score = ?, played_games = ?, wins = ?, looses = ?, draws = ?, goals_in = ?, goals_out = ?";
					$query = nut_db::query($sql, array($id, $data['id'], $data['score'], $data['played_games'], $data['wins'], $data['looses'], $data['draws'], $data['goals_in'], $data['goals_out']));
				}
				elseif($data['action'] == "2") // 2 - update value
				{
					$sql = "UPDATE " . TB_COMPETITION_PARTICIPANTS . " SET score = ?, played_games = ?, wins = ?, looses = ?, draws = ?, goals_in = ?, goals_out = ? WHERE competition_id = ? AND participant_id = ?";
					$query = nut_db::query($sql, array($data['score'], $data['played_games'], $data['wins'], $data['looses'], $data['draws'], $data['goals_in'], $data['goals_out'], $id, $data['id']));
				}
			}
			nut_db::transactionCommit("updateCompetitionParticipants");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateCompetitionParticipants");
			throw $e;
		}

	}

	public  function getLogo($id)
	{
		try
		{
			$sql = "SELECT  logo FROM ". TB_COMPETITIONS ." WHERE id = ? ";
			$query = nut_db::query($sql, $id);
			$data =  $query->num_rows() > 0 ? $query->row_array(0): NULL;
			$logo = !is_null($data) ? $data['logo'] : null;
			return $logo;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public  function setLogo($id,$filename)
	{
		try
		{
			$filename = trim($filename);
			$sql = "UPDATE " . TB_COMPETITIONS . " SET logo = ? WHERE id = ?";
			$query = nut_db::query($sql, array($filename, $id));
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	
	public function removeCompetition($id)
	{
		try
		{
			nut_db::transactionStart("removeCompetition");
			$logoFileName = $this->getLogo($id);
			$sql = "DELETE FROM " . TB_COMPETITIONS  .  " WHERE id = ?";
			$query = nut_db::query($sql, $id);	

			chili_translate::deleteAllTranslations(chili_translate::$COMPETITIONS, $id);

			$imageFullPath = asset_model::getAssetFolder("competitions") . $logoFileName;
			if($logoFileName)
			{
				asset_model::removeAsset($imageFullPath);
			}
			
			nut_db::transactionCommit("removeCompetition");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("removeCompetition");
			throw $e;
		}
	}

	private function aux_append($id, &$data, $langid, $aux_info)
	{
		if (validator::arr()->each(validator::arr())->validate($data))
		{
			foreach($data as $key => $value) {
				self::aux_appender($value['id'], $data[$key], $langid, $aux_info);
			}
		}
		elseif (!is_null($data))
		{
			self::aux_appender($id, $data, $langid, $aux_info);
		}
	}

	private function aux_appender($id, &$data, $langid, $aux_info)
	{
		if(is_string($aux_info))
		{
			$data["aux_info"] = array() ;
			switch($aux_info)
			{
				
				case self::$AUX_TRANSLATION:
					$data["aux_info"][self::$AUX_TRANSLATION] = chili_translate::getFullTranslations($id, chili_translate::$COMPETITIONS);
					break;
				case self::$AUX_PARTICIPANTS:
						$this->load->model(MODULE_API_FOLDER . "/participants_model");
						$data["aux_info"][self::$AUX_PARTICIPANTS] = $this->participants_model->getParticipantsByCompetitionId($id, $langid,array());
						break;
				
			}
		}

		if(is_array($aux_info) && count($aux_info) > 0)
		{
			$data["aux_info"] = array();

			foreach($aux_info as $aux)
			{
				switch($aux)
				{
					case self::$AUX_TRANSLATION:
						$data["aux_info"][self::$AUX_TRANSLATION] = chili_translate::getFullTranslations($id, chili_translate::$COMPETITIONS);
						break;
					case self::$AUX_PARTICIPANTS:
						$this->load->model(MODULE_API_FOLDER . "/participants_model");
						$data["aux_info"][self::$AUX_PARTICIPANTS] = $this->participants_model->getParticipantsByCompetitionId($id, $langid,array());
						break;
				}
			}
		}		
	}


}