<?php

class sports_model extends CI_Model {

	private static $AUX_TRANSLATION = "translations";

	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');
	}

	public function getAllSports($langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							name,
							logo,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$SPORTS.") AS tname
							FROM ". TB_SPORTS;
			
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();

			$sportData = array();
			foreach ($data as $key => $sportItem) {
				$spId = $sportItem["id"];
				$sportData[$spId] = $sportItem;
			}

			return $sportData;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}


	public function getSportById($id, $langid, $aux_info)
	{
		try
		{
			$sql = "SELECT  id, 
							name,
						    GetTranslation(". $langid .",  id  ,". chili_translate::$SPORTS.") AS tname
							FROM ". TB_SPORTS . " WHERE id = ?";
			
			$query = nut_db::query($sql, array($id));
			$data =  $query->num_rows() > 0 ? $query->row_array(): NULL;

			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}		
	}

}