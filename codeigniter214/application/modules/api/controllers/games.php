<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Games extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);
		
		$this->load->model(MODULE_API_FOLDER . "/games_model");
		$this->load->model(MODULE_API_FOLDER . "/participants_model");
		$this->load->model(MODULE_API_FOLDER . "/events_model");
		$this->load->model(MODULE_API_FOLDER . "/bets_model");
		$this->load->model(MODULE_API_FOLDER . "/challenge_model");
	}


	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())
							->key('bookmaker_id', validator::numeric(), FALSE)
             				->check($this->postdata);

			$langid = $this->lang_id;
			$id = $this->postdata["id"];
			$bookmaker_id = element('bookmaker_id', $this->postdata, null);

			$aux_info =  element('aux_info', $this->postdata, array()); 		

			$data = $this->games_model->getGameDataById($id, $langid, $aux_info, $bookmaker_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_by_ids()
	{
		try
		{
			validator::arr()->key('ids', validator::arr()->each(validator::numeric()))
             				->check($this->postdata);

			$langid = $this->lang_id;
			$ids = $this->postdata["ids"];

			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->games_model->getGameDataByIds($ids, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "get_by_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "get_by_ids" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_teams_past_games()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())
             				->check($this->postdata);

			$langid = $this->lang_id;
			$id = $this->postdata["id"];

			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->games_model->getTeamsPastGames($id, $langid);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "get_teams_past_games" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "get_teams_past_games" , $e->getMessage(), 1, null, TRUE);
		}		
	}


	public function set_only_game_result()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
							->key('results',  validator::arr())
							->key('sport_id', validator::numeric())
             				->check($this->postdata);

			$game_id  = $this->postdata["game_id"];
			$results  = $this->postdata["results"];
			$sport_id = $this->postdata["sport_id"];

			$data = $this->games_model->setGameResultText($game_id, $results, $sport_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "set_only_game_result" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "set_only_game_result" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function set_game_result()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
							->key('results',  validator::arr())
							->key('sport_id', validator::numeric())
							->key('bookmaker_id', validator::numeric())
             				->check($this->postdata);

			$game_id  = $this->postdata["game_id"];
			$results  = $this->postdata["results"];
			$sport_id = $this->postdata["sport_id"];
			$bk_id    = $this->postdata["bookmaker_id"];

			$data = $this->games_model->setGameResult($game_id, $results, $sport_id, $bk_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "set_game_result" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "set_game_result" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function return_game_result()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
							->key('bookmaker_id', validator::numeric())
             				->check($this->postdata);

			$game_id  = $this->postdata["game_id"];
			$bk_id  = $this->postdata["bookmaker_id"];

			$data = $this->games_model->returnGame($game_id, $bk_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "return_game_result" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "return_game_result" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_filtered_by()
	{
		try
		{
			validator::arr()->key('id',         validator::numeric(), FALSE)
							->key('sport_id',         validator::numeric())
							->key('competitions',     validator::arr())
							->key('user_id',   		  validator::numeric(), FALSE)
							->key('participant_id',   validator::numeric(), FALSE)
							->key('limit',		      validator::numeric(), FALSE)
							->key('offset',		      validator::numeric(), FALSE)
							->key('from_date',  	  validator::date(), FALSE)
							->key('to_date',  		  validator::date(), FALSE)
							->key('state',   	  	  validator::numeric(), FALSE)
							->key('result',   	  	  validator::numeric(), FALSE)
							->key('bookmaker_id',  	  validator::numeric(), FALSE)
							->key('count',  	  	  validator::numeric(), FALSE)
             				->check($this->postdata);

			$langid = $this->lang_id;
			$id     = element('id', $this->postdata, NULL);
			$cids   = $this->postdata["competitions"];
			$skid   = $this->postdata["sport_id"];
			$from_date = element('from_date', $this->postdata, NULL);
			$to_date   = element('to_date', $this->postdata, NULL);
			$limit     = element('limit', $this->postdata, NULL);
			$offset    = element('offset', $this->postdata, NULL);
			$state     = element('state', $this->postdata, NULL);
			$result    = element('result', $this->postdata, NULL);
			$user_id   = element('user_id', $this->postdata, NULL);
			$count     = element('count', $this->postdata, NULL);
			$participant_id  = element('participant_id', $this->postdata, NULL);
			$bookmaker_id    = element('bookmaker_id', $this->postdata, NULL);

			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->games_model->getAllGamesBy($id, $skid, $cids, $langid, $aux_info, 
													  $from_date, $to_date, $limit, $offset,
													  $user_id, $participant_id, $state, $result, $bookmaker_id, $count);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "get_filtered_by" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "get_filtered_by" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_widget_games()
	{
		try
		{
			validator::arr()->key('widget_id',        validator::numeric())
							->key('bookmaker_id',  	  validator::numeric())
							->key('user_id',          validator::numeric(), FALSE)
							->key('league_id',	      validator::numeric(), FALSE)
							->key('from',  	          validator::numeric(), FALSE)
							->key('to',  		      validator::numeric(), FALSE)
							->key('limit',		      validator::numeric(), FALSE)
							->key('offset',		      validator::numeric(), FALSE)
							->key('user_timezone',	  validator::numeric(), FALSE)
             				->check($this->postdata);

			$widgetId       = $this->postdata["widget_id"];
			$bookmakerId    = $this->postdata["bookmaker_id"];
			$langId         = $this->lang_id;
			$userId         = element("user_id" , $this->postdata, NULL);
 			$leagueId       = element('league_id',  $this->postdata, NULL);
			$from 	        = element('from',     $this->postdata, NULL);
			$to   	        = element('to',       $this->postdata, NULL);
			$limit          = element('limit',    $this->postdata, NULL);
			$offset         = element('offset',   $this->postdata, NULL);
			$utz 			= element('user_timezone',   $this->postdata, NULL);

			$data = $this->games_model->getWidgetGames($widgetId, $userId, $leagueId, $from, $to, $limit, $offset, $langId, $bookmakerId, $utz);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "get_widget_games" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "get_widget_games" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function add()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('name',  		  validator::string()->notEmpty())
							->key('start_date',   validator::date())
							->key('competition_id',   validator::numeric())
							->key('sport_id', 	  validator::numeric())
							->key('calculated',   validator::numeric(), FALSE)
							->key('priority',     validator::numeric(), FALSE)
							->key('blocked',      validator::numeric(), FALSE)
							->key('status',       validator::numeric(), FALSE)
							->key('result_text',  validator::numeric(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);

			$data = $this->games_model->insertGames($this->postdata);
			echo json_encode($data);
			Events::trigger('added_games', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',    validator::numeric())
							->key('name',  		  validator::string()->notEmpty(), FALSE)
							->key('start_date',   validator::date(), FALSE)
							->key('competition_id',   validator::numeric(), FALSE)
							->key('sport_id', 	  validator::numeric(), FALSE)
							->key('calculated',   validator::numeric(), FALSE)
							->key('priority',     validator::numeric(), FALSE)
							->key('blocked',      validator::numeric(), FALSE)
							->key('status',       validator::numeric(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);
		
			$this->games_model->updateGames($this->postdata);
			Events::trigger('updated_games', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "update" , $e->getMessage(), 1, null, TRUE);
		}
	}

	//remove game from DB
	public function remove()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())->check($this->postdata);
			$id = $this->postdata["id"];
			$this->games_model->removeGame($id);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("games", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("games", "remove" , $e->getMessage(), 1, null, TRUE);
		}
	}
}
