<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Participants extends chili_api_controller {

	public static $DEFAULT_PRIORITY = 1;

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER."/participants_model");
	}
	
	public function get_by_id()
	{
		try 
		{
			validator::key('id', validator::numeric())->check($this->postdata);

			$id       = $this->postdata['id'];
			$langid   = $this->lang_id;
			$aux_info = element('aux_info', $this->postdata, array()); 

			$data = $this->participants_model->getParticipantInfoById($id, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}


	// with this call we get participants data by ids as array
	public function get_by_ids()
	{
		try
		{
			validator::arr()->key('ids', validator::arr()->each(validator::numeric()))
                 			->check($this->postdata);

 			$langid = $this->lang_id;
			$ids = $this->postdata["ids"];

			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->participants_model->getParticipantsDataByIds($ids, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "get_by_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "get_by_ids" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_by_country()
	{
		try
		{
			validator::arr()->key('country_id',   validator::numeric())
							->key('sport_id', validator::numeric())
					        ->check($this->postdata);

			$cid      = $this->postdata["country_id"];
			$skid     = $this->postdata["sport_id"];
			$langid   = $this->lang_id;
			$aux_info = element('aux_info', $this->postdata, array());
			
			$data = $this->participants_model->getParticipantsByCountryId($cid, $skid, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "get_by_country" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "get_by_country" , $e->getMessage(), 1, null, TRUE);
		}
	}

	// TODO::NUT WE HAVE THE SAME AS AUX IN COMPETITIONS CONTROLLER
	public function get_by_competition()
	{
		try
		{
			validator::arr()->key('competition_id', validator::numeric())->check($this->postdata);

			$cid      = $this->postdata["competition_id"];
			$langid   = $this->lang_id;
			$aux_info = element('aux_info', $this->postdata, array());
			$data = $this->participants_model->getParticipantsByCompetitionId($cid, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "get_by_competition" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "get_by_competition" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_by_game()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())->check($this->postdata);
		
			$gid      = $this->postdata["game_id"];
			$langid   = $this->lang_id;
			$aux_info = element('aux_info', $this->postdata, array()); 

			$data = $this->participants_model->getParticipantsByGameId($gid, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "get_by_game" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "get_by_game" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_by_game_ids()
	{
		try
		{
			validator::arr()->key('game_ids', validator::arr()->each(validator::numeric()))
             				->check($this->postdata);
		
			$gids      = $this->postdata["game_ids"];
			$langid   = $this->lang_id;
			$aux_info = element('aux_info', $this->postdata, array()); 

			$data = $this->participants_model->getParticipantsByGameIDs($gids, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "get_by_game_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "get_by_game_ids" , $e->getMessage(), 1, null, TRUE);
		}
	}


	public function add()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('sport_id',  validator::numeric())
							->key('country_id',   validator::numeric())
							->key('priority',   validator::numeric(), FALSE)
							->key('logo',  validator::string()->notEmpty(), FALSE)
							->key('info',  validator::string(), FALSE)
							->key('name',  validator::string()->notEmpty())
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);

			$data = $this->participants_model->insertParticipants($this->postdata);
			echo json_encode($data);
			Events::trigger('added_participants', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}


	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',    validator::numeric())
							->key('sport_id',  validator::numeric(), FALSE)
							->key('country_id',   validator::numeric(), FALSE)
							->key('priority',   validator::numeric(), FALSE)
							->key('logo',  validator::string()->notEmpty(), FALSE)
							->key('info',  validator::string(), FALSE)
							->key('name',  validator::string()->notEmpty(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);
		
			$this->participants_model->updateParticipants($this->postdata);
			Events::trigger('updated_participants', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "update" , $e->getMessage(), 1, null, TRUE);
		}
	}

	//remove participant from DB and remove participant assets
	public function remove()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())->check($this->postdata);
			$id = $this->postdata["id"];
			$this->participants_model->removeParticipant($id);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("participants", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("participants", "remove" , $e->getMessage(), 1, null, TRUE);
		}
	}	
}