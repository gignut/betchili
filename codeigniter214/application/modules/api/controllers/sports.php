<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Sports extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER."/sports_model");
	}
	
	public function get_all()
	{
		try
		{
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->sports_model->getAllSports($langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("sports", "get_all" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("sports", "get_all" , $e->getMessage(), 1, null, TRUE);
		}
	}
	
	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

            $id       = $this->postdata["id"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->sports_model->getSportById($id, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("sports", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("sports", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}

}