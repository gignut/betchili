<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Widgetdata extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER . "/games_model");
		$this->load->model(MODULE_API_FOLDER . "/widget_model");
	}

	// 2014 FEB 16
	// avar
	public function get_widget_competitions() // to Delete
	{
		try
		{
			validator::arr()->key('widget_id', validator::numeric()->notEmpty())
							->check($this->postdata);

			$widgetId = $this->postdata["widget_id"];
			$competitionData = $this->widget_model->getCompetitions($widgetId, $this->lang_id);
			echo json_encode($competitionData);
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("widgetdata", "get_widget_competitions" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("widgetdata", "get_widget_competitions" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_widget_leagues()
	{
		try
		{
			validator::arr()->key('widget_id', validator::numeric()->notEmpty())
							->check($this->postdata);

			$widgetId = $this->postdata["widget_id"];
			$leaguesData = $this->widget_model->getLeagues($widgetId, $this->lang_id);
			echo json_encode($leaguesData);
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("widgetdata", "get_widget_leagues" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("widgetdata", "get_widget_leagues" , $e->getMessage(), 1, null, TRUE);
		}
	}

	//2014 FEB 16
	//avar
	public function get_widget_css()
	{
		try
		{
			validator::arr()->key('widget_id', validator::numeric()->notEmpty())
						->check($this->postdata);

			$widgetId = $this->postdata["widget_id"];
			$cssData = $this->widget_model->getCSSData($widgetId);
			echo json_encode($cssData);
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("widgetdata", "get_widget_css" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("widgetdata", "get_widget_css" , $e->getMessage(), 1, null, TRUE);
		}
	}

	// REFACTOR::AVAR
	public function get_widget_data()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

            $wId   = $this->postdata["id"];			
            $wData = $this->widget_model->getWidgetData($wId);
			echo json_encode($wData);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("widgetdata", "get_widget_data" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("widgetdata", "get_widget_data" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function get_leaderboard(){
		try
		{
			validator::arr()->key('widget_id', validator::numeric())
						    ->key('user_id', validator::numeric())
						    ->key('limit',   validator::numeric())
						    ->key('offset',  validator::numeric())
             				->check($this->postdata);

            $widgetId  = $this->postdata["widget_id"];		
            $userId    = $this->postdata["user_id"];
            $offset    = $this->postdata["offset"];
            $limit     = $this->postdata["limit"];
            $retData   = $this->widget_model->getLeaderboard($widgetId, $userId, $limit, $offset);
			echo json_encode($retData);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("widgetdata", "get_leaderboard" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("widgetdata", "get_leaderboard" , $e->getMessage(),1, null, TRUE);
		}
	}
}
