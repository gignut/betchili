<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as v;

class Users extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER . "/users_model");
	}

	//## CREATE CHILI ACCOUNT ##
	public function register_chili_account()
	{
		try
		{
			v::arr()->key('first_name',	 		v::alnum()->noWhitespace(), FALSE)
					->key('last_name',	 		v::alnum()->noWhitespace(), FALSE)
					->key('email',	 			v::email())
					->key('pass',	 			v::alnum()->noWhitespace()->length(6,40))
					->key('confirm_password',	v::alnum()->noWhitespace()->length(6,40))
             		->check($this->postdata);

            $password 		 = $this->postdata['pass'];	 		
            $confirmPassword = $this->postdata['confirm_password'];	    
            $email =  $this->postdata['email'];	    
            $firstName = $this->postdata['first_name'];	
            $lastName = $this->postdata['last_name'];	

   			$data = $this->users_model->registerChiliAccount($email, $password, $confirmPassword ,  $firstName, $lastName);
    	    echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "register_chili_account" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "register_chili_account" , null, 1);
		}	
	}

	//ajax, signin chili account popup
	public function signin_chili_account()
	{
		try
		{
			v::arr()->key('username',   v::string()->notEmpty())
					->key('password', 	v::string()->notEmpty())
             		->check($this->postdata);

			$username = $this->postdata["username"];
			$password = $this->postdata["password"];
			$userData = $this->users_model->getChiliUserData($username, $password);
			if(!$userData)
			{
				$userData = array("error"=> 1, "message" => "Incorect Username or Password", "code" => 103);
			}
			
			echo json_encode($userData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "signin_chili_account" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "signin_chili_account" , null, 1);
		}	
	}

	// avar 16 JUL 2013
	public function init_player()
	{
		try
		{
			//user id is platform specific id
			v::arr()->key('platform_id', v::numeric())
					->key('user_id', 	 v::numeric())
					->key('site_id', 	 v::numeric())
					->key('widget_id',   v::numeric())
             		->check($this->postdata);

			$userId 	 = $this->postdata["user_id"];
			$siteId 	 = $this->postdata["site_id"];
			$platformId  = $this->postdata["platform_id"];
			$widgetId    = $this->postdata["widget_id"];

			$data = $this->users_model->initPlatformUser($platformId, $userId, $siteId, $widgetId);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $this->get_error_translation($e->getCode()), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "init_player" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "init_player" , null, 1, null, TRUE);
		}	
	}

	public function add()
	{
		try
		{
			//TODO::CHILI
			v::arr()->key('platform_id', v::numeric())
					->key('site_id',     v::numeric())
					->key('widget_id',   v::numeric())
					->check($this->postdata);

			$platformId = $this->postdata["platform_id"];
			$widgetId   = $this->postdata["widget_id"];
			$userID     = null;
			switch($platformId)
			{
				case PLATFORM_FACEBOOK:
				case PLATFORM_GOOGLE:
				case PLATFORM_VKONTAKTE:
				case PLATFORM_ODNOKLASSNIKI:
				case PLATFORM_MAILRU:
					v::arr()->key('platform_uid', v::numeric())
							->key('first_name',   v::string()->notEmpty())
							->key('last_name',    v::string()->notEmpty())
							->key('email',        v::email(), FALSE)
							->key('image',        v::string(), FALSE)
							->key('gender',       v::string(), FALSE)
							->key('blocked',      v::numeric())
							->key('balance',      v::numeric())
							->key('shield_percent',      v::numeric(), FALSE)
							->key('boost_multiplier',    v::numeric(), FALSE)
							->key('shield_count',        v::numeric(), FALSE)
							->key('boost_count',         v::numeric(), FALSE)							
							->check($this->postdata);

					$siteId       = $this->postdata['site_id'];			
					$platformUID  = $this->postdata['platform_uid'];
					$firstName    = element("first_name", $this->postdata, null);
					$lastName     = element("last_name", $this->postdata, null);
					$email		  = element('email', $this->postdata, null);
					$gender		  = $this->postdata['gender'];
					$image		  = $this->postdata['image'];
					$blocked      = $this->postdata['blocked'];
					$balance      = $this->postdata['balance'];	
					// TODO make strict after merge
					$shield_percent      = element("shield_percent", $this->postdata,   SHIELD_PERCENT);
					$boost_multiplier    = element("boost_multiplier", $this->postdata, BOOST_MULTIPLIER);
					$shield_count        = element("shield_count", $this->postdata,   SHIELD_COUNT);
					$boost_count         = element("boost_count", $this->postdata, BOOST_COUNT);

					$userID = $this->users_model->insertSocialPlatformUser($siteId, $widgetId, $platformId, $platformUID, $firstName,
																		   $lastName, $email, $gender, $image, $blocked, $balance,
																		   $shield_count, $shield_percent, $boost_count, $boost_multiplier);
					break;

				case PLATFORM_CHILI:
					v::arr()->key('username',   v::string()->notEmpty())
							->key('password',   v::string()->notEmpty())
							->key('first_name', v::string()->notEmpty(), FALSE)
							->key('last_name',  v::string()->notEmpty(), FALSE)
							->key('email',      v::email(), FALSE)
							->key('blocked',    v::numeric())
							->key('balance',    v::numeric())
							->key('shield_percent',      v::numeric(), FALSE)
							->key('boost_multiplier',    v::numeric(), FALSE)
							->check($this->postdata);

					$userID = $this->users_model->insertChiliPlatformUser($this->postdata);	
				break;			
			}
			
			$retData = $this->users_model->getPlayerProfileAndBalance($siteId, $widgetId, $userID);
			echo json_encode($retData);	
			Events::trigger('added_users', $userID);
		}

		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "add" , null, 1, null, TRUE);
		}	
	}

	
	public function get_player_profile()
	{
		try
		{
			v::arr()->key('site_id',   v::numeric())
					->key('widget_id', v::numeric())
					->key('user_id',   v::numeric())
                	->check($this->postdata);

			$siteId   = $this->postdata["site_id"];
			$widgetId = $this->postdata["widget_id"];
			$userId   = $this->postdata["user_id"];

			$data = $this->users_model->getPlayerProfileAndBalance($siteId, $widgetId, $userId);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "get_by_id" , null, 1, null, TRUE);
		}	
	}

	public function get_player_balance()
	{
		try
		{
			v::arr()->key('site_id',   v::numeric())
					->key('widget_id', v::numeric())
					->key('user_id',   v::numeric())
                	->check($this->postdata);

			$siteId   = $this->postdata["site_id"];
			$widgetId = $this->postdata["widget_id"];
			$userId   = $this->postdata["user_id"];

			$data = $this->users_model->getPlayerBalance($siteId, $widgetId, $userId);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "get_player_balance" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "get_player_balance" , null, 1, null, TRUE);
		}	
	}

	public function get_profile_data_by_id()
	{
		try
		{
			v::arr()->key('id', v::numeric())
             		->check($this->postdata);

			$id 	 = $this->postdata["id"];

			$aux_info =  element('aux_info', $this->postdata, array()); 		

			$data = $this->users_model->getUserProfileDataById($id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "get_profile_data_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "get_profile_data_by_id" , null, 1, null, TRUE);
		}	
	}

	public function get_by_ids()
	{
		try
		{
			v::arr()->key('ids',     v::arr()->each(v::numeric()))
					->key('site_id', v::numeric())
             		->check($this->postdata);

			$langid = $this->lang_id;
			$ids 	 = $this->postdata["ids"];
			$site_id = $this->postdata["site_id"];

			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->users_model->getUserDataByIds($ids, $site_id, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "get_by_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "get_by_ids" , null, 1, null, TRUE);
		}	
	}

	//remove user from DB
	public function remove()
	{
		try
		{
			v::arr()->key('id',      v::numeric())
					->key('site_id', v::numeric())
					->check($this->postdata);
			$id      = $this->postdata["id"];
			$site_id = $this->postdata["site_id"];
			$this->users_model->removeUser($id, $site_id);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "remove" , null, 1, null, TRUE);
		}	
	}

	public function filter_opponents() {
		try
		{
			v::arr()->key('site_id',     v::numeric())
					->key('widget_id',	 v::numeric(), FALSE)
					->key('user_id',	 v::numeric(), FALSE)
					->key('limit',		 v::numeric(), FALSE)
					->key('offset',		 v::numeric(), FALSE)
					->key('search',		 v::string()->notEmpty(), FALSE)
             		->check($this->postdata);

			$langid    	= $this->lang_id;
			$widget_id     	= element('widget_id', $this->postdata, NULL);
			$user_id     	= element('user_id', $this->postdata, NULL);
			$limit     	= element('limit', $this->postdata, NULL);
			$offset    	= element('offset', $this->postdata, NULL);
			$search    	= element('search', $this->postdata, NULL);
			$site_id   	= element('site_id', $this->postdata, NULL);

			$data = $this->users_model->getOpponents($site_id, $widget_id, $user_id, $limit, $offset, $search);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "filter_opponents" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "filter_opponents" , null, 1, null, TRUE);
		}		
	}

	public function get_filtered_by()
	{
		try
		{
			v::arr()->key('site_id',   		  v::numeric())
							->key('widget_id',	      v::numeric(), FALSE)
							->key('user_id',	      v::numeric(), FALSE)
							->key('limit',		      v::numeric(), FALSE)
							->key('offset',		      v::numeric(), FALSE)
							->key('state',   	  	  v::numeric(), FALSE)
             				->check($this->postdata);

			$langid    	= $this->lang_id;
			$widget_id     	= element('widget_id', $this->postdata, NULL);
			$user_id     	= element('user_id', $this->postdata, NULL);
			$limit     	= element('limit', $this->postdata, NULL);
			$offset    	= element('offset', $this->postdata, NULL);
			$state     	= element('state', $this->postdata, NULL);
			$site_id   	= element('site_id', $this->postdata, NULL);

			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->users_model->getAllUsersBy($site_id, $widget_id, $user_id, $aux_info, $limit,
													  $offset, $state);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "get_filtered_by" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "get_filtered_by" , null, 1, null, TRUE);
		}	
	}

	public function update_user_profile()
	{
		try
		{
			v::arr()->key('id',   		  v::numeric())
							->key('first_name',	      v::string(), FALSE)
							->key('last_name',	      v::string(), FALSE)
							->key('email',		      v::string(), FALSE)
							->key('image',   	  	  v::string(), FALSE)
							->key('gender',   	  	  v::string(), FALSE)
							->key('blocked',   	  	  v::numeric(), FALSE)
							->key('mode',   	  	  v::numeric(), FALSE)
             				->check($this->postdata);

			$langid    	= $this->lang_id;
			$id  = $this->postdata['id'];
			$first_name  = element('first_name', $this->postdata, NULL);
			$last_name   = element('last_name', $this->postdata, NULL);
			$email    	= element('email', $this->postdata, NULL);
			$image     	= element('image', $this->postdata, NULL);
			$gender   	= element('gender', $this->postdata, NULL);
			$blocked   	= element('blocked', $this->postdata, NULL);
			$mode   	= element('mode', $this->postdata, NULL);

			$aux_info =  element('aux_info', $this->postdata, array());

			$this->users_model->updateUserProfile($id, $first_name, $last_name, $email, $image, $gender, $blocked, $mode);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "update_user_profile" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "update_user_profile" , null, 1, null, TRUE);
		}		
	}

	public function update_show_intro()
	{
		try
		{
			v::arr()->key('user_id',   		  v::numeric())
					->key('site_id',	      v::numeric())
					->key('widget_id',	      v::numeric())
					->key('show',		      v::numeric())
					->check($this->postdata);

			$user_id	 	  = $this->postdata['user_id'];
			$site_id	 	  = $this->postdata['site_id'];
			$widget_id  	  = $this->postdata['widget_id'];
			$show  	     	  = $this->postdata['show'];

			$data = $this->users_model->changeUserIntroState($user_id, $site_id, $widget_id, $show);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "update_show_intro" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "update_show_intro" , null, 1, null, TRUE);
		}
	}

	public function change_user_balance()
	{
		try
		{
			v::arr()->key('user_id',   		  v::numeric())
					->key('site_id',	      v::numeric())
					->key('widget_id',	      v::numeric())
					->key('change',		      v::numeric())
					->check($this->postdata);

			$user_id	 	  = $this->postdata['user_id'];
			$site_id	 	  = $this->postdata['site_id'];
			$widget_id  	  = $this->postdata['widget_id'];
			$change  	  	  = $this->postdata['change'];

			$this->users_model->changeUserBalance($user_id, $site_id, $widget_id, $change);		
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "change_user_balance" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "change_user_balance" , null, 1, null, TRUE);
		}
	}

	public function update_user_balances()
	{
		try
		{
			v::arr()->key('user_id',   		  v::numeric())
					->key('site_id',	      v::numeric())
					->key('widget_id',	      v::numeric())
					->key('balance',	      v::numeric(), FALSE)
					->key('shield_count',  	  v::string(), FALSE)
					->key('shield_percent',	  v::string(), FALSE)
					->key('boost_count',  	  v::numeric(), FALSE)
					->key('boost_multiplier', v::numeric(), FALSE)
     				->check($this->postdata);

			$langid    	 	  = $this->lang_id;
			$user_id	 	  = $this->postdata['user_id'];
			$site_id	 	  = $this->postdata['site_id'];
			$widget_id  	  = $this->postdata['widget_id'];
			$balance    	  = element('balance', $this->postdata, NULL);
			$shield_count     = element('shield_count', $this->postdata, NULL);
			$shield_percent   = element('shield_percent', $this->postdata, NULL);
			$boost_count   	  = element('boost_count', $this->postdata, NULL);
			$boost_multiplier = element('boost_multiplier', $this->postdata, NULL);

			$this->users_model->updateUserBalances($user_id, $site_id, $widget_id, $balance, $shield_count, $shield_percent, $boost_count, $boost_multiplier);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "update_user_balances" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "update_user_balances" , null, 1, null, TRUE);
		}

	}

	public function get_user_activity_statistics () 
	{
		try
		{
			v::arr()->key('user_id',    v::numeric())
				    ->key('widget_id',	v::numeric())
				    ->key('site_id',	v::numeric())
             				->check($this->postdata);

			$user_id    = $this->postdata['user_id'];
			$widget_id  = $this->postdata['widget_id'];
			$site_id    = $this->postdata['site_id'];

			$retData = $this->users_model->getUserActivityStatistics($user_id, $widget_id, $site_id);
			echo json_encode($retData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("users", "get_user_activity_statistics" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("users", "get_user_activity_statistics" , null, 1, null, TRUE);
		}		
	}
}

