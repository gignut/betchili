<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Challenge extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER."/challenge_model");
	}

	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$id = $this->postdata['id'];

			$data = $this->challenge_model->getById($id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_by_game_for_publishers()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$game_id = $this->postdata['game_id'];

			$data = $this->challenge_model->getAllChallengesByGameForPublishers($game_id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "get_all_by_game" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "get_all_by_game" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_by_site_and_widget()
	{
		try
		{
			validator::arr()->key('site_id', validator::numeric())
							->key('widget_id', validator::numeric(), FALSE)
							->key('user_id', validator::numeric(), FALSE)
							->key('game_id', validator::numeric(), FALSE)
							->key('limit',   validator::numeric(), FALSE)
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$site_id = $this->postdata['site_id'];
			$widget_id = element('widget_id', $this->postdata, null);
			$user_id = element('user_id', $this->postdata, null);
			$game_id = element('game_id', $this->postdata, null);
			$limit = element('limit', $this->postdata, null);

			$data = $this->challenge_model->getAllChallengesBySite($site_id, $widget_id, $user_id, $game_id, $aux_info, $limit);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "get_all_by_game" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "get_all_by_game" , $e->getMessage(), 1, null, TRUE);
		}		
	}

	public function get_all_by_game()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$game_id = $this->postdata['game_id'];

			$data = $this->challenge_model->getAllChallengesByGame($game_id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "get_all_by_game" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "get_all_by_game" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_by_user()
	{
		try
		{
			validator::arr()->key('user_id',   validator::numeric())
							->key('widget_id', validator::numeric())
							->key('limit',     validator::numeric(), FALSE)
							->key('offset',    validator::numeric(), FALSE)
 							->check($this->postdata);

			$userId 	= $this->postdata['user_id'];
			$widgetId   = $this->postdata['widget_id'];
			
			$limit  = element("limit", $this->postdata, NULL);
			$offset = element("offset", $this->postdata, NULL);

			$data = $this->challenge_model->getAllChallengesByUser($userId, $widgetId, $limit, $offset, $this->lang_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "get_all_by_user" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "get_all_by_user" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function create_challange()
	{
		try
		{
			$postData = $this->postdata;
			validator::arr()->key('source_user_id', validator::numeric())
							->key('target_user_id', validator::numeric(), FALSE)
							->key('site_id', 		validator::numeric())
							->key('widget_id', 		validator::numeric())
							->key('game_id', 		validator::numeric())
							->key('event_id',   	validator::numeric())
							->key('amount', 		validator::numeric())
							->key('coefficient', 	validator::float())
							->key('basis',   		validator::float(), FALSE)
							->check($postData);

			$challengeId = $this->challenge_model->insertChallenge($postData);
			
			$siteId   = $postData["site_id"];
			$widgetId = $postData["widget_id"];
			$userId   = $postData["source_user_id"];

			$this->load->model(MODULE_API_FOLDER . "/users_model");
			$currentBalance = $this->users_model->getPlayerBalance($siteId, $widgetId, $userId);

			$responseData = array("balance" => $currentBalance, "challenge_id" => $challengeId);
			echo json_encode($responseData);
			Events::trigger('challenge', $responseData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $this->get_error_translation($e->getCode()), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "create_challange" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "create_challange" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function accept_challenge()
	{
		try
		{
			validator::arr()->key('challenge_id',	 validator::numeric())
							->key('opponent_user_id',validator::numeric())
							->key('event_id', 		 validator::numeric())
							->key('coefficient',     validator::float()) // stugel vor server-i het ham@nknuma
							->key('basis', 		     validator::float(), FALSE) // stugel vor server-i het ham@nknuma | hashvi arnel pox@
							->check($this->postdata);

			$data = $this->challenge_model->acceptChallenge($this->postdata);
			echo json_encode($data);
			Events::trigger('accept_challenge', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $this->get_error_translation($e->getCode()), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "accept_challenge" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "accept_challenge" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_public_challenge()
	{
		try
		{
			$postData = $this->postdata;
			validator::arr()->key('widget_id', validator::numeric())
							->key('user_id',   validator::numeric(), FALSE)
							->key('offset',    validator::numeric(), FALSE)	
							->check($postData);

			$widgetId      = $postData["widget_id"];
			$sourceUserId  = element("user_id", $postData, null);
			$offset        = element("offset",  $postData, 0);

			$langId   = $this->lang_id;

			$challengeData = $this->challenge_model->getPublicChallenge($widgetId, $sourceUserId, $offset);

			$retData = array();
			if(isset($challengeData["challenge_id"]))
			{
				$gameId =  $challengeData["game_id"];
				$betId  =  $challengeData["publisher_bet_id"];

				$this->load->model(MODULE_API_FOLDER . "/games_model");
				$gameData = $this->games_model->getGameDataById($gameId, $langId, array("participants", "events"), 1);

				$this->load->model(MODULE_API_FOLDER . "/bets_model");
				$betData = $this->bets_model->getBetById($betId, $langId);
				//  PREPARING RESPONSE DATA
				
				$retData["id"]     = $challengeData["challenge_id"];
				$retData["amount"] = $challengeData["amount"];
				$retData["is_new"] = false;
				
				// PREPARE GAME DATA START
				$retData["game"]   = array();
				$retData["game"]["id"] = $gameData["id"];
				// TODO in game avar added start_utc
				$retData["game"]["start_date"] = $gameData["start_utc"];
				$retData["game"]["name"]  = $gameData["name"];
				$retData["game"]["tname"] = $gameData["tname"];

				//preparing participants	
				$participant1 = $gameData["aux_info"]["participants"][0];  
				$participant2 = $gameData["aux_info"]["participants"][1];  
				$retData["game"]["aux_info"]["participants"][0] = array("id" => $participant1["id"], "logo" => $participant1["logo"], "name" => $participant1["name"], "tname" => $participant1["tname"]);
				$retData["game"]["aux_info"]["participants"][1] = array("id" => $participant2["id"], "logo" => $participant2["logo"], "name" => $participant2["name"], "tname" => $participant2["tname"]);
				$retData["game"]["aux_info"]["events"] = $gameData["aux_info"]["events"] ;
				// PREPARE GAME DATA END

				// PREPARE CHALLENGE PUBLISHER INFO AND DATA
				$eventData = $betData["event_chain"][0];

				$publisherUser = array();
				$publisherUser["id"]                = $challengeData["publisher_id"];
				$publisherUser["name"]              = $challengeData["first_name"] . " " . $challengeData["last_name"];
				$publisherUser["avatar"]            = $challengeData["image"];
				$publisherUser["event_name"]        = $eventData["event_name"];
				$publisherUser["event_template_id"] = $eventData["template_id"];
				$publisherUser["basis"]             = $eventData["basis"];
				$publisherUser["coefficient"]       = $eventData["event_coefficient"];
				
				$retData["opponent1"] = $challengeData['target_user_id'] ? array('id' => $challengeData['target_user_id'], 'name' => $challengeData['target_user_name'], 'avatar' => $challengeData['target_user_image']) : array();
				$retData["opponent2"] = $publisherUser;
			}
			else
			{
				$retData["block"] = true;
			}

			// END PREPARE CHALLENGE PUBLISHER INFO AND DATA
		
			echo json_encode($retData);
			Events::trigger('get_public_challenge', $retData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("challenge", "get_public_challenge" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("challenge", "get_public_challenge" , $e->getMessage(), 1, null, TRUE);
		}
	}
}