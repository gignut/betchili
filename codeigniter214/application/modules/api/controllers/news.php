<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class News extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);
		$this->load->model(MODULE_API_FOLDER . "/news_model");
	}

	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())
             				->check($this->postdata);

			$id = $this->postdata["id"];

			$data = $this->news_model->getNewsById($id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("news", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("news", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_by_ids()
	{
		try
		{
			validator::arr()->key('ids', validator::arr()->each(validator::numeric()))
             				->check($this->postdata);

			$ids = $this->postdata["ids"];

			$data = $this->news_model->getNewsByIds($ids);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("news", "get_by_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("news", "get_by_ids" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_lastest_news_by($type=NULL, $refer_table=NULL, $field_id=NULL, $limit=NULL, $offset=NULL)
	{
		try
		{
			validator::arr()->key('field_id',    validator::numeric(), FALSE)
							->key('type',	     validator::numeric(), FALSE)
							->key('refer_table', validator::string(), FALSE)
							->key('limit',	     validator::numeric(), FALSE)
							->key('offset',      validator::numeric(), FALSE)
             				->check($this->postdata);

			$field_id  = element('field_id', $this->postdata, NULL);
			$type      = element('type', $this->postdata, NULL);
			$refer_table  = element('refer_table', $this->postdata, NULL);
			$limit      = element('limit', $this->postdata, NULL);
			$offset     = element('offset', $this->postdata, NULL);

			$data = $this->news_model->getAllNewsBy($type, $field_id, $refer_table, $limit, $offset);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("news", "get_lastest_news_by" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("news", "get_lastest_news_by" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function add()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('type',  		  validator::numeric())
							->key('field_id',     validator::numeric(), FALSE)
							->key('refer_table',  validator::string()->notEmpty(), FALSE)
							->key('link',	      validator::string()->notEmpty(), FALSE)
							->key('description',  validator::string()->notEmpty(), FALSE)
							)->check($this->postdata);

			$data = $this->news_model->insertNews($this->postdata);
			echo json_encode($data);
			Events::trigger('added_news', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("news", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("news", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',    		  validator::numeric())
							->key('type',  		  validator::numeric(), FALSE)
							->key('field_id',     validator::numeric(), FALSE)
							->key('refer_table',  validator::string()->notEmpty(), FALSE)
							->key('link',	      validator::string()->notEmpty(), FALSE)
							->key('description',  validator::string()->notEmpty(), FALSE)
							)->check($this->postdata);
		
			$this->news_model->updateNews($this->postdata);
			Events::trigger('updated_news', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("news", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("news", "update" , $e->getMessage(), 1, null, TRUE);
		}
	}

	//remove news from DB
	public function remove()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())->check($this->postdata);
			$id = $this->postdata["id"];
			$this->news_model->removeNews($id);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("news", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("news", "remove" , $e->getMessage(), 1, null, TRUE);
		}
	}
}
