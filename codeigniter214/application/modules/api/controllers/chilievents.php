<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class ChiliEvents extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER."/events_model");
	}

	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$id = $this->postdata['id'];

			$data = $this->events_model->getEventById($id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_bookmakers()
	{
		try
		{
			$data = $this->events_model->getAllBookmakers();
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "get_all_bookmakers" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "get_all_bookmakers" , $e->getMessage(), 1, null, TRUE);
		}
	}
	
	public function get_all()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
							->key('bookmaker_id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$game_id = $this->postdata['game_id'];
			$bookmaker_id = $this->postdata['bookmaker_id'];

			$data = $this->events_model->getAllEvents($game_id, $bookmaker_id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "get_all" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "get_all" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function add()
	{
		try
		{
			validator::arr()->key('game_id',   		validator::numeric())
							->key('bookmaker_id', 	validator::numeric())
							->check($this->postdata);

			$data = $this->events_model->insertEvents($this->postdata);
			echo json_encode($data);
			Events::trigger('added_events', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',		     	validator::numeric())
							->key('template_id',  	validator::numeric(), FALSE)
							->key('game_id',   		validator::numeric(), FALSE)
							->key('bookmaker_id', 	validator::numeric(), FALSE)
							->key('coefficient',    validator::numeric(), FALSE)
							->key('basis',		    validator::numeric(), FALSE)
							->key('data',  			validator::string(),  FALSE)
							->key('result',         validator::numeric(), FALSE)
							->key('result_status',  validator::numeric(), FALSE)
							->key('blocked',        validator::numeric(), FALSE)
							)->check($this->postdata);

			$data = $this->events_model->updateEvents($this->postdata);
			echo json_encode($data);
			Events::trigger('updated_events', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_event_groups()
	{
		try
		{
			validator::arr()->key('sport_id', validator::numeric())->check($this->postdata);

			$data = $this->events_model->getAllEventGroups($this->postdata['sport_id']);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "get_all_event_groups" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "get_all_event_groups" , $e->getMessage(), 1, null, TRUE);
		}		
	}

	//2014 FEB 16
	//avar
	public function get_all_event_templates()
	{
		try
		{
			validator::arr()->key('sport_id', validator::numeric())
							->check($this->postdata);

			$sportId = $this->postdata['sport_id'];				
			$data    = $this->events_model->getAllEventTemplates($sportId, $this->lang_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("chilievents", "get_all_event_templates" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("chilievents", "get_all_event_templates" , $e->getMessage(), 1, null, TRUE);
		}		
	}
}