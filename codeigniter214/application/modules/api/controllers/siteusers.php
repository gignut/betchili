<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Siteusers extends chili_api_controller {

	public function __construct($p= null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER . "/siteusers_model");
	}

	// send request for using betchili
	public function sendrequest()
	{
		try
		{
			$pdata = $this->postdata;

			validator::arr()->key('name', validator::length(1,64))
                            ->key('email',      validator::email()->length(1,256))
                            ->key('domain',     validator::length(1,128))
                            ->check($pdata);

			$name        = $pdata["name"];
            $email       = $pdata["email"];
            $domain      = $pdata["domain"];

			$data = $this->siteusers_model->createClientRequest($name, $email, $domain);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "sendrequest" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "sendrequest" , $e->getMessage(), 1 , null, TRUE);
		}	
	}

	public function get_all() {
		try
		{
			$aux_info =  element('aux_info', $this->postdata, array());

			$data = $this->siteusers_model->getAllSiteUsers($aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "get_all" , $e->getMainMessage(), 2);
		}

		catch(Exception $e)
		{
			$this->api_error("siteusers", "get_all" , $e->getMessage(),1, null, TRUE);
		}			
	}

	public function get_by_id() 
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())
             				->check($this->postdata);

			$id = $this->postdata["id"];
			$data = $this->siteusers_model->getSiteUserDataById($id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "get_by_id" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function get_by_email() 
	{
		try
		{
			validator::arr()->key('email',    validator::email()->notEmpty())
             				->check($this->postdata);

			$email = $this->postdata["email"];
			$data = $this->siteusers_model->getSiteUserDataByEmail($email);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "get_by_email" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "get_by_email" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function is_registered()
	{
		try
		{
			$pdata = $this->postdata;
			validator::arr()->key('email',      validator::email()->notEmpty())
							->key('pass',       validator::regex('/^[a-zA-Z0-9._]{6,}$/')->length(6,24))
             				->check($this->postdata);

			$email = $pdata["email"];
			$pass = $pdata["pass"];
			$md5pass     = $pass;
			$md5pass     = md5($pass);

			$data = $this->siteusers_model->isSiteUserRegistered($email, $md5pass);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "is_registered" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "is_registered" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function create()
	{
		try
		{
			$pdata = $this->postdata;

			validator::arr()->key('first_name', validator::length(1,64))
                            ->key('last_name',  validator::length(1,64)) 
                            ->key('email',      validator::email()->length(1,256))
                            ->key('domain',     validator::length(1,64))
                            ->key('pass',       validator::regex('/^[a-zA-Z0-9._]{6,}$/')->length(6,24))
                            ->check($pdata);

			$firstName   = $pdata["first_name"];
            $lastName    = $pdata["last_name"];
            $email       = $pdata["email"];
            $domain      = $pdata["domain"];
            $pass        = $pdata["pass"];
            $md5pass     = md5($pass);

			$this->siteusers_model->createSiteUser($firstName, $lastName, $email, $domain, $md5pass);
			$data = $this->siteusers_model->getSiteUserDataByEmail($email);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "create" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "create" , $e->getMessage(), 1 , null, TRUE);
		}	
	}

	public function update()
	{
		try
		{
			$pdata = $this->postdata;
			validator::arr()->key('id',         validator::numeric()->notEmpty())
							->key('first_name', validator::length(1,64), FALSE)
                            ->key('last_name',  validator::length(1,64), FALSE) 
                            ->key('email',      validator::email()->notEmpty(), FALSE)
                            ->key('domain',     validator::length(6,24), FALSE)
                            ->key('blocked',    validator::numeric(), FALSE)
                            ->check($pdata);

            $id          = $pdata["id"];  	              
			$firstName   = element('first_name', $pdata, NULl);
            $lastName    = element('last_name', $pdata, NULl);
            $email       = element('email', $pdata, NULl);
            $domain      = element('domain', $pdata, NULl);
            $blocked     = element('blocked', $pdata, NULl);

			$this->siteusers_model->updateSiteUserData($id, $firstName, $lastName, $email, $domain, $blocked);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "update" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function confirm()
	{
		try
		{
			$pdata = $this->postdata;
			validator::arr()->key('id',         validator::numeric()->notEmpty())
                            ->check($pdata);

            $id          = $pdata["id"];  	              

			$this->siteusers_model->confirmSiteUserCreation($id);
			$data = array("site_user" => $id, "confirmed" => 1);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "confirm" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "confirm" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function change_password()
	{
		try
		{
			$pdata = $this->postdata;

			validator::arr()->key('user_id', validator::numeric()->notEmpty())
							->key('pass',    validator::regex('/^[a-zA-Z0-9._]{6,}$/')->length(6,24))
                            ->check($pdata);

            $id          = $pdata["user_id"];  	        
            $pass        = $pdata["pass"];
            $md5pass     = md5($pass);

			$this->siteusers_model->updateSiteUserPassword($id, $md5pass);
			$data = array("site_user" => $id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "change_password" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "change_password" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function get_widget_types()
	{
		try
		{
			$data = $this->siteusers_model->getAllWidgetTypes();
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "get_widget_types" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "get_widget_types" , $e->getMessage(),1, null, TRUE);
		}	
	}

	//get all packages of site user by site user id
	public function get_widgets(){
		try
		{
			validator::arr()->key('id',   		   validator::numeric())
             				->check($this->postdata);

			$id = $this->postdata["id"];
			$langId = $this->lang_id;

			$data = $this->siteusers_model->getSiteUserWidgets($id, $langId);

			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "get_siteuser_widgets" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "get_siteuser_widgets" , $e->getMessage(),1, null, TRUE);
		}	
	}

	//get all packages of site user by siteuser id
	public function get_all_widgets(){
		try
		{
			//CHILI:: TODO ARRAY VALIDATION OF WIDGET TYPES
			validator::arr()->key('id',   		   validator::numeric())
             				->check($this->postdata);
             				// ->key('widget_types',  validator::numeric())	

			$langId = $this->lang_id;
			$siteUserId = $this->postdata["id"];
			$widgetTypes = $this->postdata["widget_types"];

			$data = array();
			foreach ($widgetTypes as $key => $wType) {
				
				switch(intval($wType))
				{
					case 1:
						$wdata = $this->siteusers_model->getGameWidgets($siteUserId, $langId);
						$data = array_merge($data, $wdata);
						break;
				}
			}
		
			echo json_encode($data);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch(InvalidArgumentException $e)
		{
			$this->api_error("siteusers", "get_siteuser_widgets" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("siteusers", "get_siteuser_widgets" , $e->getMessage(),1, null, TRUE);
		}	
	}

	public function get_game_widget_data()
	{
		try
		{
			//CHILI::TODO leagues validation and max lenght of name
			validator::arr()->key('widget_id',  validator::numeric()->notEmpty())
							->check($this->postdata);
		
			$wId = $this->postdata["widget_id"];

			$wData = $this->siteusers_model->getGameWidgetData($wId, $this->lang_id);

			echo json_encode($wData);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch (InvalidArgumentException $e) 
		{
			$this->api_error("siteusers", "get_game_widget_data" , $e->getMainMessage(), 2);
		}
		catch (Exception $e) 
		{
			$this->api_error("siteusers", "get_game_widget_data" , $e->getMessage(),1, null, TRUE);
		}
	}

	public function create_game_widget()
	{
		try
		{
			//CHILI::TODO leagues validation and max lenght of name
			validator::arr()->key('siteuser_id',  validator::numeric()->notEmpty())
							->key('name',         validator::string()->notEmpty())
							->key('size_id',      validator::numeric()->notEmpty())
							->key('lang_id',	  validator::numeric()->notEmpty())
							->key('leagues',       validator::arr()->notEmpty())
							->check($this->postdata);
		
			$siteUserId = $this->postdata["siteuser_id"];
			$name       = $this->postdata["name"];
			$sizeId     = $this->postdata["size_id"];
			$langId     = $this->postdata["lang_id"];
			$leagues    = $this->postdata["leagues"];
			
			$wId   = $this->siteusers_model->createGameWidget($siteUserId, $name, $sizeId, $langId, $leagues);
			$wData = $this->siteusers_model->getGameWidgetData($wId, $langId);
			echo json_encode($wData);
			Events::trigger('create_game_widget', $this->postdata);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch (InvalidArgumentException $e) 
		{
			$this->api_error("siteusers", "create_game_widget" , $e->getMainMessage(), 2);
		}
		catch (Exception $e) 
		{
			$this->api_error("siteusers", "create_game_widget" , $e->getMessage(),1, null, TRUE);
		}
	}

	public function update_game_widget()
	{
		try
		{
			//CHILI::TODO leagues validation and max lenght of name
			validator::arr()->key('siteuser_id',  validator::numeric()->notEmpty())
							->key('id',  		  validator::numeric()->notEmpty())
							->key('lang_id', validator::numeric()->notEmpty())
							->check($this->postdata);
		
			$siteUserId = $this->postdata["siteuser_id"];
			$widgetId   = $this->postdata["id"];
			$langId     = $this->postdata["lang_id"];

			if(isset( $this->postdata["css"]))
			{
				$css = json_encode($this->postdata["css"]);
				$this->siteusers_model->updateWidgetDesign($siteUserId, $widgetId, $css);
			}	

			if(isset($this->postdata["name"]))
			{
				validator::arr()->key('name'   , validator::string()->notEmpty())
							    ->key('size_id', validator::numeric()->notEmpty())
							   
							    ->key('leagues', validator::arr()->notEmpty())
							    ->check($this->postdata);

				$name     = $this->postdata["name"];
				$sizeId   = $this->postdata["size_id"];
				$leagues  = $this->postdata["leagues"];

				$this->siteusers_model->updateGameWidgetConfigs($siteUserId, $widgetId, $name, $sizeId, $langId, $leagues);
			}

			$wData = $this->siteusers_model->getGameWidgetData($widgetId, $langId);
			
			echo json_encode($wData);
			Events::trigger('update_game_widget', $this->postdata);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch (InvalidArgumentException $e) 
		{
			$this->api_error("siteusers", "update_game_widget" , $e->getMainMessage(), 2);
		}
		catch (Exception $e) 
		{
			$this->api_error("siteusers", "update_game_widget" , $e->getMessage(),1, null, TRUE);
		}
	}

	public function remove_widget(){
		try
		{
			validator::arr()->key('widget_id',	  		 validator::numeric())
							->key('siteuser_id', validator::numeric())		
							->check($this->postdata);

			//load widget model				
			$this->load->model(MODULE_API_FOLDER . "/widget_model");				
		
			$wId = $this->postdata["widget_id"];
			$siteUserId = $this->postdata["siteuser_id"];
			$wData = $this->widget_model->getWidgetData($wId);

			$wType = $wData["widget_type"];
			
			switch(intval($wType))
			{
				case 1:
					$this->siteusers_model->removeGameWidget($wId, $siteUserId);
					break;
			}
			
			Events::trigger('remove_widget', $this->postdata);
		}
		catch(Api_exception $e)
        {
            $this->api_error("", "", $e->getMessage(), $e->getCode(), $e->getData());
        }
		catch (InvalidArgumentException $e) 
		{
			$this->api_error("siteusers", "remove_widget" , $e->getMainMessage(), 2);
		}
		catch (Exception $e) 
		{
			$this->api_error("siteusers", "remove_widget" , $e->getMessage(),1, null, TRUE);
		}
	}	
}
