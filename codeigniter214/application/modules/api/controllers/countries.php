<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Countries extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);
		$this->load->model(MODULE_API_FOLDER . "/countries_model");
	}

	/**
	 * [get_all - With this call we get all countries id,name(according to translation),flag,code]
	 * @return [type] [description]
	 */
	public function get_all()
	{
		try
		{
			$langid   = $this->lang_id;	
			$aux_info =  element('aux_info', $this->postdata, array());
			$data = $this->countries_model->getAllCountriesData($langid, $aux_info);
		
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "get_all" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "get_all" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	// with this call we get country data by id
	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())
             				->check($this->postdata);

			$langid = $this->lang_id;
			$id = $this->postdata["id"];

			$aux_info =  element('aux_info', $this->postdata, array()); 		

			$data = $this->countries_model->getCountryDataById($id, $langid, $aux_info);
			echo json_encode($data);
		}

		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}		
	}


	// with this call we get country data by ids as array
	public function get_by_ids()
	{
		try
		{
			validator::arr()->key('ids', validator::arr()->each(validator::numeric()))
                 			->check($this->postdata);

			$langid = $this->lang_id;
			$ids = $this->postdata["ids"];

			$aux_info =  element('aux_info', $this->postdata, array()); 		

			$data = $this->countries_model->getCountryDataByIds($ids, $langid, $aux_info);
			echo json_encode($data);
		}

		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "get_by_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "get_by_ids" , $e->getMessage(), 1, null, TRUE);
		}		
	}

	//get country data by code
	public function get_by_code()
	{
		try
		{
			validator::arr()->key('code',   validator::alpha()->notEmpty()->noWhitespace()->length(2, 3, TRUE))
             				->check($this->postdata);

            $langid = $this->lang_id; 				
			$code = $this->postdata["code"];
			$aux_info =  element('aux_info', $this->postdata, array()); 		
			
			$id = $this->countries_model->getCountryIdByCode($code); 

			$data = $this->countries_model->getCountryDataById($id, $langid, $aux_info);
			echo json_encode($data);

		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "get_by_code" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "get_by_code" , $e->getMessage(), 1, null, TRUE);
		}	
	}


	public function add()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('flag',  validator::string()->notEmpty(), FALSE)
							->key('code',  validator::string()->notEmpty(), FALSE)
							->key('name',  validator::string()->notEmpty())
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);
		
			$ids = $this->countries_model->insertCountries($this->postdata);
			$langid = $this->lang_id;
			$data = $this->countries_model->getCountryDataByIds($ids, $langid, "translations");
			echo json_encode(array("countries" => $data));
			Events::trigger('added_countires', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "add" , $e->getMessage(), 1, null, TRUE);
		}	
	}


	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',    validator::numeric())
							->key('flag',  validator::string()->notEmpty(), FALSE)
							->key('code',  validator::string(), FALSE)
							->key('name',  validator::string()->notEmpty(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);

			$this->countries_model->updateCountries($this->postdata);
			Events::trigger('updated_countries', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "update" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	//remove the country from DB and remove country assets
	public function remove()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())->check($this->postdata);
			$id = $this->postdata["id"];
			$data = $this->countries_model->removeCountry($id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("countries", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("countries", "remove" , $e->getMessage(), 1, null, TRUE);
		}	
	}	

}