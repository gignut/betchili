<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Competitions extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);
		$this->load->model(MODULE_API_FOLDER."/competitions_model");
	}
	
	public function get_all()
	{
		try
		{
			validator::arr()->key('active',   validator::bool())							
             				->check($this->postdata);

            $active   = element('active', $this->postdata, TRUE);
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->competitions_model->getAllCompetitions($active, $langid, $aux_info);
		
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "get_all" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "get_all" , $e->getMessage(), 1, null, TRUE);
		}
	}

	
	public function get_by_sport()
	{
		try
		{
			validator::arr()->key('active',   validator::numeric(), FALSE)
							->key('sport_id', validator::numeric())
             				->check($this->postdata);

            $active   = element('active', $this->postdata, TRUE);
            $skid     = $this->postdata["sport_id"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->competitions_model->getAllCompetitionsBySportId($skid, $active, $langid, $aux_info);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "get_by_sport" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "get_by_sport" , $e->getMessage(), 1, null, TRUE);
		}
	}

	
	public function get_by_league()
	{
		try
		{
			validator::arr()->key('league_id', validator::numeric())
             				->check($this->postdata);

            $lid     = $this->postdata["league_id"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->competitions_model->getCompetitionsByLeagueId($lid, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "get_by_league" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "get_by_league" , $e->getMessage(), 1, null, TRUE);
		}
	}

	
	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

            $id     = $this->postdata["id"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->competitions_model->getCompetitionById($id, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}

	// create competition, also copy league logo to competitions assets folder / force copy
	public function add()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('name',  validator::string()->notEmpty())
							->key('league_id',  validator::numeric())
							->key('start_date',  validator::date())
							->key('end_date',    validator::date())
							->key('priority',  validator::numeric(), FALSE)
							->key('logo',  validator::string()->notEmpty(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);
			
			foreach ($this->postdata as $competition)
			{
				$start_date = new DateTime($competition['start_date']);
				$end_date   = new DateTime($competition['end_date']);
				validator::positive()->check($end_date->getTimestamp() - $start_date->getTimestamp());
			}

			$ligueId = $this->postdata[0]["league_id"];

			$this->load->model(MODULE_API_FOLDER . "/leagues_model");
			$this->load->model(MODULE_ADMIN_FOLDER . "/asset_model");

			$logoName = $this->leagues_model->getLogo($ligueId);

			$leagueAssetPath = asset_model::getAssetFolder("leagues");
			$competitionAssetPath = asset_model::getAssetFolder("competitions");

			if($logoName)
			{
				foreach($this->postdata as $key => $competition)
				{
					$copiedNewName = asset_model::copyAsset($leagueAssetPath , $competitionAssetPath  , $logoName,  true);	
					$this->postdata[$key]["logo"] = $copiedNewName; 
				}  
			}

			$data = $this->competitions_model->insertCompetitions($this->postdata);
			echo json_encode($data);
			Events::trigger('added_competitions', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',    validator::numeric())
							->key('league_id',  validator::numeric(), FALSE)
							->key('name',  validator::string()->notEmpty(), FALSE)
							->key('logo',  validator::string()->notEmpty(), FALSE)
							->key('start_date',  validator::date(), FALSE)
							->key('end_date',    validator::date(), FALSE)
							->key('priority',  validator::numeric(), FALSE)
							->key('blocked',   validator::numeric(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);
		
			foreach ($this->postdata as $cdata)
			{
				$start = element('start_date', $cdata, NULL);
				$end = element('end_date', $cdata, NULL);
				if(!is_null($start) && !is_null($end))
				{
					$start_date = new DateTime($start);
					$end_date   = new DateTime($end);
					validator::positive()->check($end_date->getTimestamp() - $start_date->getTimestamp());	
				}

			}
			
			$this->competitions_model->updateCompetitions($this->postdata);
			Events::trigger('updated_competitions', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "update" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function update_participants()
	{
		try
		{
			validator::arr()->key('id',validator::numeric())
							->key('participants',  validator::arr()->each(validator::arr()
								->key('id',   validator::numeric())
								->key('action', validator::numeric()->between(0, 1001, TRUE))
								->key('score', validator::numeric(), FALSE)
								->key('played_games', validator::numeric(), FALSE)
								->key('wins', validator::numeric(), FALSE)
								->key('looses', validator::numeric(), FALSE)
								->key('draws', validator::numeric(), FALSE)
								->key('goals_in', validator::numeric(), FALSE)
								->key('goals_out', validator::numeric(), FALSE)
							))
							->check($this->postdata);

			$this->competitions_model->updateCompetitionParticipants($this->postdata['id'], $this->postdata['participants']);
			Events::trigger('updated_scores', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "update_participants" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "update_participants" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function remove()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())->check($this->postdata);
			$id = $this->postdata["id"];
			$this->competitions_model->removeCompetition($id);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("competitions", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("competitions", "remove" , $e->getMessage(), 1, null, TRUE);
		}
	}	

}