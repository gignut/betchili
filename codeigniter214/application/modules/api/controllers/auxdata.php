<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Auxdata extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);
	}

	public function get_sizes()
	{
		try
		{
			$data = array();
			$sql = "SELECT  * FROM " . TB_SIZE ;
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("auxdata", "get_sizes" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("auxdata", "get_sizes" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_languages()
	{
		try
		{
			$data = array();
			$sql = "SELECT  * FROM  " . TB_LANGUAGES ;
			$query = nut_db::query($sql);
			$data =  $query->num_rows() > 0 ? $query->result_array(): array();
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("auxdata", "get_languages" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("auxdata", "get_languages" , $e->getMessage(), 1, null, TRUE);
		}
	}
}