<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Leagues extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER."/leagues_model");
	}
	
	public function get_all()
	{
		try
		{
			$langid   = $this->lang_id;
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->leagues_model->getAllLeagues($langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "get_all" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "get_all" , $e->getMessage(), 1, null, TRUE);
		}
	}

	
	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

            $id       = $this->postdata["id"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->leagues_model->getLeagueById($id, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}
	
	// with this call we get league data by ids as array
	public function get_by_ids()
	{
		try
		{
			validator::arr()->key('ids', validator::arr()->each(validator::numeric()))
                 			->check($this->postdata);

			$langid = $this->lang_id;
			$ids = $this->postdata["ids"];

			$aux_info =  element('aux_info', $this->postdata, array()); 		

			$data = $this->leagues_model->getLeagueDataByIds($ids, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "get_by_ids" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "get_by_ids" , $e->getMessage(), 1, null, TRUE);
		}	
	}

	public function get_by_sport()
	{
		try
		{
			validator::arr()->key('sport_id', validator::numeric())
             				->check($this->postdata);

            $skid     = $this->postdata["sport_id"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->leagues_model->getAllLeaguesBySportId($skid, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "get_by_sport" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "get_by_sport" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_by_sports()
	{
		try
		{
			validator::arr()->key('sport_ids', validator::arr())
             				->check($this->postdata);

            $skids    = $this->postdata["sport_ids"];
			$langid   = $this->lang_id;	
			$aux_info = element('aux_info', $this->postdata, array());

			$data = $this->leagues_model->getAllLeaguesBySportIds($skids, $langid, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "get_by_sport" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "get_by_sport" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function add()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('logo',  validator::string()->notEmpty(), FALSE)
							->key('sport_id',  validator::numeric())
							->key('priority',  validator::numeric(), FALSE)
							->key('name',  validator::string()->notEmpty())
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);

							//->key('country_id',  validator::numeric(), FALSE)
		
			$data = $this->leagues_model->insertLeagues($this->postdata);
			echo json_encode($data);
			Events::trigger('added_leagues', $data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function update()
	{
		try
		{
			validator::arr()->each(validator::arr()
							->key('id',    validator::numeric())
							->key('sport_id',  validator::numeric(), FALSE)
							->key('name',  validator::string()->notEmpty(), FALSE)
							->key('logo',  validator::string()->notEmpty(), FALSE)
							->key('priority',  validator::numeric(), FALSE)
							->key('aux_info', validator::arr(), FALSE)
							)->check($this->postdata);

							//->key('country_id',  validator::string()->notEmpty(), FALSE)	
		
			
			$this->leagues_model->updateLeagues($this->postdata);
			Events::trigger('updated_leagues', $this->postdata);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "update" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "update" , $e->getMessage(), 1, null, TRUE);
		}
	}

	//remove the league from DB and remove league assets
	public function remove()
	{
		try
		{
			validator::arr()->key('id',   validator::numeric())->check($this->postdata);
			$id = $this->postdata["id"];
			$this->leagues_model->removeLeague($id);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("leagues", "remove" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("leagues", "remove" , $e->getMessage(), 1, null, TRUE);
		}
	}		

}