<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Bets extends chili_api_controller {

	public function __construct($p = null)
	{
		parent::__construct($p);

		$this->load->model(MODULE_API_FOLDER."/bets_model");
	}

	public function get_by_id()
	{
		try
		{
			validator::arr()->key('id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$id = $this->postdata['id'];

			$data = $this->bets_model->getBetById($id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "get_by_id" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "get_by_id" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_by_game()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$game_id = $this->postdata['game_id'];

			$data = $this->bets_model->getAllBetsByGame($game_id, $aux_info);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "get_all_by_game" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "get_all_by_game" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_sites_and_widgets()
	{
		try
		{
			$data = $this->bets_model->getAllSiteUsers();
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "get_all_sites_and_widgets" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "get_all_sites_and_widgets" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_all_by_site_and_widget()
	{
		try
		{
			validator::arr()->key('site_id', validator::numeric())
						    ->key('widget_id', validator::numeric(), FALSE)
						    ->key('user_id', validator::numeric(), FALSE)
						    ->key('game_id', validator::numeric(), FALSE)
						    ->key('limit',   validator::numeric(), FALSE)
             				->check($this->postdata);

			$aux_info = element('aux_info', $this->postdata, array());
			$site_id = $this->postdata['site_id'];
			$widget_id = element('widget_id', $this->postdata, null);
			$user_id = element('user_id', $this->postdata, null);
			$game_id = element('game_id', $this->postdata, null);
			$limit = element('limit', $this->postdata, null);

			$data = $this->bets_model->getAllBetsByWidget($site_id, $widget_id, $user_id, $game_id, $aux_info, $limit);

			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "get_all_by_site_and_widget" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "get_all_by_site_and_widget" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function get_bets_count_by_game()
	{
		try
		{
			validator::arr()->key('game_id', validator::numeric())
             				->check($this->postdata);

			$game_id = $this->postdata['game_id'];

			$data = $this->bets_model->getBetsCountByGame($game_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "get_bets_count_by_game" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "get_bets_count_by_game" , $e->getMessage(), 1, null, TRUE);
		}
	}	

	public function get_user_bets()
	{
		try
		{
			validator::arr()->key('user_id',   validator::numeric())
							->key('widget_id', validator::numeric())
							->key('limit',     validator::numeric(), FALSE)
							->key('offset',    validator::numeric(), FALSE)
             				->check($this->postdata);

			//TODO
			$aux_info = element('aux_info', $this->postdata, array());

			$userId 	= $this->postdata['user_id'];
			$widgetId   = $this->postdata['widget_id'];
			
			$limit = element("limit", $this->postdata, NULL);
			$offset = element("offset", $this->postdata, NULL);

			$data = $this->bets_model->getAllBetsByUser($userId, $widgetId, $aux_info, $limit, $offset, $this->lang_id);
			echo json_encode($data);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "get_user_bets" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "get_user_bets" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function add()
	{
		try
		{
			$postData = $this->postdata;
			validator::arr()->key('user_id',   		validator::numeric())
							->key('site_id',   		validator::numeric())
							->key('widget_id', 		validator::numeric())
							->key('amount', 	    validator::numeric())
							->key('won_amount', 	validator::numeric())
							->key('coefficient', 	validator::float())
							->key('bet_type',	 	validator::numeric(), FALSE)
							->key('event_chain', 	validator::arr()->each(validator::arr()
								->key('game_id',   	    validator::numeric())
								->key('event_id',   	validator::numeric())
								->key('coefficient',   	validator::float())
								->key('basis',   		validator::float(), FALSE)
								))
							->check($postData);

			//TODO:: CHILI 

			$eventChain = $postData["event_chain"];
			foreach ($eventChain as $key => &$value) {
				if($value["basis"] == "")
				{
					$value["basis"] = 0;
				}
			}

			$bet_type = element("bet_type", $this->postdata, 0); // TBD remove in future and make bet_type important
			$postData["bet_type"] = $bet_type;

			$betId = $this->bets_model->insertBet($postData);

			$siteId   = $postData["site_id"];
			$widgetId = $postData["widget_id"];
			$userId   = $postData["user_id"];

			$this->load->model(MODULE_API_FOLDER . "/users_model");
			$currentBalance = $this->users_model->getPlayerBalance($siteId, $widgetId, $userId);
			$responseData = array("balance" => $currentBalance, "bet_id" => $betId);

			echo json_encode($responseData);
			Events::trigger('added_bet', $responseData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $this->get_error_translation($e->getCode()), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "add" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "add" , $e->getMessage(), 1, null, TRUE);
		}
	}

	public function boostbet()
	{
		try
		{
			$postData = $this->postdata;
			validator::arr()->key('id',   		validator::numeric())
							->key('bet_type',   validator::numeric())
							->check($postData);

			// TBD remove in future and make bet_type important
			$betId   = element("id",       $postData, null); 
			$betType = element("bet_type", $postData, 0); 

			$retBetId = $this->bets_model->setBoost($betId, $betType);
			
			$responseData = array("bet_id" => $retBetId);
			echo json_encode($responseData);
			Events::trigger('boostbet', $responseData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $this->get_error_translation($e->getCode()), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			$this->api_error("bets", "boostbet" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			$this->api_error("bets", "boostbet" , $e->getMessage(), 1, null, TRUE);
		}
	}

}