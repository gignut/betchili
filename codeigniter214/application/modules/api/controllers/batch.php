<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Respect\Validation\Validator as validator;

class Batch extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function action()
	{
		try
		{
			$modulesPool = array();

			$apiData = $this->input->post("chili");
			$retData = array();

			ob_start();
		
			foreach ($apiData as $key => $value) 
			{
				if(!isset($value["url"]))
				{
					continue;
				}
				
				$postURL  = $value["url"];
				$postArray = explode("/", $postURL);
				$postCtrl = $postArray[0];
				$postFunc = $postArray[1];

				$postVars = array();
				$postVars["chili"] = element("vars", $value, array());

				$modulePath = MODULE_API_FOLDER . '/' . $postCtrl;
				
				$moduleObj = null;
				if(isset($modulesPool[$postCtrl]))
				{
					$moduleObj = $modulesPool[$postCtrl];
					$moduleObj->initData($postVars);
				}
				else
				{
					$moduleObj = $this->load->module($modulePath, $postVars);
					$modulesPool[$postCtrl] = $moduleObj;
				}

				call_user_func(array($moduleObj, $postFunc));

				$buffer = ob_get_contents();
				ob_clean();
				$retData[$key] = json_decode($buffer);
			}

			$modulesPool = null;
			ob_end_clean();
			echo json_encode($retData);
		}
		catch(Api_exception $e)
		{
			$this->api_error('', '', $e->getMessage(), $e->getCode(), $e->getData());
		}
		catch(InvalidArgumentException $e)
		{
			nut_log::log("error", "ERROR::BATCH InvalidArgumentException \n" . $e->getMainMessage());
			//$this->api_error("batch", "action" , $e->getMainMessage(), 2);
		}
		catch(Exception $e)
		{
			nut_log::log("error", "ERROR::BATCH Exception \n" . $e->getMessage());
			// $this->api_error("batch", "action" , $e->getMessage(), 1, null, TRUE);
		}	
		
	}
}