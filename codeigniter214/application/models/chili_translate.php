<?php
/**
 * translation model
 */
class chili_translate extends CI_Model {	
	public static $AVAILABLE_LANGUAGES  = array("1" => EN , "2" => RU , "3" => AM );
	public static $DEFAULT_TRANSLATIONS = array("1" => DEFAULT_EN , "2" => DEFAULT_RU, "3" => DEFAULT_AM);
	
	/* TRANSLATION CODES FOR VARIOUS TABLES*/
	public static $EVENT_TEMPLATES = 101;
	public static $EVENT_TEMPLATES_LONG = 1011;
	public static $LEAGUES 		= 201;
	public static $PARTICIPANTS = 301;
	public static $COUNTRIES 	= 401;
	public static $SPORT        = 501;
	public static $COMPETITIONS = 601;
	public static $GAMES		= 701;
	public static $SPORTS		= 801;

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * [getLangId returns language id by language code, if wrong code is provided will return default language id]
	 * @param  Strin
	 * g $name [language code e.g "en", "ru"]
	 * @return int       [returns lang id]
	 */
	public static function getLangId($name)
	{
		try
		{
			$ret_id = NULL;
			foreach(chili_translate::$AVAILABLE_LANGUAGES as $langIdKey => $langName)
			{
				if($name === $langName)
				{
					$ret_id = $langIdKey;
					break;
				}
			}
			
			return $ret_id;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public static function getLangCode($id)
	{
		try
		{
			$ret_code = NULL;
			foreach(chili_translate::$AVAILABLE_LANGUAGES as $langIdKey => $langName)
			{
				if(intval($id) === intval($langIdKey))
				{
					$ret_code = $langName;
					break;
				}
			}
			
			return $ret_code;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public static function getTranslation($langId, $rowId, $tableId)
	{
		try
		{
			$sql = "SELECT * FROM " . TB_TRANSLATIONS . " WHERE type_id = ? AND row_id = ? AND lang_id = ? ";
			$query = nut_db::query($sql, array($tableId, $rowId, $langId));
				
			$data =  $query->num_rows() > 0 ? $query->result_array(): null;
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}

	}

	public static function addTranslation($langId, $rowId, $tableId , $translation)
	{
		try
		{
			$sql = "INSERT INTO " . TB_TRANSLATIONS . " (lang_id, row_id, type_id, translation) VALUES (?, ? , ? , ? )";
			$query = nut_db::query($sql, array($langId, $rowId, $tableId, $translation));
			return $query;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public static function updateTranslation($langId, $rowId, $tableId , $translation)
	{
		try
		{
			$sql = "UPDATE  " . TB_TRANSLATIONS . " SET translation = ? WHERE  row_id = ? AND type_id = ? AND lang_id = ?";
			$query = nut_db::query($sql, array($translation, $rowId, $tableId, $langId ));
			return $query;
		}
		catch(Exception $e)
		{
			throw $e;
		}

	}

	public static function getAllTranslations($rowId, $tableId)
	{
		try
		{
			$sql = "SELECT * FROM " . TB_TRANSLATIONS . " WHERE type_id = ? AND row_id = ?";
			$query = nut_db::query($sql, array($tableId,$rowId));
			$data = array();
				
			$langid = null;
			$translatedLanguages = array();
				
			foreach($query->result_array() as $row)
			{
				$langid = $row['lang_id'];
				$translatedLanguages[$langid] = true;
				$data[$langid] = array('lang' => chili_translate::$AVAILABLE_LANGUAGES[$langid], "translation" => $row['translation']);
			}
				
			foreach(chili_translate::$AVAILABLE_LANGUAGES as $langIdKey => $langName)
			{
				if(!isset($translatedLanguages[$langIdKey]))
				{
					$defaultTranslation = self::$DEFAULT_TRANSLATIONS[$langIdKey];
					self::addTranslation($langIdKey, $rowId, $tableId , $defaultTranslation);
					$data[$langIdKey] = array('lang' => $langName, "translation" => $defaultTranslation);
				}
			}
				
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public static function getFullTranslations($rowId, $tableId)
	{
		try
		{
			$sql = "SELECT * FROM " . TB_TRANSLATIONS . " WHERE type_id = ? AND row_id = ?";
			$query = nut_db::query($sql, array($tableId,$rowId));
			$data = array();
				
			$langid = null;
			$langcode = null;
			$translatedLanguages = array();
				
			foreach($query->result_array() as $row)
			{
				$langid = $row['lang_id'];
				$langcode = chili_translate::$AVAILABLE_LANGUAGES[$langid];
				$translatedLanguages[$langid] = true;
				$data[$langcode] = $row['translation'];
			}
				
			foreach(chili_translate::$AVAILABLE_LANGUAGES as $langIdKey => $langNameCode)
			{
				if(!isset($translatedLanguages[$langIdKey]))
				{
					$defaultTranslation = self::$DEFAULT_TRANSLATIONS[$langIdKey];
					$data[$langNameCode] = $defaultTranslation;
				}
			}
				
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public static function getFullTranslationsByIDs($rowIds, $tableId)
	{
		try
		{
			$values = implode(',', array_fill(0, count($rowIds), "?"));
			$sql = "SELECT * FROM " . TB_TRANSLATIONS . " WHERE type_id = ? AND row_id IN (".$values.")";
			array_unshift($rowIds, $tableId);

			$query = nut_db::query($sql, $rowIds);

			$data = array();
				
			$langid = null;
			$langcode = null;
			$translatedLanguages = array();
			
			$result =  $query->num_rows() > 0 ? $query->result_array(): array();

			foreach($result as $row) {
				$data[$row['row_id']] = array();
			}
			foreach($result as $row)
			{
				$langid = $row['lang_id'];
				$langcode = chili_translate::$AVAILABLE_LANGUAGES[$langid];
				$translatedLanguages[$langid] = true;
				$data[$row['row_id']][$langcode] = $row['translation'];
			}
				
			foreach(chili_translate::$AVAILABLE_LANGUAGES as $langIdKey => $langNameCode)
			{
				if(!isset($translatedLanguages[$langIdKey]))
				{
					foreach($result as $row) {
						$defaultTranslation = self::$DEFAULT_TRANSLATIONS[$langIdKey];
						$data[$row['row_id']][$langNameCode] = $defaultTranslation;
					}
					
				}
			}
				
			return $data;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public static function createDefaultTranslations($rowId,$tableId)
	{
		try
		{
			nut_db::transactionStart("createDefaultTranslations");
			foreach(chili_translate::$AVAILABLE_LANGUAGES as $langId => $langName)
			{
				$defaultTranslation = self::$DEFAULT_TRANSLATIONS[$langId];
				self::addTranslation($langId, $rowId, $tableId, $defaultTranslation);
			}
			nut_db::transactionCommit("createDefaultTranslations");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("createDefaultTranslations");
			throw $e;
		}
	}

	public static function deleteAllTranslations($typeId, $rowId)
	{
		try
		{
			$sql = "DELETE FROM " . TB_TRANSLATIONS . " WHERE type_id = ? AND row_id = ?";
			$query = nut_db::query($sql, array($typeId, $rowId));
			return $query;
		}
		catch (Exception $e)
		{
			throw $e;
		}
	}

	public static function deleteAllTranslationsBatch($typeId, $rowIds)
	{
		try
		{
			$values = implode(',', array_fill(0, count($rowIds), "?"));
			$sql = "DELETE FROM " . TB_TRANSLATIONS . " WHERE type_id = ? AND row_id in (". $values . ")";
			$query = nut_db::query($sql, array_merge((array)$typeId, $rowIds));
			return $query;
		}
		catch (Exception $e)
		{
			throw $e;
		}

	}

	// ['lang' => 1 , 'id' => 54 , 'type' => 401, 'text' = > "translation text" ]
	public static function updateTranslations($data)
	{
		try
		{
			nut_db::transactionStart("updateTranslations");
			foreach($data as $translation)
			{
				self::updateTranslation($translation['lang'], $translation['id'], $translation['type'], $translation['text']);
			}
			nut_db::transactionCommit("updateTranslations");
		}
		catch(Exception $e)
		{
			nut_db::transactionRollback("updateTranslations");
			throw $e;
		}

	}
}