<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
$pratacol = "http://";

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
		|| $_SERVER['SERVER_PORT'] == 443) {

	$pratacol = "https://";
}

define("PRATACOL",       $pratacol);
define("BASE_DOMAIN",    "www.betchili.com");
define("BASE_URL",       $pratacol . BASE_DOMAIN . '/');
define("INDEX_PHP",      "index.php");
define("BASE_INDEX",     BASE_URL . INDEX_PHP . "/");
define("MEDIA_URL",      BASE_URL . "media/");
define("VENDORLIB",      "vendorlib");

define("ENCRYPTION_KEY", "gn14chili");
define("SESSION_NAME",   "PHPSESSID");



// LIBRARY CONFIGURATION
define("LIB_NUT", "nut/core/");
define("LIB_FIRELOG", "nut/firelog/");
define("LIB_FACEBOOK", "facebook_sdkv3.2.2/");

// PLATFORMS CONFIGURATION
define("PLATFORM_CHILI", 0);

define("PLATFORM_FACEBOOK", 1);
define("FB_APP_ID", "356092921178484");
define("FB_APP_SECRET", "6a19ae9f0d9252fc2ab1daf80752a216");

define("PLATFORM_GOOGLE", 2);
define("GOOGLE_CLIENT_ID", "1003526247439-nrsg7d0ijjnsnpf4bcbhl65hkge2m375.apps.googleusercontent.com");
define("GOOGLE_CLIENT_SECRET", "tOGFvI9torgCV7E3h9ufn6rs");

define("PLATFORM_ODNOKLASSNIKI", 3);
define("ODN_APP_ID", 1085245440);
define("ODN_APP_PUBLIC_KEY",  "CBAJIQKBEBABABABA");
define("ODN_APP_PRIVATE_KEY", "FB5E94588EA6D359AAE78251");


define("PLATFORM_VKONTAKTE", 4);
define("VK_APP_ID", 4285432);
define("VK_APP_SECRET", "vHddRnldw3s8fNFIKt2h");

define("PLATFORM_MAILRU", 5);
define("MAILRU_APP_ID", 719390);
define("MAILRU_APP_PRIVATE_KEY", "da27ca72b7e7547d1abc84955e81c7f5");
define("MAILRU_APP_SECRET", "0e3b2eb82d68b70360ed728241ad0ea3");

// SUPERSONIC PRIVATE KEY
//app id, sometimes refered in docs as app key
define("SUPERSONIC_APP_ID", "2f6014fd");
define("SUPERSONIC_PRIVATE_KEY", "bet2014bibar");

// SUPERSONIC API CREDENTIALS
define("SUPERSONIC_API_URL", 'http://www.supersonicads.com/delivery/brandConnect.php');
define("SUPERSONIC_ACCESS_KEY", "6301041b");
define("SUPERSONIC_SECRET_KEY", "808848bbc9cadf665d8beba246e017ef");


//DATABASE TABLES

define("TB_COUNTRIES"       			, "countries");
define("TB_PARTICIPANTS"    			, "participants");
define("TB_COMPETITIONS"    			, "competitions");
define("TB_LEAGUES"         			, "leagues");
define("TB_TRANSLATIONS"    			, "translations");
define("TB_COMPETITION_PARTICIPANTS"    , "scores");
define("TB_GAMES"                       , "games");
define("TB_SPORTS"                      , "sportkind");
define("TB_RESULTS"                     , "results");
define("TB_GAMEPARTICIPANTS"            , "gameparticipants");
define("TB_EVENTS"                      , "events");
define("TB_EVENTS_GROUPS"               , "eventsgroups");
define("TB_EVENTS_TEMPLATES"            , "eventstemplates");
define("TB_BOOKMAKERS"                  , "bookmakers");

define("TB_SITEUSERS"                   , "siteusers");
define("TB_WIDGETS"            			, "widgets");
define("TB_WIDGET_TYPES"                , "widget_types");
define("TB_WIDGET_LEAGUES"              , "widget_leagues");
define("TB_LANGUAGES"                   , "languages");
define("TB_SIZE"                   		, "widget_size");

define("TB_USERSBALANCES"               , "usersbalances");
define("TB_USERSPROFILES"               , "usersprofiles");
define("TB_USERS_CHILI"                 , "users_chili");
define("TB_USERS_FACEBOOK"              , "users_facebook");
define("TB_USERS_GOOGLE"                , "users_google");
define("TB_USERS_ODN"                   , "users_odnoklassniki");
define("TB_USERS_VK"                    , "users_vkontakte");
define("TB_USERS_MR"                    , "users_mailru");
define("TB_BETS"                        , "bets");
define("TB_EVENT_CHAIN"                 , "event_chain");
define("TB_NEWS"                        , "news");
define("TB_CHALLENGES"                  , "challenges");
define("TB_LINKS"                       , "links");
define("TB_CLIENT_REQUEST"             , "client_request");

define("REDIS_HOST", "127.0.0.1");
define("REDIS_PORT", 6379);
define("REDIS_SCHEME", "tcp");

define("FIREBASE", "https://betchili.firebaseio.com/");

// GAME CONFIGS
define("GAME_QTY_AUTHORIZED", 20);
define("GAME_QTY_UNAUTHORIZED", 5);
define("ACTIVITY_BET_OFFSET", 7);
define("TOPS_LIMIT", 50);
define("USERS_LIMIT", 20);
define("MIN_BET_AMOUNT", 100);
define("MAX_BET_AMOUNT", 500);
define("MIN_CHALLANGE_AMOUNT", 100);
define("MAX_CHALLANGE_AMOUNT", 1000);

