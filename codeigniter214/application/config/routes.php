<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['404_override'] = '';

$route['default_controller'] 	= MODULE_SITE_FOLDER . "/show/index";
$route['home/(:any)'] 			= MODULE_SITE_FOLDER . "/show/index/$1";
$route['signup']             	= MODULE_SITE_FOLDER . "/show/signup";
$route['login']              	= MODULE_SITE_FOLDER . "/show/login";
$route['request']              	= MODULE_SITE_FOLDER . "/show/request";
$route['logout']              	= MODULE_SITE_FOLDER . "/account/logout";
$route['forgotpassword']     	= MODULE_SITE_FOLDER . "/show/forgotpassword";
$route['resetpassword/(:any)']  = MODULE_SITE_FOLDER . "/show/resetpassword/$1";

$route['widgets']               = MODULE_SITE_FOLDER . "/show/widgetsDashboard";
$route['widget/(:num)']         = MODULE_SITE_FOLDER . "/show/game/$1";
$route['widget/(:num)/design']  = MODULE_SITE_FOLDER . "/show/game/$1/design";
$route['create']                = MODULE_SITE_FOLDER . "/show/game/new";

$route['account/settings']      = MODULE_SITE_FOLDER . "/show/accountSettings";
$route['account/password']      = MODULE_SITE_FOLDER . "/show/accountPassword";

$route['demo']                  = MODULE_SITE_FOLDER . "/demo/game";

$route['auth/odnoklassniki/(:any)']   = MODULE_EMBED_FOLDER . "/wauth/odnoklassniki/$1";
$route['auth/vkontakte/(:any)']       = MODULE_EMBED_FOLDER . "/wauth/vkontakte/$1";
$route['auth/mailru/(:any)']          = MODULE_EMBED_FOLDER . "/wauth/mailru/$1";

// SUPERSONIC ADS
$route['supersonic/grant']      = MODULE_EMBED_FOLDER . "/supersonic/grant";
$route['supersonic/revoke']     = MODULE_EMBED_FOLDER . "/supersonic/revoke";

/* End of file routes.php */
/* Location: ./application/config/routes.php */