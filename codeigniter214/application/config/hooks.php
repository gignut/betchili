<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_system'][] = array(
	'function' => 'chili_define_constants',
	'filename' => 'define.php',
	'filepath' => 'hooks/chili',
);

// NAMESPACE AUTOLOADER
$hook['pre_system'][] = array(
 'function' => 'chili_load_libraries',
 'filename' => 'autoload.php',
 'filepath' => 'hooks/chili'
);

$hook['pre_system'][] = array(
 'function' => 'chili_load_redis',
 'filename' => 'autoload.php',
 'filepath' => 'hooks/chili'
);

$hook['pre_system'][] = array(
      'class'    => 'Errorhandler',
      'function' => 'register_handlers',
      'filename' => 'Errorhandler.php',
      'filepath' => 'hooks/chili'
);

// close connection of mysql
$hook['post_system'][] = array(
      'function' => 'mysqlConnectionClose',
      'filename' => 'mysql.php',
      'filepath' => 'hooks/chili'
);

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */