
###############################################
##
## Xampp environment setup tool. Version 1.1
## Supported OS: windows, mac
## Tested on windows 7, Mac
##
###############################################


import os
import argparse
import re
import time
import sys
import getpass
import subprocess


############## CONFIG #############

OS = 'nt'

CONFIG={'posix': {'XAMP_PATH'  : '/Applications/XAMPP/', 
                  'HOSTS_FILE' : '/etc/hosts',
                  'VHOSTS_FILE': os.path.join('etc','extra','httpd-vhosts.conf')},
        'nt'   : {'XAMP_PATH'  : 'C:\\xampp',
                  'HOSTS_FILE' : 'C:\\Windows\\System32\\drivers\\etc\\hosts',
                  'VHOSTS_FILE': os.path.join('apache','conf','extra','httpd-vhosts.conf')},
       }

XAMPP_18 = '''##start##
## {2} ##
NameVirtualHost *:80

<Directory "{0}">
Require all granted
# Any other directory-specific stuff
</Directory>

<VirtualHost *:80>
DocumentRoot "{0}"
ServerName {1}
</VirtualHost>
##end##
'''

XAMPP_17 = '''##start##
## {2} ##
NameVirtualHost *:80

<Directory "{0}">
Order Allow, Deny
Allow from All
# Any other directory-specific stuff
</Directory>

<VirtualHost *:80>
DocumentRoot "{0}"
ServerName {1}
</VirtualHost>
##end##
'''
###################################


# main entry
def main():
    ### get options
    args = get_options()
    ### set OS global variable
    set_os()
    ### Check options
    check_options(args)
    ### Get current dir
    path = args.workspace if args.workspace else os.path.dirname(os.path.abspath(__file__))
    xampp_path = args.xampp if args.xampp else CONFIG[OS]['XAMP_PATH']
    domain = args.domain.strip()
    ### Make linux style path
    lin_path = os.path.abspath(path).replace('\\', '/')
    ### Setup environment
    setup_env(lin_path, xampp_path, domain, args.xamp_version.strip())
    ### Set user permissions on workspace
    #set_permissions(path)
    ### Restart XAMPP
    restart_xampp(xampp_path)


def set_permissions(path):
    print 'Setting permissions to "{0}"...'.format(path)
    username = getpass.getuser()
    try:
        out = ''
        FNULL = open(os.devnull, 'w')
        #retcode = call('takeown.exe /f "{0}" /r /d y'.format(path), stdout=PIPE, shell=True)
        #if retcode < 0: print >> sys.stderror, "Terminated by signal", -retcode
        retcode = call('icacls.exe "{0}" /reset /T'.format(path),  stdout=FNULL, shell=True)
        if retcode < 0: print >> sys.stderror, "Terminated by signal", -retcode
        retcode = call('icacls.exe "{0}" /grant:r "{1}:F"'.format(path, username), stdout=FNULL, shell=True)
        if retcode < 0: print >> sys.stderror, "Terminated by signal", -retcode
        retcode = call('icacls.exe "{0}" /grant:r "EVERYONE:R"'.format(path), stdout=FNULL, shell=True)
        if retcode < 0: print >> sys.stderror, "Terminated by signal", -retcode
        retcode = call('icacls.exe "{0}" /inheritance:r'.format(path), stdout=FNULL, shell=True)
        if retcode < 0: print >> sys.stderror, "Terminated by signal", -retcode
    except OSError as e:
        print >> sys.stderr, "ERROR:", e


def restart_xampp(xampp_dir):
    print 'Restart XAMPP...'
    try:
        if (OS == 'nt'):
            retcode = subprocess.Popen('start /b taskkill /IM xampp-control.exe /F', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).wait()
            time.sleep(1)
            retcode = subprocess.Popen('start /b {0}'.format(os.path.join(xampp_dir, 'xampp-control.exe')), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).wait()
            cmd = '{0}'.format(os.path.join(xampp_dir, 'xampp_stop.exe'))
            sts = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).wait()
        else:
            retcode = subprocess.Popen("ps -ef | grep 'XAMPP Contro[l]' | awk '{print $2}' | xargs kill -9", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).wait()
            time.sleep(1)
            retcode = subprocess.Popen('open {0}'.format(os.path.join(xampp_dir, 'XAMPP\ Control.app')), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).wait()
            cmd = 'sudo {0} stop'.format(os.path.join(xampp_dir, 'xamppfiles', 'xampp'))
            sts = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).wait()
        print "Done"
    except OSError as e:
        print >> sys.stderr, "ERROR:", e


def check_options(args):
    if args.xampp and not os.path.exists(args.xampp):
        print 'ERROR: XAMPP path "{0}" does not exist.'.format(args.xampp.strip())
        exit()
    if args.workspace and not os.path.exists(args.workspace):
        print 'ERROR: workspace path "{0}" does not exist.'.format(args.workspace)
        exit()
    if not re.match('[a-zA-Z\d-]{,63}(\.[a-zA-Z\d-]{,63})+', args.domain.strip()):
        print 'ERROR: invalid domain name "{0}" specified.'.format(args.domain.strip())
        exit()


def setup_env(env_dir, xampp_dir, domain, xversion):
    print 'Setting up environment...'
    if xversion.startswith('1.8'):
    	xconf = XAMPP_18
    elif xversion.startswith('1.7'):
    	xconf = XAMPP_17
    username = getpass.getuser()
    vhost_file = os.path.join(CONFIG[OS]['XAMP_PATH'], CONFIG[OS]['VHOSTS_FILE'])
    fh = open(vhost_file, 'r')
    fstr = fh.read()
    fh.close()
    if re.search(domain, fstr):
        m = re.match('(.*)(##start##\n## (\w+) ##\n.*?<Directory\s*\"([^\"]*)\">.*?'+domain+'.*?##end##)(.*)', fstr, re.DOTALL)
	if m:
	    #if m.group(3) == username:
	        print 'You have already configuration for domain "{0}" ==> [{1}]'.format(domain, m.group(4))
		print 'Would you like to overwrite ? [y/n]'
		var = raw_input("")
		if var == 'y':
		    m = re.match('(.*)(##start##.*?'+domain+'.*?##end##)(.*)', fstr, re.DOTALL)
		    if m:
		        new = m.group(1) + xconf.format(env_dir, domain, username) + m.group(3)
		        fh = open(vhost_file, 'w')
		        fh.write(new)
		    else:
		        print 'Error occured. Please try again or check "{0}" file.'.format(vhost_file)
		        print 'Environment setup is terminated'
		        exit(0)
		else: 
		    print 'Environment setup is terminated'
		    exit(0)
	    #else:
		#     print 'XAMPP already have an configuration with "{0}" domain. Pleas use another domain.'.format(domain)
		#     exit(0)
	else: 
            print 'Error occured. Please try again or check vhosts file.'
            exit(0)
    else:
        fstr = fstr + xconf.format(env_dir, domain, username)
        fh = open(vhost_file, 'w')
        fh.write(fstr)
	fh.close()
	fh = open(CONFIG[OS]['HOSTS_FILE'], 'a')
	host = '\n127.0.0.1	{0}'.format(domain)
	fh.write(host)
	fh.close()
    print 'Your environment "{1}" is set up on \'{0}\''.format(env_dir, domain)


def set_os():
    global OS
    OS = os.name


def get_options():
    parser = argparse.ArgumentParser(
        fromfile_prefix_chars='+',
        description='Setup the workspace.',
        usage='''
		%(prog)s [options]
	      ''')
    parser.add_argument('-xd', '--xampp',
        help="XAMPP root directory",
        metavar='<path>',
        required=False,
        type=str,
        default=None,
        action='store')
    parser.add_argument('-w', '--workspace',
        help='''Workspace directory. By default will be used current directory''',
        metavar='<path>',
        required=False,
        type=str,
        default=None,
        action='store')
    parser.add_argument('-x', '--xamp_version',
	help="XAMPP version. Example: 1.7.0, 1.8.1 ...",
        metavar='<str>',
        required=False,
        type=str,
        default='1.8.1',
        action='store')
    parser.add_argument('-d', '--domain',
        help="Domain name",
        metavar=' <str>',
        required=True,
        type=str,
        default=None,
        action='store')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()

