  function ErrorData(){
    this.error;
    this.message;
    this.data;
    return this;
  }

   var chilisiteApp = angular.module("chilisiteApp", ['ngResource', 'ngNutron', '$strap.directives', 'colorpicker.module' ]);

    chilisiteApp.directive('pickerOpen', function () {
      return {
        restrict: 'A',
           link: function(scope, elm, attrs) {
            elm.addClass('cp-cursor');
            scope.$on('colorPicker', function(ev, data) {
              if (data == attrs.pickerOpen) {
                elm.click();
              }
            });
          }
      };
    });

    //ng-click="openColorPicker($event, 'challange_bg_color')"

    chilisiteApp.directive('pickerClick', function ($rootScope) {
      return {
        restrict: 'A',
           link: function(scope, elm, attrs) {
            elm.addClass('cp-cursor');
            var name = attrs.pickerClick;
            elm.on('click', function($event) {
              if (name != 'stop') {
                $rootScope.$broadcast('colorPicker', name);
              }
              $event.stopPropagation();
            });
          }
      };
    });    

   chilisiteApp.factory("ChiliConfigs", function($window){
    return {
              languages  : ["en" , "ru"],
              BASE_INDEX : $window.BASE_INDEX,
              BASE_URL   : $window.BASE_URL,
              MEDIA_URL  : $window.MEDIA_URL,
              API_KEY    : "chili"
          }; 
    });

    chilisiteApp.factory("ChiliUtils" , function($window){
      var uself =  {
          block : function(){
            //backgroundColor: '#f00', color: '#fff'
            jQuery.blockUI({
              showOverlay: false,  
              css: {
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                 } 
          }); 
          },
          unblock : function(){
            jQuery.unblockUI({ fadeOut: 200 }); 
          },

          action: function(responseData){
              if(responseData && responseData["module"])
              {
                var moduleName = responseData["module"];
                switch(moduleName)
                {
                  case "noty" :
                      return uself.noty(responseData.action,responseData.message);
                      break;
                  case "redirect" :
                    uself.redirect(responseData.action,responseData.message);
                    break;
                }
              }

              return true;
          },

          redirect: function(action, message){
            if(!action)
            {
                return;
            }
            if(action == 'reload')
            {
              location.reload(true);
              return;
            }

            window.location.href = message;
          },

          noty : function(action,message){
            if(!action)
            {
                return;
            }

            var notyOb = null;
            switch(action)
            {
                case 'success':
                  message = typeof message !== 'undefined' ? message : "Everything is OK";
                  notyOb = $window.noty({text: message, layout: 'topRight', type: 'success', timeout : true, closeWith: ['click', 'hover'] });
                  break;
                case 'error':
                  message = typeof message !== 'undefined' ? message : "Something wnet WRONG";
                  notyOb = $window.noty({text: message, layout: 'topRight', type: 'error', timeout : true, closeWith: ['click', 'hover']});
                  break;
            }
          }
      };

      return uself;
    });