chilisiteApp.directive('colorEdit', function() {
    return {
        scope:true,
        require: '?ngModel',
        link: function(scope, elm, attrs, ctrl) {

            scope.lastValidModel = "#000000";
            scope.validity;

            scope.isValidSymbol = function(ind, charAtInd){
                    switch(ind)
                    {
                      case 0:
                        return charAtInd === "#";
                        break;
                      case 1:
                      case 2:
                      case 3:
                      case 4:
                      case 5:
                      case 6:
                      return  charAtInd === '0' || 
                              charAtInd === '1' ||
                              charAtInd === '2' ||
                              charAtInd === '3' ||
                              charAtInd === '4' ||
                              charAtInd === '5' ||
                              charAtInd === '6' ||                       
                              charAtInd === '7' ||
                              charAtInd === '8' ||
                              charAtInd === '9' ||
                              charAtInd === 'A' ||
                              charAtInd === 'B' ||
                              charAtInd === 'C' ||
                              charAtInd === 'D' ||
                              charAtInd === 'E' ||
                              charAtInd === 'F' ;
                      break;
                    }
            };

            scope.prepareModel = function(){
                ctrl.$viewValue = scope.lastValidModel;
                ctrl.$render();
                ctrl.$setValidity('colorEdit', true);
                return scope.lastValidModel;
            };

            ctrl.$formatters.push(function(viewValue) {
                  if(viewValue)
                  {
                    return viewValue.toUpperCase();
                  }
                  else
                  {
                    return  scope.lastValidModel;
                  }
             });

            ctrl.$parsers.push(function(viewValue) {
                scope.validity = true;
                
                if(viewValue === undefined || viewValue === "" || viewValue.length > 7)
                {
                   scope.validity = false;
                   return scope.prepareModel();
                }

                viewValue = viewValue.toUpperCase();
                var chars = viewValue.split('');
                var len = chars.length;
                for(var i = 0; i < len; i++)
                {
                   if(!scope.isValidSymbol(i, chars[i]))
                   {
                       scope.validity = false;
                       break;
                   }
                }

                if(scope.validity)
                {
                  scope.lastValidModel  = viewValue;
                }
              
                return scope.prepareModel();
            });
        }
    };
});


chilisiteApp.factory("SiteWidgetAPI" , function(ChiliConfigs, $http){
      return {
            createGameWidget : function(obj){
              return $http({
                  method: "POST",
                  url: ChiliConfigs.BASE_INDEX + "betchili/widgets/createGameWidget",
                  data :  $.param(obj),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });   
            },

            updateGameWidget : function(obj){
              return $http({
                  method: "POST",
                  url: ChiliConfigs.BASE_INDEX + "betchili/widgets/updateGameWidget",
                  data :  $.param(obj),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });   
            },

            removeWidget : function(id) {
              var ob = { "widget_id" :  id };
              return $http({
                  method: "POST",
                  url: ChiliConfigs.BASE_INDEX + "betchili/widgets/removeWidget",
                  data :  $.param(ob),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });   
            }

          };
});


var WidgetFactory = {
  createModel: function(data){
      var m = null;
      switch(parseInt(data.widget_type_id))
      {
        case 1:
          m = new GameWidgetModel();
          m.setData(data);
          break;
      }

      return m;
  }
};

function GameWidgetModel(){
  this.id;
  this.name;
  this.size_id;
  this.size_label;
  this.height;
  this.width;
  this.css;
  this.widget_type_id;  
  this.widget_type_name;
  this.lang_id;
  this.lang_name;
 
  this.create_stamp;
  this.update_stamp;  

  //specific to game widget
  this.leagues = new Array();
  return this;
}

GameWidgetModel.prototype.setData = function(data){
      this.id                    = data.id;
      this.name                  = data.name;    
      this.size_id               = data.size_id;
      this.size_label            = data.size_label;
      this.height                = data.height;
      this.width                 = data.width;
      this.css                   = data.css;
      this.widget_type_id        = data.widget_type_id;
      this.lang_id               = data.lang_id;
      this.lang_name             = data.lang_name;
      this.create_stamp          = data.create_stamp;
      this.update_stamp          = data.update_stamp;

      this.leagues               = data.leagues;
};

GameWidgetModel.prototype.getData = function(){
      var obj = {};
      obj.id                    = this.id;
      obj.name                  = this.name;    
      obj.size_id               = this.size_id;
      obj.lang_id               = this.lang_id;
      obj.css                   = this.css;
      obj.leagues               = this.leagues;
      return obj;
};

function WidgetDashboardCtrl($scope, $http, $window, ChiliUtils, SiteWidgetAPI) {
  $scope.siteUserDomain;
  $scope.siteUserWidgets;
  $scope.widgetCount  = 0;

  $scope.currentWidget;

  $scope.init = function(){
    $scope.siteUserDomain = $window.siteUserDomain;
    $scope.siteUserWidgets  = {};
    var siteUserWidgets     = $window.siteUserWidgets;
    var count = 0;
    for(var key in siteUserWidgets){
      var obj = siteUserWidgets[key];
      var wm =   WidgetFactory.createModel(obj);
      $scope.siteUserWidgets[obj.id] = wm;
      count++;
    }
    $scope.widgetCount = count;
  };
}

function GameWidgetCtrl($scope, $http, $window, ChiliConfigs, ChiliUtils, SiteWidgetAPI) {
  $scope.widgetModel = new GameWidgetModel();
  $scope.widgetTypes;
  $scope.leaguesList = [];
  $scope.selectedLeagues = [];

  $scope.gameWidgetStep = 'main';
  $scope.gameWidgetFormSubmited;

  var hasConfigChanged = false;
  var hasDesignChanged = false;

    // DESIGN PART
  $scope.designModel = null;

  $scope.mapToDesign = function(data) {
    angular.forEach(data, function(item, index) {
      $scope.designModel[index] = item;
    });
  };

  $scope.saveGameWidget = function(){
      
      $scope.widgetModel.leagues = new Array();
      $scope.gameWidgetFormSubmited = true;
      
      if($scope.gameWidgetForm.$valid)
      {
          var postData = {}; 
          if($scope.gameWidgetForm.$dirty)
          {
              //update configs
              angular.forEach( $scope.selectedLeagues, function(value, key){
                $scope.widgetModel.leagues.push(value);
              });

              postData = $scope.widgetModel.getData();
          }
          
          
          if(!$scope.widgetModel.id)
          {
            ChiliUtils.block();
            SiteWidgetAPI.createGameWidget(postData).success(function(data, status, headers, config) {
                if(ChiliUtils.action(data))
                {
                  // TODO:: now after create we redirect the page to design tab
                  // thats why we dont need the below ones
                  
                  // $scope.widgetModel.setData(data);
                  // $scope.resetGameForm();
                  // $scope.gameWidgetStep = 'design';
                }
                 ChiliUtils.unblock();
            }).error(function(data, status, headers, config) {
                console.log("js::GameWidgetCtrl saveGameWidget");
            });
          }
          else
          {
              // after updating existing ones we dont switch to design tab
              postData.id       =  $scope.widgetModel.id;
              postData.lang_id  =  $scope.widgetModel.lang_id;
              postData.css      =  $scope.designModel;
              ChiliUtils.block();
              SiteWidgetAPI.updateGameWidget(postData).success(function(data, status, headers, config) {
                    if(ChiliUtils.action(data)) {
                       $scope.widgetModel.setData(data);
                       $scope.mapToDesign(JSON.parse(data['css']));
                       $scope.resetGameForm();
                    }
                    ChiliUtils.unblock();
              }).error(function(data, status, headers, config) {
                    console.log("js::GameWidgetCtrl saveGameWidget");
              });
          }
      }
      else
      {
        $scope.gameWidgetStep = "main";
      }
  };

  $scope.removeGameWidget = function(){
        ChiliUtils.block();
        SiteWidgetAPI.removeWidget($scope.widgetModel.id).success(function(data, status, headers, config) {
              if(ChiliUtils.action(data))
              {
                 $window.location.href = ChiliConfigs.BASE_INDEX + 'widgets';
              }
             
        }).error(function(data, status, headers, config) {
              console.log("js::profile saveGameWidgetConfigs");
        });
  };

  $scope.resetGameForm = function(){
    $scope.gameWidgetFormSubmited = false;
    $scope.gameWidgetForm.name.$dirty = false;
  };

  $scope.init = function(){
      $scope.langList     = $window.allLanguages;
      $scope.sizeList     = $window.sizeList;
      $scope.leaguesList  = $window.leaguesList;

      $scope.gameWidgetStep = $window.gameWidgetStep;
      if($window.gameWidgetData)
      {
        $scope.widgetModel = new GameWidgetModel();
        $scope.widgetModel.setData($window.gameWidgetData); 
        $scope.selectedLeagues = [];

        var widgetLeagues = $scope.widgetModel.leagues;

        angular.forEach(widgetLeagues, function(value, key){
            $scope.selectedLeagues.push(value.id);
        });
      }
      // DESIGN PART
      $scope.designModel = $window.cssdata;
  };

}
