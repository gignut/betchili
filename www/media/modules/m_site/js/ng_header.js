chilisiteApp.factory("HeaderApi" , function(ChiliConfigs, $http){
      return {
            changeLanguage : function(langCode){
              return $http({
                  method: "POST",
                  url: ChiliConfigs.BASE_INDEX + "betchili/show/changeLanguage/"+ langCode,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              });   
            }
          };
});

function HeaderCtrl($scope, $window, ChiliUtils, ChiliConfigs, HeaderApi){
	$scope.currentLanguageCode;
	$scope.currentLanguageName;
	$scope.allLanguages;

	$scope.changeLang = function(code) {
		HeaderApi.changeLanguage(code).success(function(data, status, headers, config) {
                    ChiliUtils.action(data);
              }).error(function(data, status, headers, config) {
                    console.log("js::HeaderCtrl changeLang");
              });
	};

	$scope.init = function() {
		$scope.currentLanguageCode = $window.currentLanguageCode;
		$scope.allLanguages = $window.allLanguages;
    angular.forEach($scope.allLanguages, function(langItem, langId){
        if($scope.currentLanguageCode == langItem.code){
            $scope.currentLanguageName = langItem['name'];
        }
    });
	};
}