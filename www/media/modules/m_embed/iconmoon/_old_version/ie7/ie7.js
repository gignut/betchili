/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referring to this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'chiliWidgetFont\'">' + entity + '</span>' + html;
	}
	var icons = {
		'chili-chevron-left': '&#xf053;',
		'chili-chevron-right': '&#xf054;',
		'chili-arrows-alt': '&#xf0b2;',
		'chili-plus-square': '&#xf0fe;',
		'chili-angle-left': '&#xf104;',
		'chili-angle-right': '&#xf105;',
		'chili-angle-up': '&#xf106;',
		'chili-angle-down': '&#xf107;',
		'chili-chevron-circle-left': '&#xf137;',
		'chili-chevron-circle-right': '&#xf138;',
		'chili-chevron-circle-up': '&#xf139;',
		'chili-chevron-circle-down': '&#xf13a;',
		'chili-bullseye': '&#xf140;',
		'chili-minus-square': '&#xf146;',
		'chili-minus-square-o': '&#xf147;',
		'chili-plus-square-o': '&#xf196;',
		'chili-list': '&#xe62a;',
		'chili-trophy': '&#xe62b;',
		'chili-bookmark-o': '&#xe62c;',
		'chili-bullhorn': '&#xe62d;',
		'chili-quote-left': '&#xe62e;',
		'chili-quote-right': '&#xe62f;',
		'chili-calendar-o': '&#xe630;',
		'chili-youtube-square': '&#xe631;',
		'chili-youtube-play': '&#xe632;',
		'chili-user': '&#xe633;',
		'chili-play-circle-o': '&#xe634;',
		'chili-bookmark': '&#xe635;',
		'chili-external-link': '&#xe636;',
		'chili-sign-in': '&#xe637;',
		'chili-heart-o': '&#xe638;',
		'chili-window7': '&#xe600;',
		'chili-notebook21': '&#xe601;',
		'chili-weekly3': '&#xe602;',
		'chili-add17': '&#xe603;',
		'chili-user91': '&#xe604;',
		'chili-total': '&#xe605;',
		'chili-menu10': '&#xe606;',
		'chili-money27': '&#xe607;',
		'chili-clipboard52': '&#xe608;',
		'chili-clock3': '&#xe609;',
		'chili-circular22': '&#xe60a;',
		'chili-cart4': '&#xe60b;',
		'chili-calendar68': '&#xe60c;',
		'chili-info9': '&#xe60d;',
		'chili-tag2': '&#xe60e;',
		'chili-heart9': '&#xe60f;',
		'chili-globe12': '&#xe610;',
		'chili-soccer18': '&#xe611;',
		'chili-slideshows': '&#xe612;',
		'chili-faq': '&#xe613;',
		'chili-dwelling1': '&#xe614;',
		'chili-shopping80': '&#xe615;',
		'chili-door9': '&#xe616;',
		'chili-shopping66': '&#xe617;',
		'chili-shopping7': '&#xe618;',
		'chili-dollar4': '&#xe619;',
		'chili-define': '&#xe61a;',
		'chili-shooting1': '&#xe61b;',
		'chili-competition': '&#xe61c;',
		'chili-settings21': '&#xe61d;',
		'chili-rankings': '&#xe61e;',
		'chili-coins15': '&#xe61f;',
		'chili-power18': '&#xe620;',
		'chili-group': '&#xe621;',
		'chili-globe': '&#xe622;',
		'chili-power-off': '&#xe623;',
		'chili-list-alt': '&#xe624;',
		'chili-diamond': '&#xe625;',
		'chili-coins': '&#xe626;',
		'chili-heart': '&#xe627;',
		'chili-heart2': '&#xe628;',
		'chili-coins17': '&#xe629;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/chili-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
