function ParticipantStatPoint(){
    this.gameId;
    this.gameName;
    this.gameStatus;
    this.gameResultTxt;
    this.gameDate;
    this.participantOrder;
    this.tooltip;
}

ParticipantStatPoint.prototype.setData = function(data){
  this.gameId           = data.id;
  this.gameName         = data.tname;
  this.gameResultTxt    = data.result_text;
  this.gameDate         = data.game_date;
  this.participantOrder = data.order;

  this.tooltip =  "<div style='width:200px;'>" + this.gameName + "<br/>" + moment(this.gameDate * 1000).format("MMM Do YYYY") + "<br/>" +this.gameResultTxt + "</div>";

  var scores = this.gameResultTxt.split(":");

  if(parseInt(scores[0])  == parseInt(scores[1]))
  {
    this.gameStatus = 0;
  }
  else
  {
      switch(this.participantOrder)
      {
        case '1':
          this.gameStatus = (parseInt(scores[0]) - parseInt(scores[1])) /Math.abs (parseInt(scores[0]) - parseInt(scores[1]));
        break;
        case '2':
          this.gameStatus = (parseInt(scores[1]) - parseInt(scores[0])) /Math.abs (parseInt(scores[0]) - parseInt(scores[1]));
        break;
      }
  }
};

function MatchCountdown(){
  this.utc;
  this.days;
  this.hours;
  this.minutes;
  this.seconds;
  this.intervalID;
}

MatchCountdown.prototype.update = function(){
    var now    = Math.floor(Date.now()/1000) 
    var duration = this.utc - now; 

    var now = moment();
    var matchTime = moment(this.utc * 1000);
    
    this.days    = matchTime.diff(now, 'days');
    
    var hoursTotal = matchTime.diff(now, 'hours');
    this.hours   = hoursTotal - this.days * 24;

    var minutesTotal = matchTime.diff(now, 'minutes');
    this.minutes = minutesTotal - hoursTotal * 60;
    
    var secondsTotal =   matchTime.diff(now, 'seconds');
    this.seconds = secondsTotal - minutesTotal * 60;

    this.days    = this.days    < 10 ? "0" + this.days    : this.days ;
    this.hours   = this.hours   < 10 ? "0" + this.hours   : this.hours;
    this.minutes = this.minutes < 10 ? "0" + this.minutes : this.minutes;
    this.seconds = this.seconds < 10 ? "0" + this.seconds : this.seconds;
}

MatchCountdown.prototype.setMatchUTC = function(utcDate){
    this.utc = utcDate;
}

function GameDetails(){
    this.participantId1;
    this.participantId2;
    this.points1 = new Array();
    this.points2 = new Array();
}

 // game details 
 GameDetails.prototype.setData = function(data){
    var p = data.aux_info.participants;
    this.participantId1 = p[0].id;
    this.participantId2 = p[1].id;

    var statData1 = data.details[this.participantId1];
    var statData2 = data.details[this.participantId2];

    var len1 = statData1.length;
    var point = null;  
    for(var i = 0; i < 5; i++)
    {
        point = new ParticipantStatPoint();
        if( i< len1)
        {
           point.setData(statData1[i]);
        }

        this.points1[5-i] = point;
    }

    var len2 = statData2.length;
    for(var j = 0; j < 5; j++)
    {
        point = new ParticipantStatPoint();
        if( j< len2)
        {
           point.setData(statData2[j]);
        }

        this.points2[5-j] = point;
    }
 };


 BetItemModel.prototype.changeBetData = function(){

    var self = this; 
    var gameEvents = self.game.aux_info.events.base_events;

    angular.forEach(gameEvents, function(val, key){
       if(val.template_id == self.eventTemplateId)
       {
            self.coeff  = val.coefficient;
              switch(parseInt(this.eventTemplateId))
              {
                 case 1:;
                 case 2:
                 case 3:
                 case 4:
                 case 5:
                 case 6:
                  this.basis = null;
                  break;
                 case 7:
                 case 8:
                  this.basis = betEvent.basis;
                  break;
                 case 9:
                 case 11:
                  this.basis = game.aux_info.events.base_events[9].coefficient;
                  break;
              }
       }
    });
}