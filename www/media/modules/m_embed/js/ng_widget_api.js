// TRANSFORMING ALL  REQUESTS DATA
chilisiteApp.config(function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    }
});

chilisiteApp.service("ChiliSupersonic", function($window, $rootScope, $http, ChiliConfigs) {

  var supersonicObj = {
    getoffers: function(getOfferCallback){
            $http({ method: "GET",
                    url: ChiliConfigs.BASE_INDEX + "embed/supersonic/getoffers",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                if (data && !data.hasOwnProperty("error"))
                {
                   if(getOfferCallback) 
                   { 
                     getOfferCallback(data); 
                   }  
                }
                else
                {
                   if(getOfferCallback) 
                   { 
                      getOfferCallback(null); 
                   }  
                }
            }).error(function(data, status, headers, config) {
                    if(getOfferCallback) 
                    { 
                       getOfferCallback(null); 
                    }  
                    Widget.error("ChiliSupersonic.getoffers");
            }); 
    },
    init: function(ssaConfig, readyCallback, completeCallback, doneCallback){
      // ##start init##
      // definitions by JSON 
      supersonicObj.readyCallback    = readyCallback;
      supersonicObj.doneCallback     = doneCallback;
      supersonicObj.completeCallback = completeCallback;
      
       $window.ssa_json = { 
       'applicationUserId': ssaConfig.user_id, 
       'applicationKey': '2f6014fd', 
       'onCampaignsReady': supersonicObj.brandConnectReadyEvent, 
       'onCampaignCompleted' : supersonicObj.brandCampaignCompleted,
       'onCampaignsDone':  supersonicObj.brandConnectDoneEvent,
       'customCss' :  ChiliConfigs.BASE_INDEX + "embed/supersonic/css/" + ssaConfig.widget_id
      }; 
      
      if($window.CHILI_ENVIRONMENT){
         $window.ssa_json.demoCampaigns = 1;
      }

      // the actual script included from our servers is minified and compressed 
      (function(d,t){ 
        var g = d.createElement(t), s = d.getElementsByTagName(t)[0]; g.async = true; 
        g.src = ('https:' != location.protocol ? 'http://jsd.supersonicads.com' : 
        'https://a248.e.akamai.net/ssastatic.s3.amazonaws.com') + 
        '/inlineDelivery/delivery.min.gz.js'; 
        s.parentNode.insertBefore(g,s); 
      }($window.document,'script')); 
      // ##end init##
    },
    brandConnectReadyEvent : function(offers){
      if(supersonicObj.readyCallback) {
         $rootScope.$apply(function(){
             supersonicObj.readyCallback(offers);
         });
      }
    },
    brandCampaignCompleted : function(completeData){
      if(supersonicObj.completeCallback) {
         $rootScope.$apply(function(){
             supersonicObj.completeCallback(completeData);
         });
      }
    },
    brandConnectDoneEvent : function(offers){
      if(supersonicObj.doneCallback) {
         $rootScope.$apply(function(){
             supersonicObj.doneCallback(offers);
         });
      }
    }
  };
  
  return supersonicObj;

});

chilisiteApp.service("ChiliUserVoice", function($window) {

  return {
    init: function(accentColor){
           $window.UserVoice = $window.UserVoice||[];
          (function(){
            var uv=document.createElement('script');
            uv.type='text/javascript';
            uv.async=true;
            uv.src='//widget.uservoice.com/ChKZHHK02UA6O2nJy4wEg.js';
            var s=document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(uv,s)
          })();

          $window.UserVoice.push(['set', {
            accent_color: accentColor
          }]);
    }
  };

});

chilisiteApp.factory("WidgetAPI", function($http, ChiliConfigs){
    return {
               API_ACTION_URL : ChiliConfigs.BASE_INDEX + "embed/action",
               rewardBoints : function(rewardData){
                   return $http({ method: "POST",
                                  url: ChiliConfigs.BASE_INDEX + "embed/supersonic/rewardBoints",
                                  data :  rewardData,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                           });
               },
               bet : function(betData){
                   var self = this;
                   return $http({ method: "POST",
                                  url: self.API_ACTION_URL + "/bet",
                                  data :  betData,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                           });
               },
               setShieldOnBet: function(betData){
                   var self = this;
                   return $http({ method: "POST",
                                  url: self.API_ACTION_URL + "/setShieldOnBet",
                                  data :  betData,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                           });
               },
               filterGames: function(filterData){
                   var self = this;
                   return   $http({
                              method: "POST",
                              url: self.API_ACTION_URL + "/getFilteredGames",
                              data :  filterData,
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            });
               },

               loadActivity : function(pageNum){
                  var self = this;
                  return $http({ method: "POST",
                                 url: self.API_ACTION_URL + "/getUserActivities",
                                 data :  { page : pageNum},
                                 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                          });
               },

               getOpponents: function(opt) {
                   var self = this;
                   return   $http({ method: "POST",
                                    url: self.API_ACTION_URL + "/getOpponents",
                                    data : opt,
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                  });
               },

               updateIntroShow: function(showState) {
                   var self = this;
                   return   $http({ method: "POST",
                                    url: self.API_ACTION_URL + "/updateIntroShow",
                                    data : {show: showState},
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                  });
               },               

               createChallange : function(challengeData) {
                  var self = this;
                  return  $http({ method: "POST",
                                  url: self.API_ACTION_URL + "/createChallenge",
                                  data : challengeData,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                               });
               },

               getNextChallange : function(){
                  var self = this;
                  return  $http({ method: "POST",
                                  url: self.API_ACTION_URL + "/getNextChallenge",
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                              });
               },

              acceptChallange : function(acceptChallengeData){
                  var self = this;
                  return  $http({ method: "POST",
                                  url: self.API_ACTION_URL + "/acceptChallenge",
                                  data :  acceptChallengeData,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                });
               },

               getLeaderboard : function(page){
                  var self = this;
                  return   $http({ method: "POST",
                                   url: self.API_ACTION_URL + "/getLeaderboard",
                                   data : {page: page},
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                });
               },

               logout : function(playerId){
                  var self = this;
                  return $http({ method: "POST",
                                 url: self.API_ACTION_URL + "/logoutplayer",
                                 data : {player_id: playerId},
                                 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                              });
               }
           }; 
});

chilisiteApp.factory("Widget", function($timeout, $rootScope, $window){

    var API_ERROR = {
            'BET_ERROR_CODES_START' : 104,
            'BET_ERROR_CODES_END'   : 110,
            'CHALLENGE_ERROR_CODES_START' : 111,
            'CHALLENGE_ERROR_CODES_END' : 122,
            'GAME_ERROR_CODE' : 123,
            'BLOKED_USER_LOGIN' : 404

    };

    var leaguesModal      = null;
    var gameLoaderElem    = null;
    var usersModalContent = null;
    //bet modals
    var betModal          = null;
    var introModal        = null;
    var betReplaceDialog  = null;
    var maxBetCountDialog = null;

    var supersonicReminderDialog = null;

    var notificationDialog = null;

    var pleaseLoginElem = null;

    var playerAuthorized = false;

    var supersonicTimeout = null;


    var isModalOpened = false;

    function closeModal(){
      isModalOpened = false;
      jQuery.unblockUI();
    }
    
    return {    // ga start
                nonInteraction: {'nonInteraction': 1} ,
                track: function(category, action, label, value, config){
                    if(angular.isString(category) && angular.isString(action) && parseInt(this.player.mode) != 1) {
                        $window.ga('send', 'event', category, action , label, value, config);
                    }
                },
                error: function(msg, isFatal){
                    isFatal = isFatal != undefined ? isFatal : true;
                    $window.console.log(msg);
                    if(parseInt(this.player.mode))
                    {
                        $window.ga('send', 'exception', {
                          'exDescription': msg,
                          'exFatal': isFatal
                        });
                    }
                },
                // ga end
                player:            {},

                initSocialJS : function(){
                   if(! this.player.hasOwnProperty("platform_id")) return;
                   switch(parseInt(this.player.platform_id))
                   {
                      //facebook
                      case 1:
                        break;
                      //google
                      case 2:
                        break;
                      //odnoklassniki
                      case 3:
                        !function (d, id, did, st) {
                              var js = d.createElement("script");
                              js.src = "http://connect.ok.ru/connect.js";
                              js.onload = js.onreadystatechange = function(){};
                              d.documentElement.appendChild(js);
                         }($window.document);
                         break;
                      //vkontakte    
                      case 4:
                        break;
                      //mailru
                      case 5: 
                        break;
                   }
                },

                widgetId           : null,
                widgetSiteId       : null,
                widgetLangId       : null,
                widgetBookmakerId  : null,

                eventTemplates     : null,
                eventTemplatesData : null, 
                leagues            : null,
                competitions       : null,
                games              : null,
                betchain           : [],
                challengeData      : {},
                
                EVENTS :{
                  GAME_UPDATED:         "gameDataUpdated",
                  NOT_AUTHORIZED_USER:  "not_authorized_user",
                  PLAYER_UPDATED:       "playerDataUpdated",
                  PLAYER_LOG_IN:        "playerLogIn",
                  PLAYER_LOG_OUT:       "playerLogOut"
                }, 

                playerLogin:function(playerData) {
                    var self = this;

                    this.player = playerData;
                    var playerId = this.player.player_id;
                    playerAuthorized = true;
                    this.broadcastEvent(this.EVENTS.PLAYER_LOG_IN,  playerId);
                },
                playerLogout: function(){
                   this.player = {}; 
                   playerAuthorized = false;
                   this.betchain = [];
                   this.broadcastEvent(this.EVENTS.PLAYER_LOG_OUT);
                },

                update: function(){
                   this.broadcastEvent(this.EVENTS.GAME_UPDATED);
                },
                
                checkAuthorized : function(){
                  if(!playerAuthorized){
                     $timeout(function(){
                         pleaseLoginElem = angular.element("#pleaseLoginElem");
                         jQuery.blockUI.defaults.overlayCSS.cursor = 'default';
                         jQuery.blockUI({ 
                            fadeIn: 0,
                            showOverlay: true,
                            onOverlayClick: $.unblockUI,
                            message: pleaseLoginElem,
                            css: { border : '0px' , left : '20%', width : '60%' , 'border-radius' : '8px', padding : '12px', 'cursor' : 'default'} 
                          });
                     }, 100);
                  }
                  return playerAuthorized;
                },

                broadcastEvent: function(event, data){
                  $rootScope.$broadcast(event, data);
                },

                startLoading: function(overlayVal){
                      isModalOpened = true;

                      $.blockUI.defaults.applyPlatformOpacityRules = true;
                      var overlay = overlayVal || true;
                      gameLoaderElem = gameLoaderElem || angular.element("#gameLoader");
                      jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay : overlay,
                        onOverlayClick: closeModal,
                        message: gameLoaderElem,
                        css: { 'border' : '0px',  'cursor': 'default'} 
                      });
                },
                endLoading : function(){
                  isModalOpened = false;
                  jQuery.unblockUI();
                },

                openLeaguesModal : function(){
                    isModalOpened = true;

                    leaguesModal = leaguesModal || angular.element("#leaguesModal");
                    jQuery.blockUI({  
                        fadeIn: 0,
                        showOverlay: true, 
                        onOverlayClick: closeModal, 
                        message : leaguesModal,
                        css: {top: '25%', left : '25%', width: '50%', height: '50%', 'cursor': 'default'} 
                    });
                },
                openUsersModal: function(overlayVal) {
                    isModalOpened = true;

                   var overlay = overlayVal || true;
                   usersModalContent =  usersModalContent || angular.element("#choosePlayerModal");
                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: overlay,
                        onOverlayClick: closeModal,  
                        message : usersModalContent, 
                        css: {top: '10%', left : '10%', width: '80%', height: '80%', 'cursor': 'default'} 
                      });
                },
                openBetCartDialog: function(overlayVal) {
                   isModalOpened = true;

                   var overlay = overlayVal || true;
                   jQuery.blockUI.defaults.overlayCSS.cursor = 'default';
                   
                   betModal = betModal || angular.element("#betModal");
                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: overlay,
                        //onOverlayClick: $.unblockUI,  
                        message : betModal, 
                        css: {top: '10%', left : '10%', width: '80%', height: '80%', cursor: "default"}
                      });
                },
                openIntroDialog: function(overlayVal) {
                   var overlay = overlayVal || true;
                   introModal = introModal || angular.element("#introModal");
                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: overlay,
                        onOverlayClick: $.unblockUI,  
                        message : introModal, 
                        css: {top: '10%', left : '10%', width: '80%', height: '80%', cursor: "default"}
                      });
                },                
                openBetReplaceDialog: function(overlayVal, isNote) {
                   isModalOpened = true;

                   var overlay = overlayVal || true;
                   betReplaceDialog = betReplaceDialog || angular.element("#betReplaceDialog");
                   
                   var cssOb = {top: '20%', left : '10%', width: '80%', height: '60%', 'cursor': 'default'};
                   if(isNote){
                      cssOb.top    = '30%';
                      cssOb.height = '40%';
                   }

                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: overlay,
                        onOverlayClick: closeModal,  
                        message : betReplaceDialog, 
                        css: cssOb
                      });
                }, 
                openMaxBetCountDialog: function(overlayVal) {
                   isModalOpened = true;

                   var overlay = overlayVal || true;
                   maxBetCountDialog = maxBetCountDialog || angular.element("#maxBetCountDialog");
                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: overlay,
                        onOverlayClick: closeModal,  
                        message : maxBetCountDialog, 
                        css: {top: '10%', left : '10%', width: '80%', height: '80%', 'cursor': 'default'} 
                      });
                },
                openSupersonicReminder: function() {
                   supersonicReminderDialog = supersonicReminderDialog || angular.element("#supersonicReminderDialog");
                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: true,
                        onOverlayClick: closeModal,  
                        message : supersonicReminderDialog, 
                        css: {top: '25%', left : '10%', width: '80%', height: '40%', 'cursor': 'default'} 
                      });
                },
                checkForOpenModal: function(){
                  return isModalOpened;
                },
                notification: {},
                showNotification: function(message,type){
                   isModalOpened = true;
                   
                   notificationDialog = notificationDialog || angular.element("#notificationDialog");
                   jQuery.blockUI({ 
                        fadeIn: 0,
                        showOverlay: true,
                        onOverlayClick: closeModal,  
                        message : notificationDialog,
                        css: {
                               top: '15%', left : '20%', 
                               width: '60%', height: '10%',
                               'cursor': 'default',
                               'border-color': 'rgb(171, 6, 6)',
                               'border-style': 'solid',
                               'border-width' : '5px',
                               'border-top-width': '20px'
                             } 
                      });
                },
                assert: function(responseData) {
                      //in case user is not authorized
                      if(responseData && responseData.hasOwnProperty('not_authorized_user') )
                      {
                         $rootScope.$broadcast(this.EVENTS.NOT_AUTHORIZED_USER);
                         return false;
                      }

                      // check api errors
                      if (responseData && responseData.hasOwnProperty('error')) {
                        if (responseData['error'] >= API_ERROR.BET_ERROR_CODES_START && responseData['error'] <= API_ERROR.BET_ERROR_CODES_END
                           || responseData['error'] >= API_ERROR.CHALLENGE_ERROR_CODES_START && responseData['error'] <= API_ERROR.CHALLENGE_ERROR_CODES_END
                           || responseData['error'] == API_ERROR.GAME_ERROR_CODE
                           ||  responseData['error'] == API_ERROR.BLOKED_USER_LOGIN) {
                                this.notification =  { 'message': responseData['message'], 'type' : responseData['error']};
                                this.showNotification();
                        } else {
                                this.showNotification('GENERAL ERROR: Try Again', null);
                        }
                        return false;
                      }

                      return true;
                }
            }; 
});
