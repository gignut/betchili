function IntroStep () {
  this.m_image;
  this.m_title;
  this.m_description;
  this.m_type = "image"; // descr
}

IntroStep.prototype.setData = function(img, title, desc) {
  this.m_image = img;
  this.m_title = title;
  this.m_description = desc;
}

function IntroTour (langCode) {
  this.steps = [];
  this.currentStep = 0;
  this.show = false; // default to showButtons
  this.langId = langCode;
}

IntroTour.prototype.currentItem = function()
{
    return this.steps[this.currentStep];
}

IntroTour.prototype.nextStep = function()
{
    this.currentStep = ((this.currentStep+1) % this.steps.length);
}

IntroTour.prototype.goStep = function (step) {
  if (step <= this.steps.length) {
    this.currentStep = step;
  }
}

IntroTour.prototype.start = function (widget, step) {
  if (step && step <= this.steps.length) {
    this.currentStep = step;
  }
  widget.openIntroDialog();
}

IntroTour.prototype.stop = function () {
}

IntroTour.prototype.setupSteps = function (steps) {
  if (steps) {
    this.steps = steps;
  } else {
    var steps = [
                  { 
                    1: {img : 'bet_en.jpg',  title: "How to Bet & Challenge", descr: "Several tips to make BET & CHALLENGE and more."},
                    2: {img : 'bet_ru.jpg',  title: "Как делать Ставки и Спори", descr: "Несколько советов, как сделать ставку, спор и многое другое."},
                    3: {img : 'bet_am.jpg',  title: "Ինչպես ստեղծել խաղադրույքներ և գրազներ", descr: "Մի քանի խորհուրդներ, թե ինչպես անել գրազ, խաղադրույք եւ ավելի."}
                  },
                  { 1 : {img : 'challenge_en.jpg', title: "Tips for appeared challenge", descr: "Some one sends you a public or direct challenge to compite with you."},
                    2 : {img : 'challenge_ru.jpg', title: "Советы по появившейся спора", descr: "Кто-то посылает вам публични или прямой вызов что бы соревноваться."},
                    3 : {img : 'challenge_am.jpg', title: "Խորհուրդներ հայտնված գրազի համար", descr: "Ինչ որ մեկը Ձեզ (կամ բոլորին) ուղարկում գրազ."}
                  },
                  { 1 : {img : 'create_challenge_en.jpg', title: "Tips to create challenge", descr: "You can create your own challenge and compete with desired friend or make public challenge."},
                    2 : {img : 'create_challenge_ru.jpg', title: "Советы по созданию спора", descr: "Вы можете создать спор и спорить с конкретным играком или сделать публичный спор."},
                    3 : {img : 'create_challenge_am.jpg', title: "Խորհուրդներ գրազ ստեղծելու համար", descr: "Դուք կարող եք ստեղծել գրազ կոնկրետ խաղացողի կամ բոլորի համար."}
                  },
                  { 1: {img : 'multibet_en.jpg', title: "Tips to create (Multi)Bet", descr: "You can create simple (one event) Bets or Multi Bets (linked multiple events)"},
                    2: {img : 'multibet_ru.jpg', title: "Советы по созданию (мульти) Ставка", descr: "Вы можете создавать простые (одно событие) ставки или мульти ставки"},
                    3: {img : 'multibet_am.jpg', title: "Խորհուրդներ էքսպրես խաղադրույք ստեղծելու համար", descr: "Դուք կարող եք ստեղծել պարզ (մեկ ելքով) կամ էքսպրես խաղադրույքներ."}
                  }
                ];
  }
  var lng = this.langId ? this.langId : 1;
  for(var i =0; i< steps.length; i++) {
    var is = new IntroStep();
    var item = steps[i][lng];
    is.setData(item.img, item.title, item.descr);
    this.steps.push(is);
  }
}

 function CSSModel(){
    this.h_bg_color          = null;
    this.h_font_color        = null;
    this.h_icon_color        = null;

    this.challange_font_color = null;
    this.challange_bg_color   = null;
    this.challange_team_bg    = null;
    this.challange_option_enabled_bg    = null;
    this.challange_option_disabled_bg   = null;
    this.challange_option_enabled_font  = null;
    this.challange_option_disabled_font = null;
    this.challange_btn_bg   = null;
    this.challange_btn_font = null;

    this.gh_bg_color         = null;
    this.gh_tmp_bg_color     = null;
    this.gh_tmp_font_color   = null;
    this.g_border_color      = null; 
    this.g_border_size       = null; 
    this.g_left_bg_color     = null;   
    this.g_right_bg_color    = null;
    this.g_img_bg_color      = null;
    this.g_left_font_color   = null; 
    this.g_tmp_bg_color      = null;   
    this.g_tmp_font_color    = null;
}

// BET ITEM 
function BetItemModel(){
  this.game;
  this.betEvent; 

  this.gameId;
  this.gameName;
  this.eventTemplateId;
  this.eventTemplateName;
  this.coeff = 1;
  this.basis;
  this.amount = 0;
}

BetItemModel.prototype.setData = function(game, betEvent){
  this.game               = game;
  this.betEvent           = betEvent;

  this.gameId             = game.id;
  this.gameName           = game.tname;
  this.eventId            = betEvent.id;
  this.coeff              = betEvent.coefficient;
  this.eventTemplateId    = betEvent.template_id;

  switch(parseInt(this.eventTemplateId))
  {
     case 1:
     case 2:
     case 3:
     case 4:
     case 5:
     case 6:
      this.basis = null;
      break;
     case 7:
     case 8:
      this.basis = betEvent.basis;
      break;
     case 9:
     case 11:
      this.basis = game.aux_info.events.base_events[9].coefficient;
      break;
  }
}


chilisiteApp.service("ChiliTour", function($window) {
    var langCode = $window.widgetLangId;
    var help_challenge_data = [ {
                                  content: {1: "Move mouse on desired game row and click on 'challenge'.", 2: "Наведите мышку на игру и нажмите на кнопку 'Оспорить'.",
                                            3: "Наведите мышку на игру и нажмите на кнопку 'Оспорить'."},
                                  target: "#gameEvents",
                                  placement: "bottom"
                                },
                                //step2
                                {
                                  content: {1: "Here is the game for challenge.", 2: "Вот это игра для спора.",
                                            3: "Вот это игра для спора."},
                                  target: "#challengeGame",
                                  placement: "bottom"
                                },
                                //step3
                                {
                                  content: {1: "Here is challenger image (it's you).", 2: "Здесь изображение создателя спора(это Вы).",
                                            3: "Здесь изображение создателя спора(это Вы)."},
                                  target: ".opAvatarContainer",
                                  placement: "bottom"
                                },
                                //step 4
                                {
                                  content: {1: "Set the challenge amount here.", 2: "Установите значение спора здесь.",
                                            3: "Установите значение спора здесь."},
                                  target: ".challengeAmountInput",
                                  placement: "bottom"
                                },
                                //step 5
                                {
                                  content: {1: "Please select the option to challenge.", 2: "Пожалуйста, выберите опцию, чтобы бросить вызов.",
                                            3: "Пожалуйста, выберите опцию, чтобы бросить вызов."},
                                  target: "#challengeOptions",
                                  placement: "bottom"
                                },
                                //step 6
                                {
                                  content: {1: "Here is the estimated won amount.", 2: "Здесь вы можете увидеть сумма выигрыша.",
                                            3: "Здесь вы можете увидеть сумма выигрыша."},
                                  target: "#estimatedWon",
                                  placement: "bottom"
                                },
                                //step 7
                                {
                                  content: {1: "You can challenge to all(public challenge) or to other player. Click on icon to select the opponent from the list.", 2: "Вы можете бросить вызов всем (публични вызов) или к другому игроку. Нажмите на значок, чтобы выбрать противника из списка.",
                                            3: "Вы можете бросить вызов всем (публични вызов) или к другому игроку. Нажмите на значок, чтобы выбрать противника из списка."},
                                  target: "#opponent2",
                                  placement: "left"
                                },
                                 //step 8
                                {
                                  content: {1: "The final step: make challenge or cancel it by click.", 2: "Последний шаг: создать спор или отменить его, нажав на кнопку.",
                                            3: "Последний шаг: создать спор или отменить его, нажав на кнопку."},
                                  target: "#challengeButtons",
                                  placement: "bottom"
                                }
                                
                              // {
                              //     content: {1: "You can see your challenges and other activities in activities page.", 2: "You can see your challenges and other activities in activities page."},
                              //     target: "#menuActivityId",
                              //     placement: "bottom"
                              //   }
                            ];

          var tour = new Object();

          tour.startIntro = function(type, flow){
              tour.how_to_challenge = {
                  steps: [],
                  showStepNumbers: false,
                  showBullets : false,
                  skipLabel: 'Exit'
                };

              var stepLen = help_challenge_data.length;              
              for(var i = 0; i < stepLen; i++) {
                  var step = {
                              intro   : help_challenge_data[i].content[langCode],
                              element : help_challenge_data[i].target,
                              position: help_challenge_data[i].placement
                            };
                  tour.how_to_challenge.steps.push(step);
              }

              tour.helpIntro = $window.introJs();
              tour.isWaiting = false;
              tour.helpIntro.onexit(function() {
                if(tour.isWaiting)
                {

                }
                else
                {
                   tour.helpIntro = null;
                   tour.isWaiting = false;
                }
              });
              tour.helpIntro.onbeforechange(function(targetElement) {  
                 var realStep = tour.helpIntro.getCurrentStep() + 1;
                 //console.log("realstep = " + realStep); 
                 switch(realStep){
                    case 1:
                    case 5:
                    case 7:
                    case 8:
                      tour.helpIntro.setOption("showButtons", false);
                      tour.helpIntro.refresh();
                      break;
                    case 9:
                      tour.helpIntro.setOption("showButtons", false);
                      tour.helpIntro.refresh();
                      break;
                    default:
                      tour.helpIntro.setOption("showButtons", true);
                  } 
              });

              switch(type){
                case 'how_to_challenge':
                  tour.helpIntro.setOptions(tour.how_to_challenge);
                  tour.helpIntro.setOption("showButtons", true);
                  tour.helpIntro.start().goToStep(1);
                  break;
                case 'how_to_bet':
                  tour.helpIntro.goToStep(10);
                  break;
              }
          };

          tour.goToStep = function(step){
              if(step && tour.helpIntro && !tour.isWaiting){
                 tour.helpIntro.goToStep(step);
              } 
          };

          tour.end = function(){
            if(tour.helpIntro){
                tour.helpIntro.exit();
            }
          };

          tour.wait = function(){
            if(tour.helpIntro){
                tour.isWaiting = true;
                tour.helpIntro.exit();
            }
          };

          tour.restart = function(step){
            if(tour.helpIntro){
                tour.helpIntro.start().goToStep(step);
            }
          };

          return tour;
});


// ####                        ######
// #### MAIN CONTROLLER STARTS ######
// ####                        ###### 

function MainCtrl($scope, $document, $http, $timeout, $window, ChiliConfigs, nutronEvents, Widget, WidgetAPI, ChiliTour, ChiliUserVoice, ChiliSupersonic){
  $scope.cssModel = new CSSModel();
  $scope.viewmode = "MODE_GAME";

  //$scope.selectedLeague;
  $scope.leagues;
  $scope.competitions;
  $scope.playerData;
  $scope.isAuthorized = false;

  $scope.showIntroTour = function() {
      $scope.setViewMode("MODE_GAME");
      $scope.introTour.start(Widget, 0);
      $scope.introTour.show = true;
      $scope.updateIntroShow();
  };

  $scope.$on("USERLOGEDIN", function(data) {
    $timeout(function() {
      if ($scope.introTour.show) {
        $scope.introTour.start(Widget, 0);
      }
    }, 3000);
  });

  $scope.introTour = new IntroTour($window.widgetLangId);
  $scope.introTour.setupSteps();

  // close dialogs by ESC
  $document.bind('keyup', function(keyEvent) {
    if (event.which === 27) {
      Widget.endLoading();
    }
  });

  // opponentsModal
    $scope.opponentList = new Array();
    $scope.opponentsLoading = false;

    var timeout;
    var offset = 0;

    $scope.openUsersModal = function() {

       if(!Widget.checkAuthorized()) return;

       ChiliTour.wait();
       offset = 0;
       Widget.openUsersModal(true);
    };

    $scope.searchInput;

    $scope.$watch('searchInput.userText', function(newVal, oldVal) {
        if(newVal == undefined) return; 
        if (timeout)
        { 
          $timeout.cancel(timeout); 
        }

       timeout = $timeout(function() {
          $scope.searchOpponent();
       }, 200);
    });

    $scope.searchOpponent = function(more) {
      if(timeout) { 
        $timeout.cancel(timeout);
      }

      offset = more ? offset : 0;
      var request = {offset : offset, search : $scope.searchInput.userText};
      searchUsers(request, more);
    };

    $scope.loadMoreUsers = function() {
      offset++;
      $scope.searchOpponent(true);
    };

    $scope.updateIntroShow = function() {
      var introShow = $scope.introTour.show ? 1 : 0;
      WidgetAPI.updateIntroShow(introShow).success(function(data, status, headers, config) {
        if (Widget.assert(data)) {
          if (data.hasOwnProperty('show')) {
            $scope.introTour.show = data.show == 1 ? true : false;
          }
        }
      }).error(function(data, status, headers, config) {
            Widget.endLoading();
            Widget.error("MainCtrl.searchUsers");
      });
    };

    function searchUsers(req, more) {
       $scope.opponentsLoading = more ? false : true;
       WidgetAPI.getOpponents(req).success(function(data, status, headers, config) {
        if (Widget.assert(data)) 
        {
            $scope.opponentsLoading = false;
            if (more) 
            {
              for(var i=0; i<data.length; i++) {
                $scope.opponentList.push(data[i]);
              }
            }
            else 
            {
              $scope.opponentList = data;
            }
        }
        }).error(function(data, status, headers, config) {
            Widget.endLoading();
            Widget.error("MainCtrl.searchUsers");
        }); 
    };

    $scope.closeModal = function(){
      Widget.endLoading();
    };

    $scope.selectOpponent = function(opponent){
      ChiliTour.restart(8);

      var ANY_OPPONENT = 'any';
      var opponentId    = opponent ? opponent.id : ANY_OPPONENT; 
      var opponentImage = opponent ? opponent.image : null;
      var opponentName = opponent ? opponent.first_name + ' ' + opponent.last_name : null;
      $scope.$broadcast("OPPONENT_SELECTED", {id: opponentId , avatar: opponentImage, name: opponentName });
      Widget.endLoading();
    };

  //aux function
  $scope.isGamePassed = function(startDate){
      var currentTime = (new Date()).getTime()/1000;
      return currentTime > 1*startDate - 600;
  };

  $scope.initData = function(){
      $scope.cssModel       = $window.cssdata; 
     
      Widget.widgetSiteId      = $window.widgetSiteId;
      Widget.widgetId          = $window.widgetId;
      Widget.widgetLangId      = $window.widgetLangId;
      Widget.widgetBookmakerId = $window.widgetBookmakerId;

      Widget.eventTemplates = $window.eventTemplates;
      var templatesData = new Array();
      angular.forEach(Widget.eventTemplates, function(val,key){
            templatesData[val.id] = val;
      });
      Widget.eventTemplatesData = templatesData;
      $scope.eventTemplatesData = eventTemplates;

      Widget.leagues        = $window.leagues;
      Widget.competitions   = $window.competitions;
      Widget.games          = $window.games;
      Widget.challengeData  = $window.challenges;
      Widget.update();

      $scope.leagues        =  Widget.leagues;

      // ###  PLATFORM ISSUE ####
      if($window.playerData && $window.playerData.id){
         Widget.playerLogin($window.playerData);
      }
  };

  $scope.openHelpDialog = function(){
    if(!Widget.checkAuthorized()) return;

    Widget.track("HELP", "watch_help", Widget.nonInteraction);

    Widget.openHelpDialog();
  };

  $scope.introFlow = function(type, flow){

    switch(type) {
      case 'challenge':
            $scope.setViewMode("MODE_GAME");
            Widget.endLoading();
            $timeout(function(){
              ChiliTour.startIntro("how_to_challenge", flow);
            }, 200);
            break;
      case 'bet':
            //Widget.endLoading();
            //ChiliTour.startIntro("how_to_bet", flow);
            break;
    }
  };

  // during logout emit this event
  $scope.$on("MODE_GAME", function(){
      $scope.viewmode = "MODE_GAME";
  });

  $scope.setViewMode = function(viewmode){
    switch(viewmode)
    {
      case "MODE_GAME":
      case "MODE_LEADERBOARD":
      case "MODE_ACTIVITY":
      case "MODE_HELP":
        $scope.viewmode = viewmode;
        break;
      default: 
        $scope.viewmode = "MODE_GAME";
    }

    $scope.$broadcast($scope.viewmode);
  };

  $scope.openBetcart = function(betGame, betEvent){
        if(!Widget.checkAuthorized()) return;

        Widget.track("BET", "open_betcart", Widget.nonInteraction);

        if(betGame && $scope.isGamePassed(betGame.start_date)) return;

        if(betGame && betEvent && betEvent.coefficient > 1 && betEvent.blocked == 0){

            var betchainLength = Widget.betchain.length;
            if(betchainLength < 5)
            {
              var betItem = new BetItemModel();
              betItem.setData(betGame, betEvent);

              for(var i = 0; i < betchainLength; i++)
              {
                 if(Widget.betchain[i].gameId == betGame.id)
                 {
                    $scope.prevGameIndex   = i; 
                    $scope.gamePrevBetItem = Widget.betchain[i];
                    $scope.gameNextBetItem = betItem;
                    var isNote = $scope.gamePrevBetItem.eventTemplateId == $scope.gameNextBetItem.eventTemplateId ;
                    Widget.openBetReplaceDialog(true, isNote);  
                    return;
                 }
              }

              Widget.betchain.push(betItem);
            }
            else 
            {
              Widget.openMaxBetCountDialog();    
              return;          
            }
        }

        if(Widget.betchain.length > 0)
        {
          Widget.openBetCartDialog();
        }
  };

  $scope.replaceBet = function(){
      Widget.track("BET", "replace_bet", Widget.nonInteraction);
      Widget.endLoading();
      Widget.betchain[$scope.prevGameIndex] = angular.copy($scope.gameNextBetItem);
      Widget.openBetCartDialog();
  };

  // WATCH THE Player
  $scope.$watch(function(){return Widget.player;}, function(){
        $scope.playerData   = Widget.player;
        $scope.introTour.show = $scope.playerData.show_intro == 1 ? true : false;
        $scope.isAuthorized = Widget.player.hasOwnProperty("id") ? true : false;
  }, true);


  var supersonicReminderTimer = null;
  
  function resetSupersonicTimer(){
    if(supersonicReminderTimer)
    {
       $timeout.cancel(supersonicReminderTimer);
       supersonicReminderTimer = null;
    }
  }

  function startSupersonicTimer(time){
    supersonicReminderTimer = $timeout(function(){

        $timeout.cancel(supersonicReminderTimer);
        var isOpened = Widget.checkForOpenModal();
        if(Widget.checkForOpenModal())
        {
            $timeout.cancel(supersonicReminderTimer);
            startSupersonicTimer(5000);
        }
        else
        {
           Widget.openSupersonicReminder();
        }
    }, time);
  }

  $scope.supersonic;

  $scope.$watch("supersonic.offers", function(newVal, oldVal){
      if(newVal == oldVal) return;

      if(newVal.length > 0) {
        $scope.supersonic.hasOffer = true;
        resetSupersonicTimer();
        startSupersonicTimer(60000, 3000);
      }
      else
      {
         $scope.supersonic.hasOffer = false;
      }
  }, true);

  $scope.$on(Widget.EVENTS.PLAYER_LOG_IN, function(){
        //resetSupersonicTimer();
        //startSupersonicTimer(1000, 5000);

        $scope.searchInput = {userText : "" };
        var label = "widget_" + Widget.widgetId;
        Widget.track("MAIN", "login", label);
        //ChiliSupersonic.getoffers($scope.onSupersonicOffer);
        ChiliSupersonic.init({user_id : Widget.player.id, widget_id: Widget.widgetId}, function(offersData){
           $scope.supersonic =  { offers: offersData , hasOffer : false}; 
        }, function(offerCompleteData){
          //video complete callback
           Widget.track("SUPERSONIC", "video_completed"); 
          $scope.rewardBoints(offerCompleteData);
        }, function(campaignDoneData){
           $scope.supersonic =  { offers: [] , hasOffer : false};  
           Widget.track("SUPERSONIC", "campaign_done"); 
        });

        Widget.initSocialJS();
        $scope.$emit("USERLOGEDIN");
  });

  $scope.rewardBoints = function(offerCompleteData){
     if(offerCompleteData){
        var rewardPostData = {};
        rewardPostData.reward = offerCompleteData.rewards;

        WidgetAPI.rewardBoints(rewardPostData).success(function(data, status, headers, config) {
        if (Widget.assert(data) && data.hasOwnProperty("balance")) 
        {
           Widget.player.balance = data.balance;
        }
        }).error(function(data, status, headers, config) {
            Widget.endLoading();
            Widget.error("MainCtrl.rewardBoints");
        }); 
     }
  };

  $scope.playSupersonic = function(){
    Widget.endLoading();
    Widget.track("SUPERSONIC", "open_video");
    $window.SSA_CORE.BrandConnect.engage();
    resetSupersonicTimer();
  };

  $scope.$on(Widget.EVENTS.PLAYER_LOG_OUT, function(){
        Widget.track("MAIN", "logout", Widget.nonInteraction);

        if ($scope.supersonic) {
            $scope.supersonic.hasOffer = false;
        }
  });

  // SESSION LOST HANDLER
  $scope.$on(Widget.EVENTS.NOT_AUTHORIZED_USER, function(){
      Widget.endLoading();
      
      Widget.playerLogout();
      
      $scope.setViewMode("MODE_GAME");
      Widget.checkAuthorized();
  });

  $scope.onSupersonicOffer = function(offersResponse){
      console.log(offersResponse);
  };

  $scope.showFeedbackModal = function(){
      Widget.track("FEEDBACK", "show_feedback", Widget.nonInteraction);
      $window.showClassicWidget($scope.cssModel);
  };

  $scope.$watch(function(){return Widget.notification;}, function(){
      $scope.notification = Widget.notification;
  }, true);

  $scope.currentBetStatus = {link : null, bet_id: null, state : "initial" };

  $scope.$watch("currentBetStatus.link", function(newVal, oldVal){
      if(newVal === oldVal) return;
      console.log($scope.currentBetStatus.link);
      console.log("platform= " + playerData.platform_id);
      switch(Widget.player.platform_id)
      {
        //facebook
        case 1:
            $scope.shareBet = function(){
                $window.FB.ui({
                  method: 'share',
                  display : 'iframe',
                  href: $scope.currentBetStatus.link,
                }, function(response){
                    if(response && !response.hasOwnProperty('error_code'))
                    {
                        setShieldOnBet($scope.currentBetStatus.bet_id, "FB");
                    }
                });
            };
            break;
        //google
        case 2:
            $scope.shareBet = function(){};
            break;
        //odnoklassniki
        case 3:

            function listenForShare() {
                if(listenForShare.hasOwnProperty("executed") && listenForShare.executed === false) return;
                if ($window.addEventListener) {
                    $window.addEventListener('message', onShare, false);
                } else {
                    $window.attachEvent('onmessage', onShare);
                }

                listenForShare.executed = true;
            }

            function onShare(e) {
                var args = e.data.split("$");
                console.log(args[0]);
                if (args[0] == "ok_shared") {
                    setShieldOnBet($scope.currentBetStatus.bet_id, "ODN"); 
                    alert(args[1]); 
                    // Вывод идентификатора фрейма кнопки - в случае нескольких кнопок на одной странице,
                    // по нему можно определить какая именно была кликнута
                }
            }

            listenForShare();

            angular.element("#shareBetOdnoklassniki").html('');
            $window.OK.CONNECT.insertShareWidget("shareBetOdnoklassniki", $scope.currentBetStatus.link, "{width:70,height:30,st:'straight',sz:20,ck:2,nc:1}");
            break;
        // vkontakte    
        case 4:
            $scope.shareBet = function(){};
            break;
        //mailru
        case 5:
            $scope.shareBet = function(){};
            break;
      }
  });

  function setShieldOnBet(betId, platformPrefix){
    var betReqData = {bet_id: betId};

    WidgetAPI.setShieldOnBet(betReqData).success(function(data, status, headers, config){
        if (Widget.assert(data)) 
        {     
              var evName = platformPrefix + "_SHARE";
               Widget.track("SOCIAL", evName, Widget.nonInteraction);
        }
    }).error(function(data, status, headers, config) {
        Widget.error("MianCtrl.setShieldOnBet");
    }); 
  }
  
}
// #### MAIN CONTROLLER END    ######



// ####                        ######
// #### GAME CONTROLLER STARTS ######
// ####                        ###### 
function GameFilter(){
   var currentDate = new Date().getTime();
   this.league = { id: null};
   this.date = currentDate;

   this.getLeagueId = function(){ 
      return this.league.id ? this.league.id : null;
   };

   this.getDateBySeconds = function() {
      return parseInt(this.date/1000);
   };
   return this;
}

function DateRange(){
   var currentDate = new Date().getTime();
   this.calendar = new Array();
   
   this.calendar.push(currentDate);

   var tmpPrev = null;
   var tmpNext = null;
   for(var i = 1; i < 4; i++ ){
     tmpNext =  currentDate + i * 24*3600000;
     tmpPrev =  currentDate - i * 24*3600000;
     this.calendar.push(tmpNext);
     this.calendar.unshift(tmpPrev);
   }

    return this;
}



function GameCtrl($scope, $timeout, $http, $window, ChiliConfigs, Widget, WidgetAPI, ChiliTour) {
    $scope.games;
    $scope.leagues;
    $scope.competitions;
    $scope.eventTemplatesData;
   
    // this is just for storing game row's betline mode states
    $scope.betLineState = {mode: []};
    $scope.betHeaderState = {mode: false};

    $scope.gameFilterData;
    var calendarRanges;
    $scope.dateRange;  

    // BET FUNCTIONALITY
    $scope.betchain   = [];

    $scope.betAmount  = "";
    $scope.totalCoeff = 1;

    $scope.toggleBetLine = function(id, state) {
      $scope.betLineState.mode[id] = state;
      $scope.betHeaderState.mode = false;
      angular.forEach($scope.betLineState.mode, function(item, index) {
        if (item === true) {
          $scope.betHeaderState.mode = true;
        }
      });
    };

    $scope.setBet = function(){
       if(!$scope.betForm.$valid) return;

        var betData = {};
        var eventChainData = new Array();

        angular.forEach($scope.betchain, function(val,key){
            var betItem = {"game_id": val.gameId, "event_id" : val.eventId, "coefficient" : val.coeff};
            betItem.basis = val.basis ? parseFloat(val.basis) : 0; 
            eventChainData.push(betItem);
        });

        betData.coefficient = $scope.totalCoeff;
        betData.amount      = $scope.betAmount;
        betData.event_chain = eventChainData;  

        Widget.track("BET", "set_bet", Widget.nonInteraction);
        
        $scope.currentBetStatus.state = "betting";

        WidgetAPI.bet(betData).success(function(data, status, headers, config){
            if (Widget.assert(data) && data.hasOwnProperty("balance")) 
            {
              //Widget.betchain = new Array();
              Widget.player.balance     = data.balance;
              console.log(data.share_link);
              $scope.currentBetStatus.bet_id = data.bet_id;
              $scope.currentBetStatus.link   = data.share_link; 
               $scope.currentBetStatus.state  = "success";
              //Widget.endLoading();
            }
            else
            {
               $scope.currentBetStatus.state = "fail";
            }
        }).error(function(data, status, headers, config) {
             $scope.currentBetStatus.state = "fail";
            Widget.error("GameCtrl.setBet");
        }); 
    };

    $scope.showMessage = null;

    $scope.checkGamesDate = function(datetime) {
      var date = new Date(datetime);
      var current = date.getFullYear() + '-' +  date.getMonth() + '-' + date.getDate();
      var game = null;
      if ($scope.games) {
        for(var key in $scope.games) {
          game = $scope.games[key][0];
          break;
        }
      }
      if (game) {
        var date = new Date(game.start_date*1000);
        var game_sd = date.getFullYear() + '-' +  date.getMonth() + '-' + date.getDate();
        $scope.showMessage = game_sd != current ? game.start_date : null;
      } else {
        $scope.showMessage = null;
      }
    };

    $scope.removeBetItem = function(ind){
      Widget.track("BET", "remove_bet_item", Widget.nonInteraction);
      Widget.betchain.splice(ind, 1);
    };

    $scope.closeBetModal = function(){
        if($scope.currentBetStatus.state != 'initial')
        {
          Widget.betchain = [];
          $scope.currentBetStatus.state = 'initial';
        }

        Widget.endLoading();
    };

    $scope.openLeaguesModal = function() {
      Widget.track("GAME", "open_competition_modal", Widget.nonInteraction);
      Widget.openLeaguesModal();
    };

    $scope.closeLeaguesModal = function(){
       Widget.endLoading();
    };

    $scope.filterByLeague = function(leagueItem){
      Widget.track("GAME", "filter_competition", Widget.nonInteraction);
      $scope.closeLeaguesModal();
      if(leagueItem)
      {
        $scope.gameFilterData.league = leagueItem;
      }
      
      $scope.getFilteredGames();
    };

    $scope.filterByTime = function(dateVal){
       Widget.track("GAME", "filter_time", Widget.nonInteraction);
      $scope.gameFilterData.date = dateVal;
      $scope.getFilteredGames();
    };

    $scope.getFilteredGames = function(){

        var tz = (new Date()).getTimezoneOffset();

        var filterData    = { league_id :  $scope.gameFilterData.getLeagueId() , date : $scope.gameFilterData.getDateBySeconds() };
        filterData.site_id        = Widget.widgetSiteId;
        filterData.widget_id      = Widget.widgetId;
        filterData.bookmaker_id   = Widget.widgetBookmakerId;
        filterData.widget_lang_id = Widget.widgetLangId;
        filterData.timezone       = tz;

        Widget.startLoading(true);
        WidgetAPI.filterGames(filterData).success(function(data, status, headers, config) {
            if (Widget.assert(data)) 
            {
              Widget.games = data;
              $scope.games = Widget.games;
              $scope.checkGamesDate($scope.gameFilterData.date);
              Widget.endLoading();
            }
        }).error(function(data, status, headers, config) {
            Widget.endLoading();
            Widget.error("GameCtrl.getFilteredGames");
        }); 
    };

    $scope.showList = {};

    $scope.initGameData = function(){
        calendarRanges        = new DateRange();
        $scope.gameFilterData = new GameFilter();
        $scope.dateRange      = calendarRanges.calendar;

        $scope.games              = Widget.games;
        $scope.competitions       = Widget.competitions;
        angular.forEach($scope.competitions, function(item, ind) {
          $scope.showList[item.id] = true;
        });
        $scope.eventTemplatesData = Widget.eventTemplatesData; 
        $scope.checkGamesDate($scope.gameFilterData.date);
    };

    $scope.getCompetitionDataById = function(cid, field) {
      var find = null;
      angular.forEach($scope.competitions, function(item, ind) {
        if (item.id == cid) {
          find = item[field];
        }
      });

      return find;
    };

    $scope.$on(Widget.UPDATED, function(){
        $scope.initGameData();
    });

    $scope.$watch(function(){ return  Widget.betchain; }, function(){ 
        $scope.betchain = Widget.betchain;

        if($scope.betchain.length == 0)
        {
            $scope.closeBetModal();
            $scope.betAmount  = "";
            $scope.totalCoeff = 1;
        }
        else
        {
            $scope.totalCoeff = 1;
            var tmpCoeff = $scope.totalCoeff;
            angular.forEach($scope.betchain, function(val, key){
                tmpCoeff = tmpCoeff * val.coeff; 
            });
            $scope.totalCoeff = tmpCoeff.toFixed(2);
        }

    }, true);

    $scope.prepareChallenge = function(gameData){
        if(!Widget.checkAuthorized()) return;
        prevChallengeData =  Widget.challengeData;
        var opData = {id : Widget.player.id, avatar : Widget.player.image, name : Widget.player.first_name + ' ' +  Widget.player.last_name}; 
        Widget.challengeData =  { game : gameData, opponent1 : opData, opponent2: {}, is_new: true };
        
        $timeout(function() {ChiliTour.goToStep(2);}, 700, true);
    };
}
// #### GAME CONTROLLER END    ######


// ####                             ######
// #### CHALLANGE CONTROLLER START  ######
// ####                             ###### 

function Opponent()
{
  this.id;
  this.name;
  this.avatar;
  this.basis;
  this.coefficient;
  this.event_name;
  this.event_template_id;

  this.setData = function(data){

  };

}

function ChallangeModel(data)
{
  this.opponent1 = new Opponent();
  this.opponent2;
  this.isPublic;
  this.amount;
}

function ChallengeCtrl($scope, $http, $window, $timeout, ChiliConfigs, nutronEvents,  Widget, WidgetAPI, ChiliTour){
    var ANY_OPPONENT = 'any';
    $scope.isNew;
    $scope.gameData;
    $scope.opponent1  = {};
    $scope.opponent2  = {};
    $scope.challengeEvent;
    $scope.challengeAmount;
    
    $scope.optionIndex;
    $scope.optionStyle;


    $scope.blockChallengeView;

    $scope.challangeLoaded;

    $scope.$watch(function(){ return  Widget.challengeData; }, function(newVal, oldVal){
        // ANIMATING CHALLANGES ROW
        $scope.challangeLoaded = true;
        $timeout(function(){

              $scope.challangeLoaded = false;

              $scope.blockChallengeView = false;
              if("block" in Widget.challengeData)
              {
                  $scope.blockChallengeView = true;
              }
              else
              {
                $scope.isNew           = Widget.challengeData.is_new;
                $scope.challengeAmount = $scope.isNew ? 100 :  Widget.challengeData.amount;
                $scope.challengeData   = Widget.challengeData;

                $scope.gameData        = Widget.challengeData.game;
                $scope.opponent1       = Widget.challengeData.opponent1; 
                $scope.opponent2       = Widget.challengeData.opponent2;
                $scope.optionIndex     = null;
              }
        }, 250, true);

    }, true);

    $scope.selectOption = function(eventIndex){

        Widget.track("CHALLANGE", "select_challange_option", Widget.nonInteraction);

        $scope.optionIndex     = eventIndex;
        $scope.challengeEvent  = $scope.gameData.aux_info.events.base_events[eventIndex];
        ChiliTour.goToStep(6);
    };

    $scope.createChallenge = function(){
          if(!Widget.checkAuthorized()) return;
          
          ChiliTour.end();

          var challengePostData = {};
          challengePostData.game_id        = $scope.gameData.id;
          challengePostData.event_id       = $scope.challengeEvent.id;
          challengePostData.coefficient    = $scope.challengeEvent.coefficient;
          challengePostData.basis          = $scope.challengeEvent.basis;  
          challengePostData.amount         = $scope.challengeAmount;
          challengePostData.target_user_id = $scope.opponent2.id != ANY_OPPONENT ? $scope.opponent2.id : null;

          Widget.track("CHALLANGE", "create_challange", Widget.nonInteraction);

          Widget.startLoading();
          WidgetAPI.createChallange(challengePostData).success(function(data, status, headers, config){
            if (Widget.assert(data) && data.hasOwnProperty("balance")) 
            {
              Widget.player.balance = data.balance;
              $scope.nextChallenge();
              Widget.endLoading();
            }
         
        }).error(function(data, status, headers, config) {
            Widget.error("ChallengeCtrl.createChallenge");
        }); 
    };

    $scope.nextChallenge = function(type){
         if(!Widget.checkAuthorized()) return;

          Widget.track("CHALLANGE", "accept_next", Widget.nonInteraction);
          ChiliTour.end();

          WidgetAPI.getNextChallange().success(function(data, status, headers, config){
            if (Widget.assert(data)) {
              if(data.hasOwnProperty("id")){
                  Widget.challengeData = data;
              }
              if(data.hasOwnProperty("block")){
                  //TODO blocking challenges view
                  Widget.challengeData = data;
              }
            }
          }).error(function(data, status, headers, config) {
             Widget.error("ChallengeCtrl.nextChallenge");
          }); 
    };

    $scope.acceptChallenge = function(){
          if(!Widget.checkAuthorized()) return;

          Widget.track("CHALLANGE", "accept_challange", Widget.nonInteraction);
          
          var acceptChallengeData = {};
          acceptChallengeData.challenge_id = $scope.challengeData.id;
          acceptChallengeData.event_id     = $scope.challengeEvent.id;
          acceptChallengeData.coefficient  = $scope.challengeEvent.coefficient;
          acceptChallengeData.basis        = $scope.challengeEvent.basis;
        
          Widget.startLoading();
          WidgetAPI.acceptChallange(acceptChallengeData).success(function(data, status, headers, config){
            if (Widget.assert(data) && data.hasOwnProperty("accepted")) 
            {
              Widget.player.balance = data.balance;
              Widget.challengeData.accepted = true;
              $scope.nextChallenge();
              Widget.endLoading();
            }
            
        }).error(function(data, status, headers, config) {
            Widget.error("ChallengeCtrl.acceptChallenge");
        }); 
    };

    $scope.$on("OPPONENT_SELECTED", function(ev, opponentData){
       Widget.track("CHALLANGE", "opponent_selected", Widget.nonInteraction);
       $scope.opponent2 = opponentData;
    });

}
// #### CHALLANGE CONTROLLER END  ######


// ####                                  ######
// #### SocialLoginCtrl CONTROLLER START ######
// ####                                  ###### 
function SocialLoginCtrl($scope, $http, $window, ChiliConfigs, nutronEvents, Widget, WidgetAPI){
      $scope.fbStatus;
      $scope.gStatus;

      function getPlatformById(platformId){
          switch(parseInt(platformId))
          {
            case 1:
              return "facebook";
            case 2:
              return "google";
            case 3:
              return "odnoklassniki";
            case 4:
              return "vkontakte";
            case 5:
              return "mailru";
          }
      }

      $window.authorized_VK_ODN_MAILRU = function(platformId, userData){
        var platform = getPlatformById(platformId);

        Widget.track("SOCIAL", "auth_" + platform, Widget.nonInteraction);

        var data = JSON.parse(userData);
        Widget.playerLogin(data.player_data);
        Widget.games = data.games;
        Widget.challengeData = data.challenges;
        $scope.$apply();
      };

      $scope.socialOauth2 = function(platform){
        try
        {
            Widget.endLoading();
            var w = 700;
            var h = 400;
            var left = ($window.screen.width/2)-(w/2);
            var top = ($window.screen.height/2)-(h/2);
            var windowURL = $window.BASE_INDEX + 'auth/' + platform + "/"+ Widget.widgetId;
            var ow = $window.open(windowURL, 'Signin', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            ow.focus();
        }
        catch(e){
        }
      };

      $scope.$on(nutronEvents.FACEBOOK_SIGNIN , function(e, data){
          Widget.track("SOCIAL", "auth_facebook", Widget.nonInteraction);

          //platform "facebook"
          $scope.autorize(1, data);
      });

      $scope.$on(nutronEvents.FACEBOOK_SIGNIN_ERROR , function(){
          Widget.error("on nutronEvents.FACEBOOK_SIGNIN_ERROR");
      });

      $scope.$on(nutronEvents.GOOGLE_SIGNIN, function(e, data){
          Widget.track("SOCIAL", "auth_google", Widget.nonInteraction);
            //platform "googe"
          $scope.autorize(2, data);
      });

      $scope.$on(nutronEvents.GOOGLE_SIGNIN_ERROR, function(){
         Widget.error("on nutronEvents.GOOGLE_SIGNIN_ERROR");
      });

      //TODO
      $scope.autorize = function(platformId, dataVal){
          var platformData = { platform_id: platformId, data : dataVal};
          $http({
                method: "POST",
                url: ChiliConfigs.BASE_INDEX + "embed/wauth/player/" + Widget.widgetId,
                data :  platformData,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function(data, status, headers, config){
                //success case
                if(Widget.assert(data) && data.error != 1)
                {
                  Widget.playerLogin(data.player_data);
                  Widget.games = data.games;
                  Widget.challengeData = data.challenges;
                  Widget.endLoading();
                }
            }).error(function(data, status, headers, config) {
                Widget.error("SocialLoginCtrl.autorize");
            });  
      };
}
// #### SocialLoginCtrl CONTROLLER START ######



// ####                                 ######
// #### MyActivityCtrl CONTROLLER START ######
// ####                                 ######
function MyActivityCtrl($scope, $http, $window, ChiliConfigs, Widget, WidgetAPI){
    $scope.activityHistory = new Array();
    $scope.userStats;

    var activityPage = 0;

    $scope.eventTemplatesData = Widget.eventTemplatesData;

    $scope.$on("MODE_ACTIVITY", function(e, data){
        
        Widget.track("ACTIVITY", "watch_activity", Widget.nonInteraction);

        activityPage = 0;
        $scope.activityHistory = new Array();
        $scope.loadMoreActivity();
    });

    $scope.loadMoreActivity = function() {
        Widget.startLoading(activityPage);
        Widget.track("ACTIVITY", "load_more", Widget.nonInteraction);

        WidgetAPI.loadActivity(activityPage).success(function(data, status, headers, config){
            if(Widget.assert(data)) 
            {
              var activities = data["activity_history"]; 
              angular.forEach(activities, function(value, key){
                $scope.activityHistory.push(value);
              });
              activityPage++;
              $scope.userStats = data["user_stats"];
              Widget.endLoading();
            }
            
        }).error(function(data, status, headers, config) {
              Widget.endLoading();
              Widget.error("MyActivityCtrl.loadMoreActivity");
        });     
    };
}
// #### MyActivityCtrl CONTROLLER START ######


// ####                                    ######
// #### ProfileHeaderCtrl CONTROLLER START ######
// ####                                    ######
function ProfileHeaderCtrl($scope, $rootScope, Widget, WidgetAPI){
    $scope.showMenu = false;

    $scope.toggleMenu = function(state){
      $scope.showMenu = state; //!$scope.showMenu;
    }

    $scope.betCartItemsQty = 0;

    $scope.$watch(function(){ return  Widget.betchain; }, function(){ 
         $scope.betCartItemsQty = Widget.betchain.length;
    }, true);

    //TODO
    $scope.logoutPlayer = function(){
        WidgetAPI.logout(Widget.player.id).success(function(data, status, headers, config){
              //success case
              if(data.action == "logout")
              {
                $scope.$emit("MODE_GAME");

                Widget.playerLogout();
              }
          }).error(function(data, status, headers, config) {
              Widget.error("ProfileHeaderCtrl.logoutPlayer");
          });  
    };
}

function LeaderboardCtrl($scope, $http, $window, ChiliConfigs,Widget, WidgetAPI){
    $scope.leaderboardData = new Array();
    $scope.currentUserData = null;

    var topPage = 0;
    
    $scope.$watch(function(){ return Widget.player; }, function(){
        $scope.player_id = Widget.player.id;
    }, true);

    $scope.$on("MODE_LEADERBOARD", function(e, data){
        Widget.track("LEADERBOARD", "watch_leaders", Widget.nonInteraction);
        topPage = 0;
        $scope.leaderboardData = new Array();
        $scope.loadLeaderboard();
    });

    $scope.loadLeaderboard = function() {
        Widget.startLoading(topPage);
        Widget.track("LEADERBOARD", "watch_leaders", Widget.nonInteraction);
        WidgetAPI.getLeaderboard(topPage).success(function(data, status, headers, config) {
            if (Widget.assert(data)) 
            {
              angular.forEach(data['list'], function(value, key){
                $scope.leaderboardData.push(value);
              });
              $scope.currentUserData = data['user'];
              topPage++;
              Widget.endLoading(); 
            }
        }).error(function(data, status, headers, config) {
            Widget.endLoading();
            Widget.error("LeaderboardCtrl.loadLeaderboard");
        });     
    };
}

