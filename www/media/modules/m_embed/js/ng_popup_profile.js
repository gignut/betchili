// document.domain =  "betchili-dev.com";

var ChiliAccountPopup = angular.module("ChiliAccountPopup", ['ngResource', 'ChiliApp']);

ChiliAccountPopup.factory("ProfileAPI" , function(ChiliConfigs, ChiliUtils, $http){
			return {

						login : function(ob){
							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.params.BASE_INDEX + "main/oauth/singin" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						register : function(ob){
							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.params.BASE_INDEX + "main/oauth/register" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						resetPassword : function(ob){
							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.params.BASE_INDEX + "main/accoun/forgotpassword" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						changePass : function(ob){
							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.params.BASE_INDEX + "main/oauth/changepassword" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						}
				   };
	});

function ProfileCtrl($scope, $http, $window,  ProfileAPI, ChiliConfigs, ChiliUtils) {
	$scope.signinModel = {};
	$scope.signupModel = {};
	$scope.forgetPassModel = {};
	$scope.errorTxt = "";

	// init variables of controller
	$scope.init = function(signed)
	{	
		ChiliConfigs.params = $window.chiliParams;
	
		$scope.loginHref = ChiliConfigs.params.BASE_INDEX + "main/oauth/signin_page";;
		$scope.registrationHref = ChiliConfigs.params.BASE_INDEX + "main/oauth/registration_page";
		$scope.forgetpasswordHref = ChiliConfigs.params.BASE_INDEX + "main/oauth/forgotpassword_page";

		if($window.addEventListener){
		  	$window.addEventListener("message", $scope.closeWindow, false)
		} else {
		  	$window.attachEvent("onmessage", $scope.closeWindow)
		}

		if(signed)
		{
			$scope.loginCallback();
		}
	};

	$scope.loginCallback = function(){
		try
		{
			$window.opener.postMessage("authorize", ChiliConfigs.params.BASE_URL);
		}
		catch(e)
		{
			$window.opener.location.reload();
			$window.close();
		}
	};

	$scope.closeWindow = function(event){
		if(ChiliConfigs.params.BASE_URL.indexOf(event.origin) != -1)
  		{
      		$window.close();
 		}
	};

	$scope.login = function(){
		ChiliUtils.block();
		ProfileAPI.login($scope.signinModel).success(function(data, status, headers, config){
				if( typeof data.error != "undefined")
	 			{
	 				$scope.errorTxt = typeof data.message != "undefined" ? data.message :"Try again!";
	 			}
	 			else
	 			{
	 				$scope.loginCallback();
	 			}
	 			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::Error ProfileCtrl login");
 	    	});	  
	};

	$scope.register = function() {
		ChiliUtils.block();
		ProfileAPI.register($scope.signupModel).success(function(data, status, headers, config){
 				if( typeof data.error != "undefined")
 				{
 					$scope.errorTxt = typeof data.message != "undefined" ? data.message :"Try again!";
 				}
 				ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::Error ProfileCtrl register");
 	    	});	  

	};

	$scope.resetPassword = function() {
		ProfileAPI.resetPassword($scope.forgetPassModel).success(function(data, status, headers, config){
	 		 	ChiliUtils.action(data);
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::Error ProfileCtrl register");
 	    	});	  
	};

	$scope.changePass = function() {
		ProfileAPI.changePass($scope.changePassModel).success(function(data, status, headers, config){
	 			ChiliUtils.action(data);
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::Error ProfileCtrl register");
 	    	});	  
	};

	
}