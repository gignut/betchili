function FakeHttp(successData, errData){
      this.success = function(successCallback){ 
        successCallback(successData); 
        return this; 
      }

      this.error = function(errCallback){  
        return this; 
      }
}

chilisiteApp.service("ChiliSupersonic", function($window) {

	return {
		init: function(ssaConfig){
		}
	};

});

chilisiteApp.service("ChiliUserVoice", function($window) {
  return {
    init: function(){
    }
  };

});


chilisiteApp.factory("WidgetAPI", function($http, ChiliConfigs){
    return {
               bet : function(betData){
                  return new FakeHttp({}); 
               },
              
               filterGames: function(filterData){
               	   var fakeGames = getFakeGames(filterData);
                   return new FakeHttp(fakeGames); 
               },

              loadActivity : function(pageNum){
                  return new FakeHttp(activityData); 
               },

              getOpponents: function() {
              	   var fakeOpponents = {};
                   return new FakeHttp(fakeOpponents); 
              },

               createChallange : function(challengeData) {
                 return new FakeHttp(challangeData); 
               },

               getNextChallange : function(){
                  return new FakeHttp(challangeData); 
               },

              acceptChallange : function(acceptChallengeData){
                  return new FakeHttp(challangeData); 
               },

               getLeaderboard : function(acceptChallengeData){
                   return new FakeHttp(leaderBoardData); 
               },

              logout : function(playerId){
                  return new FakeHttp({action: 'fake'}); 
               }
           }; 
});

chilisiteApp.factory("Widget", function($timeout, $rootScope, $window){

    var API_ERROR = {
            'BET_ERROR_CODE' : 104,
            'CHALLENGE_ERROR_CODE' : 105,
            'GAME_ERROR_CODE' : 106,
    };

    var leaguesModal      = null;
    var gameLoaderElem    = null;
    var usersModalContent = null;
    //bet modals
    var betModal          = null;
    var betReplaceDialog  = null;
    var maxBetCountDialog = null;

    var pleaseLoginElem = null;

     var playerAuthorized = true;
    
    return {    //ga start
                nonInteraction: {'nonInteraction': 1} ,
                track: function(category, action, label, value, config){
                },
                error: function(msg, isFatal){
                },
                // ga end
                player:            {},
                widgetId           : null,
                siteId             : null,
                eventTemplates     : null,
                eventTemplatesData : null, 
                leagues            : null,
                competitions       : null,
                games              : null,
                betchain           : [], 
                challengeData      : {},      
                
                EVENTS :{
                  GAME_UPDATED:         "gameDataUpdated",
                  NOT_AUTHORIZED_USER : "not_authorized_user",
                  PLAYER_UPDATED:       "playerDataUpdated",
                  PLAYER_LOG_IN:        "playerLogIn",
                  PLAYER_LOG_OUT:       "playerLogOut"
                }, 

                playerLogin:function(playerId){
                   this.player = playerData;
                    var playerId = this.player.player_id;
                    playerAuthorized = true;
                    this.broadcastEvent(this.EVENTS.PLAYER_LOG_IN,  playerId);
                },
                playerLogout: function(){
                   this.broadcastEvent(this.EVENTS.PLAYER_LOG_OUT);
                },
                update: function(){
                   this.broadcastEvent(this.EVENTS.GAME_UPDATED);
                },
                checkAuthorized : function(){
                },
                broadcastEvent: function(event, data){
                },
                startLoading: function(overlayVal){
                },
                endLoading : function(){
                },
                openLeaguesModal : function(){
                },
                openUsersModal: function(overlayVal) {
                },
                openBetCartDialog: function(overlayVal) {
                },
                openBetReplaceDialog: function(overlayVal, isNote) {
                }, 
                openMaxBetCountDialog: function(overlayVal) {
                },
                showNotification: function(message,type){
                  console.log(message);
                },
                assert: function(responseData) {
                    return true;
                }
            }; 
});
