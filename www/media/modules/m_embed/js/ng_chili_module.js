var chilisiteApp = angular.module("chilisiteApp", ['ngResource', 'ngAnimate', 'ngNutron', 'ui.bootstrap']); 

chilisiteApp.directive('chiliDigit', function () {
    return {
       
        link: function (scope, element, attrs) {
          scope.checkInput = function(evt){
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              // allow delete, backspace, arrows
              if ( (charCode > 47 && charCode < 58) || charCode == 8 ||  charCode == 46 ||  (charCode >= 35 && charCode <= 40))  
              {
                  return true;
              }
              return false;
          };    
          
          element.on("keypress", function(event){
              return scope.checkInput(event);
          });
      }
    };
  }); 

// UPDATE MODEL WHEN INPUT GET BLURED EVENT
chilisiteApp.directive('betMin', ['$parse', function($parse) {
   return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, elm, attrs, ngModelCtrl) {
          ngModelCtrl.$parsers.unshift(function(value) {
             var valid =  parseInt(attrs.betMin) <= parseInt(value);
             ngModelCtrl.$setValidity('betMin', valid);
             return valid ? value : 0;
          });
        }
    };
}]);

// UPDATE MODEL WHEN INPUT GET BLURED EVENT
chilisiteApp.directive('betMax', ['$parse', function($parse) {
   return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, elm, attrs, ngModelCtrl) {
          ngModelCtrl.$parsers.unshift(function(value) {
             var valid =  parseInt(attrs.betMax) >= parseInt(value);
             ngModelCtrl.$setValidity('betMax', valid);
             return valid ? value : 0;
          });
        }
    };
}]);


chilisiteApp.factory("ChiliConfigs", function($window){
      return {
                BASE_INDEX  : $window.BASE_INDEX,
                BASE_DOMAIN : $window.BASE_DOMAIN,
                BASE_URL    : $window.BASE_URL,
                MEDIA_URL   : $window.MEDIA_URL,
                languages   : ["en" , "ru"],
                platforms   : {chili :0 , facebook :1 , twitter :2, google : 3}
            }; 
});



