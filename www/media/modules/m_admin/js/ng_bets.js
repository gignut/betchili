   	
chiliadminApp.factory("BetsAPI" , function(ChiliConfigs, $http){
	return {
				getBetsList : function (s_id, w_id) {

							var ob = new Object();
							ob.chili = {site_id : s_id, widget_id : w_id};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/bets/get_all_by_site_and_widget",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						}
			};
});

function BetsCtrl($scope, $window, $timeout, ChiliMainService, BetsAPI, ChiliUtils ) {
 
 	$scope.sitesData;
	$scope.betsList;
	$scope.domainList;
	$scope.widgetList;
	$scope.selectedDomain;
	$scope.selectedWidget;

	$scope.siteSelect = function()
	{
		$scope.widgetList = new Array();
		angular.forEach($scope.selectedDomain.widgets, function(item, index) {
			$scope.widgetList.push(item);
		});
	};

	$scope.widgetSelect = function()
	{
		ChiliUtils.block();
  		BetsAPI.getBetsList($scope.selectedDomain.id, $scope.selectedWidget.id).success(function(data, status, headers, config){
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
	  				$scope.betsList = data;
	  			}
  			}
  			ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::bets widgetSelect");
    	});
	};

	$scope.init = function() {
	 	$scope.sitesData = $window.sitesData;
	};

}


