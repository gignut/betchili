   	chiliadminApp.factory("ParticipantsAPI" , function(ChiliConfigs, $http){
			return {
						getParticipantsList : function (lang, sport_id, cid) {

							var ob = new Object();
							ob.chili = {language : lang, sport_id : sport_id, country_id : cid}; // , aux_info : 'translations'

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/participants/get_by_country",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getItemDataById : function(pid, lang, aux){
							var ob = new Object();
							ob.chili = {id : pid, language : lang, aux_info : aux};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/participants/get_by_id",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});		
						},	

						updateParticipant : function(obj){
							var ob = new Object();
							ob.chili = [obj];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/participants/update",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						saveParticipants : function(obj){
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/participants/add",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});								
						},

						removeParticipant : function(pid){
							var ob = new Object();
							ob.chili = {id : pid}

							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/participants/remove",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});		
						}
					};
	});