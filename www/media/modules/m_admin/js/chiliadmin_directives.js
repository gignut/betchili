//stop propogation directive
    chiliadminApp.directive('stopevent', function () {
      return {
         restrict: 'A',
          link: function (scope, element, attrs){
              element.on(attrs.stopevent, function (e) {
                    e.stopPropagation();
                  });
          }
      };
    });

    chiliadminApp.directive('news', function($timeout) {
      return {
          restrict: "E",
          scope : {nglink : '@', ngtype : '@', ngdesc : '@', ngrowid : '@', ngtable : '@'},
          link: function(scope, element, attrs) {
              scope.$on('newsGet', function(event, args){
                  scope.showTumbnails(scope.nglink, scope.ngtype);
              });

              scope.getVideoId = function(link, type){
                  if (type == 1) { // youtube
                      var videoid = link.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
                      if(videoid != null) {
                         return videoid[1];
                      } else { 
                          console.log("The youtube url is not valid.");
                      }
                  } else if(type == 2) { // vimeo
                      var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;

                      var match = link.match(regExp);

                      if (match){
                          return match[2];
                      }else{
                          alert("The vimeo url is not valid.");
                      }
                  }
              };

              element.click(function(){
                  var videoid = scope.getVideoId(scope.nglink, scope.ngtype);

                  if (scope.ngtype == 1 || scope.ngtype == 2) // video
                  {
                      var el = angular.element('<div id = "v_'+videoid+'" style="width:360px; height:300px;"></div>');
                      element.replaceWith(el);
                      Popcorn.smart('#v_' + videoid,  scope.nglink);
                  }

              });

              scope.showTumbnails = function(link, type){
                  var videoid = scope.getVideoId(link, type);
                  var imageUrl;
                  var ref_table = '';
                  if(typeof scope.ngtable != 'undefined')
                  {
                    ref_table = scope.ngtable;
                  }
                  var ng_id = '';
                  if(scope.ngrowid != null)
                  {
                    ng_id = '#'+scope.ngrowid;
                  }
                  if (type == 1)      // youtube
                  {
                      imageUrl = "http://i2.ytimg.com/vi/"+videoid+"/0.jpg";

                      el = angular.element('<div><h5>'+ref_table+ng_id+'</h5><img src="'+imageUrl+'"><p>'+scope.ngdesc+'</p></div>');
                      element.append(el);
                  }
                  else if (type == 2) // vimeo
                  {
                      $.ajax({
                          type:'GET',
                          url: 'http://vimeo.com/api/v2/video/' + videoid + '.json',
                          jsonp: 'callback',
                          dataType: 'jsonp',
                          success: function(data) {
                              imageUrl = data[0].thumbnail_medium;
               
                              el = angular.element('<div><h5>'+ref_table+ng_id+'</h5><img src="'+imageUrl+'"><p>'+scope.ngdesc+'</p></div>');
                              element.append(el);
                          }
                      });
                  }
                  else if (type == 3)
                  {
                      el = angular.element('<div><h5>'+ref_table+ng_id+'</h5><a href="'+scope.nglink+'">'+scope.nglink+'</a><p>'+scope.ngdesc+'</p></div>');
                      element.append(el);
                  }
                  else if (type == 4)
                  {
                      el = angular.element('<div><h5>'+ref_table+ng_id+'</h5><p>'+scope.ngdesc+'</p></div>');
                      element.append(el);
                  }                  
              }
          }
      };
    });


    chiliadminApp.directive('tooltip', function () {
        return {
            restrict:'A',
            link: function(scope, element, attrs){
                $(element)
                    .attr('title',scope.$eval(attrs.tooltip))
                    .tooltip({placement: "left"});
            }
        }
    });

    // blocks responsive view
    chiliadminApp.directive('blocks', function() {
      return {
          restrict: "E",
          transclude: true,
          scope: {},
          template: '<div ng-transclude></div>',
           link : function (scope, element, attrs) {

               scope.positionBlocks = function (){
                    scope.blockItems.each(function(){
                        var min = Math.min.apply(Math, scope.blocks);
                        var index = $.inArray(min, scope.blocks);
                        var leftPos = scope.margin+(index*(scope.colWidth+scope.margin));
                        $(this).css({
                            'left':leftPos+'px',
                            'top':min+'px'
                        });
                        scope.blocks[index] = min+$(this).outerHeight()+scope.margin;
                    });
                };

                scope.maxH = function(array){
                    var h = 0;
                    for(var i=0; i<array.length; i++)
                    {
                      var z = $(array[i]).height();
                      if (h < z) {
                        h = z;
                      }
                    }
                    return h;
                };

                scope.maxTop = function(array){
                    var h = 0;
                    for(var i=0; i<array.length; i++)
                    {
                      var pos = $(array[i]).position();
                      if (h < pos.top) {
                        h = pos.top;
                      }
                    }
                    return h;
                };

                scope.setupBlocks = function() {
                  scope.width = element.parent().width();
                  scope.blocks = [];
                  scope.colCount = Math.floor(scope.width/(scope.colWidth+scope.margin*2));
                  for(var i=0;i<scope.colCount;i++){
                      scope.blocks.push(scope.margin);
                  }
                  scope.positionBlocks();
                };

                scope.calcHeight = function() {
                  scope.parentHeight = 100 + scope.maxTop(scope.blockItems) + scope.maxHeight;
                  $(element).css({'height':scope.parentHeight+'px'});
                };

                scope.whichTransitionEvent = function (){
                    var t;
                    var el = document.createElement('fakeelement');
                    var transitions = {
                      'transition':'transitionend',
                      'OTransition':'oTransitionEnd',
                      'MozTransition':'transitionend',
                      'WebkitTransition':'webkitTransitionEnd'
                    }

                    for(t in transitions){
                        if( el.style[t] !== undefined ){
                            return transitions[t];
                        }
                    }
                }

                scope.start = function() {
                  scope.blockItems = element.find(".block");
                  scope.margin = 10;
                  scope.parentHeight = 0;
                  scope.colWidth = $(scope.blockItems[0]).outerWidth();
                  scope.maxHeight = scope.maxH(scope.blockItems);
                  scope.calcHeight();
                  scope.setupBlocks();
                };

                scope.$on('newsGet', function(){
                  scope.start();
                });
                  
                $(window).resize(scope.setupBlocks);

                var transitionEnd = scope.whichTransitionEvent();
                  element.on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function() {
                  scope.calcHeight();
                  scope.$broadcast('newsGet');
                });
            }
      };
    });

    // // datetimepicker
    // chiliadminApp.directive('nutdatetimepicker', function() {
    //   return {
    //       restrict: "E",
    //       template :  '<div class="input-append">'+
    //                   '  <input class="nutpicker" type="text" ng-model="dateval">'+
    //                   '  <span class="add-on">'+
    //                   '     <i class="icon-time"></i>'+
    //                   '   </span>'+
    //                   '</div>',
    //       scope: {
    //         dateval: '=',
    //         mindate : "=",
    //         maxdate: "=",
    //         onchange : "&",
    //         beforeshow : "&"
    //       },
          
    //       link : function (scope, element, attrs, controller){
    //             scope.pickerElem = element.find(".nutpicker");
    //             scope.pickerElem.datetimepicker({
    //                                 stepHour: 1,
    //                                 stepMinute: 1,
    //                                 showSecond: true,
    //                                 timeFormat: 'HH:mm:ss',
    //                                 dateFormat: 'yy-mm-dd',
    //                                 beforeShow : function(){
    //                                   scope.updateDateLimits();
    //                                   //scope.beforeshow();
    //                                 },
    //                                 onClose: function(){
    //                                   var s = element.find(".nutpicker").val();
    //                                    scope.$apply(function(){scope.dateval = s;});
    //                                    scope.onchange(s);
    //                                 },
    //                                 onSelect :function(){
    //                                   var s = element.find(".nutpicker").val();
    //                                    scope.$apply(function(){scope.dateval = s;});
    //                                    scope.onchange();
    //                                 }
    //             });
                
    //             scope.updateDateLimits = function(){
    //                 scope.pickerElem.datetimepicker("option", "maxDate", new Date(scope.maxdate));
    //                 scope.pickerElem.datetimepicker("option", "minDate", new Date(scope.mindate));
    //             };
    //        }
    //   };
    // });
    
    // UPLOADER DIRECTIVE
    chiliadminApp.directive('nutupload', function($http, $window) {
    return {
              restrict: "E",
              scope :{},
              controller:function($scope, $element, $rootScope, $window){
                  $scope.actionstatus = { selected : false , photoexists : false , uploading : false , notselected : true };
                  $scope.uploadAction;
                  $scope.removeAction; 
                  $scope.noPhoto;      
                  $scope.assetPath; 
                  
                  $scope.previewboxSrc;
                  $scope.fileuploader;
                  $scope.reader;
                  $scope.progress = 0;

                  $scope.assetId;
                  $scope.assetType;

                  $scope.filedata;
                  $scope.submitFileData;

                  $scope.resourceSelected = false;

                  $scope.setStatus = function (status){
                     $scope.actionstatus.selected = false; 
                     $scope.actionstatus.photoexists = false; 
                     $scope.actionstatus.uploading = false; 
                     $scope.actionstatus.notselected = false; 
                     $scope.actionstatus[status] = true;
                     $scope.state = status;
                  }; 

                  $scope.startUpload = function (){
                      if($scope.actionstatus.selected) 
                      {
                        $scope.setStatus("uploading");
                        $scope.submitFileData.submit();
                      }
                  }; 

                  $scope.cancelUpload = function (){
                       $scope.progress = 0;
                       $scope.previewboxSrc = $scope.noPhoto;
                       $scope.setStatus("notselected");
                  };

                  $scope.removeRemoteImage = function(){
                      if($scope.actionstatus.photoexists)
                      {
                         $http.post($scope.getRemoveActionUrl()).success( function(){
                            $scope.cancelUpload();
                         });
                      }
                  };

                  $scope.createPreview = function (file){
                      $scope.reader.readAsDataURL(file);
                  };

                  $scope.$on("resourceChanged" , function(event, args){
                    $scope.cancelUpload();
                    $scope.assetId   = args.assetId;
                    $scope.assetType = args.assetType;
                    var image        = args.logo;

                    if(image)
                    {
                        $scope.previewboxSrc =  $scope.assetPath + image;
                        $scope.setStatus("photoexists");
                    }
                    else 
                    {
                        $scope.setStatus("notselected");
                    }

                    $scope.resourceSelected = true;
                  });

                  // ### DIRECTIVE API #### 
                  $scope.getUploadActionUrl = function(){
                      var id   = $scope.assetId ? $scope.assetId : null;
                      var type = $scope.assetType ? $scope.assetType : null;
                      return $scope.uploadAction + type + "/" + id;
                  };

                  $scope.getRemoveActionUrl = function(){
                      var id   = $scope.assetId ? $scope.assetId : null;
                      var type = $scope.assetType ? $scope.assetType : null;
                      return $scope.removeAction +  type + "/" + id;
                  };

                  $scope.fileSelected = function(e, data){
                      $scope.filedata = data;
                      $scope.progress = 0; 
                      var file = data.files[0];
                      $scope.createPreview(file); 
                      $scope.setStatus("selected");
                  };
               },
              template : '<div><div class="row-fuid dropbox" style="margin-bottom:10px;" ng-show="resourceSelected">'+
                          '        <div class="span2 " >'+
                          '            <img  width="128px" height="128px" ng-src="{{previewboxSrc}}">'+
                          '        </div>'+
                          '        <div class="span2">'+
                          '              <div ng-show="actionstatus.notselected">'+
                          '                 <span class="btn btn-success fileinput-button btn-block " >'+
                          '                        <i class="icon-plus icon-white"></i>'+
                          '                        <span>Select</span>'+
                          '                        <input type="file" name="file" class="nutfileupload" >'+
                          '                  </span>'+
                          '              </div>'+
                          '              <div  ng-show="actionstatus.selected">'+
                          '                <button ng-click="startUpload()" type="submit" class="btn btn-primary btn-block start  ">'+
                          '                  <i class="icon-upload icon-white"></i>'+
                          '                  <span>Start</span>'+
                          '                </button>'+
                          '              </div>'+
                          '              <div  ng-show="actionstatus.selected" style="margin-top:5px;">'+
                          '                <button ng-click="cancelUpload()" type="reset" class="btn btn-warning btn-block cancel  ">'+
                          '                  <i class="icon-ban-circle icon-white"></i>'+
                          '                  <span>Cancel</span>'+
                          '                </button>'+
                          '              </div>'+
                          '              <div  ng-show="actionstatus.photoexists">'+
                          '                <button   ng-click="removeRemoteImage()" type="button" class="btn btn-danger btn-block delete  ">'+
                          '                  <i class="icon-trash icon-white"></i>'+
                          '                  <span>Delete</span>'+
                          '                </button>'+
                          '              </div> '+
                          '        </div>'+
                          '        <div class="span7" >'+
                          '              <!-- PROGRESS BAR -->'+
                          '              <div  class="progress progress-striped row-fluid">'+
                          '                     <div class= "bar " style="width: {{progress}}%;"></div>'+
                          '              </div>'+
                          '        </div>'+
                          '  </div></div>',
               link : function (scope, element, attrs){
                      scope.uploadAction = attrs.uploadaction;
                      scope.removeAction = attrs.removeaction;
                      scope.noPhoto      = attrs.nophoto;
                      scope.assetPath    = attrs.assetpath;
                      scope.setStatus("notselected");

                      scope.reader = new FileReader();
                      scope.reader.onload = function(e){
                          scope.previewboxSrc = e.target.result;
                          scope.$apply();
                      };

                      scope.fileuploader =  element.find(".nutfileupload");
                      scope.fileuploader.fileupload({
                        add : function (e,data) { 
                          data.url = scope.getUploadActionUrl();
                          scope.submitFileData = data;
                          scope.setStatus("selected");
                          scope.$apply();
                        },
                        dropZone: element
                    });

                      scope.fileuploader.bind('fileuploadstart', function (e, data) {
                          // console.log("uploading");
                      });

                      scope.fileuploader.bind('fileuploaddrop', function (e, data) {
                          scope.fileSelected(e, data);
                          scope.$apply();
                      });
                      
                      scope.fileuploader.bind('fileuploadchange', function (e, data) {
                           scope.fileSelected(e, data);
                           scope.$apply();
                      });

                      scope.fileuploader.bind('fileuploadprogress', function (e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            scope.progress = progress;
                            scope.$apply();
                      });

                      scope.fileuploader.bind('fileuploaddone', function (e, data) {
                          scope.setStatus("photoexists");
                          scope.$apply();
                      });
               }
           };
    });
