   	chiliadminApp.factory("CountriesAPI" , function(ChiliConfigs, $http){
			return {
						getAllItems : function (lang) {
							var ob =  {"chili" : {"language": lang} } ;

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/countries/get_all" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getItemDataById : function(cid, lang, aux){
							var ob = { "chili" : { "id": cid, "language": lang, "aux_info" : aux } };
							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/countries/get_by_id" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});		
						},

						updateItem : function(obj){
							var ob = new Object();
							ob.chili = [obj];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/countries/update" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						saveNewItems : function(obj){
							var ob = {"chili" : obj};
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/countries/add" ,
			    				data :   $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});		
						},

						removeItem : function(cid){
							var ob = { "chili" : {"id" : cid} };
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/countries/remove" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});		
						}
					};
	});


function CountryModel()
{
	this.id;
	this.name;
	this.code;
	this.translations = new TranslationModel();
	return this;
}	

function CountriesCtrl($scope, $http, $window,  ChiliConfigs, CountriesAPI , ChiliMainService ,ChiliUtils  ) {
	$scope.list;
	$scope.searchtxt;
	$scope.selectedItem;
	$scope.countrymodel;
	$scope.newItemList = new Array();

	$scope.modelEncode  = function(data){
		var ob = new Object();
		ob.id = data.id;
		ob.name = data.name;
		ob.code = data.code;
		ob.translations = new TranslationModel();

		angular.forEach(data.aux_info.translations, function(item, index) {
			ob.translations[index].text = item;
		});

		return ob;
	};

	$scope.modelDecode = function(mob){
		var ob = new Object();
		ob.id = mob.id;
		ob.name = mob.name;
		ob.code = mob.code;
		ob.aux_info = {"translations" : new Object()};
		angular.forEach(mob.translations, function(item, index) {
			ob.aux_info.translations[index] = item .text;
		});

		return ob;
	};

	// updateList = true if we want to update list item
	$scope.setSelectedElement = function(data, updateList){
		$scope.selectedItem = data;
		$scope.countrymodel = $scope.modelEncode(data);	

		if(updateList)
		{
			var id = $scope.countrymodel.id;
			angular.forEach($scope.list, function(item, index) {
					if(item.id == id)
					{
						$scope.list[index] =  $scope.selectedItem ;
						return;	
					}	
				});
		}

		var eventdata = {};
		eventdata.assetId = $scope.selectedItem.id;
		eventdata.assetType = "countries";
		eventdata.logo = $scope.selectedItem.flag;
		$scope.$broadcast("resourceChanged" , eventdata);
	};

	$scope.resetSelectedElement = function(){
		$scope.selectedItem = null;
		$scope.countrymodel = null;
	};
	 
	// adding a new country item
	$scope.createNewItem = function(){
		$scope.resetSelectedElement();
		$scope.newItemList.push({name : "" , code : ""});
	};

	$scope.removeNewItem = function(index) {
	    $scope.newItemList.splice(index,1);
	};

	$scope.saveNewItems = function() {
		var param  =  new Array();
		angular.forEach($scope.newItemList, function(item, index) {
			param.push({"name" : item.name, "code" : item.code});
		});

		ChiliUtils.block();

    	CountriesAPI.saveNewItems(param).success(function(data, status, headers, config){
    			if(ChiliUtils.action(data) && data.countries)
    			{
    				var countriesData = data.countries;
    				$scope.newItemList = new Array();
  					var len  = countriesData.length;
  					for (var i=0; i < len; i++) {
  						$scope.list.push(countriesData[i]);
  					}
  					ChiliUtils.noty("Created successfuly");	
    			}
    			
  				ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::country saveNewItems");
	    	});	   
	};

	// update country data by id, we get updated element data from server in <data>
	$scope.updateListItem = function() {
		$ob = $scope.modelDecode($scope.countrymodel);
		ChiliUtils.block();
		
		CountriesAPI.updateItem($ob).success(function(data, status, headers, config){
				if(ChiliUtils.action(data))
				{
					$scope.setSelectedElement(data,true);
					ChiliUtils.noty("success");
				}
				
  				ChiliUtils.unblock();	
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::country updateListItem");
	    	});	      	
	};

	// get country data via ajax call and assign data to selectedItem
	$scope.showItem = function(cid, e) {	
		ChiliUtils.block();
		var lang = ChiliMainService.getCurrentLanguage();
  		CountriesAPI.getItemDataById(cid, lang , "translations").success(function(data, status, headers, config) {
			if(ChiliUtils.action(data))
			{
				$scope.setSelectedElement(data, false);
			}
		
  			ChiliUtils.unblock();
      	}).error(function(data, status, headers, config) {
        	console.log("js::country showItem");
    	});
	};

	//remove countriy item by id
	$scope.removeListItem = function() {
		ChiliUtils.block();
		var removeItemId = $scope.countrymodel.id;
	    CountriesAPI.removeItem(removeItemId).success(function(data, status, headers, config){
		    	if (ChiliUtils.action(data))
		    	{
		    		angular.forEach($scope.list, function(item, index) {
						if(item.id == removeItemId)
						{
							$scope.list.splice(index,1)
							$scope.resetSelectedElement();
							return;	
						}	
					});	

					ChiliUtils.noty("success");
		    	} 

				ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::country removeListItem");
	    	});	 
	};

	$scope.refreshList = function (){
		var lang  = ChiliMainService.getCurrentLanguage();

	  	CountriesAPI.getAllItems(lang).success(function(data, status, headers, config){
	  			if (ChiliUtils.action(data))
		    	{
		    		$scope.list = data;
		    	}
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::country refreshList");
 	    	});	      	
	};

	// init variables of controller
	 $scope.init = function(){	
	 	$scope.list = $window.countryData;
	 };
}

CountriesCtrl.$inject = ['$scope', '$http' , '$window',  'ChiliConfigs', 'CountriesAPI', 'ChiliMainService', 'ChiliUtils' ];

