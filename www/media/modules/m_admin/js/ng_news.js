   	
chiliadminApp.factory("NewsAPI" , function(ChiliConfigs, $http){
	return {
					getNewsByFilter : function (relation, id) {

						var ob = new Object();
						ob.chili = {'refer_table' : relation, 'field_id' : id, 'limit' : 15};

						return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/news/get_lastest_news_by",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
					},

					addNewNews : function (news) {

						var ob = new Object();
						ob.chili = [news];

						return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/news/add",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
					}
			};
});

function FilterModel() {
	this.ftable  = "All";
	this.ftables = new Array("All", "games", "competitions", "countries");
	this.row_number;

	return this;
}

function NewNews() {
	this.description='';

	this.type="youtube";

	this.link='';

	this.table="Any";

	this.row_id="";

	this.tables = new Array("Any", "games", "competitions", "countries");

	this.types = new Array("youtube", "vimeo", "plain text", "other link");

	this.setup = function (table, row) {
		this.table = table;
		this.row_id = row;
	};

	return this;
}

function NewsCtrl($scope, $window, $timeout, ChiliMainService, NewsAPI, ChiliUtils ) {
 
	$scope.newsList;
	$scope.newNews;
	$scope.filtermodel;

	$scope.showCreate = null;

	$scope.init = function() {
	 	$scope.newsList = $window.newsData;
	 	$scope.filtermodel = FilterModel();

	   	if ($window.sub1 != null) {
	   		$scope.filtermodel.ftable = $window.sub1;
	   	}
	   	if ($window.sub2 != null) {
	   		$scope.filtermodel.row_number = $window.sub2;
	   	}	 	
		$timeout(function() {
			$scope.$broadcast('newsGet');
	   	}, 1);
		$scope.setRelation();
	};

	$scope.setRelation = function()	{
		var rel = $scope.filtermodel.ftable;
		var id = $scope.filtermodel.row_number;
		var vrel = rel;
		if (rel == 'All')
		{
			vrel = null;
		}
		ChiliUtils.block();
  		NewsAPI.getNewsByFilter(vrel, id).success(function(data, status, headers, config){
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.newsList = data;
					$timeout(function() {
			 			$scope.$broadcast('newsGet');
			 		}, 0);
	  			}
  			}
  			ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
  			console.log("js::games setRelation");
  			ChiliUtils.unblock();
    	});
	};

	$scope.createNews = function() {
		if($scope.showCreate == null)
		{
			$scope.showCreate = 1;
		} else 
		{
			$scope.showCreate = null;
		}
		$scope.newNews = NewNews();
		$scope.newNews.setup($scope.filtermodel.ftable, $scope.filtermodel.row_number);
	};

	$scope.addNews = function()	{
		var news = new Object();
		if($scope.newNews.description != '')
		{
			news.description = $scope.newNews.description;
		}
		if ($scope.newNews.row_id != '')
		{
			news.field_id = $scope.newNews.row_id;
		}
		if ($scope.newNews.table != 'Any')
		{
			news.refer_table = $scope.newNews.table;	
		}
		if ($scope.newNews.link != '')
		{
			news.link = $scope.newNews.link;
		}
		if ($scope.newNews.type == 'youtube')
		{
			news.type = 1;
		} 
		if ($scope.newNews.type == 'vimeo')
		{
			news.type = 2;
		} 
		if ($scope.newNews.type == 'other link')
		{
			news.type = 3;
		}
		if ($scope.newNews.type == 'plain text')
		{
			news.type = 4;
		}
		ChiliUtils.block();
  		NewsAPI.addNewNews(news).success(function(data, status, headers, config){
  			if(ChiliUtils.action(data)) {
	  			if (data != '' && 'error' in data) {
					ChiliUtils.noty('error', data['message']);
				}
			}
  			ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
  			console.log("js::games addNews");
  			ChiliUtils.unblock();
    	});	

    	$scope.newNews.link = '';
		$scope.newNews.description = '';
		$scope.newNews.row_id = '';
		$window.location.reload();
	};

}


