
   	
chiliadminApp.factory("ActivityAPI" , function(ChiliConfigs, $http) {
	return {
				
				getWidgetsList: function (suid) {
					var ob = new Object();
					ob.chili = {id : suid};

					return $http({
		    				method: "POST",
		    				url: ChiliConfigs.BASE_INDEX + "admin/siteusers/get_widgets",
		    				data :  $.param(ob),
		    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});					
				},

				updateSiteUserProfile : function (obj) {
					var ob = new Object();
					ob.chili = obj;

					return $http({
		    				method: "POST",
		    				url: ChiliConfigs.BASE_INDEX + "admin/siteusers/update",
		    				data :  $.param(ob),
		    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
				},


			};
});
   	
chiliadminApp.factory("UsersAPI" , function(ChiliConfigs, $http) {
	return {
				getUsersListByFilter : function (suid, widgetid, userid, state, aux_info) {

					var ob = new Object();
					ob.chili = {site_id : suid, widget_id: widgetid, user_id: userid, state : state, aux_info: aux_info};

					return $http({
		    				method: "POST",
		    				url: ChiliConfigs.BASE_INDEX + "admin/users/get_filtered_by",
		    				data :  $.param(ob),
		    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
				},
				
				updateUserProfile : function (obj) {
					var ob = new Object();
					ob.chili = obj;

					return $http({
		    				method: "POST",
		    				url: ChiliConfigs.BASE_INDEX + "admin/users/update",
		    				data :  $.param(ob),
		    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
				},

    		};
});

chiliadminApp.factory("BetsAPI" , function(ChiliConfigs, $http) {
	return {
				getBetsListByFilter : function (suid, widgetid, data, limit) {

					var ob = new Object();
					ob.chili = {site_id : suid, widget_id: widgetid, limit: limit};
					angular.forEach(data, function(item, ind) {
						ob.chili[ind] = item;
					});

					return $http({
		    				method: "POST",
		    				url: ChiliConfigs.BASE_INDEX + "admin/bets/get_all_by_site_and_widget",
		    				data :  $.param(ob),
		    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
				},
				
    		};
});

chiliadminApp.factory("ChallengesAPI" , function(ChiliConfigs, $http) {
	return {
				getChallengesListByFilter : function (suid, widgetid, data, limit) {

					var ob = new Object();
					ob.chili = {site_id : suid, widget_id: widgetid, limit: limit};
					angular.forEach(data, function(item, ind) {
						ob.chili[ind] = item;
					});

					return $http({
		    				method: "POST",
		    				url: ChiliConfigs.BASE_INDEX + "admin/challenges/get_all_by_site_and_widget",
		    				data :  $.param(ob),
		    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
				},
				
    		};
});

function FilterModel()
{
	this.fromdate = new Date();
	this.todate = '';
	this.state  = "All";     // All, Blocked, Unblocked

	this.states = new Array("All", "Bl", "Unbl");

	this.getStateValue = function()
	{
		var state = null;
		if (this.state == 'Bl') {
			state = 1;
		} else if (this.state == 'Unbl') {
			state = 0;
		} else if (this.state == 'All') {
			state = null;
		}
		return state;
	};

	return this;
}

function ActivityCtrl($scope, $window, ChiliMainService, ActivityAPI, ChiliUtils, ChiliConfigs ) {
 
 	$scope.eventTemplates;
	$scope.siteUsersList;
	$scope.selectedSiteUser;
	$scope.widgetList;
	$scope.selectedWidget;

	$scope.page = 'users';

	$scope.waitingResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/waiting.png";
	$scope.wonResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/won.png";
	$scope.looseResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/loose.png";
	$scope.blockedResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/blocked.png";
	$scope.returnedResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/returned.png";	

	$scope.showResultStatusLogo = function(result_status) {
		if (result_status == -1) {
			return $scope.waitingResultLogo;
		} else if (result_status == 0) {
			return $scope.returnedResultLogo;
		} else if (result_status == 1) {
			return $scope.looseResultLogo;
		} else if (result_status == 2) {
			return $scope.wonResultLogo;
		}
	};

	$scope.getWidgetsCount = function(su_id) {
		var count = 0;
		angular.forEach($scope.siteUsersList, function(item, ind) {
			if (item.id == su_id) {
				count = item.widgets_count ? item.widgets_count : 0;
			}
		});
		return count;
	};

	// select competition
	$scope.siteUserSelect = function(suitem) {
		$scope.selectedSiteUser = suitem;
		$scope.getWidgetsList();
		$scope.$broadcast('getUsers', null);
	};

	$scope.widgetSelect = function() {
		if ($scope.page == 'users') {
			$scope.$broadcast('getUsers', null);
		} else if ($scope.page == 'bets') {
			$scope.$broadcast('getBets', null);
		} else if ($scope.page == 'challenges') {
			$scope.$broadcast('getChallenges', null);
		}
	};

	$scope.getEventTemplateName = function(tid) {
		var name = '';
		angular.forEach($scope.eventTemplates, function(item, ind) {
			if (item.id == tid) {
				name = item.name;
			}
		});
		return name;
	};

	$scope.changeSiteUserState = function(id, state)
	{
		var userdata = new Object();
		userdata.id = id;
		
		if (state == 0) {
			userdata.blocked = 1;
		} else if (state == 1) {
			userdata.blocked = 0;
		}

		ChiliUtils.block();
		ActivityAPI.updateSiteUserProfile(userdata).success(function(data, status, headers, config) {
				if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
	  				$scope.selectedSiteUser.blocked = data.blocked;
	  				}
	  			}
	  			ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::users changeSiteUserState");
        	ChiliUtils.unblock();
    	});
	};

	$scope.showPage = function (page) {
		if (page == 'users')  {
			$scope.page = 'users';
			$scope.$broadcast('getUsers', null);
		} else if (page == 'bets') {
			$scope.page = 'bets';
			$scope.$broadcast('getBets', null);
		} else if (page == 'challenges') {
			$scope.page = 'challenges';
			$scope.$broadcast('getChallenges', null);
		}
	};

	$scope.$on('changePage', function(ev, edata) {
		$scope.page = edata;
	});

	$scope.getWidgetName = function (id) {
		var ret;
		angular.forEach($scope.widgetList, function(item, ind) {
			if (item.id == id) {
				ret = item.name;
			}
		});
		return ret;
	};

	$scope.getWidgetsList = function () {
		if ($scope.selectedSiteUser != null)
		{
	  		ActivityAPI.getWidgetsList($scope.selectedSiteUser.id).success(function(data, status, headers, config) {
	  			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
	  					$scope.widgetList = data;
	  				}
	  			}
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::users getWidgetsList");
	    	});
	  	}
	};

	$scope.getWidgets = function () {
		var lst = [];
		angular.forEach($scope.widgetList, function(item, ind) {
			lst.push(item);
		});
		return lst;
	};

	$scope.init = function()
	{
	 	$scope.siteUsersList = $window.siteUsersData;
	 	$scope.eventTemplates = $window.eventTemplates;
	};
}



function UsersCtrl($scope, $window, $rootScope, ChiliMainService, UsersAPI, ChiliUtils ) {

	$scope.usersList;

	$scope.usersCount;

	$scope.search = {user_id : null};

	$scope.updateOnUserList = function(obj)
	{
		angular.forEach($scope.usersList, function(item, index){
			if(item.id == obj.id) {
				item.blocked = obj.blocked;
				item.mode = obj.mode;
			}
		});
	};

	$scope.showBets = function(uid) {
		var data = {'user_id': uid};
		$scope.setPage('bets');
		$("#mytabs li a:contains('Bets')").tab('show');
		$rootScope.$broadcast('getBets', data);
	};

	$scope.showChallenges = function(uid) {
		var data = {'user_id': uid};
		$scope.setPage('challenges');
		$("#mytabs li a:contains('Challenges')").tab('show');
		$rootScope.$broadcast('getChallenges', data);
	};	

	$scope.setPage = function(page) {
		$rootScope.$broadcast('changePage', page);
	};

	$scope.filtermodel = new FilterModel();

	$scope.$on('getUsers', function(ev, data) {
		if (data) {
			if ('user_id' in data) {
				$scope.search.user_id = data.user_id;
			}
		}
		$scope.getUsersListByFilter();
	});

	$scope.makeSearch = function() {
		$rootScope.$broadcast('getUsers', null);
	};

	$scope.getUsersListByFilter = function(edata)
	{
		var lang  = ChiliMainService.getCurrentLanguage();
		var currentSport = ChiliMainService.getCurrentSport();

		if ($scope.selectedSiteUser != null)
		{
			var state = $scope.filtermodel.getStateValue();
			var w_id = null;
			var u_id = null;
			if ($scope.selectedWidget) {
				w_id = $scope.selectedWidget.id;
			}
			if ($scope.search.user_id) {
				u_id = $scope.search.user_id;
			}			
			ChiliUtils.block(); // Call with bets info
	  		UsersAPI.getUsersListByFilter($scope.selectedSiteUser.id, w_id, u_id, state, 'bets_count').success(function(data, status, headers, config){
	  			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
			  			$scope.usersList = data;
			  			$scope.usersCount = data.length;
			  		}
			  	}
	  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::users getUsersListByFilter");
	  			ChiliUtils.unblock();
	    	});
	  	}		
	};


	$scope.filterState = function(state)
	{
		$scope.filtermodel.state = state;
		$scope.getUsersListByFilter();
	};

	$scope.changeUserState = function(id, state, property)
	{
		var userdata = new Object();
		userdata.id = id;
		
		switch (property) {
			case 'state' : 
					userdata.blocked = (state == 0) ? 1 : 0;
					break;
			case 'mode':
					userdata.mode = (state == 0) ? 1 : 0;
					break;				
		}

		ChiliUtils.block();
		UsersAPI.updateUserProfile(userdata).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
			  			$scope.updateOnUserList(data);
			  		}
		  	}
		  	ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::users changeUserState");
        	ChiliUtils.unblock();
    	});
	};

}

function BetsCtrl($scope, $window, $rootScope, ChiliMainService, BetsAPI, ChiliUtils ) {

	$scope.betsList;
	$scope.showID = null;
	$scope.search = {user_id : "", game_id : ""};
	$scope.betsCount;
	$scope.betGameInfo = null;

	$scope.showContent = function (id) {
		$scope.showID = $scope.showID ? null : id;
	};

	$scope.makeSearch = function() {
		$rootScope.$broadcast('getBets', null);
	};

	$scope.$on('getBets', function(ev, data) {
		var post = {};
		if (data) {
			if ('user_id' in data) {
				$scope.search.user_id = data.user_id;
			}
		}
		angular.forEach($scope.search, function(item, ind) {
			if (item) {
				post[ind] = $scope.search[ind];
			}			
		});

		$scope.getBetsListByFilter(post);
	});

	$scope.getBetsListByFilter = function(edata)
	{
		var edata = (typeof edata !== 'undefined') ? edata : null;
		if ($scope.selectedSiteUser != null)
		{
			var w_id = null;
			if ($scope.selectedWidget) {
				w_id = $scope.selectedWidget.id;
			}
			ChiliUtils.block(); // Call with bets info
	  		BetsAPI.getBetsListByFilter($scope.selectedSiteUser.id, w_id, edata, 50).success(function(data, status, headers, config) {
	  			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
		  				$scope.betsList = data;
		  				$scope.betsCount = Object.keys(data).length;
		  			}
		  		}
		  		ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::users getBetsListByFilter");
	  			ChiliUtils.unblock();
	    	});
	  	}
	};

}

function ChallengesCtrl($scope, $window, $rootScope, ChiliMainService, ChallengesAPI, ChiliUtils ) {

	$scope.challengesList;
	$scope.search = {user_id : "", game_id : "" };

	$scope.chllengeGameInfo = null;

	$scope.challengesCount;

	$scope.showGameInfo = function(cid) {
		$scope.chllengeGameInfo = $scope.chllengeGameInfo ? null : cid;
	};

	$scope.makeSearch = function() {
		$rootScope.$broadcast('getChallenges', null);
	};

	$scope.$on('getChallenges', function(ev, data) {
		var post = {};
		if (data) {
			if ('user_id' in data) {
				$scope.search.user_id = data.user_id;
			}
		}
		angular.forEach($scope.search, function(item, ind) {
			if (item) {
				post[ind] = $scope.search[ind];
			}			
		});

		$scope.getChallengesListByFilter(post);
	});

	$scope.setPage = function(page) {
		$rootScope.$broadcast('changePage', page);
	};

	$scope.showUser = function(uid) {
		var data = {'user_id': uid};
		$scope.setPage('users');
		$("#mytabs li a:contains('Users')").tab('show');
		$rootScope.$broadcast('getUsers', data);
	};

	$scope.getChallengesListByFilter = function(edata)
	{
		var edata = (typeof edata !== 'undefined') ? edata : null;
		if ($scope.selectedSiteUser != null)
		{
			var w_id = null;
			if ($scope.selectedWidget) {
				w_id = $scope.selectedWidget.id;
			}
			ChiliUtils.block(); // Call with bets info
	  		ChallengesAPI.getChallengesListByFilter($scope.selectedSiteUser.id, w_id, edata, 50).success(function(data, status, headers, config) {
	  			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
		  				$scope.challengesList = data;
		  				$scope.challengesCount = data.length;
		  			}
		  		}
		  		ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::users getChallengesListByFilter");
	  			ChiliUtils.unblock();
	    	});
	  	}
	};

}
