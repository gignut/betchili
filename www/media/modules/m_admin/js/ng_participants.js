function ParticipantModel()
{
	this.id = '';
	this.name = '';
	this.country_id = '';
	this.sport_id = '';
	this.priority = '';
	this.translations = new TranslationModel();

	return this;
}	

function ParticipantsCtrl($scope, $http, $window, ChiliMainService, ChiliConfigs, ParticipantsAPI, ChiliUtils ) {
 
	$scope.participantsList;
	$scope.countryList;

	$scope.selectedItem;
	$scope.selectedCountry;
	$scope.participantmodel;

	$scope.showNameError = null;

	$scope.newItemList = new Array();

	$scope.encodeModel = function (data) {
		model = new ParticipantModel();
		model.id = data.id;
		model.name = data.name;
		model.priority = data.priority;
		model.sport_id = data.sport_id;
		model.country_id = data.country_id;
		model.translations = new Object();
		if('aux_info' in data && 'translations' in data.aux_info) {
			angular.forEach(data.aux_info.translations, function(item, index) {
				model.translations[index] = new Object();
				model.translations[index].text = item;
			});
		} else {
			model.translations = new TranslationModel();
		}

		return model;
	};

	$scope.decodeModel = function (model) {
		var data = new Object();
		data.id = model.id;
		data.name = model.name;
		data.priority = model.priority;
		data.country_id = $scope.selectedCountry.id;
		var currentSport = ChiliMainService.getCurrentSport();
  		data.sport_id = currentSport.id;
		data.aux_info = new Object();
		data.aux_info.translations = new Object();
	 	angular.forEach(model.translations, function(item, index) {
  			data.aux_info.translations[index] = item.text;
  		});

	 	return data;
	};

	//refresh participant list according to selected country
	$scope.getParticipantsList = function (){

		var lang  = ChiliMainService.getCurrentLanguage();
		var currentSportId = ChiliMainService.getCurrentSportId();	

		if ($scope.selectedCountry != null)
		{
			ChiliUtils.block();
	  		ParticipantsAPI.getParticipantsList(lang, currentSportId, $scope.selectedCountry.id).success(function(data, status, headers, config){
	  			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.participantsList = data;
		  			}
		  			ChiliUtils.unblock();
	  			}
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::participants getParticipantsList");
	    	});
	  	}
	};

	// change country from dropdown
	$scope.countrySelect = function(){
		$scope.getParticipantsList();
		$scope.resetSelectedElement();
		$scope.newItemList = new Array();
	};

	// get data for selected participant and show it
	$scope.showItem = function(pid) {
		ChiliUtils.block();
		var lang  = ChiliMainService.getCurrentLanguage();
  		ParticipantsAPI.getItemDataById(pid, lang , "translations").success(function(data, status, headers, config) {
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.setSelectedElement(data, false);
	  			}
  			}
	        ChiliUtils.unblock();
      	}).error(function(data, status, headers, config) {
        	console.log("js::participants showItem");
    	});
	};

	// set selected participant
	$scope.setSelectedElement = function(data, updateOnList){
		$scope.selectedItem = data;
		$scope.participantmodel = $scope.encodeModel($scope.selectedItem);
		$scope.newItemList = new Array();

		if (updateOnList = true) {
			var updatedItemId = data.id;
			angular.forEach($scope.participantsList, function(item, index) {
				if(item.id == updatedItemId)
				{
					$scope.participantsList[index] =  data;
				}
			});
		}

		var eventdata = {};
		eventdata.assetId = $scope.selectedItem.id;
		eventdata.assetType = "participants";
		eventdata.logo = $scope.selectedItem.logo;
		$scope.$broadcast("resourceChanged" , eventdata);
	};

	$scope.resetSelectedElement = function(){
		$scope.selectedItem = null;
		$scope.participantmodel = null;
	};

	// update participant data by id
	$scope.updateParticipant = function() {
		var participantdata = $scope.decodeModel($scope.participantmodel);
		ChiliUtils.block();
		ParticipantsAPI.updateParticipant(participantdata).success(function(data, status, headers, config){
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.setSelectedElement(data, true);
	  				ChiliUtils.noty("success");
				}
  			}
  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::participants updateParticipant");
	    	});	      	
	};

	// adding a new participant item
	$scope.createNewItem = function(){
		$scope.resetSelectedElement();
		var sportId = ChiliMainService.getCurrentSportId();
		var countryId = $scope.selectedCountry.id;
		$scope.newItemList.push({name : "", priority: 0,  country_id : countryId , sport_id : sportId});
	};

	$scope.removeNewItem = function(index) {
	    $scope.newItemList.splice(index,1);
	};

	$scope.saveNewItems = function(){
		$scope.showNameError = null;
		var dublicates = $scope.getDublicates($scope.newItemList);

		if($scope.showNameError == null && dublicates.length != 0) {
			$scope.showNameError = new Array();
			for (var i =0; i < dublicates.length; i++) {
				$scope.showNameError[dublicates[i]] = dublicates[i];
			}
		}

		var param  =  new Array();
		angular.forEach($scope.newItemList, function(item, index) {
			if ($scope.isNameAlreadyExists(item.name)) {
				if($scope.showNameError == null) {
					$scope.showNameError = new Array();
				}
				$scope.showNameError[item.name] = item.name;
			}
			param.push({"name" : item.name, "priority" : item.priority, "country_id" : item.country_id, "sport_id" : item.sport_id});
		});

		if ($scope.showNameError == null) {
			ChiliUtils.block();
	    	ParticipantsAPI.saveParticipants(param).success(function(data, status, headers, config){
	    		if(ChiliUtils.action(data)) {
		    		if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.newItemList = [];
						for (var prop in data) {
							$scope.participantsList[prop] = data[prop];
						}
						ChiliUtils.noty("success");
					}
				}
	  				ChiliUtils.unblock();	
		  		}).error(function(data, status, headers, config) {
		        	console.log("js::participants saveNewItems");
		    	});
	  	}
	};

	$scope.removeParticipant = function(){
		var removeItemId = $scope.selectedItem.id;
		ChiliUtils.block();
	    ParticipantsAPI.removeParticipant(removeItemId)
	   		.success(function(data, status, headers, config){
	   			if(ChiliUtils.action(data)) {
		   			if (data != '' && 'error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						angular.forEach($scope.participantsList, function(item, index) {
							if(item.id == removeItemId)
							{
								delete $scope.participantsList[index];
								$scope.resetSelectedElement();
								return;	
							}	
						});	
						ChiliUtils.noty("success");
					}
				}
  				ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	$scope.errorMessage = "Please refresh the page";
	    	});	 
	};

	$scope.isNameAlreadyExists = function(newName) {
		var st = false;
		angular.forEach($scope.participantsList, function(item, index) {
			if(item.name == newName) {
				st = true;
			}
		});
		return st;
	};

	$scope.getDublicates = function(list) {
		var arr = new Array();
		angular.forEach(list, function(item) {
			arr.push(item.name);
		});

		var sorted_arr = arr.sort();

		var results = new Array();
		for (var i = 0; i < arr.length - 1; i++) {
		    if (sorted_arr[i + 1] == sorted_arr[i]) {
		        results.push(sorted_arr[i]);
		    }
		}
		return results;
	};

	$scope.init = function(){	
	 	$scope.countryList = $window.countryData;
	};
}

