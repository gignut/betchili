function TranslationModel()
{
  var langCount = this.languages.length
  var ob = new Object();
  for(var i = 0; i < langCount ; i++)
  {
    var langObject = this.languages[i];
    ob[langObject.lang] = {"text" : langObject.defaultText };
  }

  return ob;
}

TranslationModel.prototype.languages = [{lang: "en", defaultText : "provide translation" } , { lang : "ru" , defaultText : "укажите перевод"} ,
                                        {lang: "am", defaultText : "թարգմանություն" } ];

var chiliadminApp = angular.module("chiliadminApp", ['ngResource', 'ngNutron', '$strap.directives']);

chiliadminApp.factory("ChiliConfigs", function($window){
  return {
            languages : ["en" , "ru", "am"],
            BASE_INDEX : $window.BASE_INDEX,
            BASE_URL   : $window.BASE_URL,
            MEDIA_URL  : $window.MEDIA_URL,
            API_KEY : "chili"
        }; 
  });

  chiliadminApp.factory("ChiliUtils" , function($window){
    var self =  {
        block : function(){
          //backgroundColor: '#f00', color: '#fff'
          jQuery.blockUI({
            showOverlay: false,  
            css: {
                  border: 'none', 
                  padding: '15px', 
                  backgroundColor: '#000', 
                  '-webkit-border-radius': '10px', 
                  '-moz-border-radius': '10px', 
                  opacity: .5, 
                  color: '#fff' 
               } 
          }); 
        },

        unblock : function(){
          jQuery.unblockUI({ fadeOut: 200 }); 
        },

        action: function(responseData){
            if(responseData && responseData["module"])
            {
              var moduleName = responseData["module"];
              switch(moduleName)
              {
                case "noty" :
                    self.noty(responseData.action, responseData.message);
                    break;
                case "redirect" :
                    self.redirect(responseData.action, responseData.message);
              }
            }

            return true;
        },

        redirect : function(action, path){
           $window.location.href = path;
        },

        noty : function(action,message){
          if(!action)
          {
              return;
          }

          var notyOb = null;
          switch(action)
          {
              case 'success':
                message = typeof message !== 'undefined' ? message : "Everything is OK";
                notyOb = $window.noty({text: message, layout: 'topRight', type: 'success', timeout : true, closeWith: ['click', 'hover'] });
                break;
              case 'error':
                message = typeof message !== 'undefined' ? message : "Something wnet WRONG";
                notyOb = $window.noty({text: message, layout: 'topRight', type: 'error', timeout : true, closeWith: ['click', 'hover']});
                break;
          }
        }
    };

    return self;
  });

  chiliadminApp.factory("ChiliMainService" , function(ChiliConfigs, $http){
      return {

          currentSport : null,
          allSportTypes : null,

          getAllSports : function(){
             return this.allSportTypes; 
          },

          setAllSports : function(data){
              this.allSportTypes = data;
          },

          getCurrentSport : function(){
              return this.currentSport;
          },

          getCurrentSportId : function(){
              return this.currentSport.id;
          },

          setCurrentSportSession : function(sport){
              var self = this;
              var ob = new Object();
              ob.sport_id = sport.id;

              return $http({
                    method: "POST",
                    url: ChiliConfigs.BASE_INDEX + "admin/sports/setSportType" ,
                    data :  $.param(ob),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                  }).success(function(ata, status, headers, config){
                      self.currentSport = sport;
                  }); 
          },

          setCurrentSportLocal : function(sport){
              this.currentSport = sport;
          },

          currentLanguage : "en",

          getCurrentLanguage : function(){
              return this.currentLanguage;
          },

          setCurrentLanguage : function (lang){
              this.currentLanguage = lang;
          }
      };
    });

  function MainCtrl($scope, $http, $window, ChiliMainService)
  {
    $scope.sports;
    $scope.currentSport;
    $scope.isAutorized;
    $scope.menuItem;

    $scope.setSport = function(vsport){
        ChiliMainService.setCurrentSportSession(vsport).success(function(){
           $window.location.reload();
        });
    };

    $scope.init = function(){
        $scope.isAutorized = $window.isAutorized;
        $scope.menuItem = $window.menuItem;

        var currentSportId = $window.currentSportId;
    
        ChiliMainService.setAllSports($window.sportData);
        $scope.sports =  ChiliMainService.getAllSports();

        angular.forEach($scope.sports, function(item, index) {
          if(item.id == currentSportId)
          {
            ChiliMainService.setCurrentSportLocal(item);
            $scope.currentSport  = ChiliMainService.getCurrentSport();
            return; 
          } 
        });
    };
  }
