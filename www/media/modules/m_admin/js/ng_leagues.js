   	chiliadminApp.factory("LeaguesAPI" , function(ChiliConfigs, $http){
			return {
						getAllItems : function (lang) {

							var ob = new Object();
							ob.chili = {language: lang};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/leagues/get_all" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getItemDataById : function(lid, lang, aux) {
							var ob = new Object();
							ob.chili = { id: lid, language: lang, aux_info : aux };

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/leagues/get_by_id" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});		
						},

						updateItem : function(obj) {
							var ob = new Object();
							ob.chili = [{ "id": obj.id, "country_id" : obj.country_id, "name" : obj.name, "priority" : obj.priority, "aux_info" : obj.aux_info }];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/leagues/update" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						saveNewItems : function(obj) {
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/leagues/add" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});		
						},

						removeItem : function(lid) {
							var ob = new Object();
							ob.chili = {id : lid};
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/leagues/remove" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});
						}
					};
	});

function LeagueModel() {
	this.id = "";
	this.name = "";
	this.country_id = "";
	this.translations = new TranslationModel();
	this.sport_id = "";
	this.priority = "";

	return this;
}	

function LeaguesCtrl($scope, $http, $window, ChiliMainService, ChiliConfigs, LeaguesAPI, ChiliUtils) {
 
	$scope.list;
	$scope.searchtxt;
	$scope.selectedItem;
	$scope.leaguemodel;
	$scope.newItemList = new Array();
	$scope.countryList;

	$scope.modelEncode  = function(data) {
		var ob  = new Object();
		ob.id   = data.id;
		ob.name = data.name;
		ob.priority = data.priority;
		ob.country_id = data.country_id;
		ob.translations = new TranslationModel();

		if('aux_info' in data && 'translations' in data.aux_info) {
			angular.forEach(data.aux_info.translations, function(item, index) {
				ob.translations[index].text = item;
			});
		}

		return ob;
	};

	$scope.modelDecode = function(mob) {
		var ob = new Object();
		ob.id   = mob.id;
		ob.name = mob.name;
		ob.priority = mob.priority;
		ob.country_id = mob.country_id;
		ob.sport_id = ChiliConfigs.getCurrentSportId;
		ob.aux_info = {"translations" : new Object()};
		angular.forEach(mob.translations, function(item, index) {
			ob.aux_info.translations[index] = item .text;
		});

		return ob;
	};

	// updateList = true if we want to update list item
	$scope.setSelectedElement = function(data, updateList) {
		$scope.selectedItem = data;
		$scope.leaguemodel = $scope.modelEncode(data);

		if(updateList)
		{
			var id = $scope.leaguemodel.id;
			angular.forEach($scope.list, function(item, index) {
					if(item.id == id)
					{
						$scope.list[index] =  $scope.selectedItem ;
						return;	
					}	
				});
		}

		var eventdata = {};
		eventdata.assetId = $scope.selectedItem.id;
		eventdata.assetType = "leagues";
		eventdata.logo = $scope.selectedItem.logo;
		$scope.$broadcast("resourceChanged" , eventdata);
	};

	$scope.resetSelectedElement = function() {
		$scope.selectedItem = null;
		$scope.leaguemodel = null;
	};
	 
	// adding a new league item
	$scope.createNewItem = function(){
		$scope.resetSelectedElement();
		var sport_id = ChiliMainService.getCurrentSportId();
		$scope.newItemList.push({name : "", sport_id : sport_id, priority : 0});
	};

	$scope.removeNewItem = function(index) {
	    $scope.newItemList.splice(index,1);
	};

	$scope.saveNewItems = function() {
		var param  =  new Array();
		angular.forEach($scope.newItemList, function(item, index) {
			param.push({"name" : item.name, "sport_id" : item.sport_id, "priority" : item.priority});
		});
		ChiliUtils.block();
    	LeaguesAPI.saveNewItems(param).success(function(data, status, headers, config) {
    		if(ChiliUtils.action(data)) {
	    		if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.newItemList = new Array();
		  			for (var i=0; i < data.length; i++) {
		  				$scope.list.push(data[i]);
		  			}
	  				ChiliUtils.noty("success");
		  		}
		  	}
  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::league saveNewItems");
	    	});	   
	};

	// update league data by id
	$scope.updateListItem = function() {
		$ob = $scope.modelDecode($scope.leaguemodel);
		ChiliUtils.block();
		LeaguesAPI.updateItem($ob).success(function(data, status, headers, config){
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.setSelectedElement(data, true);
  					ChiliUtils.noty("success");
		  		}
		  	}
  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::league updateListItem");
	    });	     	
	};

	// get league data via ajax call and assign data to selectedItem
	$scope.showItem = function(lid,e) {
		ChiliUtils.block();
		var lang = ChiliMainService.getCurrentLanguage();
  		LeaguesAPI.getItemDataById(lid, lang , "translations").success(function(data, status, headers, config) {
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.setSelectedElement(data, false);
		  		}
	  		}
	        ChiliUtils.unblock();
      	}).error(function(data, status, headers, config) {
        	console.log("js::league showItem");
    	});
	};

	//remove league item by id
	$scope.removeListItem = function() {
		ChiliUtils.block();
		var removeItemId = $scope.selectedItem.id;
	    LeaguesAPI.removeItem(removeItemId).success(function(data, status, headers, config){
	    	if(ChiliUtils.action(data)) {
	    		if (data != '' && 'error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					angular.forEach($scope.list, function(item, index) {
						if(item.id == removeItemId)
						{
							$scope.list.splice(index,1)
							$scope.resetSelectedElement();
							return;	
						}	
					});	
					ChiliUtils.noty("success");
	  			}
	  		}
  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::league removeListItem");
	    	});	 
	};

	$scope.refreshList = function (){
		var lang  = ChiliMainService.getCurrentLanguage();

	  	LeaguesAPI.getAllItems(lang).success(function(data, status, headers, config){
	  		if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.list = data;
	  			}
	  		}
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::league refreshList");
	    	});	      	
	};

	// init variables of controller
	 $scope.init = function() {	
	 	$scope.list = $window.leagueData;
	 	$scope.countryList = $window.countryData;
	 };
}

LeaguesCtrl.$inject = ['$scope', '$http' , '$window',  'ChiliMainService', 'ChiliConfigs', 'LeaguesAPI' , 'ChiliUtils'];

