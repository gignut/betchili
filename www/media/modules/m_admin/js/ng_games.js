   	chiliadminApp.factory("GameAPI" , function(ChiliConfigs, $http){
			return {

						getGamesListByFilter : function (gid, lang, skid, cids, state, result, bk_id, from, to) {
							var ob = new Object();
							ob.chili = {id: gid, language : lang, competitions : cids, sport_id : skid, aux_info : ['participants', 'events'],
									    state : state, bookmaker_id : bk_id, from_date : from, to_date : to, result: result};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/games/get_filtered_by",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getGamesCounts : function (cids, skid) {
							var ob = new Object();
							ob.chili = {competitions : cids, sport_id : skid, count : 1};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/games/get_filtered_by",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getParticipantsList : function (lang, cid) {

							var ob = new Object();
							ob.chili = {language : lang, competition_id : cid, aux_info : 'translations'};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/participants/get_by_competition",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getItemDataById : function(gid, lang, aux) {
							var ob = new Object();
							ob.chili = {id : gid, language : lang, aux_info : aux};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/games/get_by_id",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});		
						},	

						updateGame : function(obj) {
							var ob = new Object();
							ob.chili = [obj];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/games/update",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						saveGame : function(obj) {
							var ob = new Object();
							ob.chili = [obj];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/games/add",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});								
						},

						removeGame : function(gid) {
							var ob = new Object();
							ob.chili = {id : gid}

							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/games/remove",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});		
						}
					};
	});

	chiliadminApp.factory("EventsAPI" , function(ChiliConfigs, $http) {
			return {

						updateEvent : function(obj)	{
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/chilievents/update",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						addEvents : function(obj) {
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/chilievents/add",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						}						
					};
	});

	chiliadminApp.factory("ResultsAPI" , function(ChiliConfigs, $http){
			return {
						saveOnlyResults : function(obj) {
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/games/set_only_game_result",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						saveResults : function(obj) {
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/games/set_game_result",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						returnResults : function(obj) {
							var ob = new Object();
							ob.chili = obj;
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/games/return_game_result",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						}						

					};
	});



function AuxEventModel() {
	this.template_id;
	this.game_id;
	this.coefficient = "0.00";
	this.old_coefficient = "0.00";
	this.basis = 0;
	this.data; // can be player id
	this.blocked = 1;

	return this;
}

function ResultModel() {
	this.game;
	this.values; // array(template_id => result)
}

function GameModel() {
	this.id;
	this.name;
	this.participants = [0, 0];
	// array of participants' id-s of game
	this.priority = 0;
	var st = new Date();
	st.setHours(0, 0, 0, 0);
 	this.start_date = st.getTime();
 	this.blocked = 1;
 	this.status = 0; // pending
 	this.calculated = -1; // waiting
 	this.competition_id = null;
	this.translations = new TranslationModel();

	return this;
}

function FilterModel(scope)
{

	this.game_id = '';
	this.competitionList = [];
	this.selectedCompetitions = [];
	this.selectedCompetitionLogo;

	this.selectedBookmakerId;
	this.bookmakersList = [];

	var date = new Date().getTime();
	this.fromdate = date;
	this.todate = '';
	this.state  = "Unbl";     // All, Blocked, Unblocked

	this.states = new Array("All", "Bl", "Unbl");

	this.getStateValue = function() {
		var state = null;
		if (this.state == 'Bl') {
			state = 1;
		} else if (this.state == 'Unbl') {
			state = 0;
		} else if (this.state == 'All') {
			state = null;
		}
		return state;
	};

	this.statuses = new Array("All", "Set", "Unset");
	this.status = "Unset";

	this.getStatusValue = function() {
		var status = null;
		if (this.status == 'Set') {
			status = 1;
		} else if (this.status == 'Unset') {
			status = 0;
		} else if (this.status == 'All') {
			status = null;
		}
		return status;
	};

	scope.$on("updateFilterToDate", function(evnt, args) {
		scope.filtermodel.todate = args;
		scope.$broadcast("filterChanged");
	});

	scope.$on("updateFilterFromDate", function(evnt, args) {
		scope.filtermodel.fromdate = args;
		scope.$broadcast("filterChanged");
	});

	this.competitionsChanged = function() {
		if (scope.filtermodel.selectedCompetitions.length == 0) {
			var date = new Date().getTime();
			this.fromdate = date;
		}
		scope.getGamesCounts();
		scope.$broadcast("filterChanged");
	};

	this.changeBookmaker = function() {
		scope.$broadcast("filterChanged");
	};

	this.filterState = function(state) {
		this.state = state;
		scope.$broadcast("filterChanged");	
	};

	this.filterStatus = function(status) {
		this.status = status;
		scope.$broadcast("filterChanged");	
	};

	return this;
}

function GameCtrl($scope, $http, $window, ChiliMainService, ChiliConfigs, GameAPI, EventsAPI, ResultsAPI, ChiliUtils ) {

	$scope.currentSport = ChiliMainService.getCurrentSport();
	$scope.page = 2; // by default show results

	$scope.gamesList;
	$scope.baseEventGroups = new Array();
	$scope.auxEventGroups = new Array();
	$scope.eventTemplates;
	$scope.baseEventsTemplates;
	$scope.auxEventsTemplates;
	$scope.currentGameId; // in event mode
	$scope.auxEventModel = new AuxEventModel();

	$scope.compList = new Array();

	$scope.selectedGame = false;
	$scope.isNewGame = false;
	$scope.gamemodel = new GameModel();
	$scope.lastCreatedGameStartDate = $scope.gamemodel.start_date;
	$scope.minDate;
	
	$scope.filtermodel = new FilterModel($scope);

	var process = 0;
	var queue = 0;
	$scope.$on("filterChanged", function() {
		if (process == 1) {
			queue++;
			return;
		}
		process = 1;
		var lang  = ChiliMainService.getCurrentLanguage();

		var state = $scope.filtermodel.getStateValue();
		var result = $scope.filtermodel.getStatusValue();
		var bk_id = $scope.filtermodel.selectedBookmakerId;

		var cids = [];
		angular.forEach($scope.filtermodel.selectedCompetitions, function(item){
			cids.push(item);
		});
		if(cids.length == 0) {
			angular.forEach($scope.filtermodel.competitionList, function(item, index){
				cids.push(item.id);
			});
		}

		if (cids.length == 0) {
			//ChiliUtils.noty('error', "There is no any competition");
			return;
		}

		var from  = $scope.filtermodel.fromdate;
		from  = from ? moment.utc(from).format("YYYY-MM-DD HH:mm:ss") : from;
		var to    = $scope.filtermodel.todate;
		to    = to ? moment.utc(to).format("YYYY-MM-DD HH:mm:ss") : to;

		ChiliUtils.block();
  		GameAPI.getGamesListByFilter($scope.filtermodel.game_id, lang, $scope.currentSport.id, cids, state, result, bk_id, from, to).success(function(data, status, headers, config){
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.gamesList = data;
					$scope.gamesCount = data.length;
					var obj = {et: null};
					$scope.setHeaderStatuses(obj);
		  		}
	  		}
	  		process = 0;
	  		if (queue > 0) {
	  			$scope.$broadcast("filterChanged");
	  			queue--;
	  		}
  			ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
  			process = 0;
  			console.log("js::games getGamesListByFilter");
  			ChiliUtils.unblock();
    	});
	});

   	$scope.splitName = function(str) {
   		return str.split('-');
   	};

	//get participants of selected competiton
	$scope.getParticipantsList = function (cid) {
		var lang  = ChiliMainService.getCurrentLanguage();
		var currentSport = ChiliMainService.getCurrentSport();
  		GameAPI.getParticipantsList(lang, cid).success(function(data, status, headers, config){
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.compList[cid] = data;
		  		}
		  	}
  		}).error(function(data, status, headers, config) {
  			console.log("js::games getParticipantsList");
    	});
	};

	// get data for selected game and show it
	$scope.showItem = function(gid, cid) {
		if ($scope.compList[cid].length == 0) {
			$scope.getParticipantsList(cid);
		}
		var lang = ChiliMainService.getCurrentLanguage();
		ChiliUtils.block();
  		GameAPI.getItemDataById(gid, lang , ["translations", "participants"]).success(function(data, status, headers, config) {
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.setSelectedElement(data, false);
		  		}
	  		}
	        ChiliUtils.unblock();
      	}).error(function(data, status, headers, config) {
      		console.log("js::games showItem");
      		ChiliUtils.unblock();
    	});
	};

	$scope.updateGameData = function(data, game) {
		for(var key in data) {
			if (key != 'aux_info') {
				game[key] = data[key];
			} else {
				if ('participants' in data.aux_info) {
					game.aux_info.participants = data.aux_info.participants;
				}
				if ('translations' in data.aux_info) {
					game.aux_info.translations = data.aux_info.translations;
				}
				if ('events' in data.aux_info) {
					game.aux_info.events = data.aux_info.events;
				}
			}
		}
	};

	$scope.setCompetition = function() {
		var cid = $scope.gamemodel.competition_id;
		if ($scope.compList[cid].length == 0) {
			$scope.getParticipantsList(cid);
		}
	};

	// set selected game
	$scope.setSelectedElement = function(data, updateOnList) {
		$scope.isNewGame  = false;
		$scope.selectedGame  = true;
		$scope.gamemodel = $scope.encodeModel(data);
		if (updateOnList = true) {
			$scope.updateOnGameList(data);
		}
	};

	$scope.updateOnGameList = function (data) {
		var updatedItemId = data.id;
		angular.forEach($scope.gamesList, function(item, index) {
			if(item.id == updatedItemId)
			{
				$scope.updateGameData(data, $scope.gamesList[index]);
			}
		});
	};

	//reset selected game
	$scope.resetSelectedElement = function() {
		$scope.selectedGame = false;
		$scope.gamemodel = new GameModel();
		$scope.isNewGame = false;
	};

	 $scope.encodeModel = function(data) {
	 	var gamemodel = new GameModel();
	 	gamemodel.id 	     = data.id;
	 	gamemodel.name       = data.name;
	 	gamemodel.priority   = data.priority;
	 	gamemodel.blocked    = data.blocked;
	 	gamemodel.status    = data.status;
	 	gamemodel.calculated  = data.calculated;
	 	gamemodel.start_date  = data.start_date * 1000;
	 	gamemodel.participants = new Array();
	 	gamemodel.competition_id = data.competition_id;
	 	angular.forEach(data.aux_info.participants, function(item, index) {
				gamemodel.participants.push(item.id);
		});
	 	gamemodel.translations = new Object();
		angular.forEach(data.aux_info.translations, function(item, index) {
			gamemodel.translations[index] = new Object();
			gamemodel.translations[index].text = item;
		});

		return gamemodel;
	 };

	 $scope.decodeModel = function(data, isNew) {
	 	var gamedata = Object();
	 	var names = new Array();
	 	for(var ind=0; ind < data.participants.length; ind++)
	 	{
	 		angular.forEach($scope.compList[data.competition_id], function(item, index) {
				if(item.id == data.participants[ind])
				{
					names.push(item.name);
				}
			});
	 	}
	 	if (data.id !== undefined) {
	 		gamedata.id = data.id;
	 	}
	 	gamedata.name = names.join(' - ');
	 	gamedata.priority = data.priority;
	 	gamedata.start_date = moment.utc(data.start_date).format("YYYY-MM-DD HH:mm:ss");
	 	gamedata.aux_info = new Object();
	 	gamedata.aux_info.participants = data.participants;
	 	gamedata.aux_info.translations = new Object();
	 	angular.forEach(data.translations, function(item, index) {
  			gamedata.aux_info.translations[index] = item.text;
  		});
  		var currentSport = ChiliMainService.getCurrentSport();
  		gamedata.sport_id = currentSport.id;
  		gamedata.bookmaker_id = $scope.filtermodel.selectedBookmakerId;
	 	if (isNew) {
	 		gamedata.competition_id =  data.competition_id;
		 	gamedata.blocked = data.blocked;
		 	gamedata.status = data.status;
		 	gamedata.calculated = data.calculated;
	 	}

	 	return gamedata;
	 };

	// adding a new game item
	$scope.createNewGame = function() {
		$scope.resetSelectedElement();
		$scope.isNewGame  = true;
		$scope.gamemodel = new GameModel();
		$scope.gamemodel.start_date = angular.copy($scope.lastCreatedGameStartDate);
		if ($scope.filtermodel.selectedCompetitions.length == 1) {
			$scope.gamemodel.competition_id = $scope.filtermodel.selectedCompetitions[0];
			$scope.setCompetition();
		}
	};

	// update game data by id
	$scope.addOrUpdateGame = function() {
		var gamedata = $scope.decodeModel($scope.gamemodel, $scope.isNewGame);
		if($scope.isNewGame) {
			ChiliUtils.block();
			GameAPI.saveGame(gamedata).success(function(data, status, headers, config) {
				if(ChiliUtils.action(data)) {
					if (data != '' && 'error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.gamesList.push(data);
						$scope.lastCreatedGameStartDate = data.start_date * 1000;
						$scope.resetSelectedElement();
			  		}
		  		}
	  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::games addOrUpdateGame");
	        	ChiliUtils.unblock();
	    	});	
		}
		else {
			ChiliUtils.block();
			GameAPI.updateGame(gamedata).success(function(data, status, headers, config) {
				if(ChiliUtils.action(data)) {
					if (data != '' && 'error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.setSelectedElement(data, true);
			  		}
		  		}
	  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::games addOrUpdateGame");
	        	ChiliUtils.unblock();
	    	});	 
		}
	};

	// remove game ajax
	$scope.removeGame = function() {
		var removeItemId = $scope.gamemodel.id;
	    GameAPI.removeGame(removeItemId).success(function(data, status, headers, config) {
	    	if(ChiliUtils.action(data)) {
	    		if (data != '' && 'error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					angular.forEach($scope.gamesList, function(item, index) {
						if(item.id == removeItemId)
						{
							$scope.gamesList.splice(index,1)
							$scope.resetSelectedElement();
						}	
					});
					$scope.setHeaderStatuses({et:null});
		  		}
		  	}
	  		}).error(function(data, status, headers, config) {
	  			console.log("js::games removeGame");
	    	});	 
	};

	$scope.getParticipantObject = function(pid, cid)	{
		var ob = null;
		angular.forEach($scope.compList[cid], function(item, index) {
			if(item.id == pid)
			{
				ob = item;
			}
		});

		return ob; 
	};

	//
	$scope.setParticipant = function(order, cid)	{
		angular.forEach($scope.compList[cid], function(item, index) {
			if(item.id == $scope.gamemodel.participants[order])
			{
				$scope.gamemodel.participants[order] = item.id;
			}
		});

		var arr = new Object();
		for(var ind=0; ind < ChiliConfigs.languages.length; ind++)
		{
			var lang = 	ChiliConfigs.languages[ind];
			arr[lang] = new Array();
		}

		for(var ind=0; ind < $scope.gamemodel.participants.length; ind++)
		{
			var pid = $scope.gamemodel.participants[ind];
			var pObj = $scope.getParticipantObject(pid, cid);
			if(pObj)
			{
				for(var lang in pObj.aux_info.translations)
				{
					arr[lang].push(pObj.aux_info.translations[lang]);
				}
			}
		}

		for(var ind=0; ind < ChiliConfigs.languages.length; ind++)
		{
			var lang = 	ChiliConfigs.languages[ind];
			$scope.gamemodel.translations[lang].text = arr[lang].join(' - ');
		}
	};

	$scope.changeGameState = function(id, state) {
		var gamedata = new Object();
		gamedata.id = id;
		gamedata.blocked = state == 0 ? 1 : 0;
		gamedata.bookmaker_id = $scope.filtermodel.selectedBookmakerId;

		ChiliUtils.block();
		GameAPI.updateGame(gamedata).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.updateOnGameList(data);
		  		}
		  	}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games changeGameState");
        	ChiliUtils.unblock();
    	});
	};

	$scope.getParticipantLogo = function(plogo)	{
		return ChiliConfigs.MEDIA_URL+"assets/participants/"+ plogo;
	};

	$scope.$on("updateGameDate", function(evnt, args) {
		$scope.gamemodel.start_date = args;
	});

	$scope.competitionName = function(cid) {
		var name = '';
		angular.forEach($scope.filtermodel.competitionList, function(item, index){
			if (cid == item.id) {
				name = item.name;
			}
		});
		return name;
	};

	////////////////////////EVENTS///////////////////////////////
	$scope.showCustomEvents = function(gitem) {
		if($scope.currentGameId == gitem.id)
		{
			$scope.currentGameId = null;
		}
		else
		{
			$scope.currentGameId = gitem.id;
			$scope.auxEventModel = new AuxEventModel();
		}
	};

	$scope.baseEventsTemplates = function() {
		var list = [];
		angular.forEach($scope.baseEventGroups, function (gitem) {
			angular.forEach($scope.eventTemplates, function (titem) {
				if(titem.group_id == gitem.id && titem.group_id != 5) { // 5 - default group id
					titem.st_blocked = 1; // the value does not affect, just any value // Aro
					list.push(titem);
				}
			});
		});
		return list;
	};

	$scope.auxEventsTemplates = function() {
		var list = [];
		angular.forEach($scope.baseEventGroups, function (gitem) {
			angular.forEach($scope.eventTemplates, function (titem) {
				if(titem.group_id == gitem.id && titem.group_id > 4) { // 5 - default group id
					list.push(titem);
				}
			});
		});
		return list;
	};

	$scope.updateEventState = function (gitem, eitem) {
		var obj = new Object();
		obj.id = eitem.id;
		obj.blocked = eitem.blocked == 1 ? 0 : 1;
		ChiliUtils.block();
		EventsAPI.updateEvent([obj]).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.updateOnEventList(gitem, data);
					var param = {et: eitem.template_id};
					$scope.setHeaderStatuses(param);
		  		}
	  		}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games updateEventState");
        	ChiliUtils.unblock();
    	});		
	};

	$scope.updateOnEventList = function (game, ev) {
		if (ev.template_id < 12) {
			var obj = game.aux_info.events['base_events'];
			angular.forEach(obj, function(item, index) {
				if (item.id == ev.id) {
					obj[index] = ev;
				}
			});
		} else {
			var obj = game.aux_info.events['aux_events'];
			angular.forEach(obj, function(item, index) {
				if (item.id == ev.id) {
					obj[index] = ev;
				}
			});
		}
	};

	$scope.updateEventValue = function (gitem, eitem, $event) {
		var input = $event.target;
		if(eitem.coefficient != input.value) {
			var obj = new Object();
			obj.id = eitem.id;
			obj.game_id = gitem.id;
			obj.template_id = eitem.template_id;
			obj.bookmaker_id = eitem.bookmaker_id;
			obj.coefficient = input.value;
			obj.old_coefficient = eitem.coefficient;
			ChiliUtils.block();
			EventsAPI.updateEvent([obj]).success(function(data, status, headers, config) {
				if(ChiliUtils.action(data)) {
					if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.updateOnEventList(gitem, data);
			  		}
			  	}
		  		ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::games updateEventValue");
	        	ChiliUtils.unblock();
	    	});
  		}
	};

		$scope.updateBasisValue = function (gitem, eitem, $event) {
		var input = $event.target;
		if(eitem.basis != input.value) {
			var obj = new Object();
			obj.id = eitem.id;
			obj.game_id = gitem.id;
			obj.template_id = eitem.template_id;
			obj.bookmaker_id = eitem.bookmaker_id;
			obj.basis = input.value;
			ChiliUtils.block();
			EventsAPI.updateEvent([obj]).success(function(data, status, headers, config) {
				if(ChiliUtils.action(data)) {
					if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.updateOnEventList(gitem, data);
			  		}
			  	}
		  		ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::games updateBasisValue");
	        	ChiliUtils.unblock();
	    	});
  		}
	};

	$scope.addEvents = function(game) {
		var obj = new Object();
		obj.game_id = game.id;
		obj.bookmaker_id = $scope.filtermodel.selectedBookmakerId;

		ChiliUtils.block();
		EventsAPI.addEvents(obj).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.updateEventsOnGameList(game.id, data);
		  		}
		  	}
	  		ChiliUtils.unblock();
	  		var param = {et: null};
	  		$scope.setHeaderStatuses(param);
  		}).error(function(data, status, headers, config) {
        	console.log("js::games updateEventState");
        	ChiliUtils.unblock();
    	});		
	};

	$scope.updateEventsOnGameList = function(game_id, events) {
		angular.forEach($scope.gamesList, function (gitem) {
			if(gitem.id == game_id) {
				gitem.aux_info.events = events;
			}
		});
	};

	$scope.selectedAuxEvent;
	$scope.auxEventChanged = function() {
		var selectedId = $scope.auxEventModel.template_id;
		var selectedAuxEvent = null;
		angular.forEach($scope.auxEventsTemplates, function(item, index) {
			if(item.id == selectedId)
			{
				selectedAuxEvent = item;
			}
		});

		$scope.auxEventModel.name = selectedAuxEvent.name;
		$scope.auxEventModel.basis = '0.00';
		$scope.auxEventModel.coefficient = '0.00';
		$scope.auxEventModel.blocked = 1;
	};

	$scope.addAuxEvents =  function() {
		var obj = new Object();
		obj.game_id = $scope.currentGameId;
		obj.events = new Array();
		var evObj = new Object();
		evObj.template_id = $scope.auxEventModel.template_id;
		evObj.coefficient = $scope.auxEventModel.coefficient;
		evObj.basis = $scope.auxEventModel.basis;
		
		evObj.data  = $scope.auxEventModel.data;
		evObj.result = 0;
		evObj.result_status = -1;
		//toddo
		evObj.blocked = $scope.auxEventModel.blocked == 1 ? 1 : 0;
		obj.events.push(evObj);

  	    obj.bookmaker_id = $scope.filtermodel.selectedBookmakerId;

		ChiliUtils.block();
		EventsAPI.addEvents(obj).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.updateEventsOnGameList(obj.game_id, data);
	  				$scope.auxEventModel = new AuxEventModel();
		  		}
		  	}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games addAuxEvents");
        	ChiliUtils.unblock();
    	});
	};

	$scope.setHeaderStatuses = function(data) {
		angular.forEach($scope.baseEventsTemplates, function(et, ind) {
			if (data.et) {
				if(data.et == et.id) {
					et.st_blocked = $scope.isAllChecked(et);
				}
			} else {
				et.st_blocked = $scope.isAllChecked(et);
			}
		});			
	};

	$scope.isAllChecked = function(et) {
		var status = 0;
		angular.forEach($scope.gamesList, function(item, index) {
			if (item.aux_info.events.base_events.length != 0 && item.aux_info.events.base_events[et.id-1].blocked == 1) {
				status = 1;
			}
		});
		return status;
	};

	$scope.checkAll = function(et) {
		et.st_blocked = et.st_blocked == 1 ? 0 : 1;
		var obj = [];
		angular.forEach($scope.gamesList, function(item, index) {
			if (item.aux_info.events.base_events.length != 0) {
				item.aux_info.events.base_events[et.id -1].blocked = et.st_blocked;
				obj.push({id : item.aux_info.events.base_events[et.id -1].id, blocked : item.aux_info.events.base_events[et.id -1].blocked});
			}
		});
		ChiliUtils.block();
		EventsAPI.updateEvent(obj).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				}
	  		}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games checkAll");
        	ChiliUtils.unblock();
    	});

	};

	/////////////////////////////////////////////////////////////

	////////////////////////Results//////////////////////////////

	$scope.resultTemplates = null;

	$scope.getAuxTemplateName = function(template_id) {
		for(var i = 0; i < $scope.auxEventsTemplates.length; i++) {
			if($scope.auxEventsTemplates[i].id == template_id) {
				return $scope.auxEventsTemplates[i].name;
			}
		}
	};

	$scope.waitingResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/waiting.png";
	$scope.wonResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/won.png";
	$scope.looseResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/loose.png";
	$scope.blockedResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/blocked.png";
	$scope.returnedResultLogo = ChiliConfigs.MEDIA_URL+"/assets/results/returned.png";

	$scope.blockedEventIcon = ChiliConfigs.MEDIA_URL+"/assets/games/unchecked1.png";
	$scope.unblockedEventIcon = ChiliConfigs.MEDIA_URL+"/assets/games/checked1.png";

	$scope.showCustomEvents = function(gitem) {
		if($scope.currentGameId == gitem.id)
		{
			$scope.currentGameId = null;
			$scope.resultTemplates = null;
		}
		else
		{
			$scope.currentGameId = gitem.id;
			$scope.resultTemplates = new Array();
			angular.forEach($scope.baseEventsTemplates, function (titem) {
				angular.forEach(gitem.aux_info.events['base_events'], function (item) {
					if(titem.in_result == 1 && item.template_id == titem.id) {
						$scope.resultTemplates.push(titem);
					}
				});
			});
			angular.forEach($scope.auxEventsTemplates, function (titem) {
				angular.forEach(gitem.aux_info.events['aux_events'], function (item) {
					if(titem.in_result == 1 && item.template_id == titem.id) {
						$scope.resultTemplates.push(titem);
					}
				});
			});
			$scope.resultModel = new ResultModel();
			$scope.resultModel.game = gitem;
			$scope.resultModel.values = new Array();
			angular.forEach($scope.resultTemplates, function(item){
				$scope.resultModel.values[item.id] = null;
			});
		}
	};

	$scope.showResultStatusTooltip = function(result_status, blocked) {
		if(blocked == 1) {
			return "Blocked";
		} else if (result_status == -1) {
			return "Waiting";
		} else if (result_status == 0) {
			return "Returned";
		} else if (result_status == 1) {
			return "Loose";
		} else if (result_status == 2) {
			return "Won";
		}
	};

	$scope.saveOnlyResults = function() {
		var obj = new Object();
		obj.results = $scope.resultModel.values;
		obj.game_id = $scope.resultModel.game.id;
		obj.sport_id = $scope.currentSport.id;
		ChiliUtils.block();
		ResultsAPI.saveOnlyResults(obj).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
	  				$scope.updateOnGameList(data);
	  			}
	  		}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games saveResults");
        	ChiliUtils.unblock();
    	});
	};

	$scope.saveResults = function() {
		var obj = new Object();
		obj.results = $scope.resultModel.values;
		obj.game_id = $scope.resultModel.game.id;
		obj.sport_id = $scope.currentSport.id;
		obj.bookmaker_id = $scope.filtermodel.selectedBookmakerId;
		ChiliUtils.block();
		ResultsAPI.saveResults(obj).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
	  				$scope.updateOnGameList(data);
	  			}
	  		}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games saveResults");
        	ChiliUtils.unblock();
    	});
	};

	$scope.returnResults = function() {
		var obj = new Object();
		obj.game_id = $scope.resultModel.game.id;
		obj.bookmaker_id = $scope.filtermodel.selectedBookmakerId;
		ChiliUtils.block();
		ResultsAPI.returnResults(obj).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
	  				$scope.updateOnGameList(data);
	  			}
	  		}
	  		ChiliUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::games returnResults");
        	ChiliUtils.unblock();
    	});
	};

	$scope.showResultStatusLogo = function(result_status, blocked) {
		if(blocked == 1) {
			return $scope.blockedResultLogo;
		} else if (result_status == -1) {
			return $scope.waitingResultLogo;
		} else if (result_status == 0) {
			return $scope.returnedResultLogo;
		} else if (result_status == 1) {
			return $scope.looseResultLogo;
		} else if (result_status == 2) {
			return $scope.wonResultLogo;
		}
	};
	/////////////////////////////////////////////////////////////

	$scope.getArray = function(obj) {
        var ret = [];
        angular.forEach(obj, function(item, index) {
          ret.push(item);
        });
        return ret;
    };

    $scope.showNews = function (id) {
    	$window.open(ChiliConfigs.BASE_INDEX + 'admin/main/show/news/games/' + id);
    };

	$scope.getUnixTimeStamp = function(date_str) {
		return moment(date_str).unix() * 1000;
	};

	$scope.countData = {'unsetCount' : {'count' : null}, 'blockedCount' : {'count' : null}, 'finished' : {'count' : null}};
	$scope.getGamesCounts = function () {
		var cids = [];
		angular.forEach($scope.filtermodel.selectedCompetitions, function(item){
			cids.push(item);
		});
		if(cids.length == 0) {
			angular.forEach($scope.filtermodel.competitionList, function(item, index) {
				cids.push(item.id);
			});
		}

		if (cids.length == 0) {
			// ChiliUtils.noty('error', "There is no any competition");
			return;
		}

	 	GameAPI.getGamesCounts(cids, $scope.currentSport.id).success(function(data, status, headers, config) {
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
					$scope.countData = data;
		  		}
		  	}
  		}).error(function(data, status, headers, config) {
        	console.log("js::games changeGameState");
    	});
	};

	$scope.showGames = function (show) {
		$scope.filtermodel.fromdate = '';
		$scope.filtermodel.todate = '';
		if (show == 'unset') {
			$scope.filtermodel.status = 'Unset';
			$scope.filtermodel.state = 'All';
		} else if (show == 'blocked') {
			$scope.filtermodel.state = 'Bl';
			$scope.filtermodel.status = 'All';
		} else if (show == 'finished') {
			$scope.filtermodel.state = 'All';
			$scope.filtermodel.status = 'Unset';
			var date = new Date().getTime();
			$scope.filtermodel.todate = date;
		}
		$scope.$broadcast("filterChanged");
	};

	$scope.searchById = function() {
		$scope.$broadcast("filterChanged");
	};

	$scope.init = function() {
	 	$scope.filtermodel.competitionList = $window.competitionData;
	 	$scope.filtermodel.bookmakersList  = $window.bookmakerData;
	 	$scope.filtermodel.game_id		   = $window.showGame;
	 	$scope.filtermodel.selectedBookmakerId = $scope.filtermodel.bookmakersList[0].id;
	 	if ($scope.filtermodel.competitionList) {
		 	for(var i = 0; i < $window.competitionData.length; i++){
		 		$scope.compList[$window.competitionData[i].id] = new Array();
		 	}
	 	}
	 	angular.forEach($window.eventGroups, function (eg) {
	 		if(1 <= eg.id <= 4) {
	 			$scope.baseEventGroups.push(eg);
	 		} else {
	 			$scope.auxEventGroups.push(eg);
	 		}
	 	});
	 	$scope.eventTemplates = $window.eventTemplates;
	 	$scope.baseEventsTemplates = $scope.baseEventsTemplates();
	 	$scope.auxEventsTemplates = $scope.auxEventsTemplates();
	 	$scope.showGames('finished');
	 	$scope.getGamesCounts();
	};
}

