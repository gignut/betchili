chiliadminApp.factory("CompetitionsAPI" , function(ChiliConfigs, $http){
			return {
						getCompetitionsList : function (lang, skid, ligid) {

							var ob = {};
							ob.chili = {language: lang, league_id : ligid, sport_id : skid};

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/competitions/get_by_league",
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});	
						},

						getCompetitionDataById : function(compId, lang, aux) {
							var ob = {};
							ob.chili = { id: compId, language: lang, aux_info : aux };

							return $http({
				    				method: "POST",
				    				url: ChiliConfigs.BASE_INDEX + "admin/competitions/get_by_id" ,
				    				data :  $.param(ob),
				    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    							});		
						},	

						updateCompetitions : function(obj) {
							var ob = {};
							ob.chili = [obj];
							var dataArray = [];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/competitions/update" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						updateCompetitonParticipants : function(cid, participantsObj) {
							var ob = {};
							ob.chili = { id : cid, participants : participantsObj};
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/competitions/update_participants" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});	
						},

						createCompetitions : function(obj) {
							var ob = {};	
							ob.chili = [obj];
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/competitions/add",
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});								
						},

						removeCompetition : function(copmId) {
							var ob = {};
							ob.chili = {id : copmId};
							return $http({
			    				method: "POST",
			    				url: ChiliConfigs.BASE_INDEX + "admin/competitions/remove" ,
			    				data :  $.param(ob),
			    				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
							});		
						}
					};
	});

function CompetitionModel() { 
	this.id;
	this.name;
	this.league_id;
	this.priority;
	this.start_date;
	this.end_date;
	this.blocked;
	this.participants={};
	this.translations = new Translations();

	return this;
}


function CompetitionsCtrl($scope, $http, $window, $filter, ChiliMainService, ChiliConfigs, CompetitionsAPI , ParticipantsAPI , ChiliUtils) {
	$scope.leagueList;
	$scope.selectedLeague;
	$scope.searchtxt;

	//competitions
	$scope.competitionsList = [];
	$scope.oldCompetitionsList = [];
	$scope.showOldCompetitions = false;
	$scope.currentCompetitionsList = [];
	$scope.selectedCompetition;
	$scope.newCompetition = null;


	//PARTICIPANTS PART
	$scope.selectedCountry;
	$scope.countryList;
	$scope.participantsList;
	$scope.selectedParticipant;
	$scope.participantSearchTxt;

	// ## Date functions and variables -start
	$scope.startDateMax;
	$scope.endDateMin;

	$scope.changeStatus = function() {
		var ob = {};
		ob.id = $scope.competitionModel.id;
		ob.blocked = parseInt($scope.competitionModel.blocked) == 0 ? 1 : 0;
		CompetitionsAPI.updateCompetitions(ob).success(function(data, status, headers, config){
				if(ChiliUtils.action(data)) {
					if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.setSelectedElement(data,true);
			  		}
			  	}
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::CompetitionCtrl changeStatus");
	    	});	      	
	};

	$scope.startDateChange = function(model) {
		$scope.endDateMin = model.start_date;
	};

	$scope.endDateChange = function(model) {
		$scope.startDateMax = model.end_date;
	};
	// ## Date functions and variables - end

	$scope.modelEncode = function(data)	{
		var ob = {};
		ob.id = data.id;
		ob.name = data.name;
		ob.priority = data.priority;
		ob.start_date =data.start_date * 1000;
		ob.end_date = data.end_date * 1000;
		ob.blocked = data.blocked;
		ob.participants = {};
		ob.translations = new TranslationModel();

		if('aux_info' in data && 'translations' in data.aux_info) {
			var translations = data.aux_info.translations;
			angular.forEach(translations, function(item, index) {
				ob.translations[index].text = item;
			});
		}

		if('aux_info' in data && 'participants' in data.aux_info) {
			var participants = data.aux_info.participants;
			angular.forEach(participants, function(item, index) {//data.aux_info.participants;
				ob.participants[index] = {
					"id": item.id,
					"name": item.name,
					"score": item.score,
					"played_games": item.played_games,
					"wins": item.wins,
					"looses": item.looses,
					"draws": item.draws,
					"goals_in": item.goals_in,
					"goals_out": item.goals_out,
					"country_id": item.country_id,
					"isedited": false,
					"isnew": false,
					"disabled": false
				};
			});
		}

		return ob;
	};

	$scope.modelDecode = function(mob) {
		var ob = {};
		ob.id = mob.id;
		ob.name = mob.name;
		ob.priority = mob.priority;
		ob.blocked = mob.blocked;
		ob.start_date = moment.utc(mob.start_date).format("YYYY-MM-DD HH:mm:ss");
		ob.end_date = moment.utc(mob.end_date).format("YYYY-MM-DD HH:mm:ss");

		ob.aux_info = {"translations" : {}};
		angular.forEach(mob.translations, function(item, index) {
			ob.aux_info.translations[index] = item .text;
		});

		return ob;
	};

	$scope.addParticipant = function() {
		if($scope.determineAddBtnState())
		{
			$scope.competitionModel.participants[$scope.selectedParticipant.id] = {"id" : $scope.selectedParticipant.id , 
													 "name" : $scope.selectedParticipant.name,
													 "score" : 0,
													 "played_games" : 0,
													 "wins" : 0,
													 "looses" : 0,
													 "draws" : 0,
													 "goals_in" : 0,
													 "goals_out" : 0,
													 "country_id" : $scope.selectedCountry.id,
													 "isedited" : false,
													 "isnew" : true,
													 "disabled" : false
													};
		}
		delete $scope.participantsList[$scope.selectedParticipant.id];
	};

	$scope.saveParticipants = function() {
		//look at competition model to understand action values
		var participantsData = [];
		var itemAction;
		angular.forEach($scope.competitionModel.participants, function(item, index) {
			if(item.disabled)
			{
				if(item.isnew == false)
				{
					// case of initial item which should be deleted
					itemAction = 0;	
				} 
				else
				{
					//nothing should happen
					itemAction = 1000;	
				}
			}
			else
			{
				if(item.isnew == false)
				{
					itemAction = item.isedited ? 2 : 1000;	
				} 
				else
				{
					itemAction = 1;
				}
			}	

		 	participantsData.push({id: item.id,
		 						   score : item.score,
		 						   played_games : item.played_games,
		 						   wins : item.wins,
		 						   looses : item.looses,
		 						   draws : item.draws,
		 						   goals_in : item.goals_in,
		 						   goals_out : item.goals_out,
		 						   action : itemAction });
		});

		ChiliUtils.block();

		CompetitionsAPI.updateCompetitonParticipants($scope.competitionModel.id, participantsData).success(function(data, status, headers, config){
			if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
	  				$scope.setSelectedElement(data,true);
	  				ChiliUtils.noty("success");
	  			}
	  		}
  			ChiliUtils.unblock();
	  	}).error(function(data, status, headers, config) {
	    	console.log("js::CompetitionsCtrl saveParticipants");
	       	ChiliUtils.unblock();
	    });
	};

	// set flags for participants
	$scope.editParticipantScore = function(participant)	{
		if(	participant.isnew == false)
		{
			participant.isedited = true;
		}
	};

	// set flags for participants
	$scope.disableParticipant = function(participant) {
		participant.disabled = true;
	};

	$scope.enableParticipant = function(participant) {
		participant.disabled = false;
	};

	$scope.determineAddBtnState = function() {
		var enabled = false;
		var existsParticipant = false;
		if($scope.selectedParticipant != null && $scope.selectedCountry != null)
		{
			var list = $scope.competitionModel.participants;
			angular.forEach(list, function(item, index) {
				if(item.id == $scope.selectedParticipant.id && !existsParticipant)
				{
					existsParticipant = true;	
				}
			});

			enabled = !existsParticipant;
		}

		return enabled;
	};

	// refresh list of participants in combo
	$scope.countrySelect = function() {
		var lang  = ChiliMainService.getCurrentLanguage();
		var currentSport = ChiliMainService.getCurrentSport();	

		ParticipantsAPI.getParticipantsList(lang, currentSport.id, $scope.selectedCountry.id).success(function(data, status, headers, config){
			if(ChiliUtils.action(data)) {
				if (data != '' && 'error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
	  				$scope.participantsList = {};
		  			angular.forEach(data, function(item, index) {
		  				if (!(index in $scope.competitionModel.participants)) {
							$scope.participantsList[item.id] = {"id" : item.id , "name" : item.name, "country_id" : item.country_id};
						}
					});
		  			$scope.selectedParticipant = null;
	  			}
	  		}
	  		}).error(function(data, status, headers, config) {
	      		console.log("js::CompetitionsCtrl countrySelect error");
	    	});
	};

	//refresh COMPETITION list according to selected LEAGUE
	$scope.getCompetitionsList = function () {

		var lang  = ChiliMainService.getCurrentLanguage();
		var currentSport = ChiliMainService.getCurrentSport();

		if ($scope.selectedLeague != null)
		{
			ChiliUtils.block();
	  		CompetitionsAPI.getCompetitionsList(lang, currentSport.id, $scope.selectedLeague.id).success(function(data, status, headers, config){
	  			if(ChiliUtils.action(data)) {
		  			if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						$scope.competitionsList = data;
						$scope.updateCompLists();
		  			}
	  			}
	  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::competitions getCompetitionsList");
	    	});
	  	}
	};

	$scope.updateCompLists = function() {
		$scope.oldCompetitionsList = [];
		$scope.currentCompetitionsList = [];
		var cur_date = new Date().getTime();
		cur_date = moment(cur_date).unix() * 1000;

		angular.forEach($scope.competitionsList, function(item, key) {
			if (item.end_date * 1000 < cur_date) {
				$scope.oldCompetitionsList.push(item);
			} else {
				$scope.currentCompetitionsList.push(item);
			}
		});
	};

	// change LEAGUE from dropdown
	$scope.leagueSelect = function() {
		$scope.getCompetitionsList();
		$scope.resetSelectedElement();
		$scope.newCompetition = null;
	};

	// get data for selected COMPETITION and show it
	$scope.showItem = function(compId,e) {
		ChiliUtils.block();
		var lang  = ChiliMainService.getCurrentLanguage();
  		CompetitionsAPI.getCompetitionDataById(compId, lang , [ "translations", "participants" ]).success(function(data, status, headers, config) {
  			if(ChiliUtils.action(data)) {
	  			if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
		  			$scope.setSelectedElement(data, false);
		  		}
	  		}
	        ChiliUtils.unblock();
      	}).error(function(data, status, headers, config) {
        	console.log("js::competitions showItem");
    	});
	};
	
	// SET SELECTED COMPETITION
	$scope.setSelectedElement = function(data, updateList) {
		$scope.selectedCompetition = data;	
		$scope.newCompetition = null;
		$scope.competitionModel = $scope.modelEncode(data);	
		if(updateList)
		{
			var id = $scope.competitionModel.id;
			angular.forEach($scope.competitionsList, function(item, index) {
					if(item.id == id)
					{
						$scope.competitionsList[index] =  $scope.selectedCompetition ;
						return;	
					}	
				});
		}

		// set min max date limits	
		$scope.startDateMax = $scope.selectedCompetition.end_date;
		$scope.endDateMin = $scope.selectedCompetition.start_date;

		if($scope.selectedLeague.country_id != 0 && $scope.selectedLeague.country_id != null) {
			var countryId = $scope.selectedLeague.country_id;
			angular.forEach($scope.countryList, function(item, index) {
				if(item.id == countryId)
				{
					$scope.selectedCountry = item;
					return;
				}
			});

			$scope.countrySelect();
		} else if ($scope.selectedCountry) {
			$scope.countrySelect();
		}

	};

	// RESET SELECTED COMPETITION
	$scope.resetSelectedElement = function() {
		$scope.selectedCompetition = null;
		$scope.competitionModel = null;
		$scope.startDateMax = "";	
		$scope.endDateMin = "";
	};

	// UPDATE COMPETITION
	$scope.updateCompetition = function() {
		ChiliUtils.block();
		var ob = $scope.modelDecode($scope.competitionModel);
		CompetitionsAPI.updateCompetitions(ob).success(function(data, status, headers, config){
				if(ChiliUtils.action(data)) {
					if ('error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
			  			$scope.setSelectedElement(data,true);
			  			ChiliUtils.noty("success");
			  		}
			  	}
  				ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::CompetitionCtrl updateCompetition");
	    	});	      	
	};

	// CREATE NEW COMPETITION
	$scope.createNewCompetition = function() {
		$scope.resetSelectedElement();
		var cur_date = new Date().getTime();
		//cur_date = (moment(cur_date).unix() + ChiliUtils.getTimeZone())* 1000;
		$scope.newCompetition = {
								 name       : $scope.selectedLeague.name, 
								 priority   : $scope.selectedLeague.priority,
								 league_id  : $scope.selectedLeague.id,
								 logo       : $scope.selectedLeague.logo,
								 start_date : cur_date,
								 end_date   : cur_date,
								 aux_info   : $scope.selectedLeague.aux_info
								};
	};

	// SAVE NEW COMPETITION
	$scope.saveNewCompetition = function() {
		ChiliUtils.block();
		var post_data = {};
		var len = $scope.newCompetition.length;
		for(var key in $scope.newCompetition) {
			post_data[key] = $scope.newCompetition[key];
		};
		post_data.start_date = moment.utc(post_data.start_date).format("YYYY-MM-DD HH:mm:ss");
		post_data.end_date = moment.utc(post_data.end_date).format("YYYY-MM-DD HH:mm:ss");
    	CompetitionsAPI.createCompetitions(post_data).success(function(data, status, headers, config){
    		if(ChiliUtils.action(data)) {
				if ('error' in data) {
					ChiliUtils.noty('error', data['message']);
				} else {
		  			$scope.newCompetition = null;
		  			$scope.getCompetitionsList(); 
		  			$scope.updateCompLists();
		  			ChiliUtils.noty("success");
		  		}
	  		}
  			ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::CompetitionCtrl saveNewCompetition");
	    	});
	};

	// REMOVE SELECTED COMPETITION
	$scope.removeCompetition = function() {
		ChiliUtils.block();
		var removeItemId = $scope.selectedCompetition.id;
	    CompetitionsAPI.removeCompetition(removeItemId).success(function(data, status, headers, config){
		    	if(ChiliUtils.action(data)) {
		    		if (data != '' && 'error' in data) {
						ChiliUtils.noty('error', data['message']);
					} else {
						angular.forEach($scope.competitionsList, function(item, index) {
							if(item.id == removeItemId)
							{
								$scope.competitionsList.splice(index,1)
								$scope.resetSelectedElement();
								$scope.updateCompLists();
								return;	
							}	
						});	
						ChiliUtils.noty("success");
			  		}
			  	}
  				ChiliUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::CompetitionCtrl removeCompetition");
	    	});	 
	};


    $scope.getArray = function(obj) {
        var ret = [];
        angular.forEach(obj, function(item, index) {
          ret.push(item);
        });
        return ret;
    };

	$scope.$on("updateEndDate", function(evnt, args) {
		$scope.competitionModel.end_date = args;
	});

	$scope.$on("updateStartDate", function(evnt, args) {
		$scope.competitionModel.start_date = args;
	});

		$scope.$on("updateNewEndDate", function(evnt, args) {
		$scope.newCompetition.end_date = args;
	});

	$scope.$on("updateNewStartDate", function(evnt, args) {
		$scope.newCompetition.start_date = args;
	});

	$scope.init = function() {	
	 	$scope.leagueList = $window.leagueData;
	 	$scope.countryList = $window.countryData;
	};
}

