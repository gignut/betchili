#!/bin/sh

TAG="mysql"

CONTAINER_ID=$(sudo docker ps | grep $TAG | awk '{print $1}')

IP=$(sudo docker inspect $CONTAINER_ID | python -c 'import json,sys;obj=json.load(sys.stdin);print obj[0]["NetworkSettings"]["IPAddress"]')

echo $IP

#mysql -u betchili -p -h $IP
